<?php

// project_name
?>
<?php if ($projects->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $projects->TableCaption() ?></h4> -->
<table id="tbl_projectsmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $projects->TableCustomInnerHtml ?>
	<tbody>
<?php if ($projects->project_name->Visible) { // project_name ?>
		<tr id="r_project_name">
			<td><?php echo $projects->project_name->FldCaption() ?></td>
			<td<?php echo $projects->project_name->CellAttributes() ?>>
<span id="el_projects_project_name">
<span<?php echo $projects->project_name->ViewAttributes() ?>>
<?php echo $projects->project_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
