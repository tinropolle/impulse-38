<?php

// employee_level_id
// employee_first_name
// employee_last_name
// employee_telephone
// lab_name
// position_name

?>
<?php if ($v_employees->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $v_employees->TableCaption() ?></h4> -->
<table id="tbl_v_employeesmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $v_employees->TableCustomInnerHtml ?>
	<tbody>
<?php if ($v_employees->employee_level_id->Visible) { // employee_level_id ?>
		<tr id="r_employee_level_id">
			<td><?php echo $v_employees->employee_level_id->FldCaption() ?></td>
			<td<?php echo $v_employees->employee_level_id->CellAttributes() ?>>
<span id="el_v_employees_employee_level_id">
<span<?php echo $v_employees->employee_level_id->ViewAttributes() ?>>
<?php echo $v_employees->employee_level_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($v_employees->employee_first_name->Visible) { // employee_first_name ?>
		<tr id="r_employee_first_name">
			<td><?php echo $v_employees->employee_first_name->FldCaption() ?></td>
			<td<?php echo $v_employees->employee_first_name->CellAttributes() ?>>
<span id="el_v_employees_employee_first_name">
<span<?php echo $v_employees->employee_first_name->ViewAttributes() ?>>
<?php echo $v_employees->employee_first_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($v_employees->employee_last_name->Visible) { // employee_last_name ?>
		<tr id="r_employee_last_name">
			<td><?php echo $v_employees->employee_last_name->FldCaption() ?></td>
			<td<?php echo $v_employees->employee_last_name->CellAttributes() ?>>
<span id="el_v_employees_employee_last_name">
<span<?php echo $v_employees->employee_last_name->ViewAttributes() ?>>
<?php echo $v_employees->employee_last_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($v_employees->employee_telephone->Visible) { // employee_telephone ?>
		<tr id="r_employee_telephone">
			<td><?php echo $v_employees->employee_telephone->FldCaption() ?></td>
			<td<?php echo $v_employees->employee_telephone->CellAttributes() ?>>
<span id="el_v_employees_employee_telephone">
<span<?php echo $v_employees->employee_telephone->ViewAttributes() ?>>
<?php echo $v_employees->employee_telephone->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($v_employees->lab_name->Visible) { // lab_name ?>
		<tr id="r_lab_name">
			<td><?php echo $v_employees->lab_name->FldCaption() ?></td>
			<td<?php echo $v_employees->lab_name->CellAttributes() ?>>
<span id="el_v_employees_lab_name">
<span<?php echo $v_employees->lab_name->ViewAttributes() ?>>
<?php echo $v_employees->lab_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($v_employees->position_name->Visible) { // position_name ?>
		<tr id="r_position_name">
			<td><?php echo $v_employees->position_name->FldCaption() ?></td>
			<td<?php echo $v_employees->position_name->CellAttributes() ?>>
<span id="el_v_employees_position_name">
<span<?php echo $v_employees->position_name->ViewAttributes() ?>>
<?php echo $v_employees->position_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
