<?php

// employee_last_name
// employee_first_name
// position_name
// period_name

?>
<?php if ($reports->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $reports->TableCaption() ?></h4> -->
<table id="tbl_reportsmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $reports->TableCustomInnerHtml ?>
	<tbody>
<?php if ($reports->employee_last_name->Visible) { // employee_last_name ?>
		<tr id="r_employee_last_name">
			<td><?php echo $reports->employee_last_name->FldCaption() ?></td>
			<td<?php echo $reports->employee_last_name->CellAttributes() ?>>
<span id="el_reports_employee_last_name">
<span<?php echo $reports->employee_last_name->ViewAttributes() ?>>
<?php echo $reports->employee_last_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($reports->employee_first_name->Visible) { // employee_first_name ?>
		<tr id="r_employee_first_name">
			<td><?php echo $reports->employee_first_name->FldCaption() ?></td>
			<td<?php echo $reports->employee_first_name->CellAttributes() ?>>
<span id="el_reports_employee_first_name">
<span<?php echo $reports->employee_first_name->ViewAttributes() ?>>
<?php echo $reports->employee_first_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($reports->position_name->Visible) { // position_name ?>
		<tr id="r_position_name">
			<td><?php echo $reports->position_name->FldCaption() ?></td>
			<td<?php echo $reports->position_name->CellAttributes() ?>>
<span id="el_reports_position_name">
<span<?php echo $reports->position_name->ViewAttributes() ?>>
<?php echo $reports->position_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($reports->period_name->Visible) { // period_name ?>
		<tr id="r_period_name">
			<td><?php echo $reports->period_name->FldCaption() ?></td>
			<td<?php echo $reports->period_name->CellAttributes() ?>>
<span id="el_reports_period_name">
<span<?php echo $reports->period_name->ViewAttributes() ?>>
<?php echo $reports->period_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
