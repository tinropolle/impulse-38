<?php

// employee_login
// employee_level_id
// employee_first_name
// employee_last_name
// employee_telephone
// employee_lab_id
// employee_position_id
// employee_salary

?>
<?php if ($e_employees->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $e_employees->TableCaption() ?></h4> -->
<table id="tbl_e_employeesmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $e_employees->TableCustomInnerHtml ?>
	<tbody>
<?php if ($e_employees->employee_login->Visible) { // employee_login ?>
		<tr id="r_employee_login">
			<td><?php echo $e_employees->employee_login->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_login->CellAttributes() ?>>
<span id="el_e_employees_employee_login">
<span<?php echo $e_employees->employee_login->ViewAttributes() ?>>
<?php echo $e_employees->employee_login->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_level_id->Visible) { // employee_level_id ?>
		<tr id="r_employee_level_id">
			<td><?php echo $e_employees->employee_level_id->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_level_id->CellAttributes() ?>>
<span id="el_e_employees_employee_level_id">
<span<?php echo $e_employees->employee_level_id->ViewAttributes() ?>>
<?php echo $e_employees->employee_level_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_first_name->Visible) { // employee_first_name ?>
		<tr id="r_employee_first_name">
			<td><?php echo $e_employees->employee_first_name->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_first_name->CellAttributes() ?>>
<span id="el_e_employees_employee_first_name">
<span<?php echo $e_employees->employee_first_name->ViewAttributes() ?>>
<?php echo $e_employees->employee_first_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_last_name->Visible) { // employee_last_name ?>
		<tr id="r_employee_last_name">
			<td><?php echo $e_employees->employee_last_name->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_last_name->CellAttributes() ?>>
<span id="el_e_employees_employee_last_name">
<span<?php echo $e_employees->employee_last_name->ViewAttributes() ?>>
<?php echo $e_employees->employee_last_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_telephone->Visible) { // employee_telephone ?>
		<tr id="r_employee_telephone">
			<td><?php echo $e_employees->employee_telephone->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_telephone->CellAttributes() ?>>
<span id="el_e_employees_employee_telephone">
<span<?php echo $e_employees->employee_telephone->ViewAttributes() ?>>
<?php echo $e_employees->employee_telephone->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_lab_id->Visible) { // employee_lab_id ?>
		<tr id="r_employee_lab_id">
			<td><?php echo $e_employees->employee_lab_id->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_lab_id->CellAttributes() ?>>
<span id="el_e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<?php echo $e_employees->employee_lab_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_position_id->Visible) { // employee_position_id ?>
		<tr id="r_employee_position_id">
			<td><?php echo $e_employees->employee_position_id->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_position_id->CellAttributes() ?>>
<span id="el_e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<?php echo $e_employees->employee_position_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_employees->employee_salary->Visible) { // employee_salary ?>
		<tr id="r_employee_salary">
			<td><?php echo $e_employees->employee_salary->FldCaption() ?></td>
			<td<?php echo $e_employees->employee_salary->CellAttributes() ?>>
<span id="el_e_employees_employee_salary">
<span<?php echo $e_employees->employee_salary->ViewAttributes() ?>>
<?php echo $e_employees->employee_salary->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
