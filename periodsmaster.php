<?php

// period_name
// period_from
// period_to
// period_active

?>
<?php if ($periods->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $periods->TableCaption() ?></h4> -->
<table id="tbl_periodsmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $periods->TableCustomInnerHtml ?>
	<tbody>
<?php if ($periods->period_name->Visible) { // period_name ?>
		<tr id="r_period_name">
			<td><?php echo $periods->period_name->FldCaption() ?></td>
			<td<?php echo $periods->period_name->CellAttributes() ?>>
<span id="el_periods_period_name">
<span<?php echo $periods->period_name->ViewAttributes() ?>>
<?php echo $periods->period_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($periods->period_from->Visible) { // period_from ?>
		<tr id="r_period_from">
			<td><?php echo $periods->period_from->FldCaption() ?></td>
			<td<?php echo $periods->period_from->CellAttributes() ?>>
<span id="el_periods_period_from">
<span<?php echo $periods->period_from->ViewAttributes() ?>>
<?php echo $periods->period_from->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($periods->period_to->Visible) { // period_to ?>
		<tr id="r_period_to">
			<td><?php echo $periods->period_to->FldCaption() ?></td>
			<td<?php echo $periods->period_to->CellAttributes() ?>>
<span id="el_periods_period_to">
<span<?php echo $periods->period_to->ViewAttributes() ?>>
<?php echo $periods->period_to->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($periods->period_active->Visible) { // period_active ?>
		<tr id="r_period_active">
			<td><?php echo $periods->period_active->FldCaption() ?></td>
			<td<?php echo $periods->period_active->CellAttributes() ?>>
<span id="el_periods_period_active">
<span<?php echo $periods->period_active->ViewAttributes() ?>>
<?php echo $periods->period_active->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
