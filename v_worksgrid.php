<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($v_works_grid)) $v_works_grid = new cv_works_grid();

// Page init
$v_works_grid->Page_Init();

// Page main
$v_works_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$v_works_grid->Page_Render();
?>
<?php if ($v_works->Export == "") { ?>
<script type="text/javascript">

// Form object
var fv_worksgrid = new ew_Form("fv_worksgrid", "grid");
fv_worksgrid.FormKeyCountName = '<?php echo $v_works_grid->FormKeyCountName ?>';

// Validate form
fv_worksgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_work_period_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_works->work_period_id->FldCaption(), $v_works->work_period_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_task_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($v_works->work_task_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_work_description");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_works->work_description->FldCaption(), $v_works->work_description->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_progress");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_works->work_progress->FldCaption(), $v_works->work_progress->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_works->work_time->FldCaption(), $v_works->work_time->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($v_works->work_time->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_work_employee_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_works->work_employee_id->FldCaption(), $v_works->work_employee_id->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fv_worksgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "work_period_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_project_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_plan_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_lab_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_task_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_description", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_progress", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_time", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_employee_id", false)) return false;
	return true;
}

// Form_CustomValidate event
fv_worksgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fv_worksgrid.ValidateRequired = true;
<?php } else { ?>
fv_worksgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fv_worksgrid.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fv_worksgrid.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fv_worksgrid.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_work_project_id"],"ChildFields":["x_work_task_id"],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
fv_worksgrid.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fv_worksgrid.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":["x_work_plan_id","x_work_lab_id"],"ChildFields":[],"FilterFields":["x_task_plan_id","x_task_lab_id"],"Options":[],"Template":""};
fv_worksgrid.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fv_worksgrid.Lists["x_work_progress"].Options = <?php echo json_encode($v_works->work_progress->Options()) ?>;
fv_worksgrid.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<?php } ?>
<?php
if ($v_works->CurrentAction == "gridadd") {
	if ($v_works->CurrentMode == "copy") {
		$bSelectLimit = $v_works_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$v_works_grid->TotalRecs = $v_works->SelectRecordCount();
			$v_works_grid->Recordset = $v_works_grid->LoadRecordset($v_works_grid->StartRec-1, $v_works_grid->DisplayRecs);
		} else {
			if ($v_works_grid->Recordset = $v_works_grid->LoadRecordset())
				$v_works_grid->TotalRecs = $v_works_grid->Recordset->RecordCount();
		}
		$v_works_grid->StartRec = 1;
		$v_works_grid->DisplayRecs = $v_works_grid->TotalRecs;
	} else {
		$v_works->CurrentFilter = "0=1";
		$v_works_grid->StartRec = 1;
		$v_works_grid->DisplayRecs = $v_works->GridAddRowCount;
	}
	$v_works_grid->TotalRecs = $v_works_grid->DisplayRecs;
	$v_works_grid->StopRec = $v_works_grid->DisplayRecs;
} else {
	$bSelectLimit = $v_works_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($v_works_grid->TotalRecs <= 0)
			$v_works_grid->TotalRecs = $v_works->SelectRecordCount();
	} else {
		if (!$v_works_grid->Recordset && ($v_works_grid->Recordset = $v_works_grid->LoadRecordset()))
			$v_works_grid->TotalRecs = $v_works_grid->Recordset->RecordCount();
	}
	$v_works_grid->StartRec = 1;
	$v_works_grid->DisplayRecs = $v_works_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$v_works_grid->Recordset = $v_works_grid->LoadRecordset($v_works_grid->StartRec-1, $v_works_grid->DisplayRecs);

	// Set no record found message
	if ($v_works->CurrentAction == "" && $v_works_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$v_works_grid->setWarningMessage(ew_DeniedMsg());
		if ($v_works_grid->SearchWhere == "0=101")
			$v_works_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$v_works_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$v_works_grid->RenderOtherOptions();
?>
<?php $v_works_grid->ShowPageHeader(); ?>
<?php
$v_works_grid->ShowMessage();
?>
<?php if ($v_works_grid->TotalRecs > 0 || $v_works->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="fv_worksgrid" class="ewForm form-inline">
<?php if ($v_works_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($v_works_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_v_works" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_v_worksgrid" class="table ewTable">
<?php echo $v_works->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$v_works_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$v_works_grid->RenderListOptions();

// Render list options (header, left)
$v_works_grid->ListOptions->Render("header", "left");
?>
<?php if ($v_works->work_period_id->Visible) { // work_period_id ?>
	<?php if ($v_works->SortUrl($v_works->work_period_id) == "") { ?>
		<th data-name="work_period_id"><div id="elh_v_works_work_period_id" class="v_works_work_period_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $v_works->work_period_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_period_id"><div><div id="elh_v_works_work_period_id" class="v_works_work_period_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $v_works->work_period_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_period_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_period_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_project_id->Visible) { // work_project_id ?>
	<?php if ($v_works->SortUrl($v_works->work_project_id) == "") { ?>
		<th data-name="work_project_id"><div id="elh_v_works_work_project_id" class="v_works_work_project_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $v_works->work_project_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_project_id"><div><div id="elh_v_works_work_project_id" class="v_works_work_project_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $v_works->work_project_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_project_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_project_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_plan_id->Visible) { // work_plan_id ?>
	<?php if ($v_works->SortUrl($v_works->work_plan_id) == "") { ?>
		<th data-name="work_plan_id"><div id="elh_v_works_work_plan_id" class="v_works_work_plan_id"><div class="ewTableHeaderCaption"><?php echo $v_works->work_plan_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_plan_id"><div><div id="elh_v_works_work_plan_id" class="v_works_work_plan_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_works->work_plan_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_plan_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_plan_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_lab_id->Visible) { // work_lab_id ?>
	<?php if ($v_works->SortUrl($v_works->work_lab_id) == "") { ?>
		<th data-name="work_lab_id"><div id="elh_v_works_work_lab_id" class="v_works_work_lab_id"><div class="ewTableHeaderCaption"><?php echo $v_works->work_lab_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_lab_id"><div><div id="elh_v_works_work_lab_id" class="v_works_work_lab_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_works->work_lab_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_lab_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_lab_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_task_id->Visible) { // work_task_id ?>
	<?php if ($v_works->SortUrl($v_works->work_task_id) == "") { ?>
		<th data-name="work_task_id"><div id="elh_v_works_work_task_id" class="v_works_work_task_id"><div class="ewTableHeaderCaption"><?php echo $v_works->work_task_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_task_id"><div><div id="elh_v_works_work_task_id" class="v_works_work_task_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_works->work_task_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_task_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_task_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_description->Visible) { // work_description ?>
	<?php if ($v_works->SortUrl($v_works->work_description) == "") { ?>
		<th data-name="work_description"><div id="elh_v_works_work_description" class="v_works_work_description"><div class="ewTableHeaderCaption"><?php echo $v_works->work_description->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_description"><div><div id="elh_v_works_work_description" class="v_works_work_description">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_works->work_description->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_description->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_description->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_progress->Visible) { // work_progress ?>
	<?php if ($v_works->SortUrl($v_works->work_progress) == "") { ?>
		<th data-name="work_progress"><div id="elh_v_works_work_progress" class="v_works_work_progress"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $v_works->work_progress->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_progress"><div><div id="elh_v_works_work_progress" class="v_works_work_progress">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $v_works->work_progress->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_progress->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_progress->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_time->Visible) { // work_time ?>
	<?php if ($v_works->SortUrl($v_works->work_time) == "") { ?>
		<th data-name="work_time"><div id="elh_v_works_work_time" class="v_works_work_time"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $v_works->work_time->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_time"><div><div id="elh_v_works_work_time" class="v_works_work_time">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $v_works->work_time->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_time->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_time->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_works->work_employee_id->Visible) { // work_employee_id ?>
	<?php if ($v_works->SortUrl($v_works->work_employee_id) == "") { ?>
		<th data-name="work_employee_id"><div id="elh_v_works_work_employee_id" class="v_works_work_employee_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $v_works->work_employee_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_employee_id"><div><div id="elh_v_works_work_employee_id" class="v_works_work_employee_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $v_works->work_employee_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_works->work_employee_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_works->work_employee_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$v_works_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$v_works_grid->StartRec = 1;
$v_works_grid->StopRec = $v_works_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($v_works_grid->FormKeyCountName) && ($v_works->CurrentAction == "gridadd" || $v_works->CurrentAction == "gridedit" || $v_works->CurrentAction == "F")) {
		$v_works_grid->KeyCount = $objForm->GetValue($v_works_grid->FormKeyCountName);
		$v_works_grid->StopRec = $v_works_grid->StartRec + $v_works_grid->KeyCount - 1;
	}
}
$v_works_grid->RecCnt = $v_works_grid->StartRec - 1;
if ($v_works_grid->Recordset && !$v_works_grid->Recordset->EOF) {
	$v_works_grid->Recordset->MoveFirst();
	$bSelectLimit = $v_works_grid->UseSelectLimit;
	if (!$bSelectLimit && $v_works_grid->StartRec > 1)
		$v_works_grid->Recordset->Move($v_works_grid->StartRec - 1);
} elseif (!$v_works->AllowAddDeleteRow && $v_works_grid->StopRec == 0) {
	$v_works_grid->StopRec = $v_works->GridAddRowCount;
}

// Initialize aggregate
$v_works->RowType = EW_ROWTYPE_AGGREGATEINIT;
$v_works->ResetAttrs();
$v_works_grid->RenderRow();
if ($v_works->CurrentAction == "gridadd")
	$v_works_grid->RowIndex = 0;
if ($v_works->CurrentAction == "gridedit")
	$v_works_grid->RowIndex = 0;
while ($v_works_grid->RecCnt < $v_works_grid->StopRec) {
	$v_works_grid->RecCnt++;
	if (intval($v_works_grid->RecCnt) >= intval($v_works_grid->StartRec)) {
		$v_works_grid->RowCnt++;
		if ($v_works->CurrentAction == "gridadd" || $v_works->CurrentAction == "gridedit" || $v_works->CurrentAction == "F") {
			$v_works_grid->RowIndex++;
			$objForm->Index = $v_works_grid->RowIndex;
			if ($objForm->HasValue($v_works_grid->FormActionName))
				$v_works_grid->RowAction = strval($objForm->GetValue($v_works_grid->FormActionName));
			elseif ($v_works->CurrentAction == "gridadd")
				$v_works_grid->RowAction = "insert";
			else
				$v_works_grid->RowAction = "";
		}

		// Set up key count
		$v_works_grid->KeyCount = $v_works_grid->RowIndex;

		// Init row class and style
		$v_works->ResetAttrs();
		$v_works->CssClass = "";
		if ($v_works->CurrentAction == "gridadd") {
			if ($v_works->CurrentMode == "copy") {
				$v_works_grid->LoadRowValues($v_works_grid->Recordset); // Load row values
				$v_works_grid->SetRecordKey($v_works_grid->RowOldKey, $v_works_grid->Recordset); // Set old record key
			} else {
				$v_works_grid->LoadDefaultValues(); // Load default values
				$v_works_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$v_works_grid->LoadRowValues($v_works_grid->Recordset); // Load row values
		}
		$v_works->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($v_works->CurrentAction == "gridadd") // Grid add
			$v_works->RowType = EW_ROWTYPE_ADD; // Render add
		if ($v_works->CurrentAction == "gridadd" && $v_works->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$v_works_grid->RestoreCurrentRowFormValues($v_works_grid->RowIndex); // Restore form values
		if ($v_works->CurrentAction == "gridedit") { // Grid edit
			if ($v_works->EventCancelled) {
				$v_works_grid->RestoreCurrentRowFormValues($v_works_grid->RowIndex); // Restore form values
			}
			if ($v_works_grid->RowAction == "insert")
				$v_works->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$v_works->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($v_works->CurrentAction == "gridedit" && ($v_works->RowType == EW_ROWTYPE_EDIT || $v_works->RowType == EW_ROWTYPE_ADD) && $v_works->EventCancelled) // Update failed
			$v_works_grid->RestoreCurrentRowFormValues($v_works_grid->RowIndex); // Restore form values
		if ($v_works->RowType == EW_ROWTYPE_EDIT) // Edit row
			$v_works_grid->EditRowCnt++;
		if ($v_works->CurrentAction == "F") // Confirm row
			$v_works_grid->RestoreCurrentRowFormValues($v_works_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$v_works->RowAttrs = array_merge($v_works->RowAttrs, array('data-rowindex'=>$v_works_grid->RowCnt, 'id'=>'r' . $v_works_grid->RowCnt . '_v_works', 'data-rowtype'=>$v_works->RowType));

		// Render row
		$v_works_grid->RenderRow();

		// Render list options
		$v_works_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($v_works_grid->RowAction <> "delete" && $v_works_grid->RowAction <> "insertdelete" && !($v_works_grid->RowAction == "insert" && $v_works->CurrentAction == "F" && $v_works_grid->EmptyRow())) {
?>
	<tr<?php echo $v_works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$v_works_grid->ListOptions->Render("body", "left", $v_works_grid->RowCnt);
?>
	<?php if ($v_works->work_period_id->Visible) { // work_period_id ?>
		<td data-name="work_period_id"<?php echo $v_works->work_period_id->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_period_id" class="form-group v_works_work_period_id">
<select data-table="v_works" data-field="x_work_period_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_period_id->DisplayValueSeparator) ? json_encode($v_works->work_period_id->DisplayValueSeparator) : $v_works->work_period_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_period_id"<?php echo $v_works->work_period_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_period_id->EditValue)) {
	$arwrk = $v_works->work_period_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_period_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_period_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_period_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_period_id->CurrentValue) ?>" selected><?php echo $v_works->work_period_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_period_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
$sWhereWrk = "";
$v_works->work_period_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_period_id->LookupFilters += array("f0" => "`period_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_period_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `period_from` DESC";
if ($sSqlWrk <> "") $v_works->work_period_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo $v_works->work_period_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="v_works" data-field="x_work_period_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($v_works->work_period_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_period_id" class="form-group v_works_work_period_id">
<select data-table="v_works" data-field="x_work_period_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_period_id->DisplayValueSeparator) ? json_encode($v_works->work_period_id->DisplayValueSeparator) : $v_works->work_period_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_period_id"<?php echo $v_works->work_period_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_period_id->EditValue)) {
	$arwrk = $v_works->work_period_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_period_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_period_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_period_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_period_id->CurrentValue) ?>" selected><?php echo $v_works->work_period_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_period_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
$sWhereWrk = "";
$v_works->work_period_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_period_id->LookupFilters += array("f0" => "`period_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_period_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `period_from` DESC";
if ($sSqlWrk <> "") $v_works->work_period_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo $v_works->work_period_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_period_id" class="v_works_work_period_id">
<span<?php echo $v_works->work_period_id->ViewAttributes() ?>>
<?php echo $v_works->work_period_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_period_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($v_works->work_period_id->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_period_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($v_works->work_period_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $v_works_grid->PageObjName . "_row_" . $v_works_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="v_works" data-field="x_work_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_id" value="<?php echo ew_HtmlEncode($v_works->work_id->CurrentValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_id" value="<?php echo ew_HtmlEncode($v_works->work_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT || $v_works->CurrentMode == "edit") { ?>
<input type="hidden" data-table="v_works" data-field="x_work_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_id" value="<?php echo ew_HtmlEncode($v_works->work_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($v_works->work_project_id->Visible) { // work_project_id ?>
		<td data-name="work_project_id"<?php echo $v_works->work_project_id->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_project_id" class="form-group v_works_work_project_id">
<?php $v_works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_project_id->DisplayValueSeparator) ? json_encode($v_works->work_project_id->DisplayValueSeparator) : $v_works->work_project_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_project_id"<?php echo $v_works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_project_id->EditValue)) {
	$arwrk = $v_works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_project_id->CurrentValue) ?>" selected><?php echo $v_works->work_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$v_works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo $v_works->work_project_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="v_works" data-field="x_work_project_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($v_works->work_project_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_project_id" class="form-group v_works_work_project_id">
<?php $v_works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_project_id->DisplayValueSeparator) ? json_encode($v_works->work_project_id->DisplayValueSeparator) : $v_works->work_project_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_project_id"<?php echo $v_works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_project_id->EditValue)) {
	$arwrk = $v_works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_project_id->CurrentValue) ?>" selected><?php echo $v_works->work_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$v_works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo $v_works->work_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_project_id" class="v_works_work_project_id">
<span<?php echo $v_works->work_project_id->ViewAttributes() ?>>
<?php echo $v_works->work_project_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_project_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($v_works->work_project_id->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_project_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($v_works->work_project_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_plan_id->Visible) { // work_plan_id ?>
		<td data-name="work_plan_id"<?php echo $v_works->work_plan_id->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_plan_id" class="form-group v_works_work_plan_id">
<?php $v_works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_plan_id->DisplayValueSeparator) ? json_encode($v_works->work_plan_id->DisplayValueSeparator) : $v_works->work_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id"<?php echo $v_works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_plan_id->EditValue)) {
	$arwrk = $v_works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_plan_id->CurrentValue) ?>" selected><?php echo $v_works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$v_works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$v_works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo $v_works->work_plan_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="v_works" data-field="x_work_plan_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($v_works->work_plan_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_plan_id" class="form-group v_works_work_plan_id">
<?php $v_works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_plan_id->DisplayValueSeparator) ? json_encode($v_works->work_plan_id->DisplayValueSeparator) : $v_works->work_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id"<?php echo $v_works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_plan_id->EditValue)) {
	$arwrk = $v_works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_plan_id->CurrentValue) ?>" selected><?php echo $v_works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$v_works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$v_works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo $v_works->work_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_plan_id" class="v_works_work_plan_id">
<span<?php echo $v_works->work_plan_id->ViewAttributes() ?>>
<?php echo $v_works->work_plan_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_plan_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($v_works->work_plan_id->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_plan_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($v_works->work_plan_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_lab_id->Visible) { // work_lab_id ?>
		<td data-name="work_lab_id"<?php echo $v_works->work_lab_id->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($v_works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<span<?php echo $v_works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<?php $v_works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_lab_id->DisplayValueSeparator) ? json_encode($v_works->work_lab_id->DisplayValueSeparator) : $v_works->work_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id"<?php echo $v_works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_lab_id->EditValue)) {
	$arwrk = $v_works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>" selected><?php echo $v_works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$v_works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo $v_works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_lab_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($v_works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<span<?php echo $v_works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<?php $v_works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_lab_id->DisplayValueSeparator) ? json_encode($v_works->work_lab_id->DisplayValueSeparator) : $v_works->work_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id"<?php echo $v_works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_lab_id->EditValue)) {
	$arwrk = $v_works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>" selected><?php echo $v_works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$v_works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo $v_works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_lab_id" class="v_works_work_lab_id">
<span<?php echo $v_works->work_lab_id->ViewAttributes() ?>>
<?php echo $v_works->work_lab_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_lab_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_task_id->Visible) { // work_task_id ?>
		<td data-name="work_task_id"<?php echo $v_works->work_task_id->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($v_works->work_task_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_task_id" class="form-group v_works_work_task_id">
<span<?php echo $v_works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_task_id" class="form-group v_works_work_task_id">
<?php
$wrkonchange = trim(" " . @$v_works->work_task_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$v_works->work_task_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" style="white-space: nowrap; z-index: <?php echo (9000 - $v_works_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="sv_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo $v_works->work_task_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>"<?php echo $v_works->work_task_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_task_id->DisplayValueSeparator) ? json_encode($v_works->work_task_id->DisplayValueSeparator) : $v_works->work_task_id->DisplayValueSeparator) ?>" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld` FROM `tasks`";
$sWhereWrk = "(`task_code` LIKE '{query_value}%' OR CONCAT(`task_code`,'" . ew_ValueSeparator(1, $Page->work_task_id) . "',`task_name`) LIKE '{query_value}%') AND ({filter})";
$v_works->Lookup_Selecting($v_works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="q_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&f1=<?php echo ew_Encrypt("`task_plan_id` IN ({filter_value})"); ?>&t1=3&f2=<?php echo ew_Encrypt("`task_lab_id` IN ({filter_value})"); ?>&t2=3&d=">
<script type="text/javascript">
fv_worksgrid.CreateAutoSuggest({"id":"x<?php echo $v_works_grid->RowIndex ?>_work_task_id","forceSelect":false});
</script>
</span>
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($v_works->work_task_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_task_id" class="form-group v_works_work_task_id">
<span<?php echo $v_works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_task_id" class="form-group v_works_work_task_id">
<?php
$wrkonchange = trim(" " . @$v_works->work_task_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$v_works->work_task_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" style="white-space: nowrap; z-index: <?php echo (9000 - $v_works_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="sv_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo $v_works->work_task_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>"<?php echo $v_works->work_task_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_task_id->DisplayValueSeparator) ? json_encode($v_works->work_task_id->DisplayValueSeparator) : $v_works->work_task_id->DisplayValueSeparator) ?>" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld` FROM `tasks`";
$sWhereWrk = "(`task_code` LIKE '{query_value}%' OR CONCAT(`task_code`,'" . ew_ValueSeparator(1, $Page->work_task_id) . "',`task_name`) LIKE '{query_value}%') AND ({filter})";
$v_works->Lookup_Selecting($v_works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="q_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&f1=<?php echo ew_Encrypt("`task_plan_id` IN ({filter_value})"); ?>&t1=3&f2=<?php echo ew_Encrypt("`task_lab_id` IN ({filter_value})"); ?>&t2=3&d=">
<script type="text/javascript">
fv_worksgrid.CreateAutoSuggest({"id":"x<?php echo $v_works_grid->RowIndex ?>_work_task_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_task_id" class="v_works_work_task_id">
<span<?php echo $v_works->work_task_id->ViewAttributes() ?>>
<?php echo $v_works->work_task_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_task_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_description->Visible) { // work_description ?>
		<td data-name="work_description"<?php echo $v_works->work_description->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_description" class="form-group v_works_work_description">
<input type="text" data-table="v_works" data-field="x_work_description" name="x<?php echo $v_works_grid->RowIndex ?>_work_description" id="x<?php echo $v_works_grid->RowIndex ?>_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_works->work_description->getPlaceHolder()) ?>" value="<?php echo $v_works->work_description->EditValue ?>"<?php echo $v_works->work_description->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_description" name="o<?php echo $v_works_grid->RowIndex ?>_work_description" id="o<?php echo $v_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($v_works->work_description->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_description" class="form-group v_works_work_description">
<input type="text" data-table="v_works" data-field="x_work_description" name="x<?php echo $v_works_grid->RowIndex ?>_work_description" id="x<?php echo $v_works_grid->RowIndex ?>_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_works->work_description->getPlaceHolder()) ?>" value="<?php echo $v_works->work_description->EditValue ?>"<?php echo $v_works->work_description->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_description" class="v_works_work_description">
<span<?php echo $v_works->work_description->ViewAttributes() ?>>
<?php echo $v_works->work_description->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_description" name="x<?php echo $v_works_grid->RowIndex ?>_work_description" id="x<?php echo $v_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($v_works->work_description->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_description" name="o<?php echo $v_works_grid->RowIndex ?>_work_description" id="o<?php echo $v_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($v_works->work_description->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress"<?php echo $v_works->work_progress->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_progress" class="form-group v_works_work_progress">
<select data-table="v_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_progress->DisplayValueSeparator) ? json_encode($v_works->work_progress->DisplayValueSeparator) : $v_works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_progress" name="x<?php echo $v_works_grid->RowIndex ?>_work_progress"<?php echo $v_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($v_works->work_progress->EditValue)) {
	$arwrk = $v_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_progress->CurrentValue) ?>" selected><?php echo $v_works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_progress->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_progress" name="o<?php echo $v_works_grid->RowIndex ?>_work_progress" id="o<?php echo $v_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($v_works->work_progress->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_progress" class="form-group v_works_work_progress">
<select data-table="v_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_progress->DisplayValueSeparator) ? json_encode($v_works->work_progress->DisplayValueSeparator) : $v_works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_progress" name="x<?php echo $v_works_grid->RowIndex ?>_work_progress"<?php echo $v_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($v_works->work_progress->EditValue)) {
	$arwrk = $v_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_progress->CurrentValue) ?>" selected><?php echo $v_works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_progress->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_progress" class="v_works_work_progress">
<span<?php echo $v_works->work_progress->ViewAttributes() ?>>
<?php echo $v_works->work_progress->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_progress" name="x<?php echo $v_works_grid->RowIndex ?>_work_progress" id="x<?php echo $v_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($v_works->work_progress->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_progress" name="o<?php echo $v_works_grid->RowIndex ?>_work_progress" id="o<?php echo $v_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($v_works->work_progress->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_time->Visible) { // work_time ?>
		<td data-name="work_time"<?php echo $v_works->work_time->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_time" class="form-group v_works_work_time">
<input type="text" data-table="v_works" data-field="x_work_time" name="x<?php echo $v_works_grid->RowIndex ?>_work_time" id="x<?php echo $v_works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_time->getPlaceHolder()) ?>" value="<?php echo $v_works->work_time->EditValue ?>"<?php echo $v_works->work_time->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_time" name="o<?php echo $v_works_grid->RowIndex ?>_work_time" id="o<?php echo $v_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($v_works->work_time->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_time" class="form-group v_works_work_time">
<input type="text" data-table="v_works" data-field="x_work_time" name="x<?php echo $v_works_grid->RowIndex ?>_work_time" id="x<?php echo $v_works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_time->getPlaceHolder()) ?>" value="<?php echo $v_works->work_time->EditValue ?>"<?php echo $v_works->work_time->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_time" class="v_works_work_time">
<span<?php echo $v_works->work_time->ViewAttributes() ?>>
<?php echo $v_works->work_time->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_time" name="x<?php echo $v_works_grid->RowIndex ?>_work_time" id="x<?php echo $v_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($v_works->work_time->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_time" name="o<?php echo $v_works_grid->RowIndex ?>_work_time" id="o<?php echo $v_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($v_works->work_time->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_works->work_employee_id->Visible) { // work_employee_id ?>
		<td data-name="work_employee_id"<?php echo $v_works->work_employee_id->CellAttributes() ?>>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_employee_id" class="form-group v_works_work_employee_id">
<select data-table="v_works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_employee_id->DisplayValueSeparator) ? json_encode($v_works->work_employee_id->DisplayValueSeparator) : $v_works->work_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id"<?php echo $v_works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_employee_id->EditValue)) {
	$arwrk = $v_works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_employee_id->CurrentValue) ?>" selected><?php echo $v_works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$v_works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $v_works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo $v_works->work_employee_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="v_works" data-field="x_work_employee_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($v_works->work_employee_id->OldValue) ?>">
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_employee_id" class="form-group v_works_work_employee_id">
<select data-table="v_works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_employee_id->DisplayValueSeparator) ? json_encode($v_works->work_employee_id->DisplayValueSeparator) : $v_works->work_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id"<?php echo $v_works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_employee_id->EditValue)) {
	$arwrk = $v_works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_employee_id->CurrentValue) ?>" selected><?php echo $v_works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$v_works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $v_works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo $v_works->work_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($v_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_works_grid->RowCnt ?>_v_works_work_employee_id" class="v_works_work_employee_id">
<span<?php echo $v_works->work_employee_id->ViewAttributes() ?>>
<?php echo $v_works->work_employee_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_employee_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($v_works->work_employee_id->FormValue) ?>">
<input type="hidden" data-table="v_works" data-field="x_work_employee_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($v_works->work_employee_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$v_works_grid->ListOptions->Render("body", "right", $v_works_grid->RowCnt);
?>
	</tr>
<?php if ($v_works->RowType == EW_ROWTYPE_ADD || $v_works->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fv_worksgrid.UpdateOpts(<?php echo $v_works_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($v_works->CurrentAction <> "gridadd" || $v_works->CurrentMode == "copy")
		if (!$v_works_grid->Recordset->EOF) $v_works_grid->Recordset->MoveNext();
}
?>
<?php
	if ($v_works->CurrentMode == "add" || $v_works->CurrentMode == "copy" || $v_works->CurrentMode == "edit") {
		$v_works_grid->RowIndex = '$rowindex$';
		$v_works_grid->LoadDefaultValues();

		// Set row properties
		$v_works->ResetAttrs();
		$v_works->RowAttrs = array_merge($v_works->RowAttrs, array('data-rowindex'=>$v_works_grid->RowIndex, 'id'=>'r0_v_works', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($v_works->RowAttrs["class"], "ewTemplate");
		$v_works->RowType = EW_ROWTYPE_ADD;

		// Render row
		$v_works_grid->RenderRow();

		// Render list options
		$v_works_grid->RenderListOptions();
		$v_works_grid->StartRowCnt = 0;
?>
	<tr<?php echo $v_works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$v_works_grid->ListOptions->Render("body", "left", $v_works_grid->RowIndex);
?>
	<?php if ($v_works->work_period_id->Visible) { // work_period_id ?>
		<td data-name="work_period_id">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_period_id" class="form-group v_works_work_period_id">
<select data-table="v_works" data-field="x_work_period_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_period_id->DisplayValueSeparator) ? json_encode($v_works->work_period_id->DisplayValueSeparator) : $v_works->work_period_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_period_id"<?php echo $v_works->work_period_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_period_id->EditValue)) {
	$arwrk = $v_works->work_period_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_period_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_period_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_period_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_period_id->CurrentValue) ?>" selected><?php echo $v_works->work_period_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_period_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
$sWhereWrk = "";
$v_works->work_period_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_period_id->LookupFilters += array("f0" => "`period_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_period_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `period_from` DESC";
if ($sSqlWrk <> "") $v_works->work_period_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo $v_works->work_period_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_period_id" class="form-group v_works_work_period_id">
<span<?php echo $v_works->work_period_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_period_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_period_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($v_works->work_period_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_period_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_period_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($v_works->work_period_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_project_id->Visible) { // work_project_id ?>
		<td data-name="work_project_id">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_project_id" class="form-group v_works_work_project_id">
<?php $v_works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_project_id->DisplayValueSeparator) ? json_encode($v_works->work_project_id->DisplayValueSeparator) : $v_works->work_project_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_project_id"<?php echo $v_works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_project_id->EditValue)) {
	$arwrk = $v_works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_project_id->CurrentValue) ?>" selected><?php echo $v_works->work_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$v_works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo $v_works->work_project_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_project_id" class="form-group v_works_work_project_id">
<span<?php echo $v_works->work_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_project_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($v_works->work_project_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_project_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_project_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($v_works->work_project_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_plan_id->Visible) { // work_plan_id ?>
		<td data-name="work_plan_id">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_plan_id" class="form-group v_works_work_plan_id">
<?php $v_works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_plan_id->DisplayValueSeparator) ? json_encode($v_works->work_plan_id->DisplayValueSeparator) : $v_works->work_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id"<?php echo $v_works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_plan_id->EditValue)) {
	$arwrk = $v_works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_plan_id->CurrentValue) ?>" selected><?php echo $v_works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$v_works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$v_works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo $v_works->work_plan_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_plan_id" class="form-group v_works_work_plan_id">
<span<?php echo $v_works->work_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_plan_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($v_works->work_plan_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_plan_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_plan_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($v_works->work_plan_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_lab_id->Visible) { // work_lab_id ?>
		<td data-name="work_lab_id">
<?php if ($v_works->CurrentAction <> "F") { ?>
<?php if ($v_works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<span<?php echo $v_works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<?php $v_works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_lab_id->DisplayValueSeparator) ? json_encode($v_works->work_lab_id->DisplayValueSeparator) : $v_works->work_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id"<?php echo $v_works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_lab_id->EditValue)) {
	$arwrk = $v_works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>" selected><?php echo $v_works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$v_works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo $v_works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_lab_id" class="form-group v_works_work_lab_id">
<span<?php echo $v_works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_lab_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_lab_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_lab_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($v_works->work_lab_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_task_id->Visible) { // work_task_id ?>
		<td data-name="work_task_id">
<?php if ($v_works->CurrentAction <> "F") { ?>
<?php if ($v_works->work_task_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_v_works_work_task_id" class="form-group v_works_work_task_id">
<span<?php echo $v_works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_v_works_work_task_id" class="form-group v_works_work_task_id">
<?php
$wrkonchange = trim(" " . @$v_works->work_task_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$v_works->work_task_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" style="white-space: nowrap; z-index: <?php echo (9000 - $v_works_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="sv_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo $v_works->work_task_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>"<?php echo $v_works->work_task_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_task_id->DisplayValueSeparator) ? json_encode($v_works->work_task_id->DisplayValueSeparator) : $v_works->work_task_id->DisplayValueSeparator) ?>" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld` FROM `tasks`";
$sWhereWrk = "(`task_code` LIKE '{query_value}%' OR CONCAT(`task_code`,'" . ew_ValueSeparator(1, $Page->work_task_id) . "',`task_name`) LIKE '{query_value}%') AND ({filter})";
$v_works->Lookup_Selecting($v_works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="q_x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&f1=<?php echo ew_Encrypt("`task_plan_id` IN ({filter_value})"); ?>&t1=3&f2=<?php echo ew_Encrypt("`task_lab_id` IN ({filter_value})"); ?>&t2=3&d=">
<script type="text/javascript">
fv_worksgrid.CreateAutoSuggest({"id":"x<?php echo $v_works_grid->RowIndex ?>_work_task_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_task_id" class="form-group v_works_work_task_id">
<span<?php echo $v_works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_task_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_description->Visible) { // work_description ?>
		<td data-name="work_description">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_description" class="form-group v_works_work_description">
<input type="text" data-table="v_works" data-field="x_work_description" name="x<?php echo $v_works_grid->RowIndex ?>_work_description" id="x<?php echo $v_works_grid->RowIndex ?>_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_works->work_description->getPlaceHolder()) ?>" value="<?php echo $v_works->work_description->EditValue ?>"<?php echo $v_works->work_description->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_description" class="form-group v_works_work_description">
<span<?php echo $v_works->work_description->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_description->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_description" name="x<?php echo $v_works_grid->RowIndex ?>_work_description" id="x<?php echo $v_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($v_works->work_description->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_description" name="o<?php echo $v_works_grid->RowIndex ?>_work_description" id="o<?php echo $v_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($v_works->work_description->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_progress" class="form-group v_works_work_progress">
<select data-table="v_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_progress->DisplayValueSeparator) ? json_encode($v_works->work_progress->DisplayValueSeparator) : $v_works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_progress" name="x<?php echo $v_works_grid->RowIndex ?>_work_progress"<?php echo $v_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($v_works->work_progress->EditValue)) {
	$arwrk = $v_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_progress->CurrentValue) ?>" selected><?php echo $v_works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_progress->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_progress" class="form-group v_works_work_progress">
<span<?php echo $v_works->work_progress->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_progress->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_progress" name="x<?php echo $v_works_grid->RowIndex ?>_work_progress" id="x<?php echo $v_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($v_works->work_progress->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_progress" name="o<?php echo $v_works_grid->RowIndex ?>_work_progress" id="o<?php echo $v_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($v_works->work_progress->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_time->Visible) { // work_time ?>
		<td data-name="work_time">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_time" class="form-group v_works_work_time">
<input type="text" data-table="v_works" data-field="x_work_time" name="x<?php echo $v_works_grid->RowIndex ?>_work_time" id="x<?php echo $v_works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_time->getPlaceHolder()) ?>" value="<?php echo $v_works->work_time->EditValue ?>"<?php echo $v_works->work_time->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_time" class="form-group v_works_work_time">
<span<?php echo $v_works->work_time->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_time->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_time" name="x<?php echo $v_works_grid->RowIndex ?>_work_time" id="x<?php echo $v_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($v_works->work_time->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_time" name="o<?php echo $v_works_grid->RowIndex ?>_work_time" id="o<?php echo $v_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($v_works->work_time->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_works->work_employee_id->Visible) { // work_employee_id ?>
		<td data-name="work_employee_id">
<?php if ($v_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_works_work_employee_id" class="form-group v_works_work_employee_id">
<select data-table="v_works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_employee_id->DisplayValueSeparator) ? json_encode($v_works->work_employee_id->DisplayValueSeparator) : $v_works->work_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id"<?php echo $v_works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_employee_id->EditValue)) {
	$arwrk = $v_works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_employee_id->CurrentValue) ?>" selected><?php echo $v_works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_works->work_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$v_works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $v_works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="s_x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo $v_works->work_employee_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_v_works_work_employee_id" class="form-group v_works_work_employee_id">
<span<?php echo $v_works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_works->work_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_employee_id" name="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($v_works->work_employee_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_works" data-field="x_work_employee_id" name="o<?php echo $v_works_grid->RowIndex ?>_work_employee_id" id="o<?php echo $v_works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($v_works->work_employee_id->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$v_works_grid->ListOptions->Render("body", "right", $v_works_grid->RowCnt);
?>
<script type="text/javascript">
fv_worksgrid.UpdateOpts(<?php echo $v_works_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($v_works->CurrentMode == "add" || $v_works->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $v_works_grid->FormKeyCountName ?>" id="<?php echo $v_works_grid->FormKeyCountName ?>" value="<?php echo $v_works_grid->KeyCount ?>">
<?php echo $v_works_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($v_works->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $v_works_grid->FormKeyCountName ?>" id="<?php echo $v_works_grid->FormKeyCountName ?>" value="<?php echo $v_works_grid->KeyCount ?>">
<?php echo $v_works_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($v_works->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="fv_worksgrid">
</div>
<?php

// Close recordset
if ($v_works_grid->Recordset)
	$v_works_grid->Recordset->Close();
?>
<?php if ($v_works_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($v_works_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($v_works_grid->TotalRecs == 0 && $v_works->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($v_works_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($v_works->Export == "") { ?>
<script type="text/javascript">
fv_worksgrid.Init();
</script>
<?php } ?>
<?php
$v_works_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$v_works_grid->Page_Terminate();
?>
