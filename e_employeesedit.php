<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "labsinfo.php" ?>
<?php include_once "positionsinfo.php" ?>
<?php include_once "plansgridcls.php" ?>
<?php include_once "worksgridcls.php" ?>
<?php include_once "e_tasks_financegridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$e_employees_edit = NULL; // Initialize page object first

class ce_employees_edit extends ce_employees {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'e_employees';

	// Page object name
	var $PageObjName = 'e_employees_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = TRUE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (e_employees)
		if (!isset($GLOBALS["e_employees"]) || get_class($GLOBALS["e_employees"]) == "ce_employees") {
			$GLOBALS["e_employees"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["e_employees"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (labs)
		if (!isset($GLOBALS['labs'])) $GLOBALS['labs'] = new clabs();

		// Table object (positions)
		if (!isset($GLOBALS['positions'])) $GLOBALS['positions'] = new cpositions();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'e_employees', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("e_employeeslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'plans'
			if (@$_POST["grid"] == "fplansgrid") {
				if (!isset($GLOBALS["plans_grid"])) $GLOBALS["plans_grid"] = new cplans_grid;
				$GLOBALS["plans_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'works'
			if (@$_POST["grid"] == "fworksgrid") {
				if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid;
				$GLOBALS["works_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'e_tasks_finance'
			if (@$_POST["grid"] == "fe_tasks_financegrid") {
				if (!isset($GLOBALS["e_tasks_finance_grid"])) $GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid;
				$GLOBALS["e_tasks_finance_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $e_employees;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($e_employees);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["employee_id"] <> "") {
			$this->employee_id->setQueryStringValue($_GET["employee_id"]);
		}

		// Set up master detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values

			// Set up detail parameters
			$this->SetUpDetailParms();
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->employee_id->CurrentValue == "")
			$this->Page_Terminate("e_employeeslist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("e_employeeslist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			Case "U": // Update
				if ($this->getCurrentDetailTable() <> "") // Master/detail edit
					$sReturnUrl = $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $this->getCurrentDetailTable()); // Master/Detail view page
				else
					$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "e_employeeslist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->employee_login->FldIsDetailKey) {
			$this->employee_login->setFormValue($objForm->GetValue("x_employee_login"));
		}
		if (!$this->employee_level_id->FldIsDetailKey) {
			$this->employee_level_id->setFormValue($objForm->GetValue("x_employee_level_id"));
		}
		if (!$this->employee_first_name->FldIsDetailKey) {
			$this->employee_first_name->setFormValue($objForm->GetValue("x_employee_first_name"));
		}
		if (!$this->employee_last_name->FldIsDetailKey) {
			$this->employee_last_name->setFormValue($objForm->GetValue("x_employee_last_name"));
		}
		if (!$this->employee_telephone->FldIsDetailKey) {
			$this->employee_telephone->setFormValue($objForm->GetValue("x_employee_telephone"));
		}
		if (!$this->employee_lab_id->FldIsDetailKey) {
			$this->employee_lab_id->setFormValue($objForm->GetValue("x_employee_lab_id"));
		}
		if (!$this->employee_position_id->FldIsDetailKey) {
			$this->employee_position_id->setFormValue($objForm->GetValue("x_employee_position_id"));
		}
		if (!$this->employee_salary->FldIsDetailKey) {
			$this->employee_salary->setFormValue($objForm->GetValue("x_employee_salary"));
		}
		if (!$this->employee_id->FldIsDetailKey)
			$this->employee_id->setFormValue($objForm->GetValue("x_employee_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->employee_id->CurrentValue = $this->employee_id->FormValue;
		$this->employee_login->CurrentValue = $this->employee_login->FormValue;
		$this->employee_level_id->CurrentValue = $this->employee_level_id->FormValue;
		$this->employee_first_name->CurrentValue = $this->employee_first_name->FormValue;
		$this->employee_last_name->CurrentValue = $this->employee_last_name->FormValue;
		$this->employee_telephone->CurrentValue = $this->employee_telephone->FormValue;
		$this->employee_lab_id->CurrentValue = $this->employee_lab_id->FormValue;
		$this->employee_position_id->CurrentValue = $this->employee_position_id->FormValue;
		$this->employee_salary->CurrentValue = $this->employee_salary->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->employee_id->setDbValue($rs->fields('employee_id'));
		$this->employee_login->setDbValue($rs->fields('employee_login'));
		$this->employee_level_id->setDbValue($rs->fields('employee_level_id'));
		$this->employee_first_name->setDbValue($rs->fields('employee_first_name'));
		$this->employee_last_name->setDbValue($rs->fields('employee_last_name'));
		$this->employee_telephone->setDbValue($rs->fields('employee_telephone'));
		$this->employee_lab_id->setDbValue($rs->fields('employee_lab_id'));
		$this->employee_position_id->setDbValue($rs->fields('employee_position_id'));
		$this->employee_salary->setDbValue($rs->fields('employee_salary'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->employee_id->DbValue = $row['employee_id'];
		$this->employee_login->DbValue = $row['employee_login'];
		$this->employee_level_id->DbValue = $row['employee_level_id'];
		$this->employee_first_name->DbValue = $row['employee_first_name'];
		$this->employee_last_name->DbValue = $row['employee_last_name'];
		$this->employee_telephone->DbValue = $row['employee_telephone'];
		$this->employee_lab_id->DbValue = $row['employee_lab_id'];
		$this->employee_position_id->DbValue = $row['employee_position_id'];
		$this->employee_salary->DbValue = $row['employee_salary'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// employee_id
		// employee_login
		// employee_level_id
		// employee_first_name
		// employee_last_name
		// employee_telephone
		// employee_lab_id
		// employee_position_id
		// employee_salary

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// employee_id
		$this->employee_id->ViewValue = $this->employee_id->CurrentValue;
		$this->employee_id->ViewCustomAttributes = "";

		// employee_login
		$this->employee_login->ViewValue = $this->employee_login->CurrentValue;
		$this->employee_login->ViewCustomAttributes = "";

		// employee_level_id
		if (strval($this->employee_level_id->CurrentValue) <> "") {
			$this->employee_level_id->ViewValue = $this->employee_level_id->OptionCaption($this->employee_level_id->CurrentValue);
		} else {
			$this->employee_level_id->ViewValue = NULL;
		}
		$this->employee_level_id->ViewCustomAttributes = "";

		// employee_first_name
		$this->employee_first_name->ViewValue = $this->employee_first_name->CurrentValue;
		$this->employee_first_name->ViewCustomAttributes = "";

		// employee_last_name
		$this->employee_last_name->ViewValue = $this->employee_last_name->CurrentValue;
		$this->employee_last_name->ViewCustomAttributes = "";

		// employee_telephone
		$this->employee_telephone->ViewValue = $this->employee_telephone->CurrentValue;
		$this->employee_telephone->ViewCustomAttributes = "";

		// employee_lab_id
		if (strval($this->employee_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->employee_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->employee_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->employee_lab_id->ViewValue = $this->employee_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->employee_lab_id->ViewValue = $this->employee_lab_id->CurrentValue;
			}
		} else {
			$this->employee_lab_id->ViewValue = NULL;
		}
		$this->employee_lab_id->ViewCustomAttributes = "";

		// employee_position_id
		if (strval($this->employee_position_id->CurrentValue) <> "") {
			$sFilterWrk = "`position_id`" . ew_SearchString("=", $this->employee_position_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->employee_position_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->employee_position_id->ViewValue = $this->employee_position_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->employee_position_id->ViewValue = $this->employee_position_id->CurrentValue;
			}
		} else {
			$this->employee_position_id->ViewValue = NULL;
		}
		$this->employee_position_id->ViewCustomAttributes = "";

		// employee_salary
		$this->employee_salary->ViewValue = $this->employee_salary->CurrentValue;
		$this->employee_salary->ViewCustomAttributes = "";

			// employee_login
			$this->employee_login->LinkCustomAttributes = "";
			$this->employee_login->HrefValue = "";
			$this->employee_login->TooltipValue = "";

			// employee_level_id
			$this->employee_level_id->LinkCustomAttributes = "";
			$this->employee_level_id->HrefValue = "";
			$this->employee_level_id->TooltipValue = "";

			// employee_first_name
			$this->employee_first_name->LinkCustomAttributes = "";
			$this->employee_first_name->HrefValue = "";
			$this->employee_first_name->TooltipValue = "";

			// employee_last_name
			$this->employee_last_name->LinkCustomAttributes = "";
			$this->employee_last_name->HrefValue = "";
			$this->employee_last_name->TooltipValue = "";

			// employee_telephone
			$this->employee_telephone->LinkCustomAttributes = "";
			$this->employee_telephone->HrefValue = "";
			$this->employee_telephone->TooltipValue = "";

			// employee_lab_id
			$this->employee_lab_id->LinkCustomAttributes = "";
			$this->employee_lab_id->HrefValue = "";
			$this->employee_lab_id->TooltipValue = "";

			// employee_position_id
			$this->employee_position_id->LinkCustomAttributes = "";
			$this->employee_position_id->HrefValue = "";
			$this->employee_position_id->TooltipValue = "";

			// employee_salary
			$this->employee_salary->LinkCustomAttributes = "";
			$this->employee_salary->HrefValue = "";
			$this->employee_salary->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// employee_login
			$this->employee_login->EditAttrs["class"] = "form-control";
			$this->employee_login->EditCustomAttributes = "";
			$this->employee_login->EditValue = $this->employee_login->CurrentValue;
			$this->employee_login->ViewCustomAttributes = "";

			// employee_level_id
			$this->employee_level_id->EditAttrs["class"] = "form-control";
			$this->employee_level_id->EditCustomAttributes = "";
			$this->employee_level_id->EditValue = $this->employee_level_id->Options(TRUE);

			// employee_first_name
			$this->employee_first_name->EditAttrs["class"] = "form-control";
			$this->employee_first_name->EditCustomAttributes = "";
			$this->employee_first_name->EditValue = $this->employee_first_name->CurrentValue;
			$this->employee_first_name->ViewCustomAttributes = "";

			// employee_last_name
			$this->employee_last_name->EditAttrs["class"] = "form-control";
			$this->employee_last_name->EditCustomAttributes = "";
			$this->employee_last_name->EditValue = $this->employee_last_name->CurrentValue;
			$this->employee_last_name->ViewCustomAttributes = "";

			// employee_telephone
			$this->employee_telephone->EditAttrs["class"] = "form-control";
			$this->employee_telephone->EditCustomAttributes = "";
			$this->employee_telephone->EditValue = $this->employee_telephone->CurrentValue;
			$this->employee_telephone->ViewCustomAttributes = "";

			// employee_lab_id
			$this->employee_lab_id->EditAttrs["class"] = "form-control";
			$this->employee_lab_id->EditCustomAttributes = "";
			if ($this->employee_lab_id->getSessionValue() <> "") {
				$this->employee_lab_id->CurrentValue = $this->employee_lab_id->getSessionValue();
			if (strval($this->employee_lab_id->CurrentValue) <> "") {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->employee_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->employee_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->employee_lab_id->ViewValue = $this->employee_lab_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->employee_lab_id->ViewValue = $this->employee_lab_id->CurrentValue;
				}
			} else {
				$this->employee_lab_id->ViewValue = NULL;
			}
			$this->employee_lab_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->employee_lab_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->employee_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->employee_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->employee_lab_id->EditValue = $arwrk;
			}

			// employee_position_id
			$this->employee_position_id->EditAttrs["class"] = "form-control";
			$this->employee_position_id->EditCustomAttributes = "";
			if ($this->employee_position_id->getSessionValue() <> "") {
				$this->employee_position_id->CurrentValue = $this->employee_position_id->getSessionValue();
			if (strval($this->employee_position_id->CurrentValue) <> "") {
				$sFilterWrk = "`position_id`" . ew_SearchString("=", $this->employee_position_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->employee_position_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->employee_position_id->ViewValue = $this->employee_position_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->employee_position_id->ViewValue = $this->employee_position_id->CurrentValue;
				}
			} else {
				$this->employee_position_id->ViewValue = NULL;
			}
			$this->employee_position_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->employee_position_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`position_id`" . ew_SearchString("=", $this->employee_position_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `positions`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->employee_position_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->employee_position_id->EditValue = $arwrk;
			}

			// employee_salary
			$this->employee_salary->EditAttrs["class"] = "form-control";
			$this->employee_salary->EditCustomAttributes = "";
			$this->employee_salary->EditValue = ew_HtmlEncode($this->employee_salary->CurrentValue);
			$this->employee_salary->PlaceHolder = ew_RemoveHtml($this->employee_salary->FldCaption());

			// Edit refer script
			// employee_login

			$this->employee_login->LinkCustomAttributes = "";
			$this->employee_login->HrefValue = "";
			$this->employee_login->TooltipValue = "";

			// employee_level_id
			$this->employee_level_id->LinkCustomAttributes = "";
			$this->employee_level_id->HrefValue = "";

			// employee_first_name
			$this->employee_first_name->LinkCustomAttributes = "";
			$this->employee_first_name->HrefValue = "";
			$this->employee_first_name->TooltipValue = "";

			// employee_last_name
			$this->employee_last_name->LinkCustomAttributes = "";
			$this->employee_last_name->HrefValue = "";
			$this->employee_last_name->TooltipValue = "";

			// employee_telephone
			$this->employee_telephone->LinkCustomAttributes = "";
			$this->employee_telephone->HrefValue = "";
			$this->employee_telephone->TooltipValue = "";

			// employee_lab_id
			$this->employee_lab_id->LinkCustomAttributes = "";
			$this->employee_lab_id->HrefValue = "";

			// employee_position_id
			$this->employee_position_id->LinkCustomAttributes = "";
			$this->employee_position_id->HrefValue = "";

			// employee_salary
			$this->employee_salary->LinkCustomAttributes = "";
			$this->employee_salary->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->employee_level_id->FldIsDetailKey && !is_null($this->employee_level_id->FormValue) && $this->employee_level_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_level_id->FldCaption(), $this->employee_level_id->ReqErrMsg));
		}
		if (!$this->employee_lab_id->FldIsDetailKey && !is_null($this->employee_lab_id->FormValue) && $this->employee_lab_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_lab_id->FldCaption(), $this->employee_lab_id->ReqErrMsg));
		}
		if (!$this->employee_position_id->FldIsDetailKey && !is_null($this->employee_position_id->FormValue) && $this->employee_position_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_position_id->FldCaption(), $this->employee_position_id->ReqErrMsg));
		}
		if (!$this->employee_salary->FldIsDetailKey && !is_null($this->employee_salary->FormValue) && $this->employee_salary->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_salary->FldCaption(), $this->employee_salary->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->employee_salary->FormValue)) {
			ew_AddMessage($gsFormError, $this->employee_salary->FldErrMsg());
		}

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("plans", $DetailTblVar) && $GLOBALS["plans"]->DetailEdit) {
			if (!isset($GLOBALS["plans_grid"])) $GLOBALS["plans_grid"] = new cplans_grid(); // get detail page object
			$GLOBALS["plans_grid"]->ValidateGridForm();
		}
		if (in_array("works", $DetailTblVar) && $GLOBALS["works"]->DetailEdit) {
			if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid(); // get detail page object
			$GLOBALS["works_grid"]->ValidateGridForm();
		}
		if (in_array("e_tasks_finance", $DetailTblVar) && $GLOBALS["e_tasks_finance"]->DetailEdit) {
			if (!isset($GLOBALS["e_tasks_finance_grid"])) $GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid(); // get detail page object
			$GLOBALS["e_tasks_finance_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		if ($this->employee_login->CurrentValue <> "") { // Check field with unique index
			$sFilterChk = "(`employee_login` = '" . ew_AdjustSql($this->employee_login->CurrentValue, $this->DBID) . "')";
			$sFilterChk .= " AND NOT (" . $sFilter . ")";
			$this->CurrentFilter = $sFilterChk;
			$sSqlChk = $this->SQL();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rsChk = $conn->Execute($sSqlChk);
			$conn->raiseErrorFn = '';
			if ($rsChk === FALSE) {
				return FALSE;
			} elseif (!$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->employee_login->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->employee_login->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
			$rsChk->Close();
		}
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Begin transaction
			if ($this->getCurrentDetailTable() <> "")
				$conn->BeginTrans();

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// employee_level_id
			$this->employee_level_id->SetDbValueDef($rsnew, $this->employee_level_id->CurrentValue, 0, $this->employee_level_id->ReadOnly);

			// employee_lab_id
			$this->employee_lab_id->SetDbValueDef($rsnew, $this->employee_lab_id->CurrentValue, 0, $this->employee_lab_id->ReadOnly);

			// employee_position_id
			$this->employee_position_id->SetDbValueDef($rsnew, $this->employee_position_id->CurrentValue, 0, $this->employee_position_id->ReadOnly);

			// employee_salary
			$this->employee_salary->SetDbValueDef($rsnew, $this->employee_salary->CurrentValue, 0, $this->employee_salary->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}

				// Update detail records
				$DetailTblVar = explode(",", $this->getCurrentDetailTable());
				if ($EditRow) {
					if (in_array("plans", $DetailTblVar) && $GLOBALS["plans"]->DetailEdit) {
						if (!isset($GLOBALS["plans_grid"])) $GLOBALS["plans_grid"] = new cplans_grid(); // Get detail page object
						$EditRow = $GLOBALS["plans_grid"]->GridUpdate();
					}
				}
				if ($EditRow) {
					if (in_array("works", $DetailTblVar) && $GLOBALS["works"]->DetailEdit) {
						if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid(); // Get detail page object
						$EditRow = $GLOBALS["works_grid"]->GridUpdate();
					}
				}
				if ($EditRow) {
					if (in_array("e_tasks_finance", $DetailTblVar) && $GLOBALS["e_tasks_finance"]->DetailEdit) {
						if (!isset($GLOBALS["e_tasks_finance_grid"])) $GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid(); // Get detail page object
						$EditRow = $GLOBALS["e_tasks_finance_grid"]->GridUpdate();
					}
				}

				// Commit/Rollback transaction
				if ($this->getCurrentDetailTable() <> "") {
					if ($EditRow) {
						$conn->CommitTrans(); // Commit transaction
					} else {
						$conn->RollbackTrans(); // Rollback transaction
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "labs") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_lab_id"] <> "") {
					$GLOBALS["labs"]->lab_id->setQueryStringValue($_GET["fk_lab_id"]);
					$this->employee_lab_id->setQueryStringValue($GLOBALS["labs"]->lab_id->QueryStringValue);
					$this->employee_lab_id->setSessionValue($this->employee_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["labs"]->lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "positions") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_position_id"] <> "") {
					$GLOBALS["positions"]->position_id->setQueryStringValue($_GET["fk_position_id"]);
					$this->employee_position_id->setQueryStringValue($GLOBALS["positions"]->position_id->QueryStringValue);
					$this->employee_position_id->setSessionValue($this->employee_position_id->QueryStringValue);
					if (!is_numeric($GLOBALS["positions"]->position_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "labs") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_lab_id"] <> "") {
					$GLOBALS["labs"]->lab_id->setFormValue($_POST["fk_lab_id"]);
					$this->employee_lab_id->setFormValue($GLOBALS["labs"]->lab_id->FormValue);
					$this->employee_lab_id->setSessionValue($this->employee_lab_id->FormValue);
					if (!is_numeric($GLOBALS["labs"]->lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "positions") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_position_id"] <> "") {
					$GLOBALS["positions"]->position_id->setFormValue($_POST["fk_position_id"]);
					$this->employee_position_id->setFormValue($GLOBALS["positions"]->position_id->FormValue);
					$this->employee_position_id->setSessionValue($this->employee_position_id->FormValue);
					if (!is_numeric($GLOBALS["positions"]->position_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);
			$this->setSessionWhere($this->GetDetailFilter());

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "labs") {
				if ($this->employee_lab_id->CurrentValue == "") $this->employee_lab_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "positions") {
				if ($this->employee_position_id->CurrentValue == "") $this->employee_position_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("plans", $DetailTblVar)) {
				if (!isset($GLOBALS["plans_grid"]))
					$GLOBALS["plans_grid"] = new cplans_grid;
				if ($GLOBALS["plans_grid"]->DetailEdit) {
					$GLOBALS["plans_grid"]->CurrentMode = "edit";
					$GLOBALS["plans_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["plans_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["plans_grid"]->setStartRecordNumber(1);
					$GLOBALS["plans_grid"]->plan_employee_id->FldIsDetailKey = TRUE;
					$GLOBALS["plans_grid"]->plan_employee_id->CurrentValue = $this->employee_id->CurrentValue;
					$GLOBALS["plans_grid"]->plan_employee_id->setSessionValue($GLOBALS["plans_grid"]->plan_employee_id->CurrentValue);
				}
			}
			if (in_array("works", $DetailTblVar)) {
				if (!isset($GLOBALS["works_grid"]))
					$GLOBALS["works_grid"] = new cworks_grid;
				if ($GLOBALS["works_grid"]->DetailEdit) {
					$GLOBALS["works_grid"]->CurrentMode = "edit";
					$GLOBALS["works_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["works_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["works_grid"]->setStartRecordNumber(1);
					$GLOBALS["works_grid"]->work_employee_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_employee_id->CurrentValue = $this->employee_id->CurrentValue;
					$GLOBALS["works_grid"]->work_employee_id->setSessionValue($GLOBALS["works_grid"]->work_employee_id->CurrentValue);
				}
			}
			if (in_array("e_tasks_finance", $DetailTblVar)) {
				if (!isset($GLOBALS["e_tasks_finance_grid"]))
					$GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid;
				if ($GLOBALS["e_tasks_finance_grid"]->DetailEdit) {
					$GLOBALS["e_tasks_finance_grid"]->CurrentMode = "edit";
					$GLOBALS["e_tasks_finance_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["e_tasks_finance_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["e_tasks_finance_grid"]->setStartRecordNumber(1);
					$GLOBALS["e_tasks_finance_grid"]->task_employee_id->FldIsDetailKey = TRUE;
					$GLOBALS["e_tasks_finance_grid"]->task_employee_id->CurrentValue = $this->employee_id->CurrentValue;
					$GLOBALS["e_tasks_finance_grid"]->task_employee_id->setSessionValue($GLOBALS["e_tasks_finance_grid"]->task_employee_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("e_employeeslist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'e_employees';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		global $Language;
		if (!$this->AuditTrailOnEdit) return;
		$table = 'e_employees';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['employee_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") { // Password Field
						$oldvalue = $Language->Phrase("PasswordMask");
						$newvalue = $Language->Phrase("PasswordMask");
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($e_employees_edit)) $e_employees_edit = new ce_employees_edit();

// Page init
$e_employees_edit->Page_Init();

// Page main
$e_employees_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$e_employees_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fe_employeesedit = new ew_Form("fe_employeesedit", "edit");

// Validate form
fe_employeesedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_employee_level_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_level_id->FldCaption(), $e_employees->employee_level_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_lab_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_lab_id->FldCaption(), $e_employees->employee_lab_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_position_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_position_id->FldCaption(), $e_employees->employee_position_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_salary");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_salary->FldCaption(), $e_employees->employee_salary->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_salary");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($e_employees->employee_salary->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fe_employeesedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fe_employeesedit.ValidateRequired = true;
<?php } else { ?>
fe_employeesedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fe_employeesedit.Lists["x_employee_level_id"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_employeesedit.Lists["x_employee_level_id"].Options = <?php echo json_encode($e_employees->employee_level_id->Options()) ?>;
fe_employeesedit.Lists["x_employee_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_employeesedit.Lists["x_employee_position_id"] = {"LinkField":"x_position_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_position_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $e_employees_edit->ShowPageHeader(); ?>
<?php
$e_employees_edit->ShowMessage();
?>
<form name="fe_employeesedit" id="fe_employeesedit" class="<?php echo $e_employees_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($e_employees_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $e_employees_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="e_employees">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($e_employees->getCurrentMasterTable() == "labs") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="labs">
<input type="hidden" name="fk_lab_id" value="<?php echo $e_employees->employee_lab_id->getSessionValue() ?>">
<?php } ?>
<?php if ($e_employees->getCurrentMasterTable() == "positions") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="positions">
<input type="hidden" name="fk_position_id" value="<?php echo $e_employees->employee_position_id->getSessionValue() ?>">
<?php } ?>
<div>
<?php if ($e_employees->employee_login->Visible) { // employee_login ?>
	<div id="r_employee_login" class="form-group">
		<label id="elh_e_employees_employee_login" for="x_employee_login" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_login->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_login->CellAttributes() ?>>
<span id="el_e_employees_employee_login">
<span<?php echo $e_employees->employee_login->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_login->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="x_employee_login" id="x_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->CurrentValue) ?>">
<?php echo $e_employees->employee_login->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_level_id->Visible) { // employee_level_id ?>
	<div id="r_employee_level_id" class="form-group">
		<label id="elh_e_employees_employee_level_id" for="x_employee_level_id" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_level_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_level_id->CellAttributes() ?>>
<span id="el_e_employees_employee_level_id">
<select data-table="e_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_level_id->DisplayValueSeparator) ? json_encode($e_employees->employee_level_id->DisplayValueSeparator) : $e_employees->employee_level_id->DisplayValueSeparator) ?>" id="x_employee_level_id" name="x_employee_level_id"<?php echo $e_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_level_id->EditValue)) {
	$arwrk = $e_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php echo $e_employees->employee_level_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_first_name->Visible) { // employee_first_name ?>
	<div id="r_employee_first_name" class="form-group">
		<label id="elh_e_employees_employee_first_name" for="x_employee_first_name" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_first_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_first_name->CellAttributes() ?>>
<span id="el_e_employees_employee_first_name">
<span<?php echo $e_employees->employee_first_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_first_name->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="x_employee_first_name" id="x_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->CurrentValue) ?>">
<?php echo $e_employees->employee_first_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_last_name->Visible) { // employee_last_name ?>
	<div id="r_employee_last_name" class="form-group">
		<label id="elh_e_employees_employee_last_name" for="x_employee_last_name" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_last_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_last_name->CellAttributes() ?>>
<span id="el_e_employees_employee_last_name">
<span<?php echo $e_employees->employee_last_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_last_name->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="x_employee_last_name" id="x_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->CurrentValue) ?>">
<?php echo $e_employees->employee_last_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_telephone->Visible) { // employee_telephone ?>
	<div id="r_employee_telephone" class="form-group">
		<label id="elh_e_employees_employee_telephone" for="x_employee_telephone" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_telephone->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_telephone->CellAttributes() ?>>
<span id="el_e_employees_employee_telephone">
<span<?php echo $e_employees->employee_telephone->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_telephone->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="x_employee_telephone" id="x_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->CurrentValue) ?>">
<?php echo $e_employees->employee_telephone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_lab_id->Visible) { // employee_lab_id ?>
	<div id="r_employee_lab_id" class="form-group">
		<label id="elh_e_employees_employee_lab_id" for="x_employee_lab_id" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_lab_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_lab_id->CellAttributes() ?>>
<?php if ($e_employees->employee_lab_id->getSessionValue() <> "") { ?>
<span id="el_e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_employee_lab_id" name="x_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_e_employees_employee_lab_id">
<select data-table="e_employees" data-field="x_employee_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_lab_id->DisplayValueSeparator) ? json_encode($e_employees->employee_lab_id->DisplayValueSeparator) : $e_employees->employee_lab_id->DisplayValueSeparator) ?>" id="x_employee_lab_id" name="x_employee_lab_id"<?php echo $e_employees->employee_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_lab_id->EditValue)) {
	$arwrk = $e_employees->employee_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_lab_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_employees->employee_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_employee_lab_id" id="s_x_employee_lab_id" value="<?php echo $e_employees->employee_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $e_employees->employee_lab_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_position_id->Visible) { // employee_position_id ?>
	<div id="r_employee_position_id" class="form-group">
		<label id="elh_e_employees_employee_position_id" for="x_employee_position_id" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_position_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_position_id->CellAttributes() ?>>
<?php if ($e_employees->employee_position_id->getSessionValue() <> "") { ?>
<span id="el_e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_position_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_employee_position_id" name="x_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_e_employees_employee_position_id">
<select data-table="e_employees" data-field="x_employee_position_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_position_id->DisplayValueSeparator) ? json_encode($e_employees->employee_position_id->DisplayValueSeparator) : $e_employees->employee_position_id->DisplayValueSeparator) ?>" id="x_employee_position_id" name="x_employee_position_id"<?php echo $e_employees->employee_position_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_position_id->EditValue)) {
	$arwrk = $e_employees->employee_position_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_position_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_position_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_position_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_position_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
$sWhereWrk = "";
$e_employees->employee_position_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_position_id->LookupFilters += array("f0" => "`position_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_position_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_position_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_employee_position_id" id="s_x_employee_position_id" value="<?php echo $e_employees->employee_position_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $e_employees->employee_position_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_employees->employee_salary->Visible) { // employee_salary ?>
	<div id="r_employee_salary" class="form-group">
		<label id="elh_e_employees_employee_salary" for="x_employee_salary" class="col-sm-2 control-label ewLabel"><?php echo $e_employees->employee_salary->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $e_employees->employee_salary->CellAttributes() ?>>
<span id="el_e_employees_employee_salary">
<input type="text" data-table="e_employees" data-field="x_employee_salary" name="x_employee_salary" id="x_employee_salary" size="30" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_salary->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_salary->EditValue ?>"<?php echo $e_employees->employee_salary->EditAttributes() ?>>
</span>
<?php echo $e_employees->employee_salary->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<input type="hidden" data-table="e_employees" data-field="x_employee_id" name="x_employee_id" id="x_employee_id" value="<?php echo ew_HtmlEncode($e_employees->employee_id->CurrentValue) ?>">
<?php
	if (in_array("plans", explode(",", $e_employees->getCurrentDetailTable())) && $plans->DetailEdit) {
?>
<?php if ($e_employees->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("plans", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "plansgrid.php" ?>
<?php } ?>
<?php
	if (in_array("works", explode(",", $e_employees->getCurrentDetailTable())) && $works->DetailEdit) {
?>
<?php if ($e_employees->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("works", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "worksgrid.php" ?>
<?php } ?>
<?php
	if (in_array("e_tasks_finance", explode(",", $e_employees->getCurrentDetailTable())) && $e_tasks_finance->DetailEdit) {
?>
<?php if ($e_employees->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("e_tasks_finance", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "e_tasks_financegrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $e_employees_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fe_employeesedit.Init();
</script>
<?php
$e_employees_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$e_employees_edit->Page_Terminate();
?>
