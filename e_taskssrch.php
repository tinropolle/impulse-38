<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "e_tasksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$e_tasks_search = NULL; // Initialize page object first

class ce_tasks_search extends ce_tasks {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'e_tasks';

	// Page object name
	var $PageObjName = 'e_tasks_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (e_tasks)
		if (!isset($GLOBALS["e_tasks"]) || get_class($GLOBALS["e_tasks"]) == "ce_tasks") {
			$GLOBALS["e_tasks"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["e_tasks"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (plans)
		if (!isset($GLOBALS['plans'])) $GLOBALS['plans'] = new cplans();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'e_tasks', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("e_taskslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->task_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $e_tasks;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($e_tasks);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "e_taskslist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->task_id); // task_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_project_id); // task_project_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_plan_id); // task_plan_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_lab_id); // task_lab_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_code); // task_code
		$this->BuildSearchUrl($sSrchUrl, $this->task_name); // task_name
		$this->BuildSearchUrl($sSrchUrl, $this->task_from); // task_from
		$this->BuildSearchUrl($sSrchUrl, $this->task_to); // task_to
		$this->BuildSearchUrl($sSrchUrl, $this->task_employee_id); // task_employee_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_coordinator_id); // task_coordinator_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_object); // task_object
		$this->BuildSearchUrl($sSrchUrl, $this->task_status_id); // task_status_id
		$this->BuildSearchUrl($sSrchUrl, $this->task_hours_planned); // task_hours_planned
		$this->BuildSearchUrl($sSrchUrl, $this->task_hours_actual); // task_hours_actual
		$this->BuildSearchUrl($sSrchUrl, $this->task_description); // task_description
		$this->BuildSearchUrl($sSrchUrl, $this->task_key); // task_key
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// task_id

		$this->task_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_id"));
		$this->task_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_id");

		// task_project_id
		$this->task_project_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_project_id"));
		$this->task_project_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_project_id");

		// task_plan_id
		$this->task_plan_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_plan_id"));
		$this->task_plan_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_plan_id");

		// task_lab_id
		$this->task_lab_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_lab_id"));
		$this->task_lab_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_lab_id");

		// task_code
		$this->task_code->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_code"));
		$this->task_code->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_code");

		// task_name
		$this->task_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_name"));
		$this->task_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_name");

		// task_from
		$this->task_from->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_from"));
		$this->task_from->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_from");
		$this->task_from->AdvancedSearch->SearchCondition = $objForm->GetValue("v_task_from");
		$this->task_from->AdvancedSearch->SearchValue2 = ew_StripSlashes($objForm->GetValue("y_task_from"));
		$this->task_from->AdvancedSearch->SearchOperator2 = $objForm->GetValue("w_task_from");

		// task_to
		$this->task_to->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_to"));
		$this->task_to->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_to");
		$this->task_to->AdvancedSearch->SearchCondition = $objForm->GetValue("v_task_to");
		$this->task_to->AdvancedSearch->SearchValue2 = ew_StripSlashes($objForm->GetValue("y_task_to"));
		$this->task_to->AdvancedSearch->SearchOperator2 = $objForm->GetValue("w_task_to");

		// task_employee_id
		$this->task_employee_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_employee_id"));
		$this->task_employee_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_employee_id");

		// task_coordinator_id
		$this->task_coordinator_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_coordinator_id"));
		$this->task_coordinator_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_coordinator_id");

		// task_object
		$this->task_object->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_object"));
		$this->task_object->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_object");

		// task_status_id
		$this->task_status_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_status_id"));
		$this->task_status_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_status_id");

		// task_hours_planned
		$this->task_hours_planned->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_hours_planned"));
		$this->task_hours_planned->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_hours_planned");
		$this->task_hours_planned->AdvancedSearch->SearchCondition = $objForm->GetValue("v_task_hours_planned");
		$this->task_hours_planned->AdvancedSearch->SearchValue2 = ew_StripSlashes($objForm->GetValue("y_task_hours_planned"));
		$this->task_hours_planned->AdvancedSearch->SearchOperator2 = $objForm->GetValue("w_task_hours_planned");

		// task_hours_actual
		$this->task_hours_actual->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_hours_actual"));
		$this->task_hours_actual->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_hours_actual");
		$this->task_hours_actual->AdvancedSearch->SearchCondition = $objForm->GetValue("v_task_hours_actual");
		$this->task_hours_actual->AdvancedSearch->SearchValue2 = ew_StripSlashes($objForm->GetValue("y_task_hours_actual"));
		$this->task_hours_actual->AdvancedSearch->SearchOperator2 = $objForm->GetValue("w_task_hours_actual");

		// task_description
		$this->task_description->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_description"));
		$this->task_description->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_description");

		// task_key
		$this->task_key->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_task_key"));
		$this->task_key->AdvancedSearch->SearchOperator = $objForm->GetValue("z_task_key");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->task_hours_planned->FormValue == $this->task_hours_planned->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_planned->CurrentValue)))
			$this->task_hours_planned->CurrentValue = ew_StrToFloat($this->task_hours_planned->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_hours_actual->FormValue == $this->task_hours_actual->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_actual->CurrentValue)))
			$this->task_hours_actual->CurrentValue = ew_StrToFloat($this->task_hours_actual->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// task_id
		// task_project_id
		// task_plan_id
		// task_lab_id
		// task_code
		// task_name
		// task_from
		// task_to
		// task_employee_id
		// task_coordinator_id
		// task_object
		// task_status_id
		// task_hours_planned
		// task_hours_actual
		// task_description
		// task_key

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// task_id
		$this->task_id->ViewValue = $this->task_id->CurrentValue;
		$this->task_id->ViewCustomAttributes = "";

		// task_project_id
		if (strval($this->task_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_project_id->ViewValue = $this->task_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_project_id->ViewValue = $this->task_project_id->CurrentValue;
			}
		} else {
			$this->task_project_id->ViewValue = NULL;
		}
		$this->task_project_id->ViewCustomAttributes = "";

		// task_plan_id
		if (strval($this->task_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->task_plan_id->ViewValue = $this->task_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_plan_id->ViewValue = $this->task_plan_id->CurrentValue;
			}
		} else {
			$this->task_plan_id->ViewValue = NULL;
		}
		$this->task_plan_id->ViewCustomAttributes = "";

		// task_lab_id
		if (strval($this->task_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_lab_id->ViewValue = $this->task_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_lab_id->ViewValue = $this->task_lab_id->CurrentValue;
			}
		} else {
			$this->task_lab_id->ViewValue = NULL;
		}
		$this->task_lab_id->ViewCustomAttributes = "";

		// task_code
		$this->task_code->ViewValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->ViewValue = $this->task_name->CurrentValue;
		$this->task_name->ViewCustomAttributes = "";

		// task_from
		$this->task_from->ViewValue = $this->task_from->CurrentValue;
		$this->task_from->ViewValue = ew_FormatDateTime($this->task_from->ViewValue, 7);
		$this->task_from->ViewCustomAttributes = "";

		// task_to
		$this->task_to->ViewValue = $this->task_to->CurrentValue;
		$this->task_to->ViewValue = ew_FormatDateTime($this->task_to->ViewValue, 7);
		$this->task_to->ViewCustomAttributes = "";

		// task_employee_id
		if (strval($this->task_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_employee_id->ViewValue = $this->task_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_employee_id->ViewValue = $this->task_employee_id->CurrentValue;
			}
		} else {
			$this->task_employee_id->ViewValue = NULL;
		}
		$this->task_employee_id->ViewCustomAttributes = "";

		// task_coordinator_id
		if (strval($this->task_coordinator_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->CurrentValue;
			}
		} else {
			$this->task_coordinator_id->ViewValue = NULL;
		}
		$this->task_coordinator_id->ViewCustomAttributes = "";

		// task_object
		$this->task_object->ViewValue = $this->task_object->CurrentValue;
		$this->task_object->ViewCustomAttributes = "";

		// task_status_id
		if (strval($this->task_status_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_status_id->ViewValue = $this->task_status_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_status_id->ViewValue = $this->task_status_id->CurrentValue;
			}
		} else {
			$this->task_status_id->ViewValue = NULL;
		}
		$this->task_status_id->ViewCustomAttributes = "";

		// task_hours_planned
		$this->task_hours_planned->ViewValue = $this->task_hours_planned->CurrentValue;
		$this->task_hours_planned->ViewCustomAttributes = "";

		// task_hours_actual
		$this->task_hours_actual->ViewValue = $this->task_hours_actual->CurrentValue;
		$this->task_hours_actual->ViewCustomAttributes = "";

		// task_description
		$this->task_description->ViewValue = $this->task_description->CurrentValue;
		$this->task_description->ViewCustomAttributes = "";

		// task_key
		$this->task_key->ViewValue = $this->task_key->CurrentValue;
		$this->task_key->ViewCustomAttributes = "";

			// task_id
			$this->task_id->LinkCustomAttributes = "";
			$this->task_id->HrefValue = "";
			$this->task_id->TooltipValue = "";

			// task_project_id
			$this->task_project_id->LinkCustomAttributes = "";
			$this->task_project_id->HrefValue = "";
			$this->task_project_id->TooltipValue = "";

			// task_plan_id
			$this->task_plan_id->LinkCustomAttributes = "";
			$this->task_plan_id->HrefValue = "";
			$this->task_plan_id->TooltipValue = "";

			// task_lab_id
			$this->task_lab_id->LinkCustomAttributes = "";
			$this->task_lab_id->HrefValue = "";
			$this->task_lab_id->TooltipValue = "";

			// task_code
			$this->task_code->LinkCustomAttributes = "";
			$this->task_code->HrefValue = "";
			$this->task_code->TooltipValue = "";

			// task_name
			$this->task_name->LinkCustomAttributes = "";
			$this->task_name->HrefValue = "";
			$this->task_name->TooltipValue = "";

			// task_from
			$this->task_from->LinkCustomAttributes = "";
			$this->task_from->HrefValue = "";
			$this->task_from->TooltipValue = "";

			// task_to
			$this->task_to->LinkCustomAttributes = "";
			$this->task_to->HrefValue = "";
			$this->task_to->TooltipValue = "";

			// task_employee_id
			$this->task_employee_id->LinkCustomAttributes = "";
			$this->task_employee_id->HrefValue = "";
			$this->task_employee_id->TooltipValue = "";

			// task_coordinator_id
			$this->task_coordinator_id->LinkCustomAttributes = "";
			$this->task_coordinator_id->HrefValue = "";
			$this->task_coordinator_id->TooltipValue = "";

			// task_object
			$this->task_object->LinkCustomAttributes = "";
			$this->task_object->HrefValue = "";
			$this->task_object->TooltipValue = "";

			// task_status_id
			$this->task_status_id->LinkCustomAttributes = "";
			$this->task_status_id->HrefValue = "";
			$this->task_status_id->TooltipValue = "";

			// task_hours_planned
			$this->task_hours_planned->LinkCustomAttributes = "";
			$this->task_hours_planned->HrefValue = "";
			$this->task_hours_planned->TooltipValue = "";

			// task_hours_actual
			$this->task_hours_actual->LinkCustomAttributes = "";
			$this->task_hours_actual->HrefValue = "";
			$this->task_hours_actual->TooltipValue = "";

			// task_description
			$this->task_description->LinkCustomAttributes = "";
			$this->task_description->HrefValue = "";
			$this->task_description->TooltipValue = "";

			// task_key
			$this->task_key->LinkCustomAttributes = "";
			$this->task_key->HrefValue = "";
			$this->task_key->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// task_id
			$this->task_id->EditAttrs["class"] = "form-control";
			$this->task_id->EditCustomAttributes = "";
			$this->task_id->EditValue = ew_HtmlEncode($this->task_id->AdvancedSearch->SearchValue);
			$this->task_id->PlaceHolder = ew_RemoveHtml($this->task_id->FldCaption());

			// task_project_id
			$this->task_project_id->EditAttrs["class"] = "form-control";
			$this->task_project_id->EditCustomAttributes = "";
			if (trim(strval($this->task_project_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_project_id->EditValue = $arwrk;

			// task_plan_id
			$this->task_plan_id->EditAttrs["class"] = "form-control";
			$this->task_plan_id->EditCustomAttributes = "";
			if (trim(strval($this->task_plan_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `plan_project_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_plan_id->EditValue = $arwrk;

			// task_lab_id
			$this->task_lab_id->EditAttrs["class"] = "form-control";
			$this->task_lab_id->EditCustomAttributes = "";
			if (trim(strval($this->task_lab_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_lab_id->EditValue = $arwrk;

			// task_code
			$this->task_code->EditAttrs["class"] = "form-control";
			$this->task_code->EditCustomAttributes = "";
			$this->task_code->EditValue = ew_HtmlEncode($this->task_code->AdvancedSearch->SearchValue);
			$this->task_code->PlaceHolder = ew_RemoveHtml($this->task_code->FldCaption());

			// task_name
			$this->task_name->EditAttrs["class"] = "form-control";
			$this->task_name->EditCustomAttributes = "";
			$this->task_name->EditValue = ew_HtmlEncode($this->task_name->AdvancedSearch->SearchValue);
			$this->task_name->PlaceHolder = ew_RemoveHtml($this->task_name->FldCaption());

			// task_from
			$this->task_from->EditAttrs["class"] = "form-control";
			$this->task_from->EditCustomAttributes = "";
			$this->task_from->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->task_from->AdvancedSearch->SearchValue, 7), 7));
			$this->task_from->PlaceHolder = ew_RemoveHtml($this->task_from->FldCaption());
			$this->task_from->EditAttrs["class"] = "form-control";
			$this->task_from->EditCustomAttributes = "";
			$this->task_from->EditValue2 = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->task_from->AdvancedSearch->SearchValue2, 7), 7));
			$this->task_from->PlaceHolder = ew_RemoveHtml($this->task_from->FldCaption());

			// task_to
			$this->task_to->EditAttrs["class"] = "form-control";
			$this->task_to->EditCustomAttributes = "";
			$this->task_to->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->task_to->AdvancedSearch->SearchValue, 7), 7));
			$this->task_to->PlaceHolder = ew_RemoveHtml($this->task_to->FldCaption());
			$this->task_to->EditAttrs["class"] = "form-control";
			$this->task_to->EditCustomAttributes = "";
			$this->task_to->EditValue2 = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->task_to->AdvancedSearch->SearchValue2, 7), 7));
			$this->task_to->PlaceHolder = ew_RemoveHtml($this->task_to->FldCaption());

			// task_employee_id
			$this->task_employee_id->EditAttrs["class"] = "form-control";
			$this->task_employee_id->EditCustomAttributes = "";
			if (trim(strval($this->task_employee_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `employee_lab_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_employee_id->EditValue = $arwrk;

			// task_coordinator_id
			$this->task_coordinator_id->EditAttrs["class"] = "form-control";
			$this->task_coordinator_id->EditCustomAttributes = "";
			if (trim(strval($this->task_coordinator_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_coordinator_id->EditValue = $arwrk;

			// task_object
			$this->task_object->EditAttrs["class"] = "form-control";
			$this->task_object->EditCustomAttributes = "";
			$this->task_object->EditValue = ew_HtmlEncode($this->task_object->AdvancedSearch->SearchValue);
			$this->task_object->PlaceHolder = ew_RemoveHtml($this->task_object->FldCaption());

			// task_status_id
			$this->task_status_id->EditAttrs["class"] = "form-control";
			$this->task_status_id->EditCustomAttributes = "";
			if (trim(strval($this->task_status_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `task_statuses`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_status_id->EditValue = $arwrk;

			// task_hours_planned
			$this->task_hours_planned->EditAttrs["class"] = "form-control";
			$this->task_hours_planned->EditCustomAttributes = "";
			$this->task_hours_planned->EditValue = ew_HtmlEncode($this->task_hours_planned->AdvancedSearch->SearchValue);
			$this->task_hours_planned->PlaceHolder = ew_RemoveHtml($this->task_hours_planned->FldCaption());
			$this->task_hours_planned->EditAttrs["class"] = "form-control";
			$this->task_hours_planned->EditCustomAttributes = "";
			$this->task_hours_planned->EditValue2 = ew_HtmlEncode($this->task_hours_planned->AdvancedSearch->SearchValue2);
			$this->task_hours_planned->PlaceHolder = ew_RemoveHtml($this->task_hours_planned->FldCaption());

			// task_hours_actual
			$this->task_hours_actual->EditAttrs["class"] = "form-control";
			$this->task_hours_actual->EditCustomAttributes = "";
			$this->task_hours_actual->EditValue = ew_HtmlEncode($this->task_hours_actual->AdvancedSearch->SearchValue);
			$this->task_hours_actual->PlaceHolder = ew_RemoveHtml($this->task_hours_actual->FldCaption());
			$this->task_hours_actual->EditAttrs["class"] = "form-control";
			$this->task_hours_actual->EditCustomAttributes = "";
			$this->task_hours_actual->EditValue2 = ew_HtmlEncode($this->task_hours_actual->AdvancedSearch->SearchValue2);
			$this->task_hours_actual->PlaceHolder = ew_RemoveHtml($this->task_hours_actual->FldCaption());

			// task_description
			$this->task_description->EditAttrs["class"] = "form-control";
			$this->task_description->EditCustomAttributes = "";
			$this->task_description->EditValue = ew_HtmlEncode($this->task_description->AdvancedSearch->SearchValue);
			$this->task_description->PlaceHolder = ew_RemoveHtml($this->task_description->FldCaption());

			// task_key
			$this->task_key->EditAttrs["class"] = "form-control";
			$this->task_key->EditCustomAttributes = "";
			$this->task_key->EditValue = ew_HtmlEncode($this->task_key->AdvancedSearch->SearchValue);
			$this->task_key->PlaceHolder = ew_RemoveHtml($this->task_key->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->task_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->task_id->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->task_from->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->task_from->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->task_from->AdvancedSearch->SearchValue2)) {
			ew_AddMessage($gsSearchError, $this->task_from->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->task_to->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->task_to->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->task_to->AdvancedSearch->SearchValue2)) {
			ew_AddMessage($gsSearchError, $this->task_to->FldErrMsg());
		}
		if (!ew_CheckNumber($this->task_hours_planned->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->task_hours_planned->FldErrMsg());
		}
		if (!ew_CheckNumber($this->task_hours_planned->AdvancedSearch->SearchValue2)) {
			ew_AddMessage($gsSearchError, $this->task_hours_planned->FldErrMsg());
		}
		if (!ew_CheckNumber($this->task_hours_actual->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->task_hours_actual->FldErrMsg());
		}
		if (!ew_CheckNumber($this->task_hours_actual->AdvancedSearch->SearchValue2)) {
			ew_AddMessage($gsSearchError, $this->task_hours_actual->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->task_id->AdvancedSearch->Load();
		$this->task_project_id->AdvancedSearch->Load();
		$this->task_plan_id->AdvancedSearch->Load();
		$this->task_lab_id->AdvancedSearch->Load();
		$this->task_code->AdvancedSearch->Load();
		$this->task_name->AdvancedSearch->Load();
		$this->task_from->AdvancedSearch->Load();
		$this->task_to->AdvancedSearch->Load();
		$this->task_employee_id->AdvancedSearch->Load();
		$this->task_coordinator_id->AdvancedSearch->Load();
		$this->task_object->AdvancedSearch->Load();
		$this->task_status_id->AdvancedSearch->Load();
		$this->task_hours_planned->AdvancedSearch->Load();
		$this->task_hours_actual->AdvancedSearch->Load();
		$this->task_description->AdvancedSearch->Load();
		$this->task_key->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("e_taskslist.php"), "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($e_tasks_search)) $e_tasks_search = new ce_tasks_search();

// Page init
$e_tasks_search->Page_Init();

// Page main
$e_tasks_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$e_tasks_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($e_tasks_search->IsModal) { ?>
var CurrentAdvancedSearchForm = fe_taskssearch = new ew_Form("fe_taskssearch", "search");
<?php } else { ?>
var CurrentForm = fe_taskssearch = new ew_Form("fe_taskssearch", "search");
<?php } ?>

// Form_CustomValidate event
fe_taskssearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fe_taskssearch.ValidateRequired = true;
<?php } else { ?>
fe_taskssearch.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fe_taskssearch.Lists["x_task_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_task_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fe_taskssearch.Lists["x_task_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_task_project_id"],"ChildFields":[],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
fe_taskssearch.Lists["x_task_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_task_employee_id"],"FilterFields":[],"Options":[],"Template":""};
fe_taskssearch.Lists["x_task_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":["x_task_lab_id"],"ChildFields":[],"FilterFields":["x_employee_lab_id"],"Options":[],"Template":""};
fe_taskssearch.Lists["x_task_coordinator_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_taskssearch.Lists["x_task_status_id"] = {"LinkField":"x_task_status_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_status_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
// Validate function for search

fe_taskssearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_task_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($e_tasks->task_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_task_from");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($e_tasks->task_from->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_task_to");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($e_tasks->task_to->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_task_hours_planned");
	if (elm && !ew_CheckNumber(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($e_tasks->task_hours_planned->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_task_hours_actual");
	if (elm && !ew_CheckNumber(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($e_tasks->task_hours_actual->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$e_tasks_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $e_tasks_search->ShowPageHeader(); ?>
<?php
$e_tasks_search->ShowMessage();
?>
<form name="fe_taskssearch" id="fe_taskssearch" class="<?php echo $e_tasks_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($e_tasks_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $e_tasks_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="e_tasks">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($e_tasks_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($e_tasks->task_id->Visible) { // task_id ?>
	<div id="r_task_id" class="form-group">
		<label for="x_task_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_id"><?php echo $e_tasks->task_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_id" id="z_task_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_id">
<input type="text" data-table="e_tasks" data-field="x_task_id" name="x_task_id" id="x_task_id" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_id->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_id->EditValue ?>"<?php echo $e_tasks->task_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_project_id->Visible) { // task_project_id ?>
	<div id="r_task_project_id" class="form-group">
		<label for="x_task_project_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_project_id"><?php echo $e_tasks->task_project_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_project_id" id="z_task_project_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_project_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_project_id">
<?php $e_tasks->task_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$e_tasks->task_project_id->EditAttrs["onchange"]; ?>
<select data-table="e_tasks" data-field="x_task_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_project_id->DisplayValueSeparator) ? json_encode($e_tasks->task_project_id->DisplayValueSeparator) : $e_tasks->task_project_id->DisplayValueSeparator) ?>" id="x_task_project_id" name="x_task_project_id"<?php echo $e_tasks->task_project_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_project_id->EditValue)) {
	$arwrk = $e_tasks->task_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_project_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_project_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$e_tasks->task_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_project_id" id="s_x_task_project_id" value="<?php echo $e_tasks->task_project_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_plan_id->Visible) { // task_plan_id ?>
	<div id="r_task_plan_id" class="form-group">
		<label for="x_task_plan_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_plan_id"><?php echo $e_tasks->task_plan_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_plan_id" id="z_task_plan_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_plan_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_plan_id">
<select data-table="e_tasks" data-field="x_task_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_plan_id->DisplayValueSeparator) ? json_encode($e_tasks->task_plan_id->DisplayValueSeparator) : $e_tasks->task_plan_id->DisplayValueSeparator) ?>" id="x_task_plan_id" name="x_task_plan_id"<?php echo $e_tasks->task_plan_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_plan_id->EditValue)) {
	$arwrk = $e_tasks->task_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_plan_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_plan_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$e_tasks->task_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$e_tasks->task_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $e_tasks->task_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_plan_id" id="s_x_task_plan_id" value="<?php echo $e_tasks->task_plan_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_lab_id->Visible) { // task_lab_id ?>
	<div id="r_task_lab_id" class="form-group">
		<label for="x_task_lab_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_lab_id"><?php echo $e_tasks->task_lab_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_lab_id" id="z_task_lab_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_lab_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_lab_id">
<?php $e_tasks->task_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$e_tasks->task_lab_id->EditAttrs["onchange"]; ?>
<select data-table="e_tasks" data-field="x_task_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_lab_id->DisplayValueSeparator) ? json_encode($e_tasks->task_lab_id->DisplayValueSeparator) : $e_tasks->task_lab_id->DisplayValueSeparator) ?>" id="x_task_lab_id" name="x_task_lab_id"<?php echo $e_tasks->task_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_lab_id->EditValue)) {
	$arwrk = $e_tasks->task_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_lab_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_lab_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_tasks->task_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_lab_id" id="s_x_task_lab_id" value="<?php echo $e_tasks->task_lab_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_code->Visible) { // task_code ?>
	<div id="r_task_code" class="form-group">
		<label for="x_task_code" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_code"><?php echo $e_tasks->task_code->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_task_code" id="z_task_code" value="LIKE"></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_code->CellAttributes() ?>>
			<span id="el_e_tasks_task_code">
<input type="text" data-table="e_tasks" data-field="x_task_code" name="x_task_code" id="x_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_code->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_code->EditValue ?>"<?php echo $e_tasks->task_code->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_name->Visible) { // task_name ?>
	<div id="r_task_name" class="form-group">
		<label for="x_task_name" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_name"><?php echo $e_tasks->task_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_task_name" id="z_task_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_name->CellAttributes() ?>>
			<span id="el_e_tasks_task_name">
<input type="text" data-table="e_tasks" data-field="x_task_name" name="x_task_name" id="x_task_name" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_name->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_name->EditValue ?>"<?php echo $e_tasks->task_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_from->Visible) { // task_from ?>
	<div id="r_task_from" class="form-group">
		<label for="x_task_from" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_from"><?php echo $e_tasks->task_from->FldCaption() ?></span>	
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_from->CellAttributes() ?>>
		<span class="ewSearchOperator"><select name="z_task_from" id="z_task_from" class="form-control" onchange="ewForms(this).SrchOprChanged(this);"><option value="="<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "=") ? " selected" : "" ?> ><?php echo $Language->Phrase("EQUAL") ?></option><option value="<>"<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "<>") ? " selected" : "" ?> ><?php echo $Language->Phrase("<>") ?></option><option value="<"<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "<") ? " selected" : "" ?> ><?php echo $Language->Phrase("<") ?></option><option value="<="<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "<=") ? " selected" : "" ?> ><?php echo $Language->Phrase("<=") ?></option><option value=">"<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == ">") ? " selected" : "" ?> ><?php echo $Language->Phrase(">") ?></option><option value=">="<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == ">=") ? " selected" : "" ?> ><?php echo $Language->Phrase(">=") ?></option><option value="IS NULL"<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "IS NULL") ? " selected" : "" ?> ><?php echo $Language->Phrase("IS NULL") ?></option><option value="IS NOT NULL"<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "IS NOT NULL") ? " selected" : "" ?> ><?php echo $Language->Phrase("IS NOT NULL") ?></option><option value="BETWEEN"<?php echo ($e_tasks->task_from->AdvancedSearch->SearchOperator == "BETWEEN") ? " selected" : "" ?> ><?php echo $Language->Phrase("BETWEEN") ?></option></select></span>
			<span id="el_e_tasks_task_from">
<input type="text" data-table="e_tasks" data-field="x_task_from" data-format="7" name="x_task_from" id="x_task_from" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_from->EditValue ?>"<?php echo $e_tasks->task_from->EditAttributes() ?>>
<?php if (!$e_tasks->task_from->ReadOnly && !$e_tasks->task_from->Disabled && !isset($e_tasks->task_from->EditAttrs["readonly"]) && !isset($e_tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_taskssearch", "x_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
			<span class="ewSearchCond btw1_task_from" style="display: none">&nbsp;<?php echo $Language->Phrase("AND") ?>&nbsp;</span>
			<span id="e2_e_tasks_task_from" class="btw1_task_from" style="display: none">
<input type="text" data-table="e_tasks" data-field="x_task_from" data-format="7" name="y_task_from" id="y_task_from" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_from->EditValue2 ?>"<?php echo $e_tasks->task_from->EditAttributes() ?>>
<?php if (!$e_tasks->task_from->ReadOnly && !$e_tasks->task_from->Disabled && !isset($e_tasks->task_from->EditAttrs["readonly"]) && !isset($e_tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_taskssearch", "y_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_to->Visible) { // task_to ?>
	<div id="r_task_to" class="form-group">
		<label for="x_task_to" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_to"><?php echo $e_tasks->task_to->FldCaption() ?></span>	
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_to->CellAttributes() ?>>
		<span class="ewSearchOperator"><select name="z_task_to" id="z_task_to" class="form-control" onchange="ewForms(this).SrchOprChanged(this);"><option value="="<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "=") ? " selected" : "" ?> ><?php echo $Language->Phrase("EQUAL") ?></option><option value="<>"<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "<>") ? " selected" : "" ?> ><?php echo $Language->Phrase("<>") ?></option><option value="<"<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "<") ? " selected" : "" ?> ><?php echo $Language->Phrase("<") ?></option><option value="<="<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "<=") ? " selected" : "" ?> ><?php echo $Language->Phrase("<=") ?></option><option value=">"<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == ">") ? " selected" : "" ?> ><?php echo $Language->Phrase(">") ?></option><option value=">="<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == ">=") ? " selected" : "" ?> ><?php echo $Language->Phrase(">=") ?></option><option value="IS NULL"<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "IS NULL") ? " selected" : "" ?> ><?php echo $Language->Phrase("IS NULL") ?></option><option value="IS NOT NULL"<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "IS NOT NULL") ? " selected" : "" ?> ><?php echo $Language->Phrase("IS NOT NULL") ?></option><option value="BETWEEN"<?php echo ($e_tasks->task_to->AdvancedSearch->SearchOperator == "BETWEEN") ? " selected" : "" ?> ><?php echo $Language->Phrase("BETWEEN") ?></option></select></span>
			<span id="el_e_tasks_task_to">
<input type="text" data-table="e_tasks" data-field="x_task_to" data-format="7" name="x_task_to" id="x_task_to" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_to->EditValue ?>"<?php echo $e_tasks->task_to->EditAttributes() ?>>
<?php if (!$e_tasks->task_to->ReadOnly && !$e_tasks->task_to->Disabled && !isset($e_tasks->task_to->EditAttrs["readonly"]) && !isset($e_tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_taskssearch", "x_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
			<span class="ewSearchCond btw1_task_to" style="display: none">&nbsp;<?php echo $Language->Phrase("AND") ?>&nbsp;</span>
			<span id="e2_e_tasks_task_to" class="btw1_task_to" style="display: none">
<input type="text" data-table="e_tasks" data-field="x_task_to" data-format="7" name="y_task_to" id="y_task_to" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_to->EditValue2 ?>"<?php echo $e_tasks->task_to->EditAttributes() ?>>
<?php if (!$e_tasks->task_to->ReadOnly && !$e_tasks->task_to->Disabled && !isset($e_tasks->task_to->EditAttrs["readonly"]) && !isset($e_tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_taskssearch", "y_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_employee_id->Visible) { // task_employee_id ?>
	<div id="r_task_employee_id" class="form-group">
		<label for="x_task_employee_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_employee_id"><?php echo $e_tasks->task_employee_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_employee_id" id="z_task_employee_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_employee_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_employee_id">
<select data-table="e_tasks" data-field="x_task_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_employee_id->DisplayValueSeparator) ? json_encode($e_tasks->task_employee_id->DisplayValueSeparator) : $e_tasks->task_employee_id->DisplayValueSeparator) ?>" id="x_task_employee_id" name="x_task_employee_id"<?php echo $e_tasks->task_employee_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_employee_id->EditValue)) {
	$arwrk = $e_tasks->task_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_employee_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_employee_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "{filter}";
$e_tasks->task_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$e_tasks->task_employee_id->LookupFilters += array("f1" => "`employee_lab_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $e_tasks->task_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_employee_id" id="s_x_task_employee_id" value="<?php echo $e_tasks->task_employee_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
	<div id="r_task_coordinator_id" class="form-group">
		<label for="x_task_coordinator_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_coordinator_id"><?php echo $e_tasks->task_coordinator_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_coordinator_id" id="z_task_coordinator_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_coordinator_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_coordinator_id">
<select data-table="e_tasks" data-field="x_task_coordinator_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_coordinator_id->DisplayValueSeparator) ? json_encode($e_tasks->task_coordinator_id->DisplayValueSeparator) : $e_tasks->task_coordinator_id->DisplayValueSeparator) ?>" id="x_task_coordinator_id" name="x_task_coordinator_id"<?php echo $e_tasks->task_coordinator_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_coordinator_id->EditValue)) {
	$arwrk = $e_tasks->task_coordinator_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_coordinator_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_coordinator_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_coordinator_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_coordinator_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$e_tasks->task_coordinator_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_coordinator_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $e_tasks->task_coordinator_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_coordinator_id" id="s_x_task_coordinator_id" value="<?php echo $e_tasks->task_coordinator_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_object->Visible) { // task_object ?>
	<div id="r_task_object" class="form-group">
		<label for="x_task_object" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_object"><?php echo $e_tasks->task_object->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_task_object" id="z_task_object" value="LIKE"></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_object->CellAttributes() ?>>
			<span id="el_e_tasks_task_object">
<input type="text" data-table="e_tasks" data-field="x_task_object" name="x_task_object" id="x_task_object" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_object->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_object->EditValue ?>"<?php echo $e_tasks->task_object->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_status_id->Visible) { // task_status_id ?>
	<div id="r_task_status_id" class="form-group">
		<label for="x_task_status_id" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_status_id"><?php echo $e_tasks->task_status_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_task_status_id" id="z_task_status_id" value="="></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_status_id->CellAttributes() ?>>
			<span id="el_e_tasks_task_status_id">
<select data-table="e_tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_status_id->DisplayValueSeparator) ? json_encode($e_tasks->task_status_id->DisplayValueSeparator) : $e_tasks->task_status_id->DisplayValueSeparator) ?>" id="x_task_status_id" name="x_task_status_id"<?php echo $e_tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_status_id->EditValue)) {
	$arwrk = $e_tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_status_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$e_tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_status_id" id="s_x_task_status_id" value="<?php echo $e_tasks->task_status_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_hours_planned->Visible) { // task_hours_planned ?>
	<div id="r_task_hours_planned" class="form-group">
		<label for="x_task_hours_planned" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_hours_planned"><?php echo $e_tasks->task_hours_planned->FldCaption() ?></span>	
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_hours_planned->CellAttributes() ?>>
		<span class="ewSearchOperator"><select name="z_task_hours_planned" id="z_task_hours_planned" class="form-control" onchange="ewForms(this).SrchOprChanged(this);"><option value="="<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == "=") ? " selected" : "" ?> ><?php echo $Language->Phrase("EQUAL") ?></option><option value="<>"<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == "<>") ? " selected" : "" ?> ><?php echo $Language->Phrase("<>") ?></option><option value="<"<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == "<") ? " selected" : "" ?> ><?php echo $Language->Phrase("<") ?></option><option value="<="<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == "<=") ? " selected" : "" ?> ><?php echo $Language->Phrase("<=") ?></option><option value=">"<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == ">") ? " selected" : "" ?> ><?php echo $Language->Phrase(">") ?></option><option value=">="<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == ">=") ? " selected" : "" ?> ><?php echo $Language->Phrase(">=") ?></option><option value="BETWEEN"<?php echo ($e_tasks->task_hours_planned->AdvancedSearch->SearchOperator == "BETWEEN") ? " selected" : "" ?> ><?php echo $Language->Phrase("BETWEEN") ?></option></select></span>
			<span id="el_e_tasks_task_hours_planned">
<input type="text" data-table="e_tasks" data-field="x_task_hours_planned" name="x_task_hours_planned" id="x_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_planned->EditValue ?>"<?php echo $e_tasks->task_hours_planned->EditAttributes() ?>>
</span>
			<span class="ewSearchCond btw1_task_hours_planned" style="display: none">&nbsp;<?php echo $Language->Phrase("AND") ?>&nbsp;</span>
			<span id="e2_e_tasks_task_hours_planned" class="btw1_task_hours_planned" style="display: none">
<input type="text" data-table="e_tasks" data-field="x_task_hours_planned" name="y_task_hours_planned" id="y_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_planned->EditValue2 ?>"<?php echo $e_tasks->task_hours_planned->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_hours_actual->Visible) { // task_hours_actual ?>
	<div id="r_task_hours_actual" class="form-group">
		<label for="x_task_hours_actual" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_hours_actual"><?php echo $e_tasks->task_hours_actual->FldCaption() ?></span>	
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_hours_actual->CellAttributes() ?>>
		<span class="ewSearchOperator"><select name="z_task_hours_actual" id="z_task_hours_actual" class="form-control" onchange="ewForms(this).SrchOprChanged(this);"><option value="="<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == "=") ? " selected" : "" ?> ><?php echo $Language->Phrase("EQUAL") ?></option><option value="<>"<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == "<>") ? " selected" : "" ?> ><?php echo $Language->Phrase("<>") ?></option><option value="<"<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == "<") ? " selected" : "" ?> ><?php echo $Language->Phrase("<") ?></option><option value="<="<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == "<=") ? " selected" : "" ?> ><?php echo $Language->Phrase("<=") ?></option><option value=">"<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == ">") ? " selected" : "" ?> ><?php echo $Language->Phrase(">") ?></option><option value=">="<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == ">=") ? " selected" : "" ?> ><?php echo $Language->Phrase(">=") ?></option><option value="BETWEEN"<?php echo ($e_tasks->task_hours_actual->AdvancedSearch->SearchOperator == "BETWEEN") ? " selected" : "" ?> ><?php echo $Language->Phrase("BETWEEN") ?></option></select></span>
			<span id="el_e_tasks_task_hours_actual">
<input type="text" data-table="e_tasks" data-field="x_task_hours_actual" name="x_task_hours_actual" id="x_task_hours_actual" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_actual->EditValue ?>"<?php echo $e_tasks->task_hours_actual->EditAttributes() ?>>
</span>
			<span class="ewSearchCond btw1_task_hours_actual" style="display: none">&nbsp;<?php echo $Language->Phrase("AND") ?>&nbsp;</span>
			<span id="e2_e_tasks_task_hours_actual" class="btw1_task_hours_actual" style="display: none">
<input type="text" data-table="e_tasks" data-field="x_task_hours_actual" name="y_task_hours_actual" id="y_task_hours_actual" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_actual->EditValue2 ?>"<?php echo $e_tasks->task_hours_actual->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_description->Visible) { // task_description ?>
	<div id="r_task_description" class="form-group">
		<label for="x_task_description" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_description"><?php echo $e_tasks->task_description->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_task_description" id="z_task_description" value="LIKE"></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_description->CellAttributes() ?>>
			<span id="el_e_tasks_task_description">
<input type="text" data-table="e_tasks" data-field="x_task_description" name="x_task_description" id="x_task_description" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_description->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_description->EditValue ?>"<?php echo $e_tasks->task_description->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_key->Visible) { // task_key ?>
	<div id="r_task_key" class="form-group">
		<label for="x_task_key" class="<?php echo $e_tasks_search->SearchLabelClass ?>"><span id="elh_e_tasks_task_key"><?php echo $e_tasks->task_key->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_task_key" id="z_task_key" value="LIKE"></p>
		</label>
		<div class="<?php echo $e_tasks_search->SearchRightColumnClass ?>"><div<?php echo $e_tasks->task_key->CellAttributes() ?>>
			<span id="el_e_tasks_task_key">
<input type="text" data-table="e_tasks" data-field="x_task_key" name="x_task_key" id="x_task_key" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_key->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_key->EditValue ?>"<?php echo $e_tasks->task_key->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
<?php if (!$e_tasks_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fe_taskssearch.Init();
</script>
<?php
$e_tasks_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$e_tasks_search->Page_Terminate();
?>
