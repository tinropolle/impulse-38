<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$employees_add = NULL; // Initialize page object first

class cemployees_add extends cemployees {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'employees';

	// Page object name
	var $PageObjName = 'employees_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = TRUE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (employees)
		if (!isset($GLOBALS["employees"]) || get_class($GLOBALS["employees"]) == "cemployees") {
			$GLOBALS["employees"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["employees"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'employees', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("employeeslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate(ew_GetUrl("employeeslist.php"));
			}
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $employees;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($employees);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["employee_id"] != "") {
				$this->employee_id->setQueryStringValue($_GET["employee_id"]);
				$this->setKey("employee_id", $this->employee_id->CurrentValue); // Set up key
			} else {
				$this->setKey("employee_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		} else {
			if ($this->CurrentAction == "I") // Load default values for blank record
				$this->LoadDefaultValues();
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("employeeslist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "employeeslist.php")
						$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
					elseif (ew_GetPageName($sReturnUrl) == "employeesview.php")
						$sReturnUrl = $this->GetViewUrl(); // View page, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->employee_login->CurrentValue = NULL;
		$this->employee_login->OldValue = $this->employee_login->CurrentValue;
		$this->employee_password->CurrentValue = NULL;
		$this->employee_password->OldValue = $this->employee_password->CurrentValue;
		$this->employee_level_id->CurrentValue = 5;
		$this->employee_first_name->CurrentValue = NULL;
		$this->employee_first_name->OldValue = $this->employee_first_name->CurrentValue;
		$this->employee_last_name->CurrentValue = NULL;
		$this->employee_last_name->OldValue = $this->employee_last_name->CurrentValue;
		$this->employee_telephone->CurrentValue = NULL;
		$this->employee_telephone->OldValue = $this->employee_telephone->CurrentValue;
		$this->employee_lab_id->CurrentValue = NULL;
		$this->employee_lab_id->OldValue = $this->employee_lab_id->CurrentValue;
		$this->employee_position_id->CurrentValue = NULL;
		$this->employee_position_id->OldValue = $this->employee_position_id->CurrentValue;
		$this->employee_salary->CurrentValue = NULL;
		$this->employee_salary->OldValue = $this->employee_salary->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->employee_login->FldIsDetailKey) {
			$this->employee_login->setFormValue($objForm->GetValue("x_employee_login"));
		}
		if (!$this->employee_password->FldIsDetailKey) {
			$this->employee_password->setFormValue($objForm->GetValue("x_employee_password"));
		}
		if (!$this->employee_level_id->FldIsDetailKey) {
			$this->employee_level_id->setFormValue($objForm->GetValue("x_employee_level_id"));
		}
		if (!$this->employee_first_name->FldIsDetailKey) {
			$this->employee_first_name->setFormValue($objForm->GetValue("x_employee_first_name"));
		}
		if (!$this->employee_last_name->FldIsDetailKey) {
			$this->employee_last_name->setFormValue($objForm->GetValue("x_employee_last_name"));
		}
		if (!$this->employee_telephone->FldIsDetailKey) {
			$this->employee_telephone->setFormValue($objForm->GetValue("x_employee_telephone"));
		}
		if (!$this->employee_lab_id->FldIsDetailKey) {
			$this->employee_lab_id->setFormValue($objForm->GetValue("x_employee_lab_id"));
		}
		if (!$this->employee_position_id->FldIsDetailKey) {
			$this->employee_position_id->setFormValue($objForm->GetValue("x_employee_position_id"));
		}
		if (!$this->employee_salary->FldIsDetailKey) {
			$this->employee_salary->setFormValue($objForm->GetValue("x_employee_salary"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->employee_login->CurrentValue = $this->employee_login->FormValue;
		$this->employee_password->CurrentValue = $this->employee_password->FormValue;
		$this->employee_level_id->CurrentValue = $this->employee_level_id->FormValue;
		$this->employee_first_name->CurrentValue = $this->employee_first_name->FormValue;
		$this->employee_last_name->CurrentValue = $this->employee_last_name->FormValue;
		$this->employee_telephone->CurrentValue = $this->employee_telephone->FormValue;
		$this->employee_lab_id->CurrentValue = $this->employee_lab_id->FormValue;
		$this->employee_position_id->CurrentValue = $this->employee_position_id->FormValue;
		$this->employee_salary->CurrentValue = $this->employee_salary->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}

		// Check if valid user id
		if ($res) {
			$res = $this->ShowOptionLink('add');
			if (!$res) {
				$sUserIdMsg = ew_DeniedMsg();
				$this->setFailureMessage($sUserIdMsg);
			}
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->employee_id->setDbValue($rs->fields('employee_id'));
		$this->employee_login->setDbValue($rs->fields('employee_login'));
		$this->employee_password->setDbValue($rs->fields('employee_password'));
		$this->employee_level_id->setDbValue($rs->fields('employee_level_id'));
		$this->employee_first_name->setDbValue($rs->fields('employee_first_name'));
		$this->employee_last_name->setDbValue($rs->fields('employee_last_name'));
		$this->employee_telephone->setDbValue($rs->fields('employee_telephone'));
		$this->employee_lab_id->setDbValue($rs->fields('employee_lab_id'));
		$this->employee_position_id->setDbValue($rs->fields('employee_position_id'));
		$this->employee_salary->setDbValue($rs->fields('employee_salary'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->employee_id->DbValue = $row['employee_id'];
		$this->employee_login->DbValue = $row['employee_login'];
		$this->employee_password->DbValue = $row['employee_password'];
		$this->employee_level_id->DbValue = $row['employee_level_id'];
		$this->employee_first_name->DbValue = $row['employee_first_name'];
		$this->employee_last_name->DbValue = $row['employee_last_name'];
		$this->employee_telephone->DbValue = $row['employee_telephone'];
		$this->employee_lab_id->DbValue = $row['employee_lab_id'];
		$this->employee_position_id->DbValue = $row['employee_position_id'];
		$this->employee_salary->DbValue = $row['employee_salary'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("employee_id")) <> "")
			$this->employee_id->CurrentValue = $this->getKey("employee_id"); // employee_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// employee_id
		// employee_login
		// employee_password
		// employee_level_id
		// employee_first_name
		// employee_last_name
		// employee_telephone
		// employee_lab_id
		// employee_position_id
		// employee_salary

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// employee_id
		$this->employee_id->ViewValue = $this->employee_id->CurrentValue;
		$this->employee_id->ViewCustomAttributes = "";

		// employee_login
		$this->employee_login->ViewValue = $this->employee_login->CurrentValue;
		$this->employee_login->ViewCustomAttributes = "";

		// employee_password
		$this->employee_password->ViewValue = $this->employee_password->CurrentValue;
		$this->employee_password->ViewCustomAttributes = "";

		// employee_level_id
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->employee_level_id->CurrentValue) <> "") {
			$this->employee_level_id->ViewValue = $this->employee_level_id->OptionCaption($this->employee_level_id->CurrentValue);
		} else {
			$this->employee_level_id->ViewValue = NULL;
		}
		} else {
			$this->employee_level_id->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->employee_level_id->ViewCustomAttributes = "";

		// employee_first_name
		$this->employee_first_name->ViewValue = $this->employee_first_name->CurrentValue;
		$this->employee_first_name->ViewCustomAttributes = "";

		// employee_last_name
		$this->employee_last_name->ViewValue = $this->employee_last_name->CurrentValue;
		$this->employee_last_name->ViewCustomAttributes = "";

		// employee_telephone
		$this->employee_telephone->ViewValue = $this->employee_telephone->CurrentValue;
		$this->employee_telephone->ViewCustomAttributes = "";

		// employee_lab_id
		if (strval($this->employee_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->employee_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->employee_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->employee_lab_id->ViewValue = $this->employee_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->employee_lab_id->ViewValue = $this->employee_lab_id->CurrentValue;
			}
		} else {
			$this->employee_lab_id->ViewValue = NULL;
		}
		$this->employee_lab_id->ViewCustomAttributes = "";

		// employee_position_id
		if (strval($this->employee_position_id->CurrentValue) <> "") {
			$sFilterWrk = "`position_id`" . ew_SearchString("=", $this->employee_position_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->employee_position_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->employee_position_id->ViewValue = $this->employee_position_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->employee_position_id->ViewValue = $this->employee_position_id->CurrentValue;
			}
		} else {
			$this->employee_position_id->ViewValue = NULL;
		}
		$this->employee_position_id->ViewCustomAttributes = "";

		// employee_salary
		$this->employee_salary->ViewValue = $this->employee_salary->CurrentValue;
		$this->employee_salary->ViewCustomAttributes = "";

			// employee_login
			$this->employee_login->LinkCustomAttributes = "";
			$this->employee_login->HrefValue = "";
			$this->employee_login->TooltipValue = "";

			// employee_password
			$this->employee_password->LinkCustomAttributes = "";
			$this->employee_password->HrefValue = "";
			$this->employee_password->TooltipValue = "";

			// employee_level_id
			$this->employee_level_id->LinkCustomAttributes = "";
			$this->employee_level_id->HrefValue = "";
			$this->employee_level_id->TooltipValue = "";

			// employee_first_name
			$this->employee_first_name->LinkCustomAttributes = "";
			$this->employee_first_name->HrefValue = "";
			$this->employee_first_name->TooltipValue = "";

			// employee_last_name
			$this->employee_last_name->LinkCustomAttributes = "";
			$this->employee_last_name->HrefValue = "";
			$this->employee_last_name->TooltipValue = "";

			// employee_telephone
			$this->employee_telephone->LinkCustomAttributes = "";
			$this->employee_telephone->HrefValue = "";
			$this->employee_telephone->TooltipValue = "";

			// employee_lab_id
			$this->employee_lab_id->LinkCustomAttributes = "";
			$this->employee_lab_id->HrefValue = "";
			$this->employee_lab_id->TooltipValue = "";

			// employee_position_id
			$this->employee_position_id->LinkCustomAttributes = "";
			$this->employee_position_id->HrefValue = "";
			$this->employee_position_id->TooltipValue = "";

			// employee_salary
			$this->employee_salary->LinkCustomAttributes = "";
			$this->employee_salary->HrefValue = "";
			$this->employee_salary->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// employee_login
			$this->employee_login->EditAttrs["class"] = "form-control";
			$this->employee_login->EditCustomAttributes = "";
			$this->employee_login->EditValue = ew_HtmlEncode($this->employee_login->CurrentValue);
			$this->employee_login->PlaceHolder = ew_RemoveHtml($this->employee_login->FldCaption());

			// employee_password
			$this->employee_password->EditAttrs["class"] = "form-control";
			$this->employee_password->EditCustomAttributes = "";
			$this->employee_password->EditValue = ew_HtmlEncode($this->employee_password->CurrentValue);
			$this->employee_password->PlaceHolder = ew_RemoveHtml($this->employee_password->FldCaption());

			// employee_level_id
			$this->employee_level_id->EditAttrs["class"] = "form-control";
			$this->employee_level_id->EditCustomAttributes = "";
			if (!$Security->CanAdmin()) { // System admin
				$this->employee_level_id->EditValue = $Language->Phrase("PasswordMask");
			} else {
			$this->employee_level_id->EditValue = $this->employee_level_id->Options(TRUE);
			}

			// employee_first_name
			$this->employee_first_name->EditAttrs["class"] = "form-control";
			$this->employee_first_name->EditCustomAttributes = "";
			$this->employee_first_name->EditValue = ew_HtmlEncode($this->employee_first_name->CurrentValue);
			$this->employee_first_name->PlaceHolder = ew_RemoveHtml($this->employee_first_name->FldCaption());

			// employee_last_name
			$this->employee_last_name->EditAttrs["class"] = "form-control";
			$this->employee_last_name->EditCustomAttributes = "";
			$this->employee_last_name->EditValue = ew_HtmlEncode($this->employee_last_name->CurrentValue);
			$this->employee_last_name->PlaceHolder = ew_RemoveHtml($this->employee_last_name->FldCaption());

			// employee_telephone
			$this->employee_telephone->EditAttrs["class"] = "form-control";
			$this->employee_telephone->EditCustomAttributes = "";
			$this->employee_telephone->EditValue = ew_HtmlEncode($this->employee_telephone->CurrentValue);
			$this->employee_telephone->PlaceHolder = ew_RemoveHtml($this->employee_telephone->FldCaption());

			// employee_lab_id
			$this->employee_lab_id->EditAttrs["class"] = "form-control";
			$this->employee_lab_id->EditCustomAttributes = "";
			if (trim(strval($this->employee_lab_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->employee_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->employee_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->employee_lab_id->EditValue = $arwrk;

			// employee_position_id
			$this->employee_position_id->EditAttrs["class"] = "form-control";
			$this->employee_position_id->EditCustomAttributes = "";
			if (trim(strval($this->employee_position_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`position_id`" . ew_SearchString("=", $this->employee_position_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `positions`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->employee_position_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->employee_position_id->EditValue = $arwrk;

			// employee_salary
			$this->employee_salary->EditAttrs["class"] = "form-control";
			$this->employee_salary->EditCustomAttributes = "";
			$this->employee_salary->EditValue = ew_HtmlEncode($this->employee_salary->CurrentValue);
			$this->employee_salary->PlaceHolder = ew_RemoveHtml($this->employee_salary->FldCaption());

			// Add refer script
			// employee_login

			$this->employee_login->LinkCustomAttributes = "";
			$this->employee_login->HrefValue = "";

			// employee_password
			$this->employee_password->LinkCustomAttributes = "";
			$this->employee_password->HrefValue = "";

			// employee_level_id
			$this->employee_level_id->LinkCustomAttributes = "";
			$this->employee_level_id->HrefValue = "";

			// employee_first_name
			$this->employee_first_name->LinkCustomAttributes = "";
			$this->employee_first_name->HrefValue = "";

			// employee_last_name
			$this->employee_last_name->LinkCustomAttributes = "";
			$this->employee_last_name->HrefValue = "";

			// employee_telephone
			$this->employee_telephone->LinkCustomAttributes = "";
			$this->employee_telephone->HrefValue = "";

			// employee_lab_id
			$this->employee_lab_id->LinkCustomAttributes = "";
			$this->employee_lab_id->HrefValue = "";

			// employee_position_id
			$this->employee_position_id->LinkCustomAttributes = "";
			$this->employee_position_id->HrefValue = "";

			// employee_salary
			$this->employee_salary->LinkCustomAttributes = "";
			$this->employee_salary->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->employee_login->FldIsDetailKey && !is_null($this->employee_login->FormValue) && $this->employee_login->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_login->FldCaption(), $this->employee_login->ReqErrMsg));
		}
		if (!$this->employee_password->FldIsDetailKey && !is_null($this->employee_password->FormValue) && $this->employee_password->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_password->FldCaption(), $this->employee_password->ReqErrMsg));
		}
		if (!$this->employee_level_id->FldIsDetailKey && !is_null($this->employee_level_id->FormValue) && $this->employee_level_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_level_id->FldCaption(), $this->employee_level_id->ReqErrMsg));
		}
		if (!$this->employee_lab_id->FldIsDetailKey && !is_null($this->employee_lab_id->FormValue) && $this->employee_lab_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_lab_id->FldCaption(), $this->employee_lab_id->ReqErrMsg));
		}
		if (!$this->employee_position_id->FldIsDetailKey && !is_null($this->employee_position_id->FormValue) && $this->employee_position_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_position_id->FldCaption(), $this->employee_position_id->ReqErrMsg));
		}
		if (!$this->employee_salary->FldIsDetailKey && !is_null($this->employee_salary->FormValue) && $this->employee_salary->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->employee_salary->FldCaption(), $this->employee_salary->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->employee_salary->FormValue)) {
			ew_AddMessage($gsFormError, $this->employee_salary->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;

		// Check if valid User ID
		$bValidUser = FALSE;
		if ($Security->CurrentUserID() <> "" && !ew_Empty($this->employee_id->CurrentValue) && !$Security->IsAdmin()) { // Non system admin
			$bValidUser = $Security->IsValidUserID($this->employee_id->CurrentValue);
			if (!$bValidUser) {
				$sUserIdMsg = str_replace("%c", CurrentUserID(), $Language->Phrase("UnAuthorizedUserID"));
				$sUserIdMsg = str_replace("%u", $this->employee_id->CurrentValue, $sUserIdMsg);
				$this->setFailureMessage($sUserIdMsg);
				return FALSE;
			}
		}
		if ($this->employee_login->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(employee_login = '" . ew_AdjustSql($this->employee_login->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->employee_login->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->employee_login->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// employee_login
		$this->employee_login->SetDbValueDef($rsnew, $this->employee_login->CurrentValue, "", FALSE);

		// employee_password
		$this->employee_password->SetDbValueDef($rsnew, $this->employee_password->CurrentValue, "", FALSE);

		// employee_level_id
		if ($Security->CanAdmin()) { // System admin
		$this->employee_level_id->SetDbValueDef($rsnew, $this->employee_level_id->CurrentValue, 0, strval($this->employee_level_id->CurrentValue) == "");
		}

		// employee_first_name
		$this->employee_first_name->SetDbValueDef($rsnew, $this->employee_first_name->CurrentValue, NULL, FALSE);

		// employee_last_name
		$this->employee_last_name->SetDbValueDef($rsnew, $this->employee_last_name->CurrentValue, NULL, FALSE);

		// employee_telephone
		$this->employee_telephone->SetDbValueDef($rsnew, $this->employee_telephone->CurrentValue, NULL, FALSE);

		// employee_lab_id
		$this->employee_lab_id->SetDbValueDef($rsnew, $this->employee_lab_id->CurrentValue, 0, FALSE);

		// employee_position_id
		$this->employee_position_id->SetDbValueDef($rsnew, $this->employee_position_id->CurrentValue, 0, FALSE);

		// employee_salary
		$this->employee_salary->SetDbValueDef($rsnew, $this->employee_salary->CurrentValue, 0, FALSE);

		// employee_id
		// Call Row Inserting event

		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->employee_id->setDbValue($conn->Insert_ID());
				$rsnew['employee_id'] = $this->employee_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->employee_id->CurrentValue);
		return TRUE;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("employeeslist.php"), "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'employees';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnAdd) return;
		$table = 'employees';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['employee_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$newvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				if ($fldname == 'employee_password')
					$newvalue = $Language->Phrase("PasswordMask");
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($employees_add)) $employees_add = new cemployees_add();

// Page init
$employees_add->Page_Init();

// Page main
$employees_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$employees_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = femployeesadd = new ew_Form("femployeesadd", "add");

// Validate form
femployeesadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_employee_login");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $employees->employee_login->FldCaption(), $employees->employee_login->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_password");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $employees->employee_password->FldCaption(), $employees->employee_password->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_level_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $employees->employee_level_id->FldCaption(), $employees->employee_level_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_lab_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $employees->employee_lab_id->FldCaption(), $employees->employee_lab_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_position_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $employees->employee_position_id->FldCaption(), $employees->employee_position_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_salary");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $employees->employee_salary->FldCaption(), $employees->employee_salary->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_salary");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($employees->employee_salary->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
femployeesadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
femployeesadd.ValidateRequired = true;
<?php } else { ?>
femployeesadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
femployeesadd.Lists["x_employee_level_id"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
femployeesadd.Lists["x_employee_level_id"].Options = <?php echo json_encode($employees->employee_level_id->Options()) ?>;
femployeesadd.Lists["x_employee_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
femployeesadd.Lists["x_employee_position_id"] = {"LinkField":"x_position_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_position_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $employees_add->ShowPageHeader(); ?>
<?php
$employees_add->ShowMessage();
?>
<form name="femployeesadd" id="femployeesadd" class="<?php echo $employees_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($employees_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $employees_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="employees">
<input type="hidden" name="a_add" id="a_add" value="A">
<!-- Fields to prevent google autofill -->
<input class="hidden" type="text" name="<?php echo ew_Encrypt(ew_Random()) ?>">
<input class="hidden" type="password" name="<?php echo ew_Encrypt(ew_Random()) ?>">
<div>
<?php if ($employees->employee_login->Visible) { // employee_login ?>
	<div id="r_employee_login" class="form-group">
		<label id="elh_employees_employee_login" for="x_employee_login" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_login->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_login->CellAttributes() ?>>
<span id="el_employees_employee_login">
<input type="text" data-table="employees" data-field="x_employee_login" name="x_employee_login" id="x_employee_login" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($employees->employee_login->getPlaceHolder()) ?>" value="<?php echo $employees->employee_login->EditValue ?>"<?php echo $employees->employee_login->EditAttributes() ?>>
</span>
<?php echo $employees->employee_login->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_password->Visible) { // employee_password ?>
	<div id="r_employee_password" class="form-group">
		<label id="elh_employees_employee_password" for="x_employee_password" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_password->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_password->CellAttributes() ?>>
<span id="el_employees_employee_password">
<div class="input-group" id="ig_employee_password">
<input type="text" data-password-generated="pgt_employee_password" data-table="employees" data-field="x_employee_password" name="x_employee_password" id="x_employee_password" value="<?php echo $employees->employee_password->EditValue ?>" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($employees->employee_password->getPlaceHolder()) ?>"<?php echo $employees->employee_password->EditAttributes() ?>>
<span class="input-group-btn">
	<button type="button" class="btn btn-default ewPasswordGenerator" title="<?php echo ew_HtmlTitle($Language->Phrase("GeneratePassword")) ?>" data-password-field="x_employee_password" data-password-confirm="c_employee_password" data-password-generated="pgt_employee_password"><?php echo $Language->Phrase("GeneratePassword") ?></button>
</span>
</div>
<span class="help-block" id="pgt_employee_password" style="display: none;"></span>
</span>
<?php echo $employees->employee_password->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_level_id->Visible) { // employee_level_id ?>
	<div id="r_employee_level_id" class="form-group">
		<label id="elh_employees_employee_level_id" for="x_employee_level_id" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_level_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_level_id->CellAttributes() ?>>
<?php if (!$Security->IsAdmin() && $Security->IsLoggedIn()) { // Non system admin ?>
<span id="el_employees_employee_level_id">
<p class="form-control-static"><?php echo $employees->employee_level_id->EditValue ?></p>
</span>
<?php } else { ?>
<span id="el_employees_employee_level_id">
<select data-table="employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($employees->employee_level_id->DisplayValueSeparator) ? json_encode($employees->employee_level_id->DisplayValueSeparator) : $employees->employee_level_id->DisplayValueSeparator) ?>" id="x_employee_level_id" name="x_employee_level_id"<?php echo $employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($employees->employee_level_id->EditValue)) {
	$arwrk = $employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($employees->employee_level_id->CurrentValue) ?>" selected><?php echo $employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php } ?>
<?php echo $employees->employee_level_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_first_name->Visible) { // employee_first_name ?>
	<div id="r_employee_first_name" class="form-group">
		<label id="elh_employees_employee_first_name" for="x_employee_first_name" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_first_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_first_name->CellAttributes() ?>>
<span id="el_employees_employee_first_name">
<input type="text" data-table="employees" data-field="x_employee_first_name" name="x_employee_first_name" id="x_employee_first_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($employees->employee_first_name->getPlaceHolder()) ?>" value="<?php echo $employees->employee_first_name->EditValue ?>"<?php echo $employees->employee_first_name->EditAttributes() ?>>
</span>
<?php echo $employees->employee_first_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_last_name->Visible) { // employee_last_name ?>
	<div id="r_employee_last_name" class="form-group">
		<label id="elh_employees_employee_last_name" for="x_employee_last_name" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_last_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_last_name->CellAttributes() ?>>
<span id="el_employees_employee_last_name">
<input type="text" data-table="employees" data-field="x_employee_last_name" name="x_employee_last_name" id="x_employee_last_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($employees->employee_last_name->getPlaceHolder()) ?>" value="<?php echo $employees->employee_last_name->EditValue ?>"<?php echo $employees->employee_last_name->EditAttributes() ?>>
</span>
<?php echo $employees->employee_last_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_telephone->Visible) { // employee_telephone ?>
	<div id="r_employee_telephone" class="form-group">
		<label id="elh_employees_employee_telephone" for="x_employee_telephone" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_telephone->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_telephone->CellAttributes() ?>>
<span id="el_employees_employee_telephone">
<input type="text" data-table="employees" data-field="x_employee_telephone" name="x_employee_telephone" id="x_employee_telephone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($employees->employee_telephone->getPlaceHolder()) ?>" value="<?php echo $employees->employee_telephone->EditValue ?>"<?php echo $employees->employee_telephone->EditAttributes() ?>>
</span>
<?php echo $employees->employee_telephone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_lab_id->Visible) { // employee_lab_id ?>
	<div id="r_employee_lab_id" class="form-group">
		<label id="elh_employees_employee_lab_id" for="x_employee_lab_id" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_lab_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_lab_id->CellAttributes() ?>>
<span id="el_employees_employee_lab_id">
<select data-table="employees" data-field="x_employee_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($employees->employee_lab_id->DisplayValueSeparator) ? json_encode($employees->employee_lab_id->DisplayValueSeparator) : $employees->employee_lab_id->DisplayValueSeparator) ?>" id="x_employee_lab_id" name="x_employee_lab_id"<?php echo $employees->employee_lab_id->EditAttributes() ?>>
<?php
if (is_array($employees->employee_lab_id->EditValue)) {
	$arwrk = $employees->employee_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($employees->employee_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $employees->employee_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($employees->employee_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($employees->employee_lab_id->CurrentValue) ?>" selected><?php echo $employees->employee_lab_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$employees->employee_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$employees->employee_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$employees->Lookup_Selecting($employees->employee_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $employees->employee_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_employee_lab_id" id="s_x_employee_lab_id" value="<?php echo $employees->employee_lab_id->LookupFilterQuery() ?>">
</span>
<?php echo $employees->employee_lab_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_position_id->Visible) { // employee_position_id ?>
	<div id="r_employee_position_id" class="form-group">
		<label id="elh_employees_employee_position_id" for="x_employee_position_id" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_position_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_position_id->CellAttributes() ?>>
<span id="el_employees_employee_position_id">
<select data-table="employees" data-field="x_employee_position_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($employees->employee_position_id->DisplayValueSeparator) ? json_encode($employees->employee_position_id->DisplayValueSeparator) : $employees->employee_position_id->DisplayValueSeparator) ?>" id="x_employee_position_id" name="x_employee_position_id"<?php echo $employees->employee_position_id->EditAttributes() ?>>
<?php
if (is_array($employees->employee_position_id->EditValue)) {
	$arwrk = $employees->employee_position_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($employees->employee_position_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $employees->employee_position_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($employees->employee_position_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($employees->employee_position_id->CurrentValue) ?>" selected><?php echo $employees->employee_position_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
$sWhereWrk = "";
$employees->employee_position_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$employees->employee_position_id->LookupFilters += array("f0" => "`position_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$employees->Lookup_Selecting($employees->employee_position_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $employees->employee_position_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_employee_position_id" id="s_x_employee_position_id" value="<?php echo $employees->employee_position_id->LookupFilterQuery() ?>">
</span>
<?php echo $employees->employee_position_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($employees->employee_salary->Visible) { // employee_salary ?>
	<div id="r_employee_salary" class="form-group">
		<label id="elh_employees_employee_salary" for="x_employee_salary" class="col-sm-2 control-label ewLabel"><?php echo $employees->employee_salary->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $employees->employee_salary->CellAttributes() ?>>
<span id="el_employees_employee_salary">
<input type="text" data-table="employees" data-field="x_employee_salary" name="x_employee_salary" id="x_employee_salary" size="30" placeholder="<?php echo ew_HtmlEncode($employees->employee_salary->getPlaceHolder()) ?>" value="<?php echo $employees->employee_salary->EditValue ?>"<?php echo $employees->employee_salary->EditAttributes() ?>>
</span>
<?php echo $employees->employee_salary->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $employees_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
femployeesadd.Init();
</script>
<?php
$employees_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$employees_add->Page_Terminate();
?>
