<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($v_employees_grid)) $v_employees_grid = new cv_employees_grid();

// Page init
$v_employees_grid->Page_Init();

// Page main
$v_employees_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$v_employees_grid->Page_Render();
?>
<?php if ($v_employees->Export == "") { ?>
<script type="text/javascript">

// Form object
var fv_employeesgrid = new ew_Form("fv_employeesgrid", "grid");
fv_employeesgrid.FormKeyCountName = '<?php echo $v_employees_grid->FormKeyCountName ?>';

// Validate form
fv_employeesgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_employee_level_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_employees->employee_level_id->FldCaption(), $v_employees->employee_level_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_lab_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_employees->lab_name->FldCaption(), $v_employees->lab_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_position_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $v_employees->position_name->FldCaption(), $v_employees->position_name->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fv_employeesgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "employee_level_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_first_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_last_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_telephone", false)) return false;
	if (ew_ValueChanged(fobj, infix, "lab_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "position_name", false)) return false;
	return true;
}

// Form_CustomValidate event
fv_employeesgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fv_employeesgrid.ValidateRequired = true;
<?php } else { ?>
fv_employeesgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fv_employeesgrid.Lists["x_employee_level_id"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fv_employeesgrid.Lists["x_employee_level_id"].Options = <?php echo json_encode($v_employees->employee_level_id->Options()) ?>;

// Form object for search
</script>
<?php } ?>
<?php
if ($v_employees->CurrentAction == "gridadd") {
	if ($v_employees->CurrentMode == "copy") {
		$bSelectLimit = $v_employees_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$v_employees_grid->TotalRecs = $v_employees->SelectRecordCount();
			$v_employees_grid->Recordset = $v_employees_grid->LoadRecordset($v_employees_grid->StartRec-1, $v_employees_grid->DisplayRecs);
		} else {
			if ($v_employees_grid->Recordset = $v_employees_grid->LoadRecordset())
				$v_employees_grid->TotalRecs = $v_employees_grid->Recordset->RecordCount();
		}
		$v_employees_grid->StartRec = 1;
		$v_employees_grid->DisplayRecs = $v_employees_grid->TotalRecs;
	} else {
		$v_employees->CurrentFilter = "0=1";
		$v_employees_grid->StartRec = 1;
		$v_employees_grid->DisplayRecs = $v_employees->GridAddRowCount;
	}
	$v_employees_grid->TotalRecs = $v_employees_grid->DisplayRecs;
	$v_employees_grid->StopRec = $v_employees_grid->DisplayRecs;
} else {
	$bSelectLimit = $v_employees_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($v_employees_grid->TotalRecs <= 0)
			$v_employees_grid->TotalRecs = $v_employees->SelectRecordCount();
	} else {
		if (!$v_employees_grid->Recordset && ($v_employees_grid->Recordset = $v_employees_grid->LoadRecordset()))
			$v_employees_grid->TotalRecs = $v_employees_grid->Recordset->RecordCount();
	}
	$v_employees_grid->StartRec = 1;
	$v_employees_grid->DisplayRecs = $v_employees_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$v_employees_grid->Recordset = $v_employees_grid->LoadRecordset($v_employees_grid->StartRec-1, $v_employees_grid->DisplayRecs);

	// Set no record found message
	if ($v_employees->CurrentAction == "" && $v_employees_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$v_employees_grid->setWarningMessage(ew_DeniedMsg());
		if ($v_employees_grid->SearchWhere == "0=101")
			$v_employees_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$v_employees_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$v_employees_grid->RenderOtherOptions();
?>
<?php $v_employees_grid->ShowPageHeader(); ?>
<?php
$v_employees_grid->ShowMessage();
?>
<?php if ($v_employees_grid->TotalRecs > 0 || $v_employees->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="fv_employeesgrid" class="ewForm form-inline">
<?php if ($v_employees_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($v_employees_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_v_employees" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_v_employeesgrid" class="table ewTable">
<?php echo $v_employees->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$v_employees_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$v_employees_grid->RenderListOptions();

// Render list options (header, left)
$v_employees_grid->ListOptions->Render("header", "left");
?>
<?php if ($v_employees->employee_level_id->Visible) { // employee_level_id ?>
	<?php if ($v_employees->SortUrl($v_employees->employee_level_id) == "") { ?>
		<th data-name="employee_level_id"><div id="elh_v_employees_employee_level_id" class="v_employees_employee_level_id"><div class="ewTableHeaderCaption"><?php echo $v_employees->employee_level_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_level_id"><div><div id="elh_v_employees_employee_level_id" class="v_employees_employee_level_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_employees->employee_level_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_employees->employee_level_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_employees->employee_level_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_employees->employee_first_name->Visible) { // employee_first_name ?>
	<?php if ($v_employees->SortUrl($v_employees->employee_first_name) == "") { ?>
		<th data-name="employee_first_name"><div id="elh_v_employees_employee_first_name" class="v_employees_employee_first_name"><div class="ewTableHeaderCaption"><?php echo $v_employees->employee_first_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_first_name"><div><div id="elh_v_employees_employee_first_name" class="v_employees_employee_first_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_employees->employee_first_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_employees->employee_first_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_employees->employee_first_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_employees->employee_last_name->Visible) { // employee_last_name ?>
	<?php if ($v_employees->SortUrl($v_employees->employee_last_name) == "") { ?>
		<th data-name="employee_last_name"><div id="elh_v_employees_employee_last_name" class="v_employees_employee_last_name"><div class="ewTableHeaderCaption"><?php echo $v_employees->employee_last_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_last_name"><div><div id="elh_v_employees_employee_last_name" class="v_employees_employee_last_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_employees->employee_last_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_employees->employee_last_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_employees->employee_last_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_employees->employee_telephone->Visible) { // employee_telephone ?>
	<?php if ($v_employees->SortUrl($v_employees->employee_telephone) == "") { ?>
		<th data-name="employee_telephone"><div id="elh_v_employees_employee_telephone" class="v_employees_employee_telephone"><div class="ewTableHeaderCaption"><?php echo $v_employees->employee_telephone->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_telephone"><div><div id="elh_v_employees_employee_telephone" class="v_employees_employee_telephone">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_employees->employee_telephone->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_employees->employee_telephone->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_employees->employee_telephone->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_employees->lab_name->Visible) { // lab_name ?>
	<?php if ($v_employees->SortUrl($v_employees->lab_name) == "") { ?>
		<th data-name="lab_name"><div id="elh_v_employees_lab_name" class="v_employees_lab_name"><div class="ewTableHeaderCaption"><?php echo $v_employees->lab_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="lab_name"><div><div id="elh_v_employees_lab_name" class="v_employees_lab_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_employees->lab_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_employees->lab_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_employees->lab_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($v_employees->position_name->Visible) { // position_name ?>
	<?php if ($v_employees->SortUrl($v_employees->position_name) == "") { ?>
		<th data-name="position_name"><div id="elh_v_employees_position_name" class="v_employees_position_name"><div class="ewTableHeaderCaption"><?php echo $v_employees->position_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="position_name"><div><div id="elh_v_employees_position_name" class="v_employees_position_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $v_employees->position_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($v_employees->position_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($v_employees->position_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$v_employees_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$v_employees_grid->StartRec = 1;
$v_employees_grid->StopRec = $v_employees_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($v_employees_grid->FormKeyCountName) && ($v_employees->CurrentAction == "gridadd" || $v_employees->CurrentAction == "gridedit" || $v_employees->CurrentAction == "F")) {
		$v_employees_grid->KeyCount = $objForm->GetValue($v_employees_grid->FormKeyCountName);
		$v_employees_grid->StopRec = $v_employees_grid->StartRec + $v_employees_grid->KeyCount - 1;
	}
}
$v_employees_grid->RecCnt = $v_employees_grid->StartRec - 1;
if ($v_employees_grid->Recordset && !$v_employees_grid->Recordset->EOF) {
	$v_employees_grid->Recordset->MoveFirst();
	$bSelectLimit = $v_employees_grid->UseSelectLimit;
	if (!$bSelectLimit && $v_employees_grid->StartRec > 1)
		$v_employees_grid->Recordset->Move($v_employees_grid->StartRec - 1);
} elseif (!$v_employees->AllowAddDeleteRow && $v_employees_grid->StopRec == 0) {
	$v_employees_grid->StopRec = $v_employees->GridAddRowCount;
}

// Initialize aggregate
$v_employees->RowType = EW_ROWTYPE_AGGREGATEINIT;
$v_employees->ResetAttrs();
$v_employees_grid->RenderRow();
if ($v_employees->CurrentAction == "gridadd")
	$v_employees_grid->RowIndex = 0;
if ($v_employees->CurrentAction == "gridedit")
	$v_employees_grid->RowIndex = 0;
while ($v_employees_grid->RecCnt < $v_employees_grid->StopRec) {
	$v_employees_grid->RecCnt++;
	if (intval($v_employees_grid->RecCnt) >= intval($v_employees_grid->StartRec)) {
		$v_employees_grid->RowCnt++;
		if ($v_employees->CurrentAction == "gridadd" || $v_employees->CurrentAction == "gridedit" || $v_employees->CurrentAction == "F") {
			$v_employees_grid->RowIndex++;
			$objForm->Index = $v_employees_grid->RowIndex;
			if ($objForm->HasValue($v_employees_grid->FormActionName))
				$v_employees_grid->RowAction = strval($objForm->GetValue($v_employees_grid->FormActionName));
			elseif ($v_employees->CurrentAction == "gridadd")
				$v_employees_grid->RowAction = "insert";
			else
				$v_employees_grid->RowAction = "";
		}

		// Set up key count
		$v_employees_grid->KeyCount = $v_employees_grid->RowIndex;

		// Init row class and style
		$v_employees->ResetAttrs();
		$v_employees->CssClass = "";
		if ($v_employees->CurrentAction == "gridadd") {
			if ($v_employees->CurrentMode == "copy") {
				$v_employees_grid->LoadRowValues($v_employees_grid->Recordset); // Load row values
				$v_employees_grid->SetRecordKey($v_employees_grid->RowOldKey, $v_employees_grid->Recordset); // Set old record key
			} else {
				$v_employees_grid->LoadDefaultValues(); // Load default values
				$v_employees_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$v_employees_grid->LoadRowValues($v_employees_grid->Recordset); // Load row values
		}
		$v_employees->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($v_employees->CurrentAction == "gridadd") // Grid add
			$v_employees->RowType = EW_ROWTYPE_ADD; // Render add
		if ($v_employees->CurrentAction == "gridadd" && $v_employees->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$v_employees_grid->RestoreCurrentRowFormValues($v_employees_grid->RowIndex); // Restore form values
		if ($v_employees->CurrentAction == "gridedit") { // Grid edit
			if ($v_employees->EventCancelled) {
				$v_employees_grid->RestoreCurrentRowFormValues($v_employees_grid->RowIndex); // Restore form values
			}
			if ($v_employees_grid->RowAction == "insert")
				$v_employees->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$v_employees->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($v_employees->CurrentAction == "gridedit" && ($v_employees->RowType == EW_ROWTYPE_EDIT || $v_employees->RowType == EW_ROWTYPE_ADD) && $v_employees->EventCancelled) // Update failed
			$v_employees_grid->RestoreCurrentRowFormValues($v_employees_grid->RowIndex); // Restore form values
		if ($v_employees->RowType == EW_ROWTYPE_EDIT) // Edit row
			$v_employees_grid->EditRowCnt++;
		if ($v_employees->CurrentAction == "F") // Confirm row
			$v_employees_grid->RestoreCurrentRowFormValues($v_employees_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$v_employees->RowAttrs = array_merge($v_employees->RowAttrs, array('data-rowindex'=>$v_employees_grid->RowCnt, 'id'=>'r' . $v_employees_grid->RowCnt . '_v_employees', 'data-rowtype'=>$v_employees->RowType));

		// Render row
		$v_employees_grid->RenderRow();

		// Render list options
		$v_employees_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($v_employees_grid->RowAction <> "delete" && $v_employees_grid->RowAction <> "insertdelete" && !($v_employees_grid->RowAction == "insert" && $v_employees->CurrentAction == "F" && $v_employees_grid->EmptyRow())) {
?>
	<tr<?php echo $v_employees->RowAttributes() ?>>
<?php

// Render list options (body, left)
$v_employees_grid->ListOptions->Render("body", "left", $v_employees_grid->RowCnt);
?>
	<?php if ($v_employees->employee_level_id->Visible) { // employee_level_id ?>
		<td data-name="employee_level_id"<?php echo $v_employees->employee_level_id->CellAttributes() ?>>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_level_id" class="form-group v_employees_employee_level_id">
<select data-table="v_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_employees->employee_level_id->DisplayValueSeparator) ? json_encode($v_employees->employee_level_id->DisplayValueSeparator) : $v_employees->employee_level_id->DisplayValueSeparator) ?>" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id"<?php echo $v_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($v_employees->employee_level_id->EditValue)) {
	$arwrk = $v_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $v_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_employees->employee_level_id->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_level_id" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_level_id" class="form-group v_employees_employee_level_id">
<select data-table="v_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_employees->employee_level_id->DisplayValueSeparator) ? json_encode($v_employees->employee_level_id->DisplayValueSeparator) : $v_employees->employee_level_id->DisplayValueSeparator) ?>" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id"<?php echo $v_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($v_employees->employee_level_id->EditValue)) {
	$arwrk = $v_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $v_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_employees->employee_level_id->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_level_id" class="v_employees_employee_level_id">
<span<?php echo $v_employees->employee_level_id->ViewAttributes() ?>>
<?php echo $v_employees->employee_level_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_level_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->FormValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_employee_level_id" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $v_employees_grid->PageObjName . "_row_" . $v_employees_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="v_employees" data-field="x_employee_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_id" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_id" value="<?php echo ew_HtmlEncode($v_employees->employee_id->CurrentValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_employee_id" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_id" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_id" value="<?php echo ew_HtmlEncode($v_employees->employee_id->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT || $v_employees->CurrentMode == "edit") { ?>
<input type="hidden" data-table="v_employees" data-field="x_employee_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_id" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_id" value="<?php echo ew_HtmlEncode($v_employees->employee_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($v_employees->employee_first_name->Visible) { // employee_first_name ?>
		<td data-name="employee_first_name"<?php echo $v_employees->employee_first_name->CellAttributes() ?>>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_first_name" class="form-group v_employees_employee_first_name">
<input type="text" data-table="v_employees" data-field="x_employee_first_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_first_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_first_name->EditValue ?>"<?php echo $v_employees->employee_first_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_first_name" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($v_employees->employee_first_name->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_first_name" class="form-group v_employees_employee_first_name">
<input type="text" data-table="v_employees" data-field="x_employee_first_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_first_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_first_name->EditValue ?>"<?php echo $v_employees->employee_first_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_first_name" class="v_employees_employee_first_name">
<span<?php echo $v_employees->employee_first_name->ViewAttributes() ?>>
<?php echo $v_employees->employee_first_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_first_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($v_employees->employee_first_name->FormValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_employee_first_name" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($v_employees->employee_first_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_employees->employee_last_name->Visible) { // employee_last_name ?>
		<td data-name="employee_last_name"<?php echo $v_employees->employee_last_name->CellAttributes() ?>>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_last_name" class="form-group v_employees_employee_last_name">
<input type="text" data-table="v_employees" data-field="x_employee_last_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_last_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_last_name->EditValue ?>"<?php echo $v_employees->employee_last_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_last_name" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($v_employees->employee_last_name->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_last_name" class="form-group v_employees_employee_last_name">
<input type="text" data-table="v_employees" data-field="x_employee_last_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_last_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_last_name->EditValue ?>"<?php echo $v_employees->employee_last_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_last_name" class="v_employees_employee_last_name">
<span<?php echo $v_employees->employee_last_name->ViewAttributes() ?>>
<?php echo $v_employees->employee_last_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_last_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($v_employees->employee_last_name->FormValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_employee_last_name" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($v_employees->employee_last_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_employees->employee_telephone->Visible) { // employee_telephone ?>
		<td data-name="employee_telephone"<?php echo $v_employees->employee_telephone->CellAttributes() ?>>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_telephone" class="form-group v_employees_employee_telephone">
<input type="text" data-table="v_employees" data-field="x_employee_telephone" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_telephone->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_telephone->EditValue ?>"<?php echo $v_employees->employee_telephone->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_telephone" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($v_employees->employee_telephone->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_telephone" class="form-group v_employees_employee_telephone">
<input type="text" data-table="v_employees" data-field="x_employee_telephone" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_telephone->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_telephone->EditValue ?>"<?php echo $v_employees->employee_telephone->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_employee_telephone" class="v_employees_employee_telephone">
<span<?php echo $v_employees->employee_telephone->ViewAttributes() ?>>
<?php echo $v_employees->employee_telephone->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_telephone" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($v_employees->employee_telephone->FormValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_employee_telephone" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($v_employees->employee_telephone->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_employees->lab_name->Visible) { // lab_name ?>
		<td data-name="lab_name"<?php echo $v_employees->lab_name->CellAttributes() ?>>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($v_employees->lab_name->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_lab_name" class="form-group v_employees_lab_name">
<span<?php echo $v_employees->lab_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->lab_name->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_lab_name" class="form-group v_employees_lab_name">
<input type="text" data-table="v_employees" data-field="x_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->lab_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->lab_name->EditValue ?>"<?php echo $v_employees->lab_name->EditAttributes() ?>>
</span>
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_lab_name" name="o<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="o<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($v_employees->lab_name->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_lab_name" class="form-group v_employees_lab_name">
<span<?php echo $v_employees->lab_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->lab_name->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_lab_name" class="form-group v_employees_lab_name">
<input type="text" data-table="v_employees" data-field="x_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->lab_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->lab_name->EditValue ?>"<?php echo $v_employees->lab_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_lab_name" class="v_employees_lab_name">
<span<?php echo $v_employees->lab_name->ViewAttributes() ?>>
<?php echo $v_employees->lab_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->FormValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_lab_name" name="o<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="o<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($v_employees->position_name->Visible) { // position_name ?>
		<td data-name="position_name"<?php echo $v_employees->position_name->CellAttributes() ?>>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($v_employees->position_name->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_position_name" class="form-group v_employees_position_name">
<span<?php echo $v_employees->position_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->position_name->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_position_name" class="form-group v_employees_position_name">
<input type="text" data-table="v_employees" data-field="x_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->position_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->position_name->EditValue ?>"<?php echo $v_employees->position_name->EditAttributes() ?>>
</span>
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_position_name" name="o<?php echo $v_employees_grid->RowIndex ?>_position_name" id="o<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->OldValue) ?>">
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($v_employees->position_name->getSessionValue() <> "") { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_position_name" class="form-group v_employees_position_name">
<span<?php echo $v_employees->position_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->position_name->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_position_name" class="form-group v_employees_position_name">
<input type="text" data-table="v_employees" data-field="x_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->position_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->position_name->EditValue ?>"<?php echo $v_employees->position_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php } ?>
<?php if ($v_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $v_employees_grid->RowCnt ?>_v_employees_position_name" class="v_employees_position_name">
<span<?php echo $v_employees->position_name->ViewAttributes() ?>>
<?php echo $v_employees->position_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->FormValue) ?>">
<input type="hidden" data-table="v_employees" data-field="x_position_name" name="o<?php echo $v_employees_grid->RowIndex ?>_position_name" id="o<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$v_employees_grid->ListOptions->Render("body", "right", $v_employees_grid->RowCnt);
?>
	</tr>
<?php if ($v_employees->RowType == EW_ROWTYPE_ADD || $v_employees->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fv_employeesgrid.UpdateOpts(<?php echo $v_employees_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($v_employees->CurrentAction <> "gridadd" || $v_employees->CurrentMode == "copy")
		if (!$v_employees_grid->Recordset->EOF) $v_employees_grid->Recordset->MoveNext();
}
?>
<?php
	if ($v_employees->CurrentMode == "add" || $v_employees->CurrentMode == "copy" || $v_employees->CurrentMode == "edit") {
		$v_employees_grid->RowIndex = '$rowindex$';
		$v_employees_grid->LoadDefaultValues();

		// Set row properties
		$v_employees->ResetAttrs();
		$v_employees->RowAttrs = array_merge($v_employees->RowAttrs, array('data-rowindex'=>$v_employees_grid->RowIndex, 'id'=>'r0_v_employees', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($v_employees->RowAttrs["class"], "ewTemplate");
		$v_employees->RowType = EW_ROWTYPE_ADD;

		// Render row
		$v_employees_grid->RenderRow();

		// Render list options
		$v_employees_grid->RenderListOptions();
		$v_employees_grid->StartRowCnt = 0;
?>
	<tr<?php echo $v_employees->RowAttributes() ?>>
<?php

// Render list options (body, left)
$v_employees_grid->ListOptions->Render("body", "left", $v_employees_grid->RowIndex);
?>
	<?php if ($v_employees->employee_level_id->Visible) { // employee_level_id ?>
		<td data-name="employee_level_id">
<?php if ($v_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_employees_employee_level_id" class="form-group v_employees_employee_level_id">
<select data-table="v_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_employees->employee_level_id->DisplayValueSeparator) ? json_encode($v_employees->employee_level_id->DisplayValueSeparator) : $v_employees->employee_level_id->DisplayValueSeparator) ?>" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id"<?php echo $v_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($v_employees->employee_level_id->EditValue)) {
	$arwrk = $v_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $v_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $v_employees->employee_level_id->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_employees_employee_level_id" class="form-group v_employees_employee_level_id">
<span<?php echo $v_employees->employee_level_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->employee_level_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_level_id" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_employee_level_id" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($v_employees->employee_level_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_employees->employee_first_name->Visible) { // employee_first_name ?>
		<td data-name="employee_first_name">
<?php if ($v_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_employees_employee_first_name" class="form-group v_employees_employee_first_name">
<input type="text" data-table="v_employees" data-field="x_employee_first_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_first_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_first_name->EditValue ?>"<?php echo $v_employees->employee_first_name->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_employees_employee_first_name" class="form-group v_employees_employee_first_name">
<span<?php echo $v_employees->employee_first_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->employee_first_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_first_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($v_employees->employee_first_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_employee_first_name" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($v_employees->employee_first_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_employees->employee_last_name->Visible) { // employee_last_name ?>
		<td data-name="employee_last_name">
<?php if ($v_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_employees_employee_last_name" class="form-group v_employees_employee_last_name">
<input type="text" data-table="v_employees" data-field="x_employee_last_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_last_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_last_name->EditValue ?>"<?php echo $v_employees->employee_last_name->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_employees_employee_last_name" class="form-group v_employees_employee_last_name">
<span<?php echo $v_employees->employee_last_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->employee_last_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_last_name" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($v_employees->employee_last_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_employee_last_name" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($v_employees->employee_last_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_employees->employee_telephone->Visible) { // employee_telephone ?>
		<td data-name="employee_telephone">
<?php if ($v_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_v_employees_employee_telephone" class="form-group v_employees_employee_telephone">
<input type="text" data-table="v_employees" data-field="x_employee_telephone" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->employee_telephone->getPlaceHolder()) ?>" value="<?php echo $v_employees->employee_telephone->EditValue ?>"<?php echo $v_employees->employee_telephone->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_v_employees_employee_telephone" class="form-group v_employees_employee_telephone">
<span<?php echo $v_employees->employee_telephone->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->employee_telephone->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_employee_telephone" name="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($v_employees->employee_telephone->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_employee_telephone" name="o<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" id="o<?php echo $v_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($v_employees->employee_telephone->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_employees->lab_name->Visible) { // lab_name ?>
		<td data-name="lab_name">
<?php if ($v_employees->CurrentAction <> "F") { ?>
<?php if ($v_employees->lab_name->getSessionValue() <> "") { ?>
<span id="el$rowindex$_v_employees_lab_name" class="form-group v_employees_lab_name">
<span<?php echo $v_employees->lab_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->lab_name->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_v_employees_lab_name" class="form-group v_employees_lab_name">
<input type="text" data-table="v_employees" data-field="x_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->lab_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->lab_name->EditValue ?>"<?php echo $v_employees->lab_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_v_employees_lab_name" class="form-group v_employees_lab_name">
<span<?php echo $v_employees->lab_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->lab_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_lab_name" name="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="x<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_lab_name" name="o<?php echo $v_employees_grid->RowIndex ?>_lab_name" id="o<?php echo $v_employees_grid->RowIndex ?>_lab_name" value="<?php echo ew_HtmlEncode($v_employees->lab_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($v_employees->position_name->Visible) { // position_name ?>
		<td data-name="position_name">
<?php if ($v_employees->CurrentAction <> "F") { ?>
<?php if ($v_employees->position_name->getSessionValue() <> "") { ?>
<span id="el$rowindex$_v_employees_position_name" class="form-group v_employees_position_name">
<span<?php echo $v_employees->position_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->position_name->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_v_employees_position_name" class="form-group v_employees_position_name">
<input type="text" data-table="v_employees" data-field="x_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_employees->position_name->getPlaceHolder()) ?>" value="<?php echo $v_employees->position_name->EditValue ?>"<?php echo $v_employees->position_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_v_employees_position_name" class="form-group v_employees_position_name">
<span<?php echo $v_employees->position_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $v_employees->position_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="v_employees" data-field="x_position_name" name="x<?php echo $v_employees_grid->RowIndex ?>_position_name" id="x<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="v_employees" data-field="x_position_name" name="o<?php echo $v_employees_grid->RowIndex ?>_position_name" id="o<?php echo $v_employees_grid->RowIndex ?>_position_name" value="<?php echo ew_HtmlEncode($v_employees->position_name->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$v_employees_grid->ListOptions->Render("body", "right", $v_employees_grid->RowCnt);
?>
<script type="text/javascript">
fv_employeesgrid.UpdateOpts(<?php echo $v_employees_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($v_employees->CurrentMode == "add" || $v_employees->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $v_employees_grid->FormKeyCountName ?>" id="<?php echo $v_employees_grid->FormKeyCountName ?>" value="<?php echo $v_employees_grid->KeyCount ?>">
<?php echo $v_employees_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($v_employees->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $v_employees_grid->FormKeyCountName ?>" id="<?php echo $v_employees_grid->FormKeyCountName ?>" value="<?php echo $v_employees_grid->KeyCount ?>">
<?php echo $v_employees_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($v_employees->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="fv_employeesgrid">
</div>
<?php

// Close recordset
if ($v_employees_grid->Recordset)
	$v_employees_grid->Recordset->Close();
?>
<?php if ($v_employees_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($v_employees_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($v_employees_grid->TotalRecs == 0 && $v_employees->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($v_employees_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($v_employees->Export == "") { ?>
<script type="text/javascript">
fv_employeesgrid.Init();
</script>
<?php } ?>
<?php
$v_employees_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$v_employees_grid->Page_Terminate();
?>
