<?php include_once "worksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php

//
// Page class
//

$works_grid = NULL; // Initialize page object first

class cworks_grid extends cworks {

	// Page ID
	var $PageID = 'grid';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'works';

	// Page object name
	var $PageObjName = 'works_grid';

	// Grid form hidden field names
	var $FormName = 'fworksgrid';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = TRUE;
    var $AuditTrailOnEdit = TRUE;
    var $AuditTrailOnDelete = TRUE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$this->FormActionName .= '_' . $this->FormName;
		$this->FormKeyName .= '_' . $this->FormName;
		$this->FormOldKeyName .= '_' . $this->FormName;
		$this->FormBlankRowName .= '_' . $this->FormName;
		$this->FormKeyCountName .= '_' . $this->FormName;
		$GLOBALS["Grid"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (works)
		if (!isset($GLOBALS["works"]) || get_class($GLOBALS["works"]) == "cworks") {
			$GLOBALS["works"] = &$this;

//			$GLOBALS["MasterTable"] = &$GLOBALS["Table"];
//			if (!isset($GLOBALS["Table"])) $GLOBALS["Table"] = &$GLOBALS["works"];

		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'grid', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'works', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			}
		}

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Set up master detail parameters
		$this->SetUpMasterParms();

		// Setup other options
		$this->SetupOtherOptions();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Export
		global $EW_EXPORT, $works;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($works);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}

//		$GLOBALS["Table"] = &$GLOBALS["MasterTable"];
		unset($GLOBALS["Grid"]);
		if ($url == "")
			return;
		$this->Page_Redirecting($url);

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $ShowOtherOptions = FALSE;
	var $DisplayRecs = 100;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Set up records per page
			$this->SetUpDisplayRecs();

			// Handle reset command
			$this->ResetCmd();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 100; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records

		// Restore master/detail filter
		$this->DbMasterFilter = $this->GetMasterFilter(); // Restore master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Restore detail filter
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "periods") {
			global $periods;
			$rsmaster = $periods->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("periodslist.php"); // Return to master page
			} else {
				$periods->LoadListRowValues($rsmaster);
				$periods->RowType = EW_ROWTYPE_MASTER; // Master row
				$periods->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "projects") {
			global $projects;
			$rsmaster = $projects->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("projectslist.php"); // Return to master page
			} else {
				$projects->LoadListRowValues($rsmaster);
				$projects->RowType = EW_ROWTYPE_MASTER; // Master row
				$projects->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "tasks") {
			global $tasks;
			$rsmaster = $tasks->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("taskslist.php"); // Return to master page
			} else {
				$tasks->LoadListRowValues($rsmaster);
				$tasks->RowType = EW_ROWTYPE_MASTER; // Master row
				$tasks->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "v_employees") {
			global $v_employees;
			$rsmaster = $v_employees->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("v_employeeslist.php"); // Return to master page
			} else {
				$v_employees->LoadListRowValues($rsmaster);
				$v_employees->RowType = EW_ROWTYPE_MASTER; // Master row
				$v_employees->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_employees") {
			global $e_employees;
			$rsmaster = $e_employees->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("e_employeeslist.php"); // Return to master page
			} else {
				$e_employees->LoadListRowValues($rsmaster);
				$e_employees->RowType = EW_ROWTYPE_MASTER; // Master row
				$e_employees->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_tasks") {
			global $e_tasks;
			$rsmaster = $e_tasks->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("e_taskslist.php"); // Return to master page
			} else {
				$e_tasks->LoadListRowValues($rsmaster);
				$e_tasks->RowType = EW_ROWTYPE_MASTER; // Master row
				$e_tasks->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_tasks_finance") {
			global $e_tasks_finance;
			$rsmaster = $e_tasks_finance->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("e_tasks_financelist.php"); // Return to master page
			} else {
				$e_tasks_finance->LoadListRowValues($rsmaster);
				$e_tasks_finance->RowType = EW_ROWTYPE_MASTER; // Master row
				$e_tasks_finance->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}
	}

	// Set up number of records displayed per page
	function SetUpDisplayRecs() {
		$sWrk = @$_GET[EW_TABLE_REC_PER_PAGE];
		if ($sWrk <> "") {
			if (is_numeric($sWrk)) {
				$this->DisplayRecs = intval($sWrk);
			} else {
				if (strtolower($sWrk) == "all") { // Display all records
					$this->DisplayRecs = -1;
				} else {
					$this->DisplayRecs = 100; // Non-numeric, load default
				}
			}
			$this->setRecordsPerPage($this->DisplayRecs); // Save to Session

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->work_time->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}
		if ($this->AuditTrailOnEdit) $this->WriteAuditTrailDummy($Language->Phrase("BatchUpdateBegin")); // Batch update begin
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->AuditTrailOnEdit) $this->WriteAuditTrailDummy($Language->Phrase("BatchUpdateSuccess")); // Batch update success
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			if ($this->AuditTrailOnEdit) $this->WriteAuditTrailDummy($Language->Phrase("BatchUpdateRollback")); // Batch update rollback
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->work_id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->work_id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		if ($this->AuditTrailOnAdd) $this->WriteAuditTrailDummy($Language->Phrase("BatchInsertBegin")); // Batch insert begin
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			if ($rowaction == "insert") {
				$this->RowOldKey = strval($objForm->GetValue($this->FormOldKeyName));
				$this->LoadOldRecord(); // Load old recordset
			}
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->work_id->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->ClearInlineMode(); // Clear grid add mode and return
			return TRUE;
		}
		if ($bGridInsert) {

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->AuditTrailOnAdd) $this->WriteAuditTrailDummy($Language->Phrase("BatchInsertSuccess")); // Batch insert success
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			if ($this->AuditTrailOnAdd) $this->WriteAuditTrailDummy($Language->Phrase("BatchInsertRollback")); // Batch insert rollback
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_work_period_id") && $objForm->HasValue("o_work_period_id") && $this->work_period_id->CurrentValue <> $this->work_period_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_project_id") && $objForm->HasValue("o_work_project_id") && $this->work_project_id->CurrentValue <> $this->work_project_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_plan_id") && $objForm->HasValue("o_work_plan_id") && $this->work_plan_id->CurrentValue <> $this->work_plan_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_lab_id") && $objForm->HasValue("o_work_lab_id") && $this->work_lab_id->CurrentValue <> $this->work_lab_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_task_id") && $objForm->HasValue("o_work_task_id") && $this->work_task_id->CurrentValue <> $this->work_task_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_description") && $objForm->HasValue("o_work_description") && $this->work_description->CurrentValue <> $this->work_description->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_progress") && $objForm->HasValue("o_work_progress") && $this->work_progress->CurrentValue <> $this->work_progress->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_time") && $objForm->HasValue("o_work_time") && $this->work_time->CurrentValue <> $this->work_time->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_work_employee_id") && $objForm->HasValue("o_work_employee_id") && $this->work_employee_id->CurrentValue <> $this->work_employee_id->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset master/detail keys
			if ($this->Command == "resetall") {
				$this->setCurrentMasterTable(""); // Clear master table
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
				$this->work_period_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_task_id->setSessionValue("");
				$this->work_plan_id->setSessionValue("");
				$this->work_lab_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_employee_id->setSessionValue("");
				$this->work_employee_id->setSessionValue("");
				$this->work_task_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_plan_id->setSessionValue("");
				$this->work_lab_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_plan_id->setSessionValue("");
				$this->work_lab_id->setSessionValue("");
				$this->work_task_id->setSessionValue("");
			}

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = FALSE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($objForm->HasValue($this->FormOldKeyName))
				$this->RowOldKey = strval($objForm->GetValue($this->FormOldKeyName));
			if ($this->RowOldKey <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $OldKeyName . "\" id=\"" . $OldKeyName . "\" value=\"" . ew_HtmlEncode($this->RowOldKey) . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentMode == "add" || $this->CurrentMode == "copy" || $this->CurrentMode == "edit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}
		if ($this->CurrentMode == "edit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->work_id->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();
	}

	// Set record key
	function SetRecordKey(&$key, $rs) {
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs->fields('work_id');
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$option = &$this->OtherOptions["addedit"];
		$option->UseDropDownButton = FALSE;
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$option->UseButtonGroup = TRUE;
		$option->ButtonClass = "btn-sm"; // Class for button group
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if (($this->CurrentMode == "add" || $this->CurrentMode == "copy" || $this->CurrentMode == "edit") && $this->CurrentAction != "F") { // Check add/copy/edit mode
			if ($this->AllowAddDeleteRow) {
				$option = &$options["addedit"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
				$item = &$option->Add("addblankrow");
				$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
				$item->Visible = $Security->CanAdd();
				$this->ShowOtherOptions = $item->Visible;
			}
		}
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->work_period_id->CurrentValue = NULL;
		$this->work_period_id->OldValue = $this->work_period_id->CurrentValue;
		$this->work_project_id->CurrentValue = NULL;
		$this->work_project_id->OldValue = $this->work_project_id->CurrentValue;
		$this->work_plan_id->CurrentValue = NULL;
		$this->work_plan_id->OldValue = $this->work_plan_id->CurrentValue;
		$this->work_lab_id->CurrentValue = NULL;
		$this->work_lab_id->OldValue = $this->work_lab_id->CurrentValue;
		$this->work_task_id->CurrentValue = NULL;
		$this->work_task_id->OldValue = $this->work_task_id->CurrentValue;
		$this->work_description->CurrentValue = NULL;
		$this->work_description->OldValue = $this->work_description->CurrentValue;
		$this->work_progress->CurrentValue = 0;
		$this->work_progress->OldValue = $this->work_progress->CurrentValue;
		$this->work_time->CurrentValue = 0;
		$this->work_time->OldValue = $this->work_time->CurrentValue;
		$this->work_employee_id->CurrentValue = CurrentUserID();
		$this->work_employee_id->OldValue = $this->work_employee_id->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$objForm->FormName = $this->FormName;
		if (!$this->work_period_id->FldIsDetailKey) {
			$this->work_period_id->setFormValue($objForm->GetValue("x_work_period_id"));
		}
		$this->work_period_id->setOldValue($objForm->GetValue("o_work_period_id"));
		if (!$this->work_project_id->FldIsDetailKey) {
			$this->work_project_id->setFormValue($objForm->GetValue("x_work_project_id"));
		}
		$this->work_project_id->setOldValue($objForm->GetValue("o_work_project_id"));
		if (!$this->work_plan_id->FldIsDetailKey) {
			$this->work_plan_id->setFormValue($objForm->GetValue("x_work_plan_id"));
		}
		$this->work_plan_id->setOldValue($objForm->GetValue("o_work_plan_id"));
		if (!$this->work_lab_id->FldIsDetailKey) {
			$this->work_lab_id->setFormValue($objForm->GetValue("x_work_lab_id"));
		}
		$this->work_lab_id->setOldValue($objForm->GetValue("o_work_lab_id"));
		if (!$this->work_task_id->FldIsDetailKey) {
			$this->work_task_id->setFormValue($objForm->GetValue("x_work_task_id"));
		}
		$this->work_task_id->setOldValue($objForm->GetValue("o_work_task_id"));
		if (!$this->work_description->FldIsDetailKey) {
			$this->work_description->setFormValue($objForm->GetValue("x_work_description"));
		}
		$this->work_description->setOldValue($objForm->GetValue("o_work_description"));
		if (!$this->work_progress->FldIsDetailKey) {
			$this->work_progress->setFormValue($objForm->GetValue("x_work_progress"));
		}
		$this->work_progress->setOldValue($objForm->GetValue("o_work_progress"));
		if (!$this->work_time->FldIsDetailKey) {
			$this->work_time->setFormValue($objForm->GetValue("x_work_time"));
		}
		$this->work_time->setOldValue($objForm->GetValue("o_work_time"));
		if (!$this->work_employee_id->FldIsDetailKey) {
			$this->work_employee_id->setFormValue($objForm->GetValue("x_work_employee_id"));
		}
		$this->work_employee_id->setOldValue($objForm->GetValue("o_work_employee_id"));
		if (!$this->work_id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->work_id->setFormValue($objForm->GetValue("x_work_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->work_id->CurrentValue = $this->work_id->FormValue;
		$this->work_period_id->CurrentValue = $this->work_period_id->FormValue;
		$this->work_project_id->CurrentValue = $this->work_project_id->FormValue;
		$this->work_plan_id->CurrentValue = $this->work_plan_id->FormValue;
		$this->work_lab_id->CurrentValue = $this->work_lab_id->FormValue;
		$this->work_task_id->CurrentValue = $this->work_task_id->FormValue;
		$this->work_description->CurrentValue = $this->work_description->FormValue;
		$this->work_progress->CurrentValue = $this->work_progress->FormValue;
		$this->work_time->CurrentValue = $this->work_time->FormValue;
		$this->work_employee_id->CurrentValue = $this->work_employee_id->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->work_id->setDbValue($rs->fields('work_id'));
		$this->work_period_id->setDbValue($rs->fields('work_period_id'));
		$this->work_project_id->setDbValue($rs->fields('work_project_id'));
		$this->work_plan_id->setDbValue($rs->fields('work_plan_id'));
		$this->work_lab_id->setDbValue($rs->fields('work_lab_id'));
		$this->work_task_id->setDbValue($rs->fields('work_task_id'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
		$this->work_started->setDbValue($rs->fields('work_started'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->work_id->DbValue = $row['work_id'];
		$this->work_period_id->DbValue = $row['work_period_id'];
		$this->work_project_id->DbValue = $row['work_project_id'];
		$this->work_plan_id->DbValue = $row['work_plan_id'];
		$this->work_lab_id->DbValue = $row['work_lab_id'];
		$this->work_task_id->DbValue = $row['work_task_id'];
		$this->work_description->DbValue = $row['work_description'];
		$this->work_progress->DbValue = $row['work_progress'];
		$this->work_time->DbValue = $row['work_time'];
		$this->work_employee_id->DbValue = $row['work_employee_id'];
		$this->work_started->DbValue = $row['work_started'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		$arKeys[] = $this->RowOldKey;
		$cnt = count($arKeys);
		if ($cnt >= 1) {
			if (strval($arKeys[0]) <> "")
				$this->work_id->CurrentValue = strval($arKeys[0]); // work_id
			else
				$bValidKey = FALSE;
		} else {
			$bValidKey = FALSE;
		}

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->work_time->FormValue == $this->work_time->CurrentValue && is_numeric(ew_StrToFloat($this->work_time->CurrentValue)))
			$this->work_time->CurrentValue = ew_StrToFloat($this->work_time->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

			// work_period_id
			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";
			$this->work_project_id->TooltipValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";
			$this->work_plan_id->TooltipValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";
			$this->work_lab_id->TooltipValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";
			$this->work_task_id->TooltipValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";
			$this->work_description->TooltipValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";
			$this->work_progress->TooltipValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";
			$this->work_time->TooltipValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
			$this->work_employee_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// work_period_id
			$this->work_period_id->EditAttrs["class"] = "form-control";
			$this->work_period_id->EditCustomAttributes = "";
			if ($this->work_period_id->getSessionValue() <> "") {
				$this->work_period_id->CurrentValue = $this->work_period_id->getSessionValue();
				$this->work_period_id->OldValue = $this->work_period_id->CurrentValue;
			if (strval($this->work_period_id->CurrentValue) <> "") {
				$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
			$sWhereWrk = "";
			$lookuptblfilter = "`period_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `period_from` DESC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
				}
			} else {
				$this->work_period_id->ViewValue = NULL;
			}
			$this->work_period_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_period_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `periods`";
			$sWhereWrk = "";
			$lookuptblfilter = "`period_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_period_id->EditValue = $arwrk;
			}

			// work_project_id
			$this->work_project_id->EditAttrs["class"] = "form-control";
			$this->work_project_id->EditCustomAttributes = "";
			if ($this->work_project_id->getSessionValue() <> "") {
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
				$this->work_project_id->OldValue = $this->work_project_id->CurrentValue;
			if (strval($this->work_project_id->CurrentValue) <> "") {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
				}
			} else {
				$this->work_project_id->ViewValue = NULL;
			}
			$this->work_project_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_project_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_project_id->EditValue = $arwrk;
			}

			// work_plan_id
			$this->work_plan_id->EditAttrs["class"] = "form-control";
			$this->work_plan_id->EditCustomAttributes = "";
			if ($this->work_plan_id->getSessionValue() <> "") {
				$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
				$this->work_plan_id->OldValue = $this->work_plan_id->CurrentValue;
			if (strval($this->work_plan_id->CurrentValue) <> "") {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
				}
			} else {
				$this->work_plan_id->ViewValue = NULL;
			}
			$this->work_plan_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_plan_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `plan_project_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_plan_id->EditValue = $arwrk;
			}

			// work_lab_id
			$this->work_lab_id->EditAttrs["class"] = "form-control";
			$this->work_lab_id->EditCustomAttributes = "";
			if ($this->work_lab_id->getSessionValue() <> "") {
				$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
				$this->work_lab_id->OldValue = $this->work_lab_id->CurrentValue;
			if (strval($this->work_lab_id->CurrentValue) <> "") {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
				}
			} else {
				$this->work_lab_id->ViewValue = NULL;
			}
			$this->work_lab_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_lab_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_lab_id->EditValue = $arwrk;
			}

			// work_task_id
			$this->work_task_id->EditAttrs["class"] = "form-control";
			$this->work_task_id->EditCustomAttributes = "";
			if ($this->work_task_id->getSessionValue() <> "") {
				$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
				$this->work_task_id->OldValue = $this->work_task_id->CurrentValue;
			if (strval($this->work_task_id->CurrentValue) <> "") {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
				}
			} else {
				$this->work_task_id->ViewValue = NULL;
			}
			$this->work_task_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_task_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `task_plan_id` AS `SelectFilterFld`, `task_lab_id` AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_task_id->EditValue = $arwrk;
			}

			// work_description
			$this->work_description->EditAttrs["class"] = "form-control";
			$this->work_description->EditCustomAttributes = "";
			$this->work_description->EditValue = ew_HtmlEncode($this->work_description->CurrentValue);
			$this->work_description->PlaceHolder = ew_RemoveHtml($this->work_description->FldCaption());

			// work_progress
			$this->work_progress->EditAttrs["class"] = "form-control";
			$this->work_progress->EditCustomAttributes = "";
			$this->work_progress->EditValue = $this->work_progress->Options(TRUE);

			// work_time
			$this->work_time->EditAttrs["class"] = "form-control";
			$this->work_time->EditCustomAttributes = "";
			$this->work_time->EditValue = ew_HtmlEncode($this->work_time->CurrentValue);
			$this->work_time->PlaceHolder = ew_RemoveHtml($this->work_time->FldCaption());
			if (strval($this->work_time->EditValue) <> "" && is_numeric($this->work_time->EditValue)) {
			$this->work_time->EditValue = ew_FormatNumber($this->work_time->EditValue, -2, -1, -2, 0);
			$this->work_time->OldValue = $this->work_time->EditValue;
			}

			// work_employee_id
			$this->work_employee_id->EditAttrs["class"] = "form-control";
			$this->work_employee_id->EditCustomAttributes = "";
			if ($this->work_employee_id->getSessionValue() <> "") {
				$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
				$this->work_employee_id->OldValue = $this->work_employee_id->CurrentValue;
			if (strval($this->work_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
				}
			} else {
				$this->work_employee_id->ViewValue = NULL;
			}
			$this->work_employee_id->ViewCustomAttributes = "";
			} elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("gridcls")) { // Non system admin
				$this->work_employee_id->CurrentValue = CurrentUserID();
			if (strval($this->work_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_employee_id->EditValue = $this->work_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_employee_id->EditValue = $this->work_employee_id->CurrentValue;
				}
			} else {
				$this->work_employee_id->EditValue = NULL;
			}
			$this->work_employee_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_employee_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["works"]->UserIDAllow("gridcls")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_employee_id->EditValue = $arwrk;
			}

			// Add refer script
			// work_period_id

			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// work_period_id
			$this->work_period_id->EditAttrs["class"] = "form-control";
			$this->work_period_id->EditCustomAttributes = "";
			if (strval($this->work_period_id->CurrentValue) <> "") {
				$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
			$sWhereWrk = "";
			$lookuptblfilter = "`period_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `period_from` DESC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_period_id->EditValue = $this->work_period_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_period_id->EditValue = $this->work_period_id->CurrentValue;
				}
			} else {
				$this->work_period_id->EditValue = NULL;
			}
			$this->work_period_id->ViewCustomAttributes = "";

			// work_project_id
			$this->work_project_id->EditAttrs["class"] = "form-control";
			$this->work_project_id->EditCustomAttributes = "";
			if ($this->work_project_id->getSessionValue() <> "") {
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
				$this->work_project_id->OldValue = $this->work_project_id->CurrentValue;
			if (strval($this->work_project_id->CurrentValue) <> "") {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
				}
			} else {
				$this->work_project_id->ViewValue = NULL;
			}
			$this->work_project_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_project_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_project_id->EditValue = $arwrk;
			}

			// work_plan_id
			$this->work_plan_id->EditAttrs["class"] = "form-control";
			$this->work_plan_id->EditCustomAttributes = "";
			if ($this->work_plan_id->getSessionValue() <> "") {
				$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
				$this->work_plan_id->OldValue = $this->work_plan_id->CurrentValue;
			if (strval($this->work_plan_id->CurrentValue) <> "") {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
				}
			} else {
				$this->work_plan_id->ViewValue = NULL;
			}
			$this->work_plan_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_plan_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `plan_project_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_plan_id->EditValue = $arwrk;
			}

			// work_lab_id
			$this->work_lab_id->EditAttrs["class"] = "form-control";
			$this->work_lab_id->EditCustomAttributes = "";
			if ($this->work_lab_id->getSessionValue() <> "") {
				$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
				$this->work_lab_id->OldValue = $this->work_lab_id->CurrentValue;
			if (strval($this->work_lab_id->CurrentValue) <> "") {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
				}
			} else {
				$this->work_lab_id->ViewValue = NULL;
			}
			$this->work_lab_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_lab_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_lab_id->EditValue = $arwrk;
			}

			// work_task_id
			$this->work_task_id->EditAttrs["class"] = "form-control";
			$this->work_task_id->EditCustomAttributes = "";
			if ($this->work_task_id->getSessionValue() <> "") {
				$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
				$this->work_task_id->OldValue = $this->work_task_id->CurrentValue;
			if (strval($this->work_task_id->CurrentValue) <> "") {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
				}
			} else {
				$this->work_task_id->ViewValue = NULL;
			}
			$this->work_task_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_task_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `task_plan_id` AS `SelectFilterFld`, `task_lab_id` AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_task_id->EditValue = $arwrk;
			}

			// work_description
			$this->work_description->EditAttrs["class"] = "form-control";
			$this->work_description->EditCustomAttributes = "";
			$this->work_description->EditValue = ew_HtmlEncode($this->work_description->CurrentValue);
			$this->work_description->PlaceHolder = ew_RemoveHtml($this->work_description->FldCaption());

			// work_progress
			$this->work_progress->EditAttrs["class"] = "form-control";
			$this->work_progress->EditCustomAttributes = "";
			$this->work_progress->EditValue = $this->work_progress->Options(TRUE);

			// work_time
			$this->work_time->EditAttrs["class"] = "form-control";
			$this->work_time->EditCustomAttributes = "";
			$this->work_time->EditValue = ew_HtmlEncode($this->work_time->CurrentValue);
			$this->work_time->PlaceHolder = ew_RemoveHtml($this->work_time->FldCaption());
			if (strval($this->work_time->EditValue) <> "" && is_numeric($this->work_time->EditValue)) {
			$this->work_time->EditValue = ew_FormatNumber($this->work_time->EditValue, -2, -1, -2, 0);
			$this->work_time->OldValue = $this->work_time->EditValue;
			}

			// work_employee_id
			$this->work_employee_id->EditAttrs["class"] = "form-control";
			$this->work_employee_id->EditCustomAttributes = "";
			if ($this->work_employee_id->getSessionValue() <> "") {
				$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
				$this->work_employee_id->OldValue = $this->work_employee_id->CurrentValue;
			if (strval($this->work_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
				}
			} else {
				$this->work_employee_id->ViewValue = NULL;
			}
			$this->work_employee_id->ViewCustomAttributes = "";
			} elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("gridcls")) { // Non system admin
				$this->work_employee_id->CurrentValue = CurrentUserID();
			if (strval($this->work_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_employee_id->EditValue = $this->work_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_employee_id->EditValue = $this->work_employee_id->CurrentValue;
				}
			} else {
				$this->work_employee_id->EditValue = NULL;
			}
			$this->work_employee_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_employee_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["works"]->UserIDAllow("gridcls")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_employee_id->EditValue = $arwrk;
			}

			// Edit refer script
			// work_period_id

			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->work_period_id->FldIsDetailKey && !is_null($this->work_period_id->FormValue) && $this->work_period_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_period_id->FldCaption(), $this->work_period_id->ReqErrMsg));
		}
		if (!$this->work_task_id->FldIsDetailKey && !is_null($this->work_task_id->FormValue) && $this->work_task_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_task_id->FldCaption(), $this->work_task_id->ReqErrMsg));
		}
		if (!$this->work_progress->FldIsDetailKey && !is_null($this->work_progress->FormValue) && $this->work_progress->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_progress->FldCaption(), $this->work_progress->ReqErrMsg));
		}
		if (!$this->work_time->FldIsDetailKey && !is_null($this->work_time->FormValue) && $this->work_time->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_time->FldCaption(), $this->work_time->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->work_time->FormValue)) {
			ew_AddMessage($gsFormError, $this->work_time->FldErrMsg());
		}
		if (!$this->work_employee_id->FldIsDetailKey && !is_null($this->work_employee_id->FormValue) && $this->work_employee_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_employee_id->FldCaption(), $this->work_employee_id->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteBegin")); // Batch delete begin

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['work_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			if ($DeleteRows) {
				foreach ($rsold as $row)
					$this->WriteAuditTrailOnDelete($row);
			}
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteSuccess")); // Batch delete success
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// work_project_id
			$this->work_project_id->SetDbValueDef($rsnew, $this->work_project_id->CurrentValue, NULL, $this->work_project_id->ReadOnly);

			// work_plan_id
			$this->work_plan_id->SetDbValueDef($rsnew, $this->work_plan_id->CurrentValue, NULL, $this->work_plan_id->ReadOnly);

			// work_lab_id
			$this->work_lab_id->SetDbValueDef($rsnew, $this->work_lab_id->CurrentValue, NULL, $this->work_lab_id->ReadOnly);

			// work_task_id
			$this->work_task_id->SetDbValueDef($rsnew, $this->work_task_id->CurrentValue, NULL, $this->work_task_id->ReadOnly);

			// work_description
			$this->work_description->SetDbValueDef($rsnew, $this->work_description->CurrentValue, NULL, $this->work_description->ReadOnly);

			// work_progress
			$this->work_progress->SetDbValueDef($rsnew, $this->work_progress->CurrentValue, 0, $this->work_progress->ReadOnly);

			// work_time
			$this->work_time->SetDbValueDef($rsnew, $this->work_time->CurrentValue, 0, $this->work_time->ReadOnly);

			// work_employee_id
			$this->work_employee_id->SetDbValueDef($rsnew, $this->work_employee_id->CurrentValue, 0, $this->work_employee_id->ReadOnly);

			// Check referential integrity for master table 'periods'
			$bValidMasterRecord = TRUE;
			$sMasterFilter = $this->SqlMasterFilter_periods();
			$KeyValue = isset($rsnew['work_period_id']) ? $rsnew['work_period_id'] : $rsold['work_period_id'];
			if (strval($KeyValue) <> "") {
				$sMasterFilter = str_replace("@period_id@", ew_AdjustSql($KeyValue), $sMasterFilter);
			} else {
				$bValidMasterRecord = FALSE;
			}
			if ($bValidMasterRecord) {
				$rsmaster = $GLOBALS["periods"]->LoadRs($sMasterFilter);
				$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
				$rsmaster->Close();
			}
			if (!$bValidMasterRecord) {
				$sRelatedRecordMsg = str_replace("%t", "periods", $Language->Phrase("RelatedRecordRequired"));
				$this->setFailureMessage($sRelatedRecordMsg);
				$rs->Close();
				return FALSE;
			}

			// Check referential integrity for master table 'v_employees'
			$bValidMasterRecord = TRUE;
			$sMasterFilter = $this->SqlMasterFilter_v_employees();
			$KeyValue = isset($rsnew['work_employee_id']) ? $rsnew['work_employee_id'] : $rsold['work_employee_id'];
			if (strval($KeyValue) <> "") {
				$sMasterFilter = str_replace("@employee_id@", ew_AdjustSql($KeyValue), $sMasterFilter);
			} else {
				$bValidMasterRecord = FALSE;
			}
			if ($bValidMasterRecord) {
				$rsmaster = $GLOBALS["v_employees"]->LoadRs($sMasterFilter);
				$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
				$rsmaster->Close();
			}
			if (!$bValidMasterRecord) {
				$sRelatedRecordMsg = str_replace("%t", "v_employees", $Language->Phrase("RelatedRecordRequired"));
				$this->setFailureMessage($sRelatedRecordMsg);
				$rs->Close();
				return FALSE;
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;

		// Check if valid User ID
		$bValidUser = FALSE;
		if ($Security->CurrentUserID() <> "" && !ew_Empty($this->work_employee_id->CurrentValue) && !$Security->IsAdmin()) { // Non system admin
			$bValidUser = $Security->IsValidUserID($this->work_employee_id->CurrentValue);
			if (!$bValidUser) {
				$sUserIdMsg = str_replace("%c", CurrentUserID(), $Language->Phrase("UnAuthorizedUserID"));
				$sUserIdMsg = str_replace("%u", $this->work_employee_id->CurrentValue, $sUserIdMsg);
				$this->setFailureMessage($sUserIdMsg);
				return FALSE;
			}
		}

		// Set up foreign key field value from Session
			if ($this->getCurrentMasterTable() == "periods") {
				$this->work_period_id->CurrentValue = $this->work_period_id->getSessionValue();
			}
			if ($this->getCurrentMasterTable() == "projects") {
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
			}
			if ($this->getCurrentMasterTable() == "tasks") {
				$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
				$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
				$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
			}
			if ($this->getCurrentMasterTable() == "v_employees") {
				$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
			}
			if ($this->getCurrentMasterTable() == "e_employees") {
				$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
			}
			if ($this->getCurrentMasterTable() == "e_tasks") {
				$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
				$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
				$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
			}
			if ($this->getCurrentMasterTable() == "e_tasks_finance") {
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
				$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
				$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
				$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
			}

		// Check referential integrity for master table 'periods'
		$bValidMasterRecord = TRUE;
		$sMasterFilter = $this->SqlMasterFilter_periods();
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sMasterFilter = str_replace("@period_id@", ew_AdjustSql($this->work_period_id->CurrentValue, "DB"), $sMasterFilter);
		} else {
			$bValidMasterRecord = FALSE;
		}
		if ($bValidMasterRecord) {
			$rsmaster = $GLOBALS["periods"]->LoadRs($sMasterFilter);
			$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
			$rsmaster->Close();
		}
		if (!$bValidMasterRecord) {
			$sRelatedRecordMsg = str_replace("%t", "periods", $Language->Phrase("RelatedRecordRequired"));
			$this->setFailureMessage($sRelatedRecordMsg);
			return FALSE;
		}

		// Check referential integrity for master table 'v_employees'
		$bValidMasterRecord = TRUE;
		$sMasterFilter = $this->SqlMasterFilter_v_employees();
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sMasterFilter = str_replace("@employee_id@", ew_AdjustSql($this->work_employee_id->CurrentValue, "DB"), $sMasterFilter);
		} else {
			$bValidMasterRecord = FALSE;
		}
		if ($bValidMasterRecord) {
			$rsmaster = $GLOBALS["v_employees"]->LoadRs($sMasterFilter);
			$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
			$rsmaster->Close();
		}
		if (!$bValidMasterRecord) {
			$sRelatedRecordMsg = str_replace("%t", "v_employees", $Language->Phrase("RelatedRecordRequired"));
			$this->setFailureMessage($sRelatedRecordMsg);
			return FALSE;
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// work_period_id
		$this->work_period_id->SetDbValueDef($rsnew, $this->work_period_id->CurrentValue, 0, FALSE);

		// work_project_id
		$this->work_project_id->SetDbValueDef($rsnew, $this->work_project_id->CurrentValue, NULL, FALSE);

		// work_plan_id
		$this->work_plan_id->SetDbValueDef($rsnew, $this->work_plan_id->CurrentValue, NULL, FALSE);

		// work_lab_id
		$this->work_lab_id->SetDbValueDef($rsnew, $this->work_lab_id->CurrentValue, NULL, FALSE);

		// work_task_id
		$this->work_task_id->SetDbValueDef($rsnew, $this->work_task_id->CurrentValue, NULL, FALSE);

		// work_description
		$this->work_description->SetDbValueDef($rsnew, $this->work_description->CurrentValue, NULL, FALSE);

		// work_progress
		$this->work_progress->SetDbValueDef($rsnew, $this->work_progress->CurrentValue, 0, FALSE);

		// work_time
		$this->work_time->SetDbValueDef($rsnew, $this->work_time->CurrentValue, 0, FALSE);

		// work_employee_id
		$this->work_employee_id->SetDbValueDef($rsnew, $this->work_employee_id->CurrentValue, 0, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->work_id->setDbValue($conn->Insert_ID());
				$rsnew['work_id'] = $this->work_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->work_employee_id->CurrentValue);
		return TRUE;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {

		// Hide foreign keys
		$sMasterTblVar = $this->getCurrentMasterTable();
		if ($sMasterTblVar == "periods") {
			$this->work_period_id->Visible = FALSE;
			if ($GLOBALS["periods"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		if ($sMasterTblVar == "projects") {
			$this->work_project_id->Visible = FALSE;
			if ($GLOBALS["projects"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		if ($sMasterTblVar == "tasks") {
			$this->work_task_id->Visible = FALSE;
			if ($GLOBALS["tasks"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_plan_id->Visible = FALSE;
			if ($GLOBALS["tasks"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_lab_id->Visible = FALSE;
			if ($GLOBALS["tasks"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_project_id->Visible = FALSE;
			if ($GLOBALS["tasks"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		if ($sMasterTblVar == "v_employees") {
			$this->work_employee_id->Visible = FALSE;
			if ($GLOBALS["v_employees"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		if ($sMasterTblVar == "e_employees") {
			$this->work_employee_id->Visible = FALSE;
			if ($GLOBALS["e_employees"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		if ($sMasterTblVar == "e_tasks") {
			$this->work_task_id->Visible = FALSE;
			if ($GLOBALS["e_tasks"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_project_id->Visible = FALSE;
			if ($GLOBALS["e_tasks"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_plan_id->Visible = FALSE;
			if ($GLOBALS["e_tasks"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_lab_id->Visible = FALSE;
			if ($GLOBALS["e_tasks"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		if ($sMasterTblVar == "e_tasks_finance") {
			$this->work_project_id->Visible = FALSE;
			if ($GLOBALS["e_tasks_finance"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_plan_id->Visible = FALSE;
			if ($GLOBALS["e_tasks_finance"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_lab_id->Visible = FALSE;
			if ($GLOBALS["e_tasks_finance"]->EventCancelled) $this->EventCancelled = TRUE;
			$this->work_task_id->Visible = FALSE;
			if ($GLOBALS["e_tasks_finance"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'works';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnAdd) return;
		$table = 'works';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['work_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$newvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		global $Language;
		if (!$this->AuditTrailOnEdit) return;
		$table = 'works';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['work_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") { // Password Field
						$oldvalue = $Language->Phrase("PasswordMask");
						$newvalue = $Language->Phrase("PasswordMask");
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnDelete) return;
		$table = 'works';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['work_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$curUser = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$oldvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
