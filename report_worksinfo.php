<?php

// Global variable for table object
$report_works = NULL;

//
// Table class for report_works
//
class creport_works extends cTable {
	var $period_id;
	var $period_from;
	var $project_name;
	var $task_code;
	var $task_name;
	var $work_time;
	var $work_progress;
	var $work_description;
	var $work_employee_id;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'report_works';
		$this->TableName = 'report_works';
		$this->TableType = 'VIEW';

		// Update Table
		$this->UpdateTable = "`report_works`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 104; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// period_id
		$this->period_id = new cField('report_works', 'report_works', 'x_period_id', 'period_id', '`period_id`', '`period_id`', 3, -1, FALSE, '`period_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->period_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['period_id'] = &$this->period_id;

		// period_from
		$this->period_from = new cField('report_works', 'report_works', 'x_period_from', 'period_from', '`period_from`', 'DATE_FORMAT(`period_from`, \'%d-%m-%Y\')', 135, 7, FALSE, '`period_from`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->period_from->FldDefaultErrMsg = str_replace("%s", "-", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['period_from'] = &$this->period_from;

		// project_name
		$this->project_name = new cField('report_works', 'report_works', 'x_project_name', 'project_name', '`project_name`', '`project_name`', 201, -1, FALSE, '`project_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['project_name'] = &$this->project_name;

		// task_code
		$this->task_code = new cField('report_works', 'report_works', 'x_task_code', 'task_code', '`task_code`', '`task_code`', 200, -1, FALSE, '`task_code`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['task_code'] = &$this->task_code;

		// task_name
		$this->task_name = new cField('report_works', 'report_works', 'x_task_name', 'task_name', '`task_name`', '`task_name`', 201, -1, FALSE, '`task_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['task_name'] = &$this->task_name;

		// work_time
		$this->work_time = new cField('report_works', 'report_works', 'x_work_time', 'work_time', '`work_time`', '`work_time`', 200, -1, FALSE, '`work_time`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->work_time->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_time'] = &$this->work_time;

		// work_progress
		$this->work_progress = new cField('report_works', 'report_works', 'x_work_progress', 'work_progress', '`work_progress`', '`work_progress`', 3, -1, FALSE, '`work_progress`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_progress->OptionCount = 12;
		$this->work_progress->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_progress'] = &$this->work_progress;

		// work_description
		$this->work_description = new cField('report_works', 'report_works', 'x_work_description', 'work_description', '`work_description`', '`work_description`', 201, -1, FALSE, '`work_description`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['work_description'] = &$this->work_description;

		// work_employee_id
		$this->work_employee_id = new cField('report_works', 'report_works', 'x_work_employee_id', 'work_employee_id', '`work_employee_id`', '`work_employee_id`', 3, -1, FALSE, '`work_employee_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->work_employee_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_employee_id'] = &$this->work_employee_id;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Current master table name
	function getCurrentMasterTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE];
	}

	function setCurrentMasterTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE] = $v;
	}

	// Session master WHERE clause
	function GetMasterFilter() {

		// Master filter
		$sMasterFilter = "";
		if ($this->getCurrentMasterTable() == "reports") {
			if ($this->period_id->getSessionValue() <> "")
				$sMasterFilter .= "`period_id`=" . ew_QuotedValue($this->period_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_employee_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `employee_id`=" . ew_QuotedValue($this->work_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sMasterFilter;
	}

	// Session detail WHERE clause
	function GetDetailFilter() {

		// Detail filter
		$sDetailFilter = "";
		if ($this->getCurrentMasterTable() == "reports") {
			if ($this->period_id->getSessionValue() <> "")
				$sDetailFilter .= "`period_id`=" . ew_QuotedValue($this->period_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_employee_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_employee_id`=" . ew_QuotedValue($this->work_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sDetailFilter;
	}

	// Master filter
	function SqlMasterFilter_reports() {
		return "`period_id`=@period_id@ AND `employee_id`=@employee_id@";
	}

	// Detail filter
	function SqlDetailFilter_reports() {
		return "`period_id`=@period_id@ AND `work_employee_id`=@work_employee_id@";
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`report_works`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "`project_name` ASC,`task_code` ASC,`work_description` ASC";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		global $Security;

		// Add User ID filter
		if ($Security->CurrentUserID() <> "" && !$Security->IsAdmin()) { // Non system admin
			if ($this->getCurrentMasterTable() == "reports" || $this->getCurrentMasterTable() == "")
				$sFilter = $this->AddDetailUserIDFilter($sFilter, "reports"); // Add detail User ID filter
		}
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('period_id', $rs))
				ew_AddFilter($where, ew_QuotedName('period_id', $this->DBID) . '=' . ew_QuotedValue($rs['period_id'], $this->period_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`period_id` = @period_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->period_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@period_id@", ew_AdjustSql($this->period_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "report_workslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "report_workslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("report_worksview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("report_worksview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "report_worksadd.php?" . $this->UrlParm($parm);
		else
			$url = "report_worksadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("report_worksedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("report_worksadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("report_worksdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		if ($this->getCurrentMasterTable() == "reports" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_period_id=" . urlencode($this->period_id->CurrentValue);
			$url .= "&fk_employee_id=" . urlencode($this->work_employee_id->CurrentValue);
		}
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "period_id:" . ew_VarToJson($this->period_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->period_id->CurrentValue)) {
			$sUrl .= "period_id=" . urlencode($this->period_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["period_id"]))
				$arKeys[] = ew_StripSlashes($_POST["period_id"]);
			elseif (isset($_GET["period_id"]))
				$arKeys[] = ew_StripSlashes($_GET["period_id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->period_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->period_id->setDbValue($rs->fields('period_id'));
		$this->period_from->setDbValue($rs->fields('period_from'));
		$this->project_name->setDbValue($rs->fields('project_name'));
		$this->task_code->setDbValue($rs->fields('task_code'));
		$this->task_name->setDbValue($rs->fields('task_name'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// period_id
		// period_from
		// project_name
		// task_code
		// task_name
		// work_time
		// work_progress
		// work_description
		// work_employee_id
		// period_id

		$this->period_id->ViewValue = $this->period_id->CurrentValue;
		$this->period_id->ViewCustomAttributes = "";

		// period_from
		$this->period_from->ViewValue = $this->period_from->CurrentValue;
		$this->period_from->ViewValue = ew_FormatDateTime($this->period_from->ViewValue, 7);
		$this->period_from->ViewCustomAttributes = "";

		// project_name
		$this->project_name->ViewValue = $this->project_name->CurrentValue;
		$this->project_name->ViewCustomAttributes = "";

		// task_code
		$this->task_code->ViewValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->ViewValue = $this->task_name->CurrentValue;
		$this->task_name->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_employee_id
		$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
		$this->work_employee_id->ViewCustomAttributes = "";

		// period_id
		$this->period_id->LinkCustomAttributes = "";
		$this->period_id->HrefValue = "";
		$this->period_id->TooltipValue = "";

		// period_from
		$this->period_from->LinkCustomAttributes = "";
		$this->period_from->HrefValue = "";
		$this->period_from->TooltipValue = "";

		// project_name
		$this->project_name->LinkCustomAttributes = "";
		$this->project_name->HrefValue = "";
		$this->project_name->TooltipValue = "";

		// task_code
		$this->task_code->LinkCustomAttributes = "";
		$this->task_code->HrefValue = "";
		$this->task_code->TooltipValue = "";

		// task_name
		$this->task_name->LinkCustomAttributes = "";
		$this->task_name->HrefValue = "";
		$this->task_name->TooltipValue = "";

		// work_time
		$this->work_time->LinkCustomAttributes = "";
		$this->work_time->HrefValue = "";
		$this->work_time->TooltipValue = "";

		// work_progress
		$this->work_progress->LinkCustomAttributes = "";
		$this->work_progress->HrefValue = "";
		$this->work_progress->TooltipValue = "";

		// work_description
		$this->work_description->LinkCustomAttributes = "";
		$this->work_description->HrefValue = "";
		$this->work_description->TooltipValue = "";

		// work_employee_id
		$this->work_employee_id->LinkCustomAttributes = "";
		$this->work_employee_id->HrefValue = "";
		$this->work_employee_id->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// period_id
		$this->period_id->EditAttrs["class"] = "form-control";
		$this->period_id->EditCustomAttributes = "";
		$this->period_id->EditValue = $this->period_id->CurrentValue;
		$this->period_id->ViewCustomAttributes = "";

		// period_from
		$this->period_from->EditAttrs["class"] = "form-control";
		$this->period_from->EditCustomAttributes = "";
		$this->period_from->EditValue = ew_FormatDateTime($this->period_from->CurrentValue, 7);
		$this->period_from->PlaceHolder = ew_RemoveHtml($this->period_from->FldCaption());

		// project_name
		$this->project_name->EditAttrs["class"] = "form-control";
		$this->project_name->EditCustomAttributes = "";
		$this->project_name->EditValue = $this->project_name->CurrentValue;
		$this->project_name->PlaceHolder = ew_RemoveHtml($this->project_name->FldCaption());

		// task_code
		$this->task_code->EditAttrs["class"] = "form-control";
		$this->task_code->EditCustomAttributes = "";
		$this->task_code->EditValue = $this->task_code->CurrentValue;
		$this->task_code->PlaceHolder = ew_RemoveHtml($this->task_code->FldCaption());

		// task_name
		$this->task_name->EditAttrs["class"] = "form-control";
		$this->task_name->EditCustomAttributes = "";
		$this->task_name->EditValue = $this->task_name->CurrentValue;
		$this->task_name->PlaceHolder = ew_RemoveHtml($this->task_name->FldCaption());

		// work_time
		$this->work_time->EditAttrs["class"] = "form-control";
		$this->work_time->EditCustomAttributes = "";
		$this->work_time->EditValue = $this->work_time->CurrentValue;
		$this->work_time->PlaceHolder = ew_RemoveHtml($this->work_time->FldCaption());

		// work_progress
		$this->work_progress->EditAttrs["class"] = "form-control";
		$this->work_progress->EditCustomAttributes = "";
		$this->work_progress->EditValue = $this->work_progress->Options(TRUE);

		// work_description
		$this->work_description->EditAttrs["class"] = "form-control";
		$this->work_description->EditCustomAttributes = "";
		$this->work_description->EditValue = $this->work_description->CurrentValue;
		$this->work_description->PlaceHolder = ew_RemoveHtml($this->work_description->FldCaption());

		// work_employee_id
		$this->work_employee_id->EditAttrs["class"] = "form-control";
		$this->work_employee_id->EditCustomAttributes = "";
		if ($this->work_employee_id->getSessionValue() <> "") {
			$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
		$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
		$this->work_employee_id->ViewCustomAttributes = "";
		} else {
		$this->work_employee_id->EditValue = $this->work_employee_id->CurrentValue;
		$this->work_employee_id->PlaceHolder = ew_RemoveHtml($this->work_employee_id->FldCaption());
		}

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->period_id->Exportable) $Doc->ExportCaption($this->period_id);
					if ($this->period_from->Exportable) $Doc->ExportCaption($this->period_from);
					if ($this->project_name->Exportable) $Doc->ExportCaption($this->project_name);
					if ($this->task_code->Exportable) $Doc->ExportCaption($this->task_code);
					if ($this->task_name->Exportable) $Doc->ExportCaption($this->task_name);
					if ($this->work_time->Exportable) $Doc->ExportCaption($this->work_time);
					if ($this->work_progress->Exportable) $Doc->ExportCaption($this->work_progress);
					if ($this->work_description->Exportable) $Doc->ExportCaption($this->work_description);
					if ($this->work_employee_id->Exportable) $Doc->ExportCaption($this->work_employee_id);
				} else {
					if ($this->project_name->Exportable) $Doc->ExportCaption($this->project_name);
					if ($this->task_code->Exportable) $Doc->ExportCaption($this->task_code);
					if ($this->task_name->Exportable) $Doc->ExportCaption($this->task_name);
					if ($this->work_time->Exportable) $Doc->ExportCaption($this->work_time);
					if ($this->work_progress->Exportable) $Doc->ExportCaption($this->work_progress);
					if ($this->work_description->Exportable) $Doc->ExportCaption($this->work_description);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->period_id->Exportable) $Doc->ExportField($this->period_id);
						if ($this->period_from->Exportable) $Doc->ExportField($this->period_from);
						if ($this->project_name->Exportable) $Doc->ExportField($this->project_name);
						if ($this->task_code->Exportable) $Doc->ExportField($this->task_code);
						if ($this->task_name->Exportable) $Doc->ExportField($this->task_name);
						if ($this->work_time->Exportable) $Doc->ExportField($this->work_time);
						if ($this->work_progress->Exportable) $Doc->ExportField($this->work_progress);
						if ($this->work_description->Exportable) $Doc->ExportField($this->work_description);
						if ($this->work_employee_id->Exportable) $Doc->ExportField($this->work_employee_id);
					} else {
						if ($this->project_name->Exportable) $Doc->ExportField($this->project_name);
						if ($this->task_code->Exportable) $Doc->ExportField($this->task_code);
						if ($this->task_name->Exportable) $Doc->ExportField($this->task_name);
						if ($this->work_time->Exportable) $Doc->ExportField($this->work_time);
						if ($this->work_progress->Exportable) $Doc->ExportField($this->work_progress);
						if ($this->work_description->Exportable) $Doc->ExportField($this->work_description);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Add master User ID filter
	function AddMasterUserIDFilter($sFilter, $sCurrentMasterTable) {
		$sFilterWrk = $sFilter;
		if ($sCurrentMasterTable == "reports") {
			$sFilterWrk = $GLOBALS["reports"]->AddUserIDFilter($sFilterWrk);
		}
		return $sFilterWrk;
	}

	// Add detail User ID filter
	function AddDetailUserIDFilter($sFilter, $sCurrentMasterTable) {
		$sFilterWrk = $sFilter;
		if ($sCurrentMasterTable == "reports") {
			$mastertable = $GLOBALS["reports"];
			if (!$mastertable->UserIDAllow()) {
				$sSubqueryWrk = $mastertable->GetUserIDSubquery($this->period_id, $mastertable->period_id);
				ew_AddFilter($sFilterWrk, $sSubqueryWrk);
			}
			$mastertable = $GLOBALS["reports"];
			if (!$mastertable->UserIDAllow()) {
				$sSubqueryWrk = $mastertable->GetUserIDSubquery($this->work_employee_id, $mastertable->employee_id);
				ew_AddFilter($sFilterWrk, $sSubqueryWrk);
			}
		}
		return $sFilterWrk;
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
