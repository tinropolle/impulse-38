<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($tasks_grid)) $tasks_grid = new ctasks_grid();

// Page init
$tasks_grid->Page_Init();

// Page main
$tasks_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tasks_grid->Page_Render();
?>
<?php if ($tasks->Export == "") { ?>
<script type="text/javascript">

// Form object
var ftasksgrid = new ew_Form("ftasksgrid", "grid");
ftasksgrid.FormKeyCountName = '<?php echo $tasks_grid->FormKeyCountName ?>';

// Validate form
ftasksgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_task_project_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_project_id->FldCaption(), $tasks->task_project_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_plan_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_plan_id->FldCaption(), $tasks->task_plan_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_lab_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_lab_id->FldCaption(), $tasks->task_lab_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_code->FldCaption(), $tasks->task_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_name->FldCaption(), $tasks->task_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_from");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($tasks->task_from->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_task_to");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($tasks->task_to->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_task_hours_planned");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_hours_planned->FldCaption(), $tasks->task_hours_planned->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_hours_planned");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($tasks->task_hours_planned->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_task_hours_actual");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $tasks->task_hours_actual->FldCaption(), $tasks->task_hours_actual->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
ftasksgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "task_project_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_plan_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_lab_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_code", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_from", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_to", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_employee_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_coordinator_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_object", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_status_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_hours_planned", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_hours_actual", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_description", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_key", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_file", false)) return false;
	return true;
}

// Form_CustomValidate event
ftasksgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftasksgrid.ValidateRequired = true;
<?php } else { ?>
ftasksgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftasksgrid.Lists["x_task_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_task_plan_id"],"FilterFields":[],"Options":[],"Template":""};
ftasksgrid.Lists["x_task_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_task_project_id"],"ChildFields":[],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
ftasksgrid.Lists["x_task_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_task_employee_id"],"FilterFields":[],"Options":[],"Template":""};
ftasksgrid.Lists["x_task_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":["x_task_lab_id"],"ChildFields":[],"FilterFields":["x_employee_lab_id"],"Options":[],"Template":""};
ftasksgrid.Lists["x_task_coordinator_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftasksgrid.Lists["x_task_status_id"] = {"LinkField":"x_task_status_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_status_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<?php } ?>
<?php
if ($tasks->CurrentAction == "gridadd") {
	if ($tasks->CurrentMode == "copy") {
		$bSelectLimit = $tasks_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$tasks_grid->TotalRecs = $tasks->SelectRecordCount();
			$tasks_grid->Recordset = $tasks_grid->LoadRecordset($tasks_grid->StartRec-1, $tasks_grid->DisplayRecs);
		} else {
			if ($tasks_grid->Recordset = $tasks_grid->LoadRecordset())
				$tasks_grid->TotalRecs = $tasks_grid->Recordset->RecordCount();
		}
		$tasks_grid->StartRec = 1;
		$tasks_grid->DisplayRecs = $tasks_grid->TotalRecs;
	} else {
		$tasks->CurrentFilter = "0=1";
		$tasks_grid->StartRec = 1;
		$tasks_grid->DisplayRecs = $tasks->GridAddRowCount;
	}
	$tasks_grid->TotalRecs = $tasks_grid->DisplayRecs;
	$tasks_grid->StopRec = $tasks_grid->DisplayRecs;
} else {
	$bSelectLimit = $tasks_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($tasks_grid->TotalRecs <= 0)
			$tasks_grid->TotalRecs = $tasks->SelectRecordCount();
	} else {
		if (!$tasks_grid->Recordset && ($tasks_grid->Recordset = $tasks_grid->LoadRecordset()))
			$tasks_grid->TotalRecs = $tasks_grid->Recordset->RecordCount();
	}
	$tasks_grid->StartRec = 1;
	$tasks_grid->DisplayRecs = $tasks_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$tasks_grid->Recordset = $tasks_grid->LoadRecordset($tasks_grid->StartRec-1, $tasks_grid->DisplayRecs);

	// Set no record found message
	if ($tasks->CurrentAction == "" && $tasks_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$tasks_grid->setWarningMessage(ew_DeniedMsg());
		if ($tasks_grid->SearchWhere == "0=101")
			$tasks_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$tasks_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$tasks_grid->RenderOtherOptions();
?>
<?php $tasks_grid->ShowPageHeader(); ?>
<?php
$tasks_grid->ShowMessage();
?>
<?php if ($tasks_grid->TotalRecs > 0 || $tasks->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="ftasksgrid" class="ewForm form-inline">
<?php if ($tasks_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($tasks_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_tasks" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_tasksgrid" class="table ewTable">
<?php echo $tasks->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$tasks_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$tasks_grid->RenderListOptions();

// Render list options (header, left)
$tasks_grid->ListOptions->Render("header", "left");
?>
<?php if ($tasks->task_project_id->Visible) { // task_project_id ?>
	<?php if ($tasks->SortUrl($tasks->task_project_id) == "") { ?>
		<th data-name="task_project_id"><div id="elh_tasks_task_project_id" class="tasks_task_project_id"><div class="ewTableHeaderCaption"><?php echo $tasks->task_project_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_project_id"><div><div id="elh_tasks_task_project_id" class="tasks_task_project_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $tasks->task_project_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_project_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_project_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_plan_id->Visible) { // task_plan_id ?>
	<?php if ($tasks->SortUrl($tasks->task_plan_id) == "") { ?>
		<th data-name="task_plan_id"><div id="elh_tasks_task_plan_id" class="tasks_task_plan_id"><div class="ewTableHeaderCaption" style="width: 240px;"><?php echo $tasks->task_plan_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_plan_id"><div><div id="elh_tasks_task_plan_id" class="tasks_task_plan_id">
			<div class="ewTableHeaderBtn" style="width: 240px;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_plan_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_plan_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_plan_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_lab_id->Visible) { // task_lab_id ?>
	<?php if ($tasks->SortUrl($tasks->task_lab_id) == "") { ?>
		<th data-name="task_lab_id"><div id="elh_tasks_task_lab_id" class="tasks_task_lab_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_lab_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_lab_id"><div><div id="elh_tasks_task_lab_id" class="tasks_task_lab_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_lab_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_lab_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_lab_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_code->Visible) { // task_code ?>
	<?php if ($tasks->SortUrl($tasks->task_code) == "") { ?>
		<th data-name="task_code"><div id="elh_tasks_task_code" class="tasks_task_code"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_code->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_code"><div><div id="elh_tasks_task_code" class="tasks_task_code">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_code->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_code->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_code->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_name->Visible) { // task_name ?>
	<?php if ($tasks->SortUrl($tasks->task_name) == "") { ?>
		<th data-name="task_name"><div id="elh_tasks_task_name" class="tasks_task_name"><div class="ewTableHeaderCaption" style="width: 260px;"><?php echo $tasks->task_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_name"><div><div id="elh_tasks_task_name" class="tasks_task_name">
			<div class="ewTableHeaderBtn" style="width: 260px;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_from->Visible) { // task_from ?>
	<?php if ($tasks->SortUrl($tasks->task_from) == "") { ?>
		<th data-name="task_from"><div id="elh_tasks_task_from" class="tasks_task_from"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_from->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_from"><div><div id="elh_tasks_task_from" class="tasks_task_from">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_from->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_from->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_from->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_to->Visible) { // task_to ?>
	<?php if ($tasks->SortUrl($tasks->task_to) == "") { ?>
		<th data-name="task_to"><div id="elh_tasks_task_to" class="tasks_task_to"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_to->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_to"><div><div id="elh_tasks_task_to" class="tasks_task_to">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_to->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_to->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_to->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_employee_id->Visible) { // task_employee_id ?>
	<?php if ($tasks->SortUrl($tasks->task_employee_id) == "") { ?>
		<th data-name="task_employee_id"><div id="elh_tasks_task_employee_id" class="tasks_task_employee_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_employee_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_employee_id"><div><div id="elh_tasks_task_employee_id" class="tasks_task_employee_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_employee_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_employee_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_employee_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
	<?php if ($tasks->SortUrl($tasks->task_coordinator_id) == "") { ?>
		<th data-name="task_coordinator_id"><div id="elh_tasks_task_coordinator_id" class="tasks_task_coordinator_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_coordinator_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_coordinator_id"><div><div id="elh_tasks_task_coordinator_id" class="tasks_task_coordinator_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_coordinator_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_coordinator_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_coordinator_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_object->Visible) { // task_object ?>
	<?php if ($tasks->SortUrl($tasks->task_object) == "") { ?>
		<th data-name="task_object"><div id="elh_tasks_task_object" class="tasks_task_object"><div class="ewTableHeaderCaption"><?php echo $tasks->task_object->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_object"><div><div id="elh_tasks_task_object" class="tasks_task_object">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $tasks->task_object->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_object->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_object->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_status_id->Visible) { // task_status_id ?>
	<?php if ($tasks->SortUrl($tasks->task_status_id) == "") { ?>
		<th data-name="task_status_id"><div id="elh_tasks_task_status_id" class="tasks_task_status_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $tasks->task_status_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_status_id"><div><div id="elh_tasks_task_status_id" class="tasks_task_status_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_status_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_status_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_status_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_hours_planned->Visible) { // task_hours_planned ?>
	<?php if ($tasks->SortUrl($tasks->task_hours_planned) == "") { ?>
		<th data-name="task_hours_planned"><div id="elh_tasks_task_hours_planned" class="tasks_task_hours_planned"><div class="ewTableHeaderCaption"><?php echo $tasks->task_hours_planned->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_hours_planned"><div><div id="elh_tasks_task_hours_planned" class="tasks_task_hours_planned">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $tasks->task_hours_planned->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_hours_planned->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_hours_planned->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_hours_actual->Visible) { // task_hours_actual ?>
	<?php if ($tasks->SortUrl($tasks->task_hours_actual) == "") { ?>
		<th data-name="task_hours_actual"><div id="elh_tasks_task_hours_actual" class="tasks_task_hours_actual"><div class="ewTableHeaderCaption"><?php echo $tasks->task_hours_actual->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_hours_actual"><div><div id="elh_tasks_task_hours_actual" class="tasks_task_hours_actual">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $tasks->task_hours_actual->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_hours_actual->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_hours_actual->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_description->Visible) { // task_description ?>
	<?php if ($tasks->SortUrl($tasks->task_description) == "") { ?>
		<th data-name="task_description"><div id="elh_tasks_task_description" class="tasks_task_description"><div class="ewTableHeaderCaption" style="width: 260px;"><?php echo $tasks->task_description->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_description"><div><div id="elh_tasks_task_description" class="tasks_task_description">
			<div class="ewTableHeaderBtn" style="width: 260px;"><span class="ewTableHeaderCaption"><?php echo $tasks->task_description->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_description->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_description->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_key->Visible) { // task_key ?>
	<?php if ($tasks->SortUrl($tasks->task_key) == "") { ?>
		<th data-name="task_key"><div id="elh_tasks_task_key" class="tasks_task_key"><div class="ewTableHeaderCaption"><?php echo $tasks->task_key->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_key"><div><div id="elh_tasks_task_key" class="tasks_task_key">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $tasks->task_key->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_key->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_key->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($tasks->task_file->Visible) { // task_file ?>
	<?php if ($tasks->SortUrl($tasks->task_file) == "") { ?>
		<th data-name="task_file"><div id="elh_tasks_task_file" class="tasks_task_file"><div class="ewTableHeaderCaption"><?php echo $tasks->task_file->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_file"><div><div id="elh_tasks_task_file" class="tasks_task_file">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $tasks->task_file->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($tasks->task_file->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($tasks->task_file->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$tasks_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$tasks_grid->StartRec = 1;
$tasks_grid->StopRec = $tasks_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($tasks_grid->FormKeyCountName) && ($tasks->CurrentAction == "gridadd" || $tasks->CurrentAction == "gridedit" || $tasks->CurrentAction == "F")) {
		$tasks_grid->KeyCount = $objForm->GetValue($tasks_grid->FormKeyCountName);
		$tasks_grid->StopRec = $tasks_grid->StartRec + $tasks_grid->KeyCount - 1;
	}
}
$tasks_grid->RecCnt = $tasks_grid->StartRec - 1;
if ($tasks_grid->Recordset && !$tasks_grid->Recordset->EOF) {
	$tasks_grid->Recordset->MoveFirst();
	$bSelectLimit = $tasks_grid->UseSelectLimit;
	if (!$bSelectLimit && $tasks_grid->StartRec > 1)
		$tasks_grid->Recordset->Move($tasks_grid->StartRec - 1);
} elseif (!$tasks->AllowAddDeleteRow && $tasks_grid->StopRec == 0) {
	$tasks_grid->StopRec = $tasks->GridAddRowCount;
}

// Initialize aggregate
$tasks->RowType = EW_ROWTYPE_AGGREGATEINIT;
$tasks->ResetAttrs();
$tasks_grid->RenderRow();
if ($tasks->CurrentAction == "gridadd")
	$tasks_grid->RowIndex = 0;
if ($tasks->CurrentAction == "gridedit")
	$tasks_grid->RowIndex = 0;
while ($tasks_grid->RecCnt < $tasks_grid->StopRec) {
	$tasks_grid->RecCnt++;
	if (intval($tasks_grid->RecCnt) >= intval($tasks_grid->StartRec)) {
		$tasks_grid->RowCnt++;
		if ($tasks->CurrentAction == "gridadd" || $tasks->CurrentAction == "gridedit" || $tasks->CurrentAction == "F") {
			$tasks_grid->RowIndex++;
			$objForm->Index = $tasks_grid->RowIndex;
			if ($objForm->HasValue($tasks_grid->FormActionName))
				$tasks_grid->RowAction = strval($objForm->GetValue($tasks_grid->FormActionName));
			elseif ($tasks->CurrentAction == "gridadd")
				$tasks_grid->RowAction = "insert";
			else
				$tasks_grid->RowAction = "";
		}

		// Set up key count
		$tasks_grid->KeyCount = $tasks_grid->RowIndex;

		// Init row class and style
		$tasks->ResetAttrs();
		$tasks->CssClass = "";
		if ($tasks->CurrentAction == "gridadd") {
			if ($tasks->CurrentMode == "copy") {
				$tasks_grid->LoadRowValues($tasks_grid->Recordset); // Load row values
				$tasks_grid->SetRecordKey($tasks_grid->RowOldKey, $tasks_grid->Recordset); // Set old record key
			} else {
				$tasks_grid->LoadDefaultValues(); // Load default values
				$tasks_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$tasks_grid->LoadRowValues($tasks_grid->Recordset); // Load row values
		}
		$tasks->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($tasks->CurrentAction == "gridadd") // Grid add
			$tasks->RowType = EW_ROWTYPE_ADD; // Render add
		if ($tasks->CurrentAction == "gridadd" && $tasks->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$tasks_grid->RestoreCurrentRowFormValues($tasks_grid->RowIndex); // Restore form values
		if ($tasks->CurrentAction == "gridedit") { // Grid edit
			if ($tasks->EventCancelled) {
				$tasks_grid->RestoreCurrentRowFormValues($tasks_grid->RowIndex); // Restore form values
			}
			if ($tasks_grid->RowAction == "insert")
				$tasks->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$tasks->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($tasks->CurrentAction == "gridedit" && ($tasks->RowType == EW_ROWTYPE_EDIT || $tasks->RowType == EW_ROWTYPE_ADD) && $tasks->EventCancelled) // Update failed
			$tasks_grid->RestoreCurrentRowFormValues($tasks_grid->RowIndex); // Restore form values
		if ($tasks->RowType == EW_ROWTYPE_EDIT) // Edit row
			$tasks_grid->EditRowCnt++;
		if ($tasks->CurrentAction == "F") // Confirm row
			$tasks_grid->RestoreCurrentRowFormValues($tasks_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$tasks->RowAttrs = array_merge($tasks->RowAttrs, array('data-rowindex'=>$tasks_grid->RowCnt, 'id'=>'r' . $tasks_grid->RowCnt . '_tasks', 'data-rowtype'=>$tasks->RowType));

		// Render row
		$tasks_grid->RenderRow();

		// Render list options
		$tasks_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($tasks_grid->RowAction <> "delete" && $tasks_grid->RowAction <> "insertdelete" && !($tasks_grid->RowAction == "insert" && $tasks->CurrentAction == "F" && $tasks_grid->EmptyRow())) {
?>
	<tr<?php echo $tasks->RowAttributes() ?>>
<?php

// Render list options (body, left)
$tasks_grid->ListOptions->Render("body", "left", $tasks_grid->RowCnt);
?>
	<?php if ($tasks->task_project_id->Visible) { // task_project_id ?>
		<td data-name="task_project_id"<?php echo $tasks->task_project_id->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($tasks->task_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_project_id" class="form-group tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_project_id" class="form-group tasks_task_project_id">
<?php $tasks->task_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$tasks->task_project_id->EditAttrs["onchange"]; ?>
<select data-table="tasks" data-field="x_task_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_project_id->DisplayValueSeparator) ? json_encode($tasks->task_project_id->DisplayValueSeparator) : $tasks->task_project_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id"<?php echo $tasks->task_project_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_project_id->EditValue)) {
	$arwrk = $tasks->task_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_project_id->CurrentValue) ?>" selected><?php echo $tasks->task_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$tasks->task_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo $tasks->task_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_project_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($tasks->task_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_project_id" class="form-group tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_project_id" class="form-group tasks_task_project_id">
<?php $tasks->task_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$tasks->task_project_id->EditAttrs["onchange"]; ?>
<select data-table="tasks" data-field="x_task_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_project_id->DisplayValueSeparator) ? json_encode($tasks->task_project_id->DisplayValueSeparator) : $tasks->task_project_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id"<?php echo $tasks->task_project_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_project_id->EditValue)) {
	$arwrk = $tasks->task_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_project_id->CurrentValue) ?>" selected><?php echo $tasks->task_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$tasks->task_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo $tasks->task_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_project_id" class="tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<?php echo $tasks->task_project_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_project_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $tasks_grid->PageObjName . "_row_" . $tasks_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="tasks" data-field="x_task_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_id" value="<?php echo ew_HtmlEncode($tasks->task_id->CurrentValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_id" value="<?php echo ew_HtmlEncode($tasks->task_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT || $tasks->CurrentMode == "edit") { ?>
<input type="hidden" data-table="tasks" data-field="x_task_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_id" value="<?php echo ew_HtmlEncode($tasks->task_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($tasks->task_plan_id->Visible) { // task_plan_id ?>
		<td data-name="task_plan_id"<?php echo $tasks->task_plan_id->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($tasks->task_plan_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<select data-table="tasks" data-field="x_task_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_plan_id->DisplayValueSeparator) ? json_encode($tasks->task_plan_id->DisplayValueSeparator) : $tasks->task_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id"<?php echo $tasks->task_plan_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_plan_id->EditValue)) {
	$arwrk = $tasks->task_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_plan_id->CurrentValue) ?>" selected><?php echo $tasks->task_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$tasks->task_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$tasks->task_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $tasks->task_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo $tasks->task_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_plan_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($tasks->task_plan_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<select data-table="tasks" data-field="x_task_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_plan_id->DisplayValueSeparator) ? json_encode($tasks->task_plan_id->DisplayValueSeparator) : $tasks->task_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id"<?php echo $tasks->task_plan_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_plan_id->EditValue)) {
	$arwrk = $tasks->task_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_plan_id->CurrentValue) ?>" selected><?php echo $tasks->task_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$tasks->task_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$tasks->task_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $tasks->task_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo $tasks->task_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_plan_id" class="tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<?php echo $tasks->task_plan_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_plan_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_lab_id->Visible) { // task_lab_id ?>
		<td data-name="task_lab_id"<?php echo $tasks->task_lab_id->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_lab_id" class="form-group tasks_task_lab_id">
<?php $tasks->task_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$tasks->task_lab_id->EditAttrs["onchange"]; ?>
<select data-table="tasks" data-field="x_task_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_lab_id->DisplayValueSeparator) ? json_encode($tasks->task_lab_id->DisplayValueSeparator) : $tasks->task_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id"<?php echo $tasks->task_lab_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_lab_id->EditValue)) {
	$arwrk = $tasks->task_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_lab_id->CurrentValue) ?>" selected><?php echo $tasks->task_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$tasks->task_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo $tasks->task_lab_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="tasks" data-field="x_task_lab_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($tasks->task_lab_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_lab_id" class="form-group tasks_task_lab_id">
<?php $tasks->task_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$tasks->task_lab_id->EditAttrs["onchange"]; ?>
<select data-table="tasks" data-field="x_task_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_lab_id->DisplayValueSeparator) ? json_encode($tasks->task_lab_id->DisplayValueSeparator) : $tasks->task_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id"<?php echo $tasks->task_lab_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_lab_id->EditValue)) {
	$arwrk = $tasks->task_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_lab_id->CurrentValue) ?>" selected><?php echo $tasks->task_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$tasks->task_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo $tasks->task_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_lab_id" class="tasks_task_lab_id">
<span<?php echo $tasks->task_lab_id->ViewAttributes() ?>>
<?php echo $tasks->task_lab_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_lab_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($tasks->task_lab_id->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_lab_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($tasks->task_lab_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_code->Visible) { // task_code ?>
		<td data-name="task_code"<?php echo $tasks->task_code->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_code" class="form-group tasks_task_code">
<input type="text" data-table="tasks" data-field="x_task_code" name="x<?php echo $tasks_grid->RowIndex ?>_task_code" id="x<?php echo $tasks_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_code->getPlaceHolder()) ?>" value="<?php echo $tasks->task_code->EditValue ?>"<?php echo $tasks->task_code->EditAttributes() ?>>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_code" name="o<?php echo $tasks_grid->RowIndex ?>_task_code" id="o<?php echo $tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($tasks->task_code->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_code" class="form-group tasks_task_code">
<span<?php echo $tasks->task_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_code->EditValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_code" name="x<?php echo $tasks_grid->RowIndex ?>_task_code" id="x<?php echo $tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($tasks->task_code->CurrentValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_code" class="tasks_task_code">
<span<?php echo $tasks->task_code->ViewAttributes() ?>>
<?php echo $tasks->task_code->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_code" name="x<?php echo $tasks_grid->RowIndex ?>_task_code" id="x<?php echo $tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($tasks->task_code->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_code" name="o<?php echo $tasks_grid->RowIndex ?>_task_code" id="o<?php echo $tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($tasks->task_code->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_name->Visible) { // task_name ?>
		<td data-name="task_name"<?php echo $tasks->task_name->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_name" class="form-group tasks_task_name">
<textarea data-table="tasks" data-field="x_task_name" name="x<?php echo $tasks_grid->RowIndex ?>_task_name" id="x<?php echo $tasks_grid->RowIndex ?>_task_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($tasks->task_name->getPlaceHolder()) ?>"<?php echo $tasks->task_name->EditAttributes() ?>><?php echo $tasks->task_name->EditValue ?></textarea>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_name" name="o<?php echo $tasks_grid->RowIndex ?>_task_name" id="o<?php echo $tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($tasks->task_name->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_name" class="form-group tasks_task_name">
<textarea data-table="tasks" data-field="x_task_name" name="x<?php echo $tasks_grid->RowIndex ?>_task_name" id="x<?php echo $tasks_grid->RowIndex ?>_task_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($tasks->task_name->getPlaceHolder()) ?>"<?php echo $tasks->task_name->EditAttributes() ?>><?php echo $tasks->task_name->EditValue ?></textarea>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_name" class="tasks_task_name">
<span<?php echo $tasks->task_name->ViewAttributes() ?>>
<?php echo $tasks->task_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_name" name="x<?php echo $tasks_grid->RowIndex ?>_task_name" id="x<?php echo $tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($tasks->task_name->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_name" name="o<?php echo $tasks_grid->RowIndex ?>_task_name" id="o<?php echo $tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($tasks->task_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_from->Visible) { // task_from ?>
		<td data-name="task_from"<?php echo $tasks->task_from->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_from" class="form-group tasks_task_from">
<input type="text" data-table="tasks" data-field="x_task_from" data-format="7" name="x<?php echo $tasks_grid->RowIndex ?>_task_from" id="x<?php echo $tasks_grid->RowIndex ?>_task_from" placeholder="<?php echo ew_HtmlEncode($tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $tasks->task_from->EditValue ?>"<?php echo $tasks->task_from->EditAttributes() ?>>
<?php if (!$tasks->task_from->ReadOnly && !$tasks->task_from->Disabled && !isset($tasks->task_from->EditAttrs["readonly"]) && !isset($tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftasksgrid", "x<?php echo $tasks_grid->RowIndex ?>_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_from" name="o<?php echo $tasks_grid->RowIndex ?>_task_from" id="o<?php echo $tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($tasks->task_from->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_from" class="form-group tasks_task_from">
<input type="text" data-table="tasks" data-field="x_task_from" data-format="7" name="x<?php echo $tasks_grid->RowIndex ?>_task_from" id="x<?php echo $tasks_grid->RowIndex ?>_task_from" placeholder="<?php echo ew_HtmlEncode($tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $tasks->task_from->EditValue ?>"<?php echo $tasks->task_from->EditAttributes() ?>>
<?php if (!$tasks->task_from->ReadOnly && !$tasks->task_from->Disabled && !isset($tasks->task_from->EditAttrs["readonly"]) && !isset($tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftasksgrid", "x<?php echo $tasks_grid->RowIndex ?>_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_from" class="tasks_task_from">
<span<?php echo $tasks->task_from->ViewAttributes() ?>>
<?php echo $tasks->task_from->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_from" name="x<?php echo $tasks_grid->RowIndex ?>_task_from" id="x<?php echo $tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($tasks->task_from->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_from" name="o<?php echo $tasks_grid->RowIndex ?>_task_from" id="o<?php echo $tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($tasks->task_from->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_to->Visible) { // task_to ?>
		<td data-name="task_to"<?php echo $tasks->task_to->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_to" class="form-group tasks_task_to">
<input type="text" data-table="tasks" data-field="x_task_to" data-format="7" name="x<?php echo $tasks_grid->RowIndex ?>_task_to" id="x<?php echo $tasks_grid->RowIndex ?>_task_to" placeholder="<?php echo ew_HtmlEncode($tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $tasks->task_to->EditValue ?>"<?php echo $tasks->task_to->EditAttributes() ?>>
<?php if (!$tasks->task_to->ReadOnly && !$tasks->task_to->Disabled && !isset($tasks->task_to->EditAttrs["readonly"]) && !isset($tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftasksgrid", "x<?php echo $tasks_grid->RowIndex ?>_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_to" name="o<?php echo $tasks_grid->RowIndex ?>_task_to" id="o<?php echo $tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($tasks->task_to->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_to" class="form-group tasks_task_to">
<input type="text" data-table="tasks" data-field="x_task_to" data-format="7" name="x<?php echo $tasks_grid->RowIndex ?>_task_to" id="x<?php echo $tasks_grid->RowIndex ?>_task_to" placeholder="<?php echo ew_HtmlEncode($tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $tasks->task_to->EditValue ?>"<?php echo $tasks->task_to->EditAttributes() ?>>
<?php if (!$tasks->task_to->ReadOnly && !$tasks->task_to->Disabled && !isset($tasks->task_to->EditAttrs["readonly"]) && !isset($tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftasksgrid", "x<?php echo $tasks_grid->RowIndex ?>_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_to" class="tasks_task_to">
<span<?php echo $tasks->task_to->ViewAttributes() ?>>
<?php echo $tasks->task_to->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_to" name="x<?php echo $tasks_grid->RowIndex ?>_task_to" id="x<?php echo $tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($tasks->task_to->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_to" name="o<?php echo $tasks_grid->RowIndex ?>_task_to" id="o<?php echo $tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($tasks->task_to->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_employee_id->Visible) { // task_employee_id ?>
		<td data-name="task_employee_id"<?php echo $tasks->task_employee_id->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($tasks->task_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<select data-table="tasks" data-field="x_task_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_employee_id->DisplayValueSeparator) ? json_encode($tasks->task_employee_id->DisplayValueSeparator) : $tasks->task_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id"<?php echo $tasks->task_employee_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_employee_id->EditValue)) {
	$arwrk = $tasks->task_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_employee_id->CurrentValue) ?>" selected><?php echo $tasks->task_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "{filter}";
$tasks->task_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$tasks->task_employee_id->LookupFilters += array("f1" => "`employee_lab_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $tasks->task_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo $tasks->task_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_employee_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($tasks->task_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<select data-table="tasks" data-field="x_task_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_employee_id->DisplayValueSeparator) ? json_encode($tasks->task_employee_id->DisplayValueSeparator) : $tasks->task_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id"<?php echo $tasks->task_employee_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_employee_id->EditValue)) {
	$arwrk = $tasks->task_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_employee_id->CurrentValue) ?>" selected><?php echo $tasks->task_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "{filter}";
$tasks->task_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$tasks->task_employee_id->LookupFilters += array("f1" => "`employee_lab_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $tasks->task_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo $tasks->task_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_employee_id" class="tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<?php echo $tasks->task_employee_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_employee_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<td data-name="task_coordinator_id"<?php echo $tasks->task_coordinator_id->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_coordinator_id" class="form-group tasks_task_coordinator_id">
<select data-table="tasks" data-field="x_task_coordinator_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_coordinator_id->DisplayValueSeparator) ? json_encode($tasks->task_coordinator_id->DisplayValueSeparator) : $tasks->task_coordinator_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id"<?php echo $tasks->task_coordinator_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_coordinator_id->EditValue)) {
	$arwrk = $tasks->task_coordinator_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_coordinator_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_coordinator_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_coordinator_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->CurrentValue) ?>" selected><?php echo $tasks->task_coordinator_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_coordinator_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$tasks->task_coordinator_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_coordinator_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $tasks->task_coordinator_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo $tasks->task_coordinator_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="tasks" data-field="x_task_coordinator_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_coordinator_id" class="form-group tasks_task_coordinator_id">
<select data-table="tasks" data-field="x_task_coordinator_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_coordinator_id->DisplayValueSeparator) ? json_encode($tasks->task_coordinator_id->DisplayValueSeparator) : $tasks->task_coordinator_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id"<?php echo $tasks->task_coordinator_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_coordinator_id->EditValue)) {
	$arwrk = $tasks->task_coordinator_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_coordinator_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_coordinator_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_coordinator_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->CurrentValue) ?>" selected><?php echo $tasks->task_coordinator_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_coordinator_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$tasks->task_coordinator_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_coordinator_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $tasks->task_coordinator_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo $tasks->task_coordinator_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_coordinator_id" class="tasks_task_coordinator_id">
<span<?php echo $tasks->task_coordinator_id->ViewAttributes() ?>>
<?php echo $tasks->task_coordinator_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_coordinator_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_coordinator_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_object->Visible) { // task_object ?>
		<td data-name="task_object"<?php echo $tasks->task_object->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_object" class="form-group tasks_task_object">
<input type="text" data-table="tasks" data-field="x_task_object" name="x<?php echo $tasks_grid->RowIndex ?>_task_object" id="x<?php echo $tasks_grid->RowIndex ?>_task_object" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_object->getPlaceHolder()) ?>" value="<?php echo $tasks->task_object->EditValue ?>"<?php echo $tasks->task_object->EditAttributes() ?>>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_object" name="o<?php echo $tasks_grid->RowIndex ?>_task_object" id="o<?php echo $tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($tasks->task_object->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_object" class="form-group tasks_task_object">
<input type="text" data-table="tasks" data-field="x_task_object" name="x<?php echo $tasks_grid->RowIndex ?>_task_object" id="x<?php echo $tasks_grid->RowIndex ?>_task_object" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_object->getPlaceHolder()) ?>" value="<?php echo $tasks->task_object->EditValue ?>"<?php echo $tasks->task_object->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_object" class="tasks_task_object">
<span<?php echo $tasks->task_object->ViewAttributes() ?>>
<?php echo $tasks->task_object->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_object" name="x<?php echo $tasks_grid->RowIndex ?>_task_object" id="x<?php echo $tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($tasks->task_object->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_object" name="o<?php echo $tasks_grid->RowIndex ?>_task_object" id="o<?php echo $tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($tasks->task_object->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_status_id->Visible) { // task_status_id ?>
		<td data-name="task_status_id"<?php echo $tasks->task_status_id->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_status_id" class="form-group tasks_task_status_id">
<select data-table="tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_status_id->DisplayValueSeparator) ? json_encode($tasks->task_status_id->DisplayValueSeparator) : $tasks->task_status_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_status_id"<?php echo $tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_status_id->EditValue)) {
	$arwrk = $tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_status_id->CurrentValue) ?>" selected><?php echo $tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_status_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo $tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="tasks" data-field="x_task_status_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($tasks->task_status_id->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_status_id" class="form-group tasks_task_status_id">
<select data-table="tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_status_id->DisplayValueSeparator) ? json_encode($tasks->task_status_id->DisplayValueSeparator) : $tasks->task_status_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_status_id"<?php echo $tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_status_id->EditValue)) {
	$arwrk = $tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_status_id->CurrentValue) ?>" selected><?php echo $tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_status_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo $tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_status_id" class="tasks_task_status_id">
<span<?php echo $tasks->task_status_id->ViewAttributes() ?>>
<?php echo $tasks->task_status_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_status_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($tasks->task_status_id->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_status_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($tasks->task_status_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<td data-name="task_hours_planned"<?php echo $tasks->task_hours_planned->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_hours_planned" class="form-group tasks_task_hours_planned">
<input type="text" data-table="tasks" data-field="x_task_hours_planned" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $tasks->task_hours_planned->EditValue ?>"<?php echo $tasks->task_hours_planned->EditAttributes() ?>>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_planned" name="o<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="o<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($tasks->task_hours_planned->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_hours_planned" class="form-group tasks_task_hours_planned">
<input type="text" data-table="tasks" data-field="x_task_hours_planned" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $tasks->task_hours_planned->EditValue ?>"<?php echo $tasks->task_hours_planned->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_hours_planned" class="tasks_task_hours_planned">
<span<?php echo $tasks->task_hours_planned->ViewAttributes() ?>>
<?php echo $tasks->task_hours_planned->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_planned" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($tasks->task_hours_planned->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_hours_planned" name="o<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="o<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($tasks->task_hours_planned->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<td data-name="task_hours_actual"<?php echo $tasks->task_hours_actual->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_hours_actual" class="form-group tasks_task_hours_actual">
<input type="text" data-table="tasks" data-field="x_task_hours_actual" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" size="30" placeholder="<?php echo ew_HtmlEncode($tasks->task_hours_actual->getPlaceHolder()) ?>" value="<?php echo $tasks->task_hours_actual->EditValue ?>"<?php echo $tasks->task_hours_actual->EditAttributes() ?>>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_actual" name="o<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="o<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($tasks->task_hours_actual->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_hours_actual" class="form-group tasks_task_hours_actual">
<span<?php echo $tasks->task_hours_actual->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_hours_actual->EditValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_actual" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($tasks->task_hours_actual->CurrentValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_hours_actual" class="tasks_task_hours_actual">
<span<?php echo $tasks->task_hours_actual->ViewAttributes() ?>>
<?php echo $tasks->task_hours_actual->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_actual" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($tasks->task_hours_actual->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_hours_actual" name="o<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="o<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($tasks->task_hours_actual->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_description->Visible) { // task_description ?>
		<td data-name="task_description"<?php echo $tasks->task_description->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_description" class="form-group tasks_task_description">
<textarea data-table="tasks" data-field="x_task_description" name="x<?php echo $tasks_grid->RowIndex ?>_task_description" id="x<?php echo $tasks_grid->RowIndex ?>_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($tasks->task_description->getPlaceHolder()) ?>"<?php echo $tasks->task_description->EditAttributes() ?>><?php echo $tasks->task_description->EditValue ?></textarea>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_description" name="o<?php echo $tasks_grid->RowIndex ?>_task_description" id="o<?php echo $tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($tasks->task_description->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_description" class="form-group tasks_task_description">
<textarea data-table="tasks" data-field="x_task_description" name="x<?php echo $tasks_grid->RowIndex ?>_task_description" id="x<?php echo $tasks_grid->RowIndex ?>_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($tasks->task_description->getPlaceHolder()) ?>"<?php echo $tasks->task_description->EditAttributes() ?>><?php echo $tasks->task_description->EditValue ?></textarea>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_description" class="tasks_task_description">
<span<?php echo $tasks->task_description->ViewAttributes() ?>>
<?php echo $tasks->task_description->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_description" name="x<?php echo $tasks_grid->RowIndex ?>_task_description" id="x<?php echo $tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($tasks->task_description->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_description" name="o<?php echo $tasks_grid->RowIndex ?>_task_description" id="o<?php echo $tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($tasks->task_description->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_key->Visible) { // task_key ?>
		<td data-name="task_key"<?php echo $tasks->task_key->CellAttributes() ?>>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_key" class="form-group tasks_task_key">
<input type="text" data-table="tasks" data-field="x_task_key" name="x<?php echo $tasks_grid->RowIndex ?>_task_key" id="x<?php echo $tasks_grid->RowIndex ?>_task_key" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_key->getPlaceHolder()) ?>" value="<?php echo $tasks->task_key->EditValue ?>"<?php echo $tasks->task_key->EditAttributes() ?>>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_key" name="o<?php echo $tasks_grid->RowIndex ?>_task_key" id="o<?php echo $tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($tasks->task_key->OldValue) ?>">
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_key" class="form-group tasks_task_key">
<input type="text" data-table="tasks" data-field="x_task_key" name="x<?php echo $tasks_grid->RowIndex ?>_task_key" id="x<?php echo $tasks_grid->RowIndex ?>_task_key" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_key->getPlaceHolder()) ?>" value="<?php echo $tasks->task_key->EditValue ?>"<?php echo $tasks->task_key->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_key" class="tasks_task_key">
<span<?php echo $tasks->task_key->ViewAttributes() ?>>
<?php echo $tasks->task_key->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_key" name="x<?php echo $tasks_grid->RowIndex ?>_task_key" id="x<?php echo $tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($tasks->task_key->FormValue) ?>">
<input type="hidden" data-table="tasks" data-field="x_task_key" name="o<?php echo $tasks_grid->RowIndex ?>_task_key" id="o<?php echo $tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($tasks->task_key->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($tasks->task_file->Visible) { // task_file ?>
		<td data-name="task_file"<?php echo $tasks->task_file->CellAttributes() ?>>
<?php if ($tasks_grid->RowAction == "insert") { // Add record ?>
<span id="el$rowindex$_tasks_task_file" class="form-group tasks_task_file">
<div id="fd_x<?php echo $tasks_grid->RowIndex ?>_task_file">
<span title="<?php echo $tasks->task_file->FldTitle() ? $tasks->task_file->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($tasks->task_file->ReadOnly || $tasks->task_file->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="tasks" data-field="x_task_file" name="x<?php echo $tasks_grid->RowIndex ?>_task_file" id="x<?php echo $tasks_grid->RowIndex ?>_task_file"<?php echo $tasks->task_file->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fn_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="0">
<input type="hidden" name="fs_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fs_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="255">
<input type="hidden" name="fx_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fx_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fm_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $tasks_grid->RowIndex ?>_task_file" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_file" name="o<?php echo $tasks_grid->RowIndex ?>_task_file" id="o<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo ew_HtmlEncode($tasks->task_file->OldValue) ?>">
<?php } elseif ($tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_file" class="tasks_task_file">
<span<?php echo $tasks->task_file->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($tasks->task_file, $tasks->task_file->ListViewValue()) ?>
</span>
</span>
<?php } else  { // Edit record ?>
<span id="el<?php echo $tasks_grid->RowCnt ?>_tasks_task_file" class="form-group tasks_task_file">
<div id="fd_x<?php echo $tasks_grid->RowIndex ?>_task_file">
<span title="<?php echo $tasks->task_file->FldTitle() ? $tasks->task_file->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($tasks->task_file->ReadOnly || $tasks->task_file->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="tasks" data-field="x_task_file" name="x<?php echo $tasks_grid->RowIndex ?>_task_file" id="x<?php echo $tasks_grid->RowIndex ?>_task_file"<?php echo $tasks->task_file->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fn_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $tasks_grid->RowIndex ?>_task_file"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fs_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="255">
<input type="hidden" name="fx_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fx_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fm_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $tasks_grid->RowIndex ?>_task_file" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$tasks_grid->ListOptions->Render("body", "right", $tasks_grid->RowCnt);
?>
	</tr>
<?php if ($tasks->RowType == EW_ROWTYPE_ADD || $tasks->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
ftasksgrid.UpdateOpts(<?php echo $tasks_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($tasks->CurrentAction <> "gridadd" || $tasks->CurrentMode == "copy")
		if (!$tasks_grid->Recordset->EOF) $tasks_grid->Recordset->MoveNext();
}
?>
<?php
	if ($tasks->CurrentMode == "add" || $tasks->CurrentMode == "copy" || $tasks->CurrentMode == "edit") {
		$tasks_grid->RowIndex = '$rowindex$';
		$tasks_grid->LoadDefaultValues();

		// Set row properties
		$tasks->ResetAttrs();
		$tasks->RowAttrs = array_merge($tasks->RowAttrs, array('data-rowindex'=>$tasks_grid->RowIndex, 'id'=>'r0_tasks', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($tasks->RowAttrs["class"], "ewTemplate");
		$tasks->RowType = EW_ROWTYPE_ADD;

		// Render row
		$tasks_grid->RenderRow();

		// Render list options
		$tasks_grid->RenderListOptions();
		$tasks_grid->StartRowCnt = 0;
?>
	<tr<?php echo $tasks->RowAttributes() ?>>
<?php

// Render list options (body, left)
$tasks_grid->ListOptions->Render("body", "left", $tasks_grid->RowIndex);
?>
	<?php if ($tasks->task_project_id->Visible) { // task_project_id ?>
		<td data-name="task_project_id">
<?php if ($tasks->CurrentAction <> "F") { ?>
<?php if ($tasks->task_project_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_tasks_task_project_id" class="form-group tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_tasks_task_project_id" class="form-group tasks_task_project_id">
<?php $tasks->task_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$tasks->task_project_id->EditAttrs["onchange"]; ?>
<select data-table="tasks" data-field="x_task_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_project_id->DisplayValueSeparator) ? json_encode($tasks->task_project_id->DisplayValueSeparator) : $tasks->task_project_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id"<?php echo $tasks->task_project_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_project_id->EditValue)) {
	$arwrk = $tasks->task_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_project_id->CurrentValue) ?>" selected><?php echo $tasks->task_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$tasks->task_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo $tasks->task_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_project_id" class="form-group tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_project_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_project_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_project_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($tasks->task_project_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_plan_id->Visible) { // task_plan_id ?>
		<td data-name="task_plan_id">
<?php if ($tasks->CurrentAction <> "F") { ?>
<?php if ($tasks->task_plan_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<select data-table="tasks" data-field="x_task_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_plan_id->DisplayValueSeparator) ? json_encode($tasks->task_plan_id->DisplayValueSeparator) : $tasks->task_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id"<?php echo $tasks->task_plan_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_plan_id->EditValue)) {
	$arwrk = $tasks->task_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_plan_id->CurrentValue) ?>" selected><?php echo $tasks->task_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$tasks->task_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$tasks->task_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $tasks->task_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo $tasks->task_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_plan_id" class="form-group tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_plan_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_plan_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_plan_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($tasks->task_plan_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_lab_id->Visible) { // task_lab_id ?>
		<td data-name="task_lab_id">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_lab_id" class="form-group tasks_task_lab_id">
<?php $tasks->task_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$tasks->task_lab_id->EditAttrs["onchange"]; ?>
<select data-table="tasks" data-field="x_task_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_lab_id->DisplayValueSeparator) ? json_encode($tasks->task_lab_id->DisplayValueSeparator) : $tasks->task_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id"<?php echo $tasks->task_lab_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_lab_id->EditValue)) {
	$arwrk = $tasks->task_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_lab_id->CurrentValue) ?>" selected><?php echo $tasks->task_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$tasks->task_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo $tasks->task_lab_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_lab_id" class="form-group tasks_task_lab_id">
<span<?php echo $tasks->task_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_lab_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($tasks->task_lab_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_lab_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_lab_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($tasks->task_lab_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_code->Visible) { // task_code ?>
		<td data-name="task_code">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_code" class="form-group tasks_task_code">
<input type="text" data-table="tasks" data-field="x_task_code" name="x<?php echo $tasks_grid->RowIndex ?>_task_code" id="x<?php echo $tasks_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_code->getPlaceHolder()) ?>" value="<?php echo $tasks->task_code->EditValue ?>"<?php echo $tasks->task_code->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_code" class="form-group tasks_task_code">
<span<?php echo $tasks->task_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_code->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_code" name="x<?php echo $tasks_grid->RowIndex ?>_task_code" id="x<?php echo $tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($tasks->task_code->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_code" name="o<?php echo $tasks_grid->RowIndex ?>_task_code" id="o<?php echo $tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($tasks->task_code->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_name->Visible) { // task_name ?>
		<td data-name="task_name">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_name" class="form-group tasks_task_name">
<textarea data-table="tasks" data-field="x_task_name" name="x<?php echo $tasks_grid->RowIndex ?>_task_name" id="x<?php echo $tasks_grid->RowIndex ?>_task_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($tasks->task_name->getPlaceHolder()) ?>"<?php echo $tasks->task_name->EditAttributes() ?>><?php echo $tasks->task_name->EditValue ?></textarea>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_name" class="form-group tasks_task_name">
<span<?php echo $tasks->task_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_name" name="x<?php echo $tasks_grid->RowIndex ?>_task_name" id="x<?php echo $tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($tasks->task_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_name" name="o<?php echo $tasks_grid->RowIndex ?>_task_name" id="o<?php echo $tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($tasks->task_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_from->Visible) { // task_from ?>
		<td data-name="task_from">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_from" class="form-group tasks_task_from">
<input type="text" data-table="tasks" data-field="x_task_from" data-format="7" name="x<?php echo $tasks_grid->RowIndex ?>_task_from" id="x<?php echo $tasks_grid->RowIndex ?>_task_from" placeholder="<?php echo ew_HtmlEncode($tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $tasks->task_from->EditValue ?>"<?php echo $tasks->task_from->EditAttributes() ?>>
<?php if (!$tasks->task_from->ReadOnly && !$tasks->task_from->Disabled && !isset($tasks->task_from->EditAttrs["readonly"]) && !isset($tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftasksgrid", "x<?php echo $tasks_grid->RowIndex ?>_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_from" class="form-group tasks_task_from">
<span<?php echo $tasks->task_from->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_from->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_from" name="x<?php echo $tasks_grid->RowIndex ?>_task_from" id="x<?php echo $tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($tasks->task_from->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_from" name="o<?php echo $tasks_grid->RowIndex ?>_task_from" id="o<?php echo $tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($tasks->task_from->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_to->Visible) { // task_to ?>
		<td data-name="task_to">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_to" class="form-group tasks_task_to">
<input type="text" data-table="tasks" data-field="x_task_to" data-format="7" name="x<?php echo $tasks_grid->RowIndex ?>_task_to" id="x<?php echo $tasks_grid->RowIndex ?>_task_to" placeholder="<?php echo ew_HtmlEncode($tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $tasks->task_to->EditValue ?>"<?php echo $tasks->task_to->EditAttributes() ?>>
<?php if (!$tasks->task_to->ReadOnly && !$tasks->task_to->Disabled && !isset($tasks->task_to->EditAttrs["readonly"]) && !isset($tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftasksgrid", "x<?php echo $tasks_grid->RowIndex ?>_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_to" class="form-group tasks_task_to">
<span<?php echo $tasks->task_to->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_to->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_to" name="x<?php echo $tasks_grid->RowIndex ?>_task_to" id="x<?php echo $tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($tasks->task_to->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_to" name="o<?php echo $tasks_grid->RowIndex ?>_task_to" id="o<?php echo $tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($tasks->task_to->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_employee_id->Visible) { // task_employee_id ?>
		<td data-name="task_employee_id">
<?php if ($tasks->CurrentAction <> "F") { ?>
<?php if ($tasks->task_employee_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<select data-table="tasks" data-field="x_task_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_employee_id->DisplayValueSeparator) ? json_encode($tasks->task_employee_id->DisplayValueSeparator) : $tasks->task_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id"<?php echo $tasks->task_employee_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_employee_id->EditValue)) {
	$arwrk = $tasks->task_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_employee_id->CurrentValue) ?>" selected><?php echo $tasks->task_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "{filter}";
$tasks->task_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$tasks->task_employee_id->LookupFilters += array("f1" => "`employee_lab_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $tasks->task_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo $tasks->task_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_employee_id" class="form-group tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_employee_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_employee_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_employee_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($tasks->task_employee_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<td data-name="task_coordinator_id">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_coordinator_id" class="form-group tasks_task_coordinator_id">
<select data-table="tasks" data-field="x_task_coordinator_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_coordinator_id->DisplayValueSeparator) ? json_encode($tasks->task_coordinator_id->DisplayValueSeparator) : $tasks->task_coordinator_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id"<?php echo $tasks->task_coordinator_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_coordinator_id->EditValue)) {
	$arwrk = $tasks->task_coordinator_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_coordinator_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_coordinator_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_coordinator_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->CurrentValue) ?>" selected><?php echo $tasks->task_coordinator_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_coordinator_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$tasks->task_coordinator_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_coordinator_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $tasks->task_coordinator_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo $tasks->task_coordinator_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_coordinator_id" class="form-group tasks_task_coordinator_id">
<span<?php echo $tasks->task_coordinator_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_coordinator_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_coordinator_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_coordinator_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($tasks->task_coordinator_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_object->Visible) { // task_object ?>
		<td data-name="task_object">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_object" class="form-group tasks_task_object">
<input type="text" data-table="tasks" data-field="x_task_object" name="x<?php echo $tasks_grid->RowIndex ?>_task_object" id="x<?php echo $tasks_grid->RowIndex ?>_task_object" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_object->getPlaceHolder()) ?>" value="<?php echo $tasks->task_object->EditValue ?>"<?php echo $tasks->task_object->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_object" class="form-group tasks_task_object">
<span<?php echo $tasks->task_object->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_object->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_object" name="x<?php echo $tasks_grid->RowIndex ?>_task_object" id="x<?php echo $tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($tasks->task_object->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_object" name="o<?php echo $tasks_grid->RowIndex ?>_task_object" id="o<?php echo $tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($tasks->task_object->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_status_id->Visible) { // task_status_id ?>
		<td data-name="task_status_id">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_status_id" class="form-group tasks_task_status_id">
<select data-table="tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($tasks->task_status_id->DisplayValueSeparator) ? json_encode($tasks->task_status_id->DisplayValueSeparator) : $tasks->task_status_id->DisplayValueSeparator) ?>" id="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_status_id"<?php echo $tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($tasks->task_status_id->EditValue)) {
	$arwrk = $tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($tasks->task_status_id->CurrentValue) ?>" selected><?php echo $tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $tasks->task_status_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$tasks->Lookup_Selecting($tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="s_x<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo $tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_status_id" class="form-group tasks_task_status_id">
<span<?php echo $tasks->task_status_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_status_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_status_id" name="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="x<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($tasks->task_status_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_status_id" name="o<?php echo $tasks_grid->RowIndex ?>_task_status_id" id="o<?php echo $tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($tasks->task_status_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<td data-name="task_hours_planned">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_hours_planned" class="form-group tasks_task_hours_planned">
<input type="text" data-table="tasks" data-field="x_task_hours_planned" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $tasks->task_hours_planned->EditValue ?>"<?php echo $tasks->task_hours_planned->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_hours_planned" class="form-group tasks_task_hours_planned">
<span<?php echo $tasks->task_hours_planned->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_hours_planned->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_planned" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($tasks->task_hours_planned->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_hours_planned" name="o<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" id="o<?php echo $tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($tasks->task_hours_planned->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<td data-name="task_hours_actual">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_hours_actual" class="form-group tasks_task_hours_actual">
<input type="text" data-table="tasks" data-field="x_task_hours_actual" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" size="30" placeholder="<?php echo ew_HtmlEncode($tasks->task_hours_actual->getPlaceHolder()) ?>" value="<?php echo $tasks->task_hours_actual->EditValue ?>"<?php echo $tasks->task_hours_actual->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_hours_actual" class="form-group tasks_task_hours_actual">
<span<?php echo $tasks->task_hours_actual->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_hours_actual->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_hours_actual" name="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($tasks->task_hours_actual->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_hours_actual" name="o<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" id="o<?php echo $tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($tasks->task_hours_actual->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_description->Visible) { // task_description ?>
		<td data-name="task_description">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_description" class="form-group tasks_task_description">
<textarea data-table="tasks" data-field="x_task_description" name="x<?php echo $tasks_grid->RowIndex ?>_task_description" id="x<?php echo $tasks_grid->RowIndex ?>_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($tasks->task_description->getPlaceHolder()) ?>"<?php echo $tasks->task_description->EditAttributes() ?>><?php echo $tasks->task_description->EditValue ?></textarea>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_description" class="form-group tasks_task_description">
<span<?php echo $tasks->task_description->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_description->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_description" name="x<?php echo $tasks_grid->RowIndex ?>_task_description" id="x<?php echo $tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($tasks->task_description->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_description" name="o<?php echo $tasks_grid->RowIndex ?>_task_description" id="o<?php echo $tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($tasks->task_description->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_key->Visible) { // task_key ?>
		<td data-name="task_key">
<?php if ($tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_tasks_task_key" class="form-group tasks_task_key">
<input type="text" data-table="tasks" data-field="x_task_key" name="x<?php echo $tasks_grid->RowIndex ?>_task_key" id="x<?php echo $tasks_grid->RowIndex ?>_task_key" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($tasks->task_key->getPlaceHolder()) ?>" value="<?php echo $tasks->task_key->EditValue ?>"<?php echo $tasks->task_key->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_tasks_task_key" class="form-group tasks_task_key">
<span<?php echo $tasks->task_key->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $tasks->task_key->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_key" name="x<?php echo $tasks_grid->RowIndex ?>_task_key" id="x<?php echo $tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($tasks->task_key->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="tasks" data-field="x_task_key" name="o<?php echo $tasks_grid->RowIndex ?>_task_key" id="o<?php echo $tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($tasks->task_key->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($tasks->task_file->Visible) { // task_file ?>
		<td data-name="task_file">
<span id="el$rowindex$_tasks_task_file" class="form-group tasks_task_file">
<div id="fd_x<?php echo $tasks_grid->RowIndex ?>_task_file">
<span title="<?php echo $tasks->task_file->FldTitle() ? $tasks->task_file->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($tasks->task_file->ReadOnly || $tasks->task_file->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="tasks" data-field="x_task_file" name="x<?php echo $tasks_grid->RowIndex ?>_task_file" id="x<?php echo $tasks_grid->RowIndex ?>_task_file"<?php echo $tasks->task_file->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fn_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fa_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="0">
<input type="hidden" name="fs_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fs_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="255">
<input type="hidden" name="fx_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fx_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $tasks_grid->RowIndex ?>_task_file" id= "fm_x<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo $tasks->task_file->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $tasks_grid->RowIndex ?>_task_file" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="tasks" data-field="x_task_file" name="o<?php echo $tasks_grid->RowIndex ?>_task_file" id="o<?php echo $tasks_grid->RowIndex ?>_task_file" value="<?php echo ew_HtmlEncode($tasks->task_file->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$tasks_grid->ListOptions->Render("body", "right", $tasks_grid->RowCnt);
?>
<script type="text/javascript">
ftasksgrid.UpdateOpts(<?php echo $tasks_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($tasks->CurrentMode == "add" || $tasks->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $tasks_grid->FormKeyCountName ?>" id="<?php echo $tasks_grid->FormKeyCountName ?>" value="<?php echo $tasks_grid->KeyCount ?>">
<?php echo $tasks_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($tasks->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $tasks_grid->FormKeyCountName ?>" id="<?php echo $tasks_grid->FormKeyCountName ?>" value="<?php echo $tasks_grid->KeyCount ?>">
<?php echo $tasks_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($tasks->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="ftasksgrid">
</div>
<?php

// Close recordset
if ($tasks_grid->Recordset)
	$tasks_grid->Recordset->Close();
?>
<?php if ($tasks_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($tasks_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($tasks_grid->TotalRecs == 0 && $tasks->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($tasks_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($tasks->Export == "") { ?>
<script type="text/javascript">
ftasksgrid.Init();
</script>
<?php } ?>
<?php
$tasks_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$tasks_grid->Page_Terminate();
?>
