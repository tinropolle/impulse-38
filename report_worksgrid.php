<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($report_works_grid)) $report_works_grid = new creport_works_grid();

// Page init
$report_works_grid->Page_Init();

// Page main
$report_works_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$report_works_grid->Page_Render();
?>
<?php if ($report_works->Export == "") { ?>
<script type="text/javascript">

// Form object
var freport_worksgrid = new ew_Form("freport_worksgrid", "grid");
freport_worksgrid.FormKeyCountName = '<?php echo $report_works_grid->FormKeyCountName ?>';

// Validate form
freport_worksgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_project_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $report_works->project_name->FldCaption(), $report_works->project_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $report_works->task_code->FldCaption(), $report_works->task_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $report_works->task_name->FldCaption(), $report_works->task_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $report_works->work_time->FldCaption(), $report_works->work_time->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($report_works->work_time->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_work_progress");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $report_works->work_progress->FldCaption(), $report_works->work_progress->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_description");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $report_works->work_description->FldCaption(), $report_works->work_description->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
freport_worksgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "project_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_code", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_time", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_progress", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_description", false)) return false;
	return true;
}

// Form_CustomValidate event
freport_worksgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
freport_worksgrid.ValidateRequired = true;
<?php } else { ?>
freport_worksgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
freport_worksgrid.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
freport_worksgrid.Lists["x_work_progress"].Options = <?php echo json_encode($report_works->work_progress->Options()) ?>;

// Form object for search
</script>
<?php } ?>
<?php
if ($report_works->CurrentAction == "gridadd") {
	if ($report_works->CurrentMode == "copy") {
		$bSelectLimit = $report_works_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$report_works_grid->TotalRecs = $report_works->SelectRecordCount();
			$report_works_grid->Recordset = $report_works_grid->LoadRecordset($report_works_grid->StartRec-1, $report_works_grid->DisplayRecs);
		} else {
			if ($report_works_grid->Recordset = $report_works_grid->LoadRecordset())
				$report_works_grid->TotalRecs = $report_works_grid->Recordset->RecordCount();
		}
		$report_works_grid->StartRec = 1;
		$report_works_grid->DisplayRecs = $report_works_grid->TotalRecs;
	} else {
		$report_works->CurrentFilter = "0=1";
		$report_works_grid->StartRec = 1;
		$report_works_grid->DisplayRecs = $report_works->GridAddRowCount;
	}
	$report_works_grid->TotalRecs = $report_works_grid->DisplayRecs;
	$report_works_grid->StopRec = $report_works_grid->DisplayRecs;
} else {
	$bSelectLimit = $report_works_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($report_works_grid->TotalRecs <= 0)
			$report_works_grid->TotalRecs = $report_works->SelectRecordCount();
	} else {
		if (!$report_works_grid->Recordset && ($report_works_grid->Recordset = $report_works_grid->LoadRecordset()))
			$report_works_grid->TotalRecs = $report_works_grid->Recordset->RecordCount();
	}
	$report_works_grid->StartRec = 1;
	$report_works_grid->DisplayRecs = $report_works_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$report_works_grid->Recordset = $report_works_grid->LoadRecordset($report_works_grid->StartRec-1, $report_works_grid->DisplayRecs);

	// Set no record found message
	if ($report_works->CurrentAction == "" && $report_works_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$report_works_grid->setWarningMessage(ew_DeniedMsg());
		if ($report_works_grid->SearchWhere == "0=101")
			$report_works_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$report_works_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$report_works_grid->RenderOtherOptions();
?>
<?php $report_works_grid->ShowPageHeader(); ?>
<?php
$report_works_grid->ShowMessage();
?>
<?php if ($report_works_grid->TotalRecs > 0 || $report_works->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="freport_worksgrid" class="ewForm form-inline">
<?php if ($report_works_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($report_works_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_report_works" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_report_worksgrid" class="table ewTable">
<?php echo $report_works->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$report_works_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$report_works_grid->RenderListOptions();

// Render list options (header, left)
$report_works_grid->ListOptions->Render("header", "left");
?>
<?php if ($report_works->project_name->Visible) { // project_name ?>
	<?php if ($report_works->SortUrl($report_works->project_name) == "") { ?>
		<th data-name="project_name"><div id="elh_report_works_project_name" class="report_works_project_name"><div class="ewTableHeaderCaption"><?php echo $report_works->project_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="project_name"><div><div id="elh_report_works_project_name" class="report_works_project_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $report_works->project_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($report_works->project_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($report_works->project_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($report_works->task_code->Visible) { // task_code ?>
	<?php if ($report_works->SortUrl($report_works->task_code) == "") { ?>
		<th data-name="task_code"><div id="elh_report_works_task_code" class="report_works_task_code"><div class="ewTableHeaderCaption"><?php echo $report_works->task_code->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_code"><div><div id="elh_report_works_task_code" class="report_works_task_code">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $report_works->task_code->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($report_works->task_code->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($report_works->task_code->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($report_works->task_name->Visible) { // task_name ?>
	<?php if ($report_works->SortUrl($report_works->task_name) == "") { ?>
		<th data-name="task_name"><div id="elh_report_works_task_name" class="report_works_task_name"><div class="ewTableHeaderCaption"><?php echo $report_works->task_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_name"><div><div id="elh_report_works_task_name" class="report_works_task_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $report_works->task_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($report_works->task_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($report_works->task_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($report_works->work_time->Visible) { // work_time ?>
	<?php if ($report_works->SortUrl($report_works->work_time) == "") { ?>
		<th data-name="work_time"><div id="elh_report_works_work_time" class="report_works_work_time"><div class="ewTableHeaderCaption"><?php echo $report_works->work_time->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_time"><div><div id="elh_report_works_work_time" class="report_works_work_time">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $report_works->work_time->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($report_works->work_time->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($report_works->work_time->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($report_works->work_progress->Visible) { // work_progress ?>
	<?php if ($report_works->SortUrl($report_works->work_progress) == "") { ?>
		<th data-name="work_progress"><div id="elh_report_works_work_progress" class="report_works_work_progress"><div class="ewTableHeaderCaption"><?php echo $report_works->work_progress->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_progress"><div><div id="elh_report_works_work_progress" class="report_works_work_progress">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $report_works->work_progress->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($report_works->work_progress->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($report_works->work_progress->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($report_works->work_description->Visible) { // work_description ?>
	<?php if ($report_works->SortUrl($report_works->work_description) == "") { ?>
		<th data-name="work_description"><div id="elh_report_works_work_description" class="report_works_work_description"><div class="ewTableHeaderCaption"><?php echo $report_works->work_description->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_description"><div><div id="elh_report_works_work_description" class="report_works_work_description">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $report_works->work_description->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($report_works->work_description->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($report_works->work_description->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$report_works_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$report_works_grid->StartRec = 1;
$report_works_grid->StopRec = $report_works_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($report_works_grid->FormKeyCountName) && ($report_works->CurrentAction == "gridadd" || $report_works->CurrentAction == "gridedit" || $report_works->CurrentAction == "F")) {
		$report_works_grid->KeyCount = $objForm->GetValue($report_works_grid->FormKeyCountName);
		$report_works_grid->StopRec = $report_works_grid->StartRec + $report_works_grid->KeyCount - 1;
	}
}
$report_works_grid->RecCnt = $report_works_grid->StartRec - 1;
if ($report_works_grid->Recordset && !$report_works_grid->Recordset->EOF) {
	$report_works_grid->Recordset->MoveFirst();
	$bSelectLimit = $report_works_grid->UseSelectLimit;
	if (!$bSelectLimit && $report_works_grid->StartRec > 1)
		$report_works_grid->Recordset->Move($report_works_grid->StartRec - 1);
} elseif (!$report_works->AllowAddDeleteRow && $report_works_grid->StopRec == 0) {
	$report_works_grid->StopRec = $report_works->GridAddRowCount;
}

// Initialize aggregate
$report_works->RowType = EW_ROWTYPE_AGGREGATEINIT;
$report_works->ResetAttrs();
$report_works_grid->RenderRow();
if ($report_works->CurrentAction == "gridadd")
	$report_works_grid->RowIndex = 0;
if ($report_works->CurrentAction == "gridedit")
	$report_works_grid->RowIndex = 0;
while ($report_works_grid->RecCnt < $report_works_grid->StopRec) {
	$report_works_grid->RecCnt++;
	if (intval($report_works_grid->RecCnt) >= intval($report_works_grid->StartRec)) {
		$report_works_grid->RowCnt++;
		if ($report_works->CurrentAction == "gridadd" || $report_works->CurrentAction == "gridedit" || $report_works->CurrentAction == "F") {
			$report_works_grid->RowIndex++;
			$objForm->Index = $report_works_grid->RowIndex;
			if ($objForm->HasValue($report_works_grid->FormActionName))
				$report_works_grid->RowAction = strval($objForm->GetValue($report_works_grid->FormActionName));
			elseif ($report_works->CurrentAction == "gridadd")
				$report_works_grid->RowAction = "insert";
			else
				$report_works_grid->RowAction = "";
		}

		// Set up key count
		$report_works_grid->KeyCount = $report_works_grid->RowIndex;

		// Init row class and style
		$report_works->ResetAttrs();
		$report_works->CssClass = "";
		if ($report_works->CurrentAction == "gridadd") {
			if ($report_works->CurrentMode == "copy") {
				$report_works_grid->LoadRowValues($report_works_grid->Recordset); // Load row values
				$report_works_grid->SetRecordKey($report_works_grid->RowOldKey, $report_works_grid->Recordset); // Set old record key
			} else {
				$report_works_grid->LoadDefaultValues(); // Load default values
				$report_works_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$report_works_grid->LoadRowValues($report_works_grid->Recordset); // Load row values
		}
		$report_works->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($report_works->CurrentAction == "gridadd") // Grid add
			$report_works->RowType = EW_ROWTYPE_ADD; // Render add
		if ($report_works->CurrentAction == "gridadd" && $report_works->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$report_works_grid->RestoreCurrentRowFormValues($report_works_grid->RowIndex); // Restore form values
		if ($report_works->CurrentAction == "gridedit") { // Grid edit
			if ($report_works->EventCancelled) {
				$report_works_grid->RestoreCurrentRowFormValues($report_works_grid->RowIndex); // Restore form values
			}
			if ($report_works_grid->RowAction == "insert")
				$report_works->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$report_works->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($report_works->CurrentAction == "gridedit" && ($report_works->RowType == EW_ROWTYPE_EDIT || $report_works->RowType == EW_ROWTYPE_ADD) && $report_works->EventCancelled) // Update failed
			$report_works_grid->RestoreCurrentRowFormValues($report_works_grid->RowIndex); // Restore form values
		if ($report_works->RowType == EW_ROWTYPE_EDIT) // Edit row
			$report_works_grid->EditRowCnt++;
		if ($report_works->CurrentAction == "F") // Confirm row
			$report_works_grid->RestoreCurrentRowFormValues($report_works_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$report_works->RowAttrs = array_merge($report_works->RowAttrs, array('data-rowindex'=>$report_works_grid->RowCnt, 'id'=>'r' . $report_works_grid->RowCnt . '_report_works', 'data-rowtype'=>$report_works->RowType));

		// Render row
		$report_works_grid->RenderRow();

		// Render list options
		$report_works_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($report_works_grid->RowAction <> "delete" && $report_works_grid->RowAction <> "insertdelete" && !($report_works_grid->RowAction == "insert" && $report_works->CurrentAction == "F" && $report_works_grid->EmptyRow())) {
?>
	<tr<?php echo $report_works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$report_works_grid->ListOptions->Render("body", "left", $report_works_grid->RowCnt);
?>
	<?php if ($report_works->project_name->Visible) { // project_name ?>
		<td data-name="project_name"<?php echo $report_works->project_name->CellAttributes() ?>>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_project_name" class="form-group report_works_project_name">
<input type="text" data-table="report_works" data-field="x_project_name" name="x<?php echo $report_works_grid->RowIndex ?>_project_name" id="x<?php echo $report_works_grid->RowIndex ?>_project_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->project_name->getPlaceHolder()) ?>" value="<?php echo $report_works->project_name->EditValue ?>"<?php echo $report_works->project_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="report_works" data-field="x_project_name" name="o<?php echo $report_works_grid->RowIndex ?>_project_name" id="o<?php echo $report_works_grid->RowIndex ?>_project_name" value="<?php echo ew_HtmlEncode($report_works->project_name->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_project_name" class="form-group report_works_project_name">
<input type="text" data-table="report_works" data-field="x_project_name" name="x<?php echo $report_works_grid->RowIndex ?>_project_name" id="x<?php echo $report_works_grid->RowIndex ?>_project_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->project_name->getPlaceHolder()) ?>" value="<?php echo $report_works->project_name->EditValue ?>"<?php echo $report_works->project_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_project_name" class="report_works_project_name">
<span<?php echo $report_works->project_name->ViewAttributes() ?>>
<?php echo $report_works->project_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_project_name" name="x<?php echo $report_works_grid->RowIndex ?>_project_name" id="x<?php echo $report_works_grid->RowIndex ?>_project_name" value="<?php echo ew_HtmlEncode($report_works->project_name->FormValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_project_name" name="o<?php echo $report_works_grid->RowIndex ?>_project_name" id="o<?php echo $report_works_grid->RowIndex ?>_project_name" value="<?php echo ew_HtmlEncode($report_works->project_name->OldValue) ?>">
<?php } ?>
<a id="<?php echo $report_works_grid->PageObjName . "_row_" . $report_works_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="report_works" data-field="x_period_id" name="x<?php echo $report_works_grid->RowIndex ?>_period_id" id="x<?php echo $report_works_grid->RowIndex ?>_period_id" value="<?php echo ew_HtmlEncode($report_works->period_id->CurrentValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_period_id" name="o<?php echo $report_works_grid->RowIndex ?>_period_id" id="o<?php echo $report_works_grid->RowIndex ?>_period_id" value="<?php echo ew_HtmlEncode($report_works->period_id->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT || $report_works->CurrentMode == "edit") { ?>
<input type="hidden" data-table="report_works" data-field="x_period_id" name="x<?php echo $report_works_grid->RowIndex ?>_period_id" id="x<?php echo $report_works_grid->RowIndex ?>_period_id" value="<?php echo ew_HtmlEncode($report_works->period_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($report_works->task_code->Visible) { // task_code ?>
		<td data-name="task_code"<?php echo $report_works->task_code->CellAttributes() ?>>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_task_code" class="form-group report_works_task_code">
<input type="text" data-table="report_works" data-field="x_task_code" name="x<?php echo $report_works_grid->RowIndex ?>_task_code" id="x<?php echo $report_works_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->task_code->getPlaceHolder()) ?>" value="<?php echo $report_works->task_code->EditValue ?>"<?php echo $report_works->task_code->EditAttributes() ?>>
</span>
<input type="hidden" data-table="report_works" data-field="x_task_code" name="o<?php echo $report_works_grid->RowIndex ?>_task_code" id="o<?php echo $report_works_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($report_works->task_code->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_task_code" class="form-group report_works_task_code">
<input type="text" data-table="report_works" data-field="x_task_code" name="x<?php echo $report_works_grid->RowIndex ?>_task_code" id="x<?php echo $report_works_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->task_code->getPlaceHolder()) ?>" value="<?php echo $report_works->task_code->EditValue ?>"<?php echo $report_works->task_code->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_task_code" class="report_works_task_code">
<span<?php echo $report_works->task_code->ViewAttributes() ?>>
<?php echo $report_works->task_code->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_task_code" name="x<?php echo $report_works_grid->RowIndex ?>_task_code" id="x<?php echo $report_works_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($report_works->task_code->FormValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_task_code" name="o<?php echo $report_works_grid->RowIndex ?>_task_code" id="o<?php echo $report_works_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($report_works->task_code->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($report_works->task_name->Visible) { // task_name ?>
		<td data-name="task_name"<?php echo $report_works->task_name->CellAttributes() ?>>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_task_name" class="form-group report_works_task_name">
<input type="text" data-table="report_works" data-field="x_task_name" name="x<?php echo $report_works_grid->RowIndex ?>_task_name" id="x<?php echo $report_works_grid->RowIndex ?>_task_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->task_name->getPlaceHolder()) ?>" value="<?php echo $report_works->task_name->EditValue ?>"<?php echo $report_works->task_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="report_works" data-field="x_task_name" name="o<?php echo $report_works_grid->RowIndex ?>_task_name" id="o<?php echo $report_works_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($report_works->task_name->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_task_name" class="form-group report_works_task_name">
<input type="text" data-table="report_works" data-field="x_task_name" name="x<?php echo $report_works_grid->RowIndex ?>_task_name" id="x<?php echo $report_works_grid->RowIndex ?>_task_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->task_name->getPlaceHolder()) ?>" value="<?php echo $report_works->task_name->EditValue ?>"<?php echo $report_works->task_name->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_task_name" class="report_works_task_name">
<span<?php echo $report_works->task_name->ViewAttributes() ?>>
<?php echo $report_works->task_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_task_name" name="x<?php echo $report_works_grid->RowIndex ?>_task_name" id="x<?php echo $report_works_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($report_works->task_name->FormValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_task_name" name="o<?php echo $report_works_grid->RowIndex ?>_task_name" id="o<?php echo $report_works_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($report_works->task_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($report_works->work_time->Visible) { // work_time ?>
		<td data-name="work_time"<?php echo $report_works->work_time->CellAttributes() ?>>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_time" class="form-group report_works_work_time">
<input type="text" data-table="report_works" data-field="x_work_time" name="x<?php echo $report_works_grid->RowIndex ?>_work_time" id="x<?php echo $report_works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($report_works->work_time->getPlaceHolder()) ?>" value="<?php echo $report_works->work_time->EditValue ?>"<?php echo $report_works->work_time->EditAttributes() ?>>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_time" name="o<?php echo $report_works_grid->RowIndex ?>_work_time" id="o<?php echo $report_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($report_works->work_time->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_time" class="form-group report_works_work_time">
<input type="text" data-table="report_works" data-field="x_work_time" name="x<?php echo $report_works_grid->RowIndex ?>_work_time" id="x<?php echo $report_works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($report_works->work_time->getPlaceHolder()) ?>" value="<?php echo $report_works->work_time->EditValue ?>"<?php echo $report_works->work_time->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_time" class="report_works_work_time">
<span<?php echo $report_works->work_time->ViewAttributes() ?>>
<?php echo $report_works->work_time->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_time" name="x<?php echo $report_works_grid->RowIndex ?>_work_time" id="x<?php echo $report_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($report_works->work_time->FormValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_work_time" name="o<?php echo $report_works_grid->RowIndex ?>_work_time" id="o<?php echo $report_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($report_works->work_time->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($report_works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress"<?php echo $report_works->work_progress->CellAttributes() ?>>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_progress" class="form-group report_works_work_progress">
<select data-table="report_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($report_works->work_progress->DisplayValueSeparator) ? json_encode($report_works->work_progress->DisplayValueSeparator) : $report_works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $report_works_grid->RowIndex ?>_work_progress" name="x<?php echo $report_works_grid->RowIndex ?>_work_progress"<?php echo $report_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($report_works->work_progress->EditValue)) {
	$arwrk = $report_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($report_works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $report_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($report_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($report_works->work_progress->CurrentValue) ?>" selected><?php echo $report_works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $report_works->work_progress->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_progress" name="o<?php echo $report_works_grid->RowIndex ?>_work_progress" id="o<?php echo $report_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($report_works->work_progress->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_progress" class="form-group report_works_work_progress">
<select data-table="report_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($report_works->work_progress->DisplayValueSeparator) ? json_encode($report_works->work_progress->DisplayValueSeparator) : $report_works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $report_works_grid->RowIndex ?>_work_progress" name="x<?php echo $report_works_grid->RowIndex ?>_work_progress"<?php echo $report_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($report_works->work_progress->EditValue)) {
	$arwrk = $report_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($report_works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $report_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($report_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($report_works->work_progress->CurrentValue) ?>" selected><?php echo $report_works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $report_works->work_progress->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_progress" class="report_works_work_progress">
<span<?php echo $report_works->work_progress->ViewAttributes() ?>>
<?php echo $report_works->work_progress->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_progress" name="x<?php echo $report_works_grid->RowIndex ?>_work_progress" id="x<?php echo $report_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($report_works->work_progress->FormValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_work_progress" name="o<?php echo $report_works_grid->RowIndex ?>_work_progress" id="o<?php echo $report_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($report_works->work_progress->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($report_works->work_description->Visible) { // work_description ?>
		<td data-name="work_description"<?php echo $report_works->work_description->CellAttributes() ?>>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_description" class="form-group report_works_work_description">
<input type="text" data-table="report_works" data-field="x_work_description" name="x<?php echo $report_works_grid->RowIndex ?>_work_description" id="x<?php echo $report_works_grid->RowIndex ?>_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->work_description->getPlaceHolder()) ?>" value="<?php echo $report_works->work_description->EditValue ?>"<?php echo $report_works->work_description->EditAttributes() ?>>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_description" name="o<?php echo $report_works_grid->RowIndex ?>_work_description" id="o<?php echo $report_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($report_works->work_description->OldValue) ?>">
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_description" class="form-group report_works_work_description">
<input type="text" data-table="report_works" data-field="x_work_description" name="x<?php echo $report_works_grid->RowIndex ?>_work_description" id="x<?php echo $report_works_grid->RowIndex ?>_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->work_description->getPlaceHolder()) ?>" value="<?php echo $report_works->work_description->EditValue ?>"<?php echo $report_works->work_description->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($report_works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $report_works_grid->RowCnt ?>_report_works_work_description" class="report_works_work_description">
<span<?php echo $report_works->work_description->ViewAttributes() ?>>
<?php echo $report_works->work_description->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_description" name="x<?php echo $report_works_grid->RowIndex ?>_work_description" id="x<?php echo $report_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($report_works->work_description->FormValue) ?>">
<input type="hidden" data-table="report_works" data-field="x_work_description" name="o<?php echo $report_works_grid->RowIndex ?>_work_description" id="o<?php echo $report_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($report_works->work_description->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$report_works_grid->ListOptions->Render("body", "right", $report_works_grid->RowCnt);
?>
	</tr>
<?php if ($report_works->RowType == EW_ROWTYPE_ADD || $report_works->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
freport_worksgrid.UpdateOpts(<?php echo $report_works_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($report_works->CurrentAction <> "gridadd" || $report_works->CurrentMode == "copy")
		if (!$report_works_grid->Recordset->EOF) $report_works_grid->Recordset->MoveNext();
}
?>
<?php
	if ($report_works->CurrentMode == "add" || $report_works->CurrentMode == "copy" || $report_works->CurrentMode == "edit") {
		$report_works_grid->RowIndex = '$rowindex$';
		$report_works_grid->LoadDefaultValues();

		// Set row properties
		$report_works->ResetAttrs();
		$report_works->RowAttrs = array_merge($report_works->RowAttrs, array('data-rowindex'=>$report_works_grid->RowIndex, 'id'=>'r0_report_works', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($report_works->RowAttrs["class"], "ewTemplate");
		$report_works->RowType = EW_ROWTYPE_ADD;

		// Render row
		$report_works_grid->RenderRow();

		// Render list options
		$report_works_grid->RenderListOptions();
		$report_works_grid->StartRowCnt = 0;
?>
	<tr<?php echo $report_works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$report_works_grid->ListOptions->Render("body", "left", $report_works_grid->RowIndex);
?>
	<?php if ($report_works->project_name->Visible) { // project_name ?>
		<td data-name="project_name">
<?php if ($report_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_report_works_project_name" class="form-group report_works_project_name">
<input type="text" data-table="report_works" data-field="x_project_name" name="x<?php echo $report_works_grid->RowIndex ?>_project_name" id="x<?php echo $report_works_grid->RowIndex ?>_project_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->project_name->getPlaceHolder()) ?>" value="<?php echo $report_works->project_name->EditValue ?>"<?php echo $report_works->project_name->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_report_works_project_name" class="form-group report_works_project_name">
<span<?php echo $report_works->project_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $report_works->project_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_project_name" name="x<?php echo $report_works_grid->RowIndex ?>_project_name" id="x<?php echo $report_works_grid->RowIndex ?>_project_name" value="<?php echo ew_HtmlEncode($report_works->project_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="report_works" data-field="x_project_name" name="o<?php echo $report_works_grid->RowIndex ?>_project_name" id="o<?php echo $report_works_grid->RowIndex ?>_project_name" value="<?php echo ew_HtmlEncode($report_works->project_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($report_works->task_code->Visible) { // task_code ?>
		<td data-name="task_code">
<?php if ($report_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_report_works_task_code" class="form-group report_works_task_code">
<input type="text" data-table="report_works" data-field="x_task_code" name="x<?php echo $report_works_grid->RowIndex ?>_task_code" id="x<?php echo $report_works_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->task_code->getPlaceHolder()) ?>" value="<?php echo $report_works->task_code->EditValue ?>"<?php echo $report_works->task_code->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_report_works_task_code" class="form-group report_works_task_code">
<span<?php echo $report_works->task_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $report_works->task_code->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_task_code" name="x<?php echo $report_works_grid->RowIndex ?>_task_code" id="x<?php echo $report_works_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($report_works->task_code->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="report_works" data-field="x_task_code" name="o<?php echo $report_works_grid->RowIndex ?>_task_code" id="o<?php echo $report_works_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($report_works->task_code->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($report_works->task_name->Visible) { // task_name ?>
		<td data-name="task_name">
<?php if ($report_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_report_works_task_name" class="form-group report_works_task_name">
<input type="text" data-table="report_works" data-field="x_task_name" name="x<?php echo $report_works_grid->RowIndex ?>_task_name" id="x<?php echo $report_works_grid->RowIndex ?>_task_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->task_name->getPlaceHolder()) ?>" value="<?php echo $report_works->task_name->EditValue ?>"<?php echo $report_works->task_name->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_report_works_task_name" class="form-group report_works_task_name">
<span<?php echo $report_works->task_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $report_works->task_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_task_name" name="x<?php echo $report_works_grid->RowIndex ?>_task_name" id="x<?php echo $report_works_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($report_works->task_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="report_works" data-field="x_task_name" name="o<?php echo $report_works_grid->RowIndex ?>_task_name" id="o<?php echo $report_works_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($report_works->task_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($report_works->work_time->Visible) { // work_time ?>
		<td data-name="work_time">
<?php if ($report_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_report_works_work_time" class="form-group report_works_work_time">
<input type="text" data-table="report_works" data-field="x_work_time" name="x<?php echo $report_works_grid->RowIndex ?>_work_time" id="x<?php echo $report_works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($report_works->work_time->getPlaceHolder()) ?>" value="<?php echo $report_works->work_time->EditValue ?>"<?php echo $report_works->work_time->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_report_works_work_time" class="form-group report_works_work_time">
<span<?php echo $report_works->work_time->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $report_works->work_time->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_time" name="x<?php echo $report_works_grid->RowIndex ?>_work_time" id="x<?php echo $report_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($report_works->work_time->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="report_works" data-field="x_work_time" name="o<?php echo $report_works_grid->RowIndex ?>_work_time" id="o<?php echo $report_works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($report_works->work_time->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($report_works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress">
<?php if ($report_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_report_works_work_progress" class="form-group report_works_work_progress">
<select data-table="report_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($report_works->work_progress->DisplayValueSeparator) ? json_encode($report_works->work_progress->DisplayValueSeparator) : $report_works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $report_works_grid->RowIndex ?>_work_progress" name="x<?php echo $report_works_grid->RowIndex ?>_work_progress"<?php echo $report_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($report_works->work_progress->EditValue)) {
	$arwrk = $report_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($report_works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $report_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($report_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($report_works->work_progress->CurrentValue) ?>" selected><?php echo $report_works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $report_works->work_progress->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_report_works_work_progress" class="form-group report_works_work_progress">
<span<?php echo $report_works->work_progress->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $report_works->work_progress->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_progress" name="x<?php echo $report_works_grid->RowIndex ?>_work_progress" id="x<?php echo $report_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($report_works->work_progress->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="report_works" data-field="x_work_progress" name="o<?php echo $report_works_grid->RowIndex ?>_work_progress" id="o<?php echo $report_works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($report_works->work_progress->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($report_works->work_description->Visible) { // work_description ?>
		<td data-name="work_description">
<?php if ($report_works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_report_works_work_description" class="form-group report_works_work_description">
<input type="text" data-table="report_works" data-field="x_work_description" name="x<?php echo $report_works_grid->RowIndex ?>_work_description" id="x<?php echo $report_works_grid->RowIndex ?>_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($report_works->work_description->getPlaceHolder()) ?>" value="<?php echo $report_works->work_description->EditValue ?>"<?php echo $report_works->work_description->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_report_works_work_description" class="form-group report_works_work_description">
<span<?php echo $report_works->work_description->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $report_works->work_description->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="report_works" data-field="x_work_description" name="x<?php echo $report_works_grid->RowIndex ?>_work_description" id="x<?php echo $report_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($report_works->work_description->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="report_works" data-field="x_work_description" name="o<?php echo $report_works_grid->RowIndex ?>_work_description" id="o<?php echo $report_works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($report_works->work_description->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$report_works_grid->ListOptions->Render("body", "right", $report_works_grid->RowCnt);
?>
<script type="text/javascript">
freport_worksgrid.UpdateOpts(<?php echo $report_works_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($report_works->CurrentMode == "add" || $report_works->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $report_works_grid->FormKeyCountName ?>" id="<?php echo $report_works_grid->FormKeyCountName ?>" value="<?php echo $report_works_grid->KeyCount ?>">
<?php echo $report_works_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($report_works->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $report_works_grid->FormKeyCountName ?>" id="<?php echo $report_works_grid->FormKeyCountName ?>" value="<?php echo $report_works_grid->KeyCount ?>">
<?php echo $report_works_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($report_works->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="freport_worksgrid">
</div>
<?php

// Close recordset
if ($report_works_grid->Recordset)
	$report_works_grid->Recordset->Close();
?>
<?php if ($report_works_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($report_works_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($report_works_grid->TotalRecs == 0 && $report_works->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($report_works_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($report_works->Export == "") { ?>
<script type="text/javascript">
freport_worksgrid.Init();
</script>
<?php } ?>
<?php
$report_works_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$report_works_grid->Page_Terminate();
?>
