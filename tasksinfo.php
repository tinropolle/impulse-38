<?php

// Global variable for table object
$tasks = NULL;

//
// Table class for tasks
//
class ctasks extends cTable {
	var $task_id;
	var $task_project_id;
	var $task_plan_id;
	var $task_lab_id;
	var $task_code;
	var $task_name;
	var $task_from;
	var $task_to;
	var $task_employee_id;
	var $task_coordinator_id;
	var $task_object;
	var $task_status_id;
	var $task_hours_planned;
	var $task_cof_planned;
	var $task_money_planned;
	var $task_hours_actual;
	var $task_cof_actual;
	var $task_money_actual;
	var $task_description;
	var $task_key;
	var $task_file;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'tasks';
		$this->TableName = 'tasks';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`tasks`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 104; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// task_id
		$this->task_id = new cField('tasks', 'tasks', 'x_task_id', 'task_id', '`task_id`', '`task_id`', 3, -1, FALSE, '`task_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->task_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_id'] = &$this->task_id;

		// task_project_id
		$this->task_project_id = new cField('tasks', 'tasks', 'x_task_project_id', 'task_project_id', '`task_project_id`', '`task_project_id`', 3, -1, FALSE, '`task_project_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->task_project_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_project_id'] = &$this->task_project_id;

		// task_plan_id
		$this->task_plan_id = new cField('tasks', 'tasks', 'x_task_plan_id', 'task_plan_id', '`task_plan_id`', '`task_plan_id`', 3, -1, FALSE, '`task_plan_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->task_plan_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_plan_id'] = &$this->task_plan_id;

		// task_lab_id
		$this->task_lab_id = new cField('tasks', 'tasks', 'x_task_lab_id', 'task_lab_id', '`task_lab_id`', '`task_lab_id`', 3, -1, FALSE, '`task_lab_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->task_lab_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_lab_id'] = &$this->task_lab_id;

		// task_code
		$this->task_code = new cField('tasks', 'tasks', 'x_task_code', 'task_code', '`task_code`', '`task_code`', 200, -1, FALSE, '`task_code`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['task_code'] = &$this->task_code;

		// task_name
		$this->task_name = new cField('tasks', 'tasks', 'x_task_name', 'task_name', '`task_name`', '`task_name`', 201, -1, FALSE, '`task_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->fields['task_name'] = &$this->task_name;

		// task_from
		$this->task_from = new cField('tasks', 'tasks', 'x_task_from', 'task_from', '`task_from`', 'DATE_FORMAT(`task_from`, \'%d-%m-%Y\')', 135, 7, FALSE, '`task_from`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_from->FldDefaultErrMsg = str_replace("%s", "-", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['task_from'] = &$this->task_from;

		// task_to
		$this->task_to = new cField('tasks', 'tasks', 'x_task_to', 'task_to', '`task_to`', 'DATE_FORMAT(`task_to`, \'%d-%m-%Y\')', 135, 7, FALSE, '`task_to`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_to->FldDefaultErrMsg = str_replace("%s", "-", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['task_to'] = &$this->task_to;

		// task_employee_id
		$this->task_employee_id = new cField('tasks', 'tasks', 'x_task_employee_id', 'task_employee_id', '`task_employee_id`', '`task_employee_id`', 3, -1, FALSE, '`task_employee_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->task_employee_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_employee_id'] = &$this->task_employee_id;

		// task_coordinator_id
		$this->task_coordinator_id = new cField('tasks', 'tasks', 'x_task_coordinator_id', 'task_coordinator_id', '`task_coordinator_id`', '`task_coordinator_id`', 3, -1, FALSE, '`task_coordinator_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->task_coordinator_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_coordinator_id'] = &$this->task_coordinator_id;

		// task_object
		$this->task_object = new cField('tasks', 'tasks', 'x_task_object', 'task_object', '`task_object`', '`task_object`', 200, -1, FALSE, '`task_object`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['task_object'] = &$this->task_object;

		// task_status_id
		$this->task_status_id = new cField('tasks', 'tasks', 'x_task_status_id', 'task_status_id', '`task_status_id`', '`task_status_id`', 3, -1, FALSE, '`task_status_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->task_status_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['task_status_id'] = &$this->task_status_id;

		// task_hours_planned
		$this->task_hours_planned = new cField('tasks', 'tasks', 'x_task_hours_planned', 'task_hours_planned', '`task_hours_planned`', '`task_hours_planned`', 5, -1, FALSE, '`task_hours_planned`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_hours_planned->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['task_hours_planned'] = &$this->task_hours_planned;

		// task_cof_planned
		$this->task_cof_planned = new cField('tasks', 'tasks', 'x_task_cof_planned', 'task_cof_planned', '`task_cof_planned`', '`task_cof_planned`', 5, -1, FALSE, '`task_cof_planned`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_cof_planned->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['task_cof_planned'] = &$this->task_cof_planned;

		// task_money_planned
		$this->task_money_planned = new cField('tasks', 'tasks', 'x_task_money_planned', 'task_money_planned', '`task_money_planned`', '`task_money_planned`', 131, -1, FALSE, '`task_money_planned`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_money_planned->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['task_money_planned'] = &$this->task_money_planned;

		// task_hours_actual
		$this->task_hours_actual = new cField('tasks', 'tasks', 'x_task_hours_actual', 'task_hours_actual', '`task_hours_actual`', '`task_hours_actual`', 5, -1, FALSE, '`task_hours_actual`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_hours_actual->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['task_hours_actual'] = &$this->task_hours_actual;

		// task_cof_actual
		$this->task_cof_actual = new cField('tasks', 'tasks', 'x_task_cof_actual', 'task_cof_actual', '`task_cof_actual`', '`task_cof_actual`', 5, -1, FALSE, '`task_cof_actual`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_cof_actual->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['task_cof_actual'] = &$this->task_cof_actual;

		// task_money_actual
		$this->task_money_actual = new cField('tasks', 'tasks', 'x_task_money_actual', 'task_money_actual', '`task_money_actual`', '`task_money_actual`', 131, -1, FALSE, '`task_money_actual`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->task_money_actual->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['task_money_actual'] = &$this->task_money_actual;

		// task_description
		$this->task_description = new cField('tasks', 'tasks', 'x_task_description', 'task_description', '`task_description`', '`task_description`', 200, -1, FALSE, '`task_description`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->fields['task_description'] = &$this->task_description;

		// task_key
		$this->task_key = new cField('tasks', 'tasks', 'x_task_key', 'task_key', '`task_key`', '`task_key`', 200, -1, FALSE, '`task_key`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['task_key'] = &$this->task_key;

		// task_file
		$this->task_file = new cField('tasks', 'tasks', 'x_task_file', 'task_file', '`task_file`', '`task_file`', 200, -1, TRUE, '`task_file`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->fields['task_file'] = &$this->task_file;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			if (strpos($sSortField, "task_code") !== false)
{
$this->setSessionOrderBy(
	" SUBSTR(task_code, 1, POSITION('.' IN task_code) - 1) " . $sThisSort . ", " .
	" CAST(SUBSTR(task_code, POSITION('.' IN task_code) + 1, LENGTH(task_code) - POSITION('.' IN task_code)) AS UNSIGNED) " . $sThisSort); // Save to Session
}
else
{
$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
}
		} else {
			$ofld->setSort("");
		}
	}

	// Current master table name
	function getCurrentMasterTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE];
	}

	function setCurrentMasterTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE] = $v;
	}

	// Session master WHERE clause
	function GetMasterFilter() {

		// Master filter
		$sMasterFilter = "";
		if ($this->getCurrentMasterTable() == "v_employees") {
			if ($this->task_employee_id->getSessionValue() <> "")
				$sMasterFilter .= "`employee_id`=" . ew_QuotedValue($this->task_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "plans") {
			if ($this->task_plan_id->getSessionValue() <> "")
				$sMasterFilter .= "`plan_id`=" . ew_QuotedValue($this->task_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->task_project_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `plan_project_id`=" . ew_QuotedValue($this->task_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sMasterFilter;
	}

	// Session detail WHERE clause
	function GetDetailFilter() {

		// Detail filter
		$sDetailFilter = "";
		if ($this->getCurrentMasterTable() == "v_employees") {
			if ($this->task_employee_id->getSessionValue() <> "")
				$sDetailFilter .= "`task_employee_id`=" . ew_QuotedValue($this->task_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "plans") {
			if ($this->task_plan_id->getSessionValue() <> "")
				$sDetailFilter .= "`task_plan_id`=" . ew_QuotedValue($this->task_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->task_project_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `task_project_id`=" . ew_QuotedValue($this->task_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sDetailFilter;
	}

	// Master filter
	function SqlMasterFilter_v_employees() {
		return "`employee_id`=@employee_id@";
	}

	// Detail filter
	function SqlDetailFilter_v_employees() {
		return "`task_employee_id`=@task_employee_id@";
	}

	// Master filter
	function SqlMasterFilter_plans() {
		return "`plan_id`=@plan_id@ AND `plan_project_id`=@plan_project_id@";
	}

	// Detail filter
	function SqlDetailFilter_plans() {
		return "`task_plan_id`=@task_plan_id@ AND `task_project_id`=@task_project_id@";
	}

	// Current detail table name
	function getCurrentDetailTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE];
	}

	function setCurrentDetailTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE] = $v;
	}

	// Get detail url
	function GetDetailUrl() {

		// Detail url
		$sDetailUrl = "";
		if ($this->getCurrentDetailTable() == "works") {
			$sDetailUrl = $GLOBALS["works"]->GetListUrl() . "?" . EW_TABLE_SHOW_MASTER . "=" . $this->TableVar;
			$sDetailUrl .= "&fk_task_id=" . urlencode($this->task_id->CurrentValue);
			$sDetailUrl .= "&fk_task_plan_id=" . urlencode($this->task_plan_id->CurrentValue);
			$sDetailUrl .= "&fk_task_lab_id=" . urlencode($this->task_lab_id->CurrentValue);
			$sDetailUrl .= "&fk_task_project_id=" . urlencode($this->task_project_id->CurrentValue);
		}
		if ($this->getCurrentDetailTable() == "v_works") {
			$sDetailUrl = $GLOBALS["v_works"]->GetListUrl() . "?" . EW_TABLE_SHOW_MASTER . "=" . $this->TableVar;
			$sDetailUrl .= "&fk_task_id=" . urlencode($this->task_id->CurrentValue);
		}
		if ($sDetailUrl == "") {
			$sDetailUrl = "taskslist.php";
		}
		return $sDetailUrl;
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`tasks`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "`task_code` ASC";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('task_id', $rs))
				ew_AddFilter($where, ew_QuotedName('task_id', $this->DBID) . '=' . ew_QuotedValue($rs['task_id'], $this->task_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`task_id` = @task_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->task_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@task_id@", ew_AdjustSql($this->task_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "taskslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "taskslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("tasksview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("tasksview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "tasksadd.php?" . $this->UrlParm($parm);
		else
			$url = "tasksadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("tasksedit.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("tasksedit.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("tasksadd.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("tasksadd.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("tasksdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		if ($this->getCurrentMasterTable() == "v_employees" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_employee_id=" . urlencode($this->task_employee_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "plans" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_plan_id=" . urlencode($this->task_plan_id->CurrentValue);
			$url .= "&fk_plan_project_id=" . urlencode($this->task_project_id->CurrentValue);
		}
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "task_id:" . ew_VarToJson($this->task_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->task_id->CurrentValue)) {
			$sUrl .= "task_id=" . urlencode($this->task_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["task_id"]))
				$arKeys[] = ew_StripSlashes($_POST["task_id"]);
			elseif (isset($_GET["task_id"]))
				$arKeys[] = ew_StripSlashes($_GET["task_id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->task_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->task_id->setDbValue($rs->fields('task_id'));
		$this->task_project_id->setDbValue($rs->fields('task_project_id'));
		$this->task_plan_id->setDbValue($rs->fields('task_plan_id'));
		$this->task_lab_id->setDbValue($rs->fields('task_lab_id'));
		$this->task_code->setDbValue($rs->fields('task_code'));
		$this->task_name->setDbValue($rs->fields('task_name'));
		$this->task_from->setDbValue($rs->fields('task_from'));
		$this->task_to->setDbValue($rs->fields('task_to'));
		$this->task_employee_id->setDbValue($rs->fields('task_employee_id'));
		$this->task_coordinator_id->setDbValue($rs->fields('task_coordinator_id'));
		$this->task_object->setDbValue($rs->fields('task_object'));
		$this->task_status_id->setDbValue($rs->fields('task_status_id'));
		$this->task_hours_planned->setDbValue($rs->fields('task_hours_planned'));
		$this->task_cof_planned->setDbValue($rs->fields('task_cof_planned'));
		$this->task_money_planned->setDbValue($rs->fields('task_money_planned'));
		$this->task_hours_actual->setDbValue($rs->fields('task_hours_actual'));
		$this->task_cof_actual->setDbValue($rs->fields('task_cof_actual'));
		$this->task_money_actual->setDbValue($rs->fields('task_money_actual'));
		$this->task_description->setDbValue($rs->fields('task_description'));
		$this->task_key->setDbValue($rs->fields('task_key'));
		$this->task_file->Upload->DbValue = $rs->fields('task_file');
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// task_id
		// task_project_id
		// task_plan_id

		$this->task_plan_id->CellCssStyle = "width: 240px;";

		// task_lab_id
		$this->task_lab_id->CellCssStyle = "white-space: nowrap;";

		// task_code
		$this->task_code->CellCssStyle = "white-space: nowrap;";

		// task_name
		$this->task_name->CellCssStyle = "width: 260px;";

		// task_from
		$this->task_from->CellCssStyle = "white-space: nowrap;";

		// task_to
		$this->task_to->CellCssStyle = "white-space: nowrap;";

		// task_employee_id
		$this->task_employee_id->CellCssStyle = "white-space: nowrap;";

		// task_coordinator_id
		$this->task_coordinator_id->CellCssStyle = "white-space: nowrap;";

		// task_object
		// task_status_id

		$this->task_status_id->CellCssStyle = "white-space: nowrap;";

		// task_hours_planned
		// task_cof_planned

		$this->task_cof_planned->CellCssStyle = "white-space: nowrap;";

		// task_money_planned
		$this->task_money_planned->CellCssStyle = "white-space: nowrap;";

		// task_hours_actual
		// task_cof_actual

		$this->task_cof_actual->CellCssStyle = "white-space: nowrap;";

		// task_money_actual
		$this->task_money_actual->CellCssStyle = "white-space: nowrap;";

		// task_description
		$this->task_description->CellCssStyle = "width: 260px;";

		// task_key
		// task_file
		// task_id

		$this->task_id->ViewValue = $this->task_id->CurrentValue;
		$this->task_id->ViewCustomAttributes = "";

		// task_project_id
		if (strval($this->task_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_project_id->ViewValue = $this->task_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_project_id->ViewValue = $this->task_project_id->CurrentValue;
			}
		} else {
			$this->task_project_id->ViewValue = NULL;
		}
		$this->task_project_id->ViewCustomAttributes = "";

		// task_plan_id
		if (strval($this->task_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->task_plan_id->ViewValue = $this->task_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_plan_id->ViewValue = $this->task_plan_id->CurrentValue;
			}
		} else {
			$this->task_plan_id->ViewValue = NULL;
		}
		$this->task_plan_id->ViewCustomAttributes = "";

		// task_lab_id
		if (strval($this->task_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_lab_id->ViewValue = $this->task_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_lab_id->ViewValue = $this->task_lab_id->CurrentValue;
			}
		} else {
			$this->task_lab_id->ViewValue = NULL;
		}
		$this->task_lab_id->ViewCustomAttributes = "";

		// task_code
		$this->task_code->ViewValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->ViewValue = $this->task_name->CurrentValue;
		$this->task_name->ViewCustomAttributes = "";

		// task_from
		$this->task_from->ViewValue = $this->task_from->CurrentValue;
		$this->task_from->ViewValue = ew_FormatDateTime($this->task_from->ViewValue, 7);
		$this->task_from->ViewCustomAttributes = "";

		// task_to
		$this->task_to->ViewValue = $this->task_to->CurrentValue;
		$this->task_to->ViewValue = ew_FormatDateTime($this->task_to->ViewValue, 7);
		$this->task_to->ViewCustomAttributes = "";

		// task_employee_id
		if (strval($this->task_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_employee_id->ViewValue = $this->task_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_employee_id->ViewValue = $this->task_employee_id->CurrentValue;
			}
		} else {
			$this->task_employee_id->ViewValue = NULL;
		}
		$this->task_employee_id->ViewCustomAttributes = "";

		// task_coordinator_id
		if (strval($this->task_coordinator_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->CurrentValue;
			}
		} else {
			$this->task_coordinator_id->ViewValue = NULL;
		}
		$this->task_coordinator_id->ViewCustomAttributes = "";

		// task_object
		$this->task_object->ViewValue = $this->task_object->CurrentValue;
		$this->task_object->ViewCustomAttributes = "";

		// task_status_id
		if (strval($this->task_status_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_status_id->ViewValue = $this->task_status_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_status_id->ViewValue = $this->task_status_id->CurrentValue;
			}
		} else {
			$this->task_status_id->ViewValue = NULL;
		}
		$this->task_status_id->ViewCustomAttributes = "";

		// task_hours_planned
		$this->task_hours_planned->ViewValue = $this->task_hours_planned->CurrentValue;
		$this->task_hours_planned->ViewCustomAttributes = "";

		// task_cof_planned
		$this->task_cof_planned->ViewValue = $this->task_cof_planned->CurrentValue;
		$this->task_cof_planned->ViewCustomAttributes = "";

		// task_money_planned
		$this->task_money_planned->ViewValue = $this->task_money_planned->CurrentValue;
		$this->task_money_planned->ViewCustomAttributes = "";

		// task_hours_actual
		$this->task_hours_actual->ViewValue = $this->task_hours_actual->CurrentValue;
		$this->task_hours_actual->ViewCustomAttributes = "";

		// task_cof_actual
		$this->task_cof_actual->ViewValue = $this->task_cof_actual->CurrentValue;
		$this->task_cof_actual->ViewCustomAttributes = "";

		// task_money_actual
		$this->task_money_actual->ViewValue = $this->task_money_actual->CurrentValue;
		$this->task_money_actual->ViewCustomAttributes = "";

		// task_description
		$this->task_description->ViewValue = $this->task_description->CurrentValue;
		$this->task_description->ViewCustomAttributes = "";

		// task_key
		$this->task_key->ViewValue = $this->task_key->CurrentValue;
		$this->task_key->ViewCustomAttributes = "";

		// task_file
		if (!ew_Empty($this->task_file->Upload->DbValue)) {
			$this->task_file->ViewValue = $this->task_file->Upload->DbValue;
		} else {
			$this->task_file->ViewValue = "";
		}
		$this->task_file->ViewCustomAttributes = "";

		// task_id
		$this->task_id->LinkCustomAttributes = "";
		$this->task_id->HrefValue = "";
		$this->task_id->TooltipValue = "";

		// task_project_id
		$this->task_project_id->LinkCustomAttributes = "";
		$this->task_project_id->HrefValue = "";
		$this->task_project_id->TooltipValue = "";

		// task_plan_id
		$this->task_plan_id->LinkCustomAttributes = "";
		$this->task_plan_id->HrefValue = "";
		$this->task_plan_id->TooltipValue = "";

		// task_lab_id
		$this->task_lab_id->LinkCustomAttributes = "";
		$this->task_lab_id->HrefValue = "";
		$this->task_lab_id->TooltipValue = "";

		// task_code
		$this->task_code->LinkCustomAttributes = "";
		$this->task_code->HrefValue = "";
		$this->task_code->TooltipValue = "";

		// task_name
		$this->task_name->LinkCustomAttributes = "";
		$this->task_name->HrefValue = "";
		$this->task_name->TooltipValue = "";

		// task_from
		$this->task_from->LinkCustomAttributes = "";
		$this->task_from->HrefValue = "";
		$this->task_from->TooltipValue = "";

		// task_to
		$this->task_to->LinkCustomAttributes = "";
		$this->task_to->HrefValue = "";
		$this->task_to->TooltipValue = "";

		// task_employee_id
		$this->task_employee_id->LinkCustomAttributes = "";
		$this->task_employee_id->HrefValue = "";
		$this->task_employee_id->TooltipValue = "";

		// task_coordinator_id
		$this->task_coordinator_id->LinkCustomAttributes = "";
		$this->task_coordinator_id->HrefValue = "";
		$this->task_coordinator_id->TooltipValue = "";

		// task_object
		$this->task_object->LinkCustomAttributes = "";
		$this->task_object->HrefValue = "";
		$this->task_object->TooltipValue = "";

		// task_status_id
		$this->task_status_id->LinkCustomAttributes = "";
		$this->task_status_id->HrefValue = "";
		$this->task_status_id->TooltipValue = "";

		// task_hours_planned
		$this->task_hours_planned->LinkCustomAttributes = "";
		$this->task_hours_planned->HrefValue = "";
		$this->task_hours_planned->TooltipValue = "";

		// task_cof_planned
		$this->task_cof_planned->LinkCustomAttributes = "";
		$this->task_cof_planned->HrefValue = "";
		$this->task_cof_planned->TooltipValue = "";

		// task_money_planned
		$this->task_money_planned->LinkCustomAttributes = "";
		$this->task_money_planned->HrefValue = "";
		$this->task_money_planned->TooltipValue = "";

		// task_hours_actual
		$this->task_hours_actual->LinkCustomAttributes = "";
		$this->task_hours_actual->HrefValue = "";
		$this->task_hours_actual->TooltipValue = "";

		// task_cof_actual
		$this->task_cof_actual->LinkCustomAttributes = "";
		$this->task_cof_actual->HrefValue = "";
		$this->task_cof_actual->TooltipValue = "";

		// task_money_actual
		$this->task_money_actual->LinkCustomAttributes = "";
		$this->task_money_actual->HrefValue = "";
		$this->task_money_actual->TooltipValue = "";

		// task_description
		$this->task_description->LinkCustomAttributes = "";
		$this->task_description->HrefValue = "";
		$this->task_description->TooltipValue = "";

		// task_key
		$this->task_key->LinkCustomAttributes = "";
		$this->task_key->HrefValue = "";
		$this->task_key->TooltipValue = "";

		// task_file
		$this->task_file->LinkCustomAttributes = "";
		if (!ew_Empty($this->task_file->Upload->DbValue)) {
			$this->task_file->HrefValue = ew_GetFileUploadUrl($this->task_file, $this->task_file->Upload->DbValue); // Add prefix/suffix
			$this->task_file->LinkAttrs["target"] = "_blank"; // Add target
			if ($this->Export <> "") $this->task_file->HrefValue = ew_ConvertFullUrl($this->task_file->HrefValue);
		} else {
			$this->task_file->HrefValue = "";
		}
		$this->task_file->HrefValue2 = $this->task_file->UploadPath . $this->task_file->Upload->DbValue;
		$this->task_file->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// task_id
		$this->task_id->EditAttrs["class"] = "form-control";
		$this->task_id->EditCustomAttributes = "";
		$this->task_id->EditValue = $this->task_id->CurrentValue;
		$this->task_id->ViewCustomAttributes = "";

		// task_project_id
		$this->task_project_id->EditAttrs["class"] = "form-control";
		$this->task_project_id->EditCustomAttributes = "";
		if ($this->task_project_id->getSessionValue() <> "") {
			$this->task_project_id->CurrentValue = $this->task_project_id->getSessionValue();
		if (strval($this->task_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_project_id->ViewValue = $this->task_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_project_id->ViewValue = $this->task_project_id->CurrentValue;
			}
		} else {
			$this->task_project_id->ViewValue = NULL;
		}
		$this->task_project_id->ViewCustomAttributes = "";
		} else {
		}

		// task_plan_id
		$this->task_plan_id->EditAttrs["class"] = "form-control";
		$this->task_plan_id->EditCustomAttributes = "";
		if ($this->task_plan_id->getSessionValue() <> "") {
			$this->task_plan_id->CurrentValue = $this->task_plan_id->getSessionValue();
		if (strval($this->task_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->task_plan_id->ViewValue = $this->task_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_plan_id->ViewValue = $this->task_plan_id->CurrentValue;
			}
		} else {
			$this->task_plan_id->ViewValue = NULL;
		}
		$this->task_plan_id->ViewCustomAttributes = "";
		} else {
		}

		// task_lab_id
		$this->task_lab_id->EditAttrs["class"] = "form-control";
		$this->task_lab_id->EditCustomAttributes = "";

		// task_code
		$this->task_code->EditAttrs["class"] = "form-control";
		$this->task_code->EditCustomAttributes = "";
		$this->task_code->EditValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->EditAttrs["class"] = "form-control";
		$this->task_name->EditCustomAttributes = "";
		$this->task_name->EditValue = $this->task_name->CurrentValue;
		$this->task_name->PlaceHolder = ew_RemoveHtml($this->task_name->FldCaption());

		// task_from
		$this->task_from->EditAttrs["class"] = "form-control";
		$this->task_from->EditCustomAttributes = "";
		$this->task_from->EditValue = ew_FormatDateTime($this->task_from->CurrentValue, 7);
		$this->task_from->PlaceHolder = ew_RemoveHtml($this->task_from->FldCaption());

		// task_to
		$this->task_to->EditAttrs["class"] = "form-control";
		$this->task_to->EditCustomAttributes = "";
		$this->task_to->EditValue = ew_FormatDateTime($this->task_to->CurrentValue, 7);
		$this->task_to->PlaceHolder = ew_RemoveHtml($this->task_to->FldCaption());

		// task_employee_id
		$this->task_employee_id->EditAttrs["class"] = "form-control";
		$this->task_employee_id->EditCustomAttributes = "";
		if ($this->task_employee_id->getSessionValue() <> "") {
			$this->task_employee_id->CurrentValue = $this->task_employee_id->getSessionValue();
		if (strval($this->task_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_employee_id->ViewValue = $this->task_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_employee_id->ViewValue = $this->task_employee_id->CurrentValue;
			}
		} else {
			$this->task_employee_id->ViewValue = NULL;
		}
		$this->task_employee_id->ViewCustomAttributes = "";
		} else {
		}

		// task_coordinator_id
		$this->task_coordinator_id->EditAttrs["class"] = "form-control";
		$this->task_coordinator_id->EditCustomAttributes = "";

		// task_object
		$this->task_object->EditAttrs["class"] = "form-control";
		$this->task_object->EditCustomAttributes = "";
		$this->task_object->EditValue = $this->task_object->CurrentValue;
		$this->task_object->PlaceHolder = ew_RemoveHtml($this->task_object->FldCaption());

		// task_status_id
		$this->task_status_id->EditAttrs["class"] = "form-control";
		$this->task_status_id->EditCustomAttributes = "";

		// task_hours_planned
		$this->task_hours_planned->EditAttrs["class"] = "form-control";
		$this->task_hours_planned->EditCustomAttributes = "";
		$this->task_hours_planned->EditValue = $this->task_hours_planned->CurrentValue;
		$this->task_hours_planned->PlaceHolder = ew_RemoveHtml($this->task_hours_planned->FldCaption());
		if (strval($this->task_hours_planned->EditValue) <> "" && is_numeric($this->task_hours_planned->EditValue)) $this->task_hours_planned->EditValue = ew_FormatNumber($this->task_hours_planned->EditValue, -2, -1, -2, 0);

		// task_cof_planned
		$this->task_cof_planned->EditAttrs["class"] = "form-control";
		$this->task_cof_planned->EditCustomAttributes = "";
		$this->task_cof_planned->EditValue = $this->task_cof_planned->CurrentValue;
		$this->task_cof_planned->PlaceHolder = ew_RemoveHtml($this->task_cof_planned->FldCaption());
		if (strval($this->task_cof_planned->EditValue) <> "" && is_numeric($this->task_cof_planned->EditValue)) $this->task_cof_planned->EditValue = ew_FormatNumber($this->task_cof_planned->EditValue, -2, -1, -2, 0);

		// task_money_planned
		$this->task_money_planned->EditAttrs["class"] = "form-control";
		$this->task_money_planned->EditCustomAttributes = "";
		$this->task_money_planned->EditValue = $this->task_money_planned->CurrentValue;
		$this->task_money_planned->PlaceHolder = ew_RemoveHtml($this->task_money_planned->FldCaption());
		if (strval($this->task_money_planned->EditValue) <> "" && is_numeric($this->task_money_planned->EditValue)) $this->task_money_planned->EditValue = ew_FormatNumber($this->task_money_planned->EditValue, -2, -1, -2, 0);

		// task_hours_actual
		$this->task_hours_actual->EditAttrs["class"] = "form-control";
		$this->task_hours_actual->EditCustomAttributes = "";
		$this->task_hours_actual->EditValue = $this->task_hours_actual->CurrentValue;
		$this->task_hours_actual->ViewCustomAttributes = "";

		// task_cof_actual
		$this->task_cof_actual->EditAttrs["class"] = "form-control";
		$this->task_cof_actual->EditCustomAttributes = "";
		$this->task_cof_actual->EditValue = $this->task_cof_actual->CurrentValue;
		$this->task_cof_actual->PlaceHolder = ew_RemoveHtml($this->task_cof_actual->FldCaption());
		if (strval($this->task_cof_actual->EditValue) <> "" && is_numeric($this->task_cof_actual->EditValue)) $this->task_cof_actual->EditValue = ew_FormatNumber($this->task_cof_actual->EditValue, -2, -1, -2, 0);

		// task_money_actual
		$this->task_money_actual->EditAttrs["class"] = "form-control";
		$this->task_money_actual->EditCustomAttributes = "";
		$this->task_money_actual->EditValue = $this->task_money_actual->CurrentValue;
		$this->task_money_actual->PlaceHolder = ew_RemoveHtml($this->task_money_actual->FldCaption());
		if (strval($this->task_money_actual->EditValue) <> "" && is_numeric($this->task_money_actual->EditValue)) $this->task_money_actual->EditValue = ew_FormatNumber($this->task_money_actual->EditValue, -2, -1, -2, 0);

		// task_description
		$this->task_description->EditAttrs["class"] = "form-control";
		$this->task_description->EditCustomAttributes = "";
		$this->task_description->EditValue = $this->task_description->CurrentValue;
		$this->task_description->PlaceHolder = ew_RemoveHtml($this->task_description->FldCaption());

		// task_key
		$this->task_key->EditAttrs["class"] = "form-control";
		$this->task_key->EditCustomAttributes = "";
		$this->task_key->EditValue = $this->task_key->CurrentValue;
		$this->task_key->PlaceHolder = ew_RemoveHtml($this->task_key->FldCaption());

		// task_file
		$this->task_file->EditAttrs["class"] = "form-control";
		$this->task_file->EditCustomAttributes = "";
		if (!ew_Empty($this->task_file->Upload->DbValue)) {
			$this->task_file->EditValue = $this->task_file->Upload->DbValue;
		} else {
			$this->task_file->EditValue = "";
		}
		if (!ew_Empty($this->task_file->CurrentValue))
			$this->task_file->Upload->FileName = $this->task_file->CurrentValue;

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->task_id->Exportable) $Doc->ExportCaption($this->task_id);
					if ($this->task_project_id->Exportable) $Doc->ExportCaption($this->task_project_id);
					if ($this->task_plan_id->Exportable) $Doc->ExportCaption($this->task_plan_id);
					if ($this->task_lab_id->Exportable) $Doc->ExportCaption($this->task_lab_id);
					if ($this->task_code->Exportable) $Doc->ExportCaption($this->task_code);
					if ($this->task_name->Exportable) $Doc->ExportCaption($this->task_name);
					if ($this->task_from->Exportable) $Doc->ExportCaption($this->task_from);
					if ($this->task_to->Exportable) $Doc->ExportCaption($this->task_to);
					if ($this->task_employee_id->Exportable) $Doc->ExportCaption($this->task_employee_id);
					if ($this->task_coordinator_id->Exportable) $Doc->ExportCaption($this->task_coordinator_id);
					if ($this->task_object->Exportable) $Doc->ExportCaption($this->task_object);
					if ($this->task_status_id->Exportable) $Doc->ExportCaption($this->task_status_id);
					if ($this->task_hours_planned->Exportable) $Doc->ExportCaption($this->task_hours_planned);
					if ($this->task_hours_actual->Exportable) $Doc->ExportCaption($this->task_hours_actual);
					if ($this->task_description->Exportable) $Doc->ExportCaption($this->task_description);
					if ($this->task_key->Exportable) $Doc->ExportCaption($this->task_key);
					if ($this->task_file->Exportable) $Doc->ExportCaption($this->task_file);
				} else {
					if ($this->task_id->Exportable) $Doc->ExportCaption($this->task_id);
					if ($this->task_project_id->Exportable) $Doc->ExportCaption($this->task_project_id);
					if ($this->task_plan_id->Exportable) $Doc->ExportCaption($this->task_plan_id);
					if ($this->task_lab_id->Exportable) $Doc->ExportCaption($this->task_lab_id);
					if ($this->task_code->Exportable) $Doc->ExportCaption($this->task_code);
					if ($this->task_name->Exportable) $Doc->ExportCaption($this->task_name);
					if ($this->task_from->Exportable) $Doc->ExportCaption($this->task_from);
					if ($this->task_to->Exportable) $Doc->ExportCaption($this->task_to);
					if ($this->task_employee_id->Exportable) $Doc->ExportCaption($this->task_employee_id);
					if ($this->task_coordinator_id->Exportable) $Doc->ExportCaption($this->task_coordinator_id);
					if ($this->task_object->Exportable) $Doc->ExportCaption($this->task_object);
					if ($this->task_status_id->Exportable) $Doc->ExportCaption($this->task_status_id);
					if ($this->task_hours_planned->Exportable) $Doc->ExportCaption($this->task_hours_planned);
					if ($this->task_hours_actual->Exportable) $Doc->ExportCaption($this->task_hours_actual);
					if ($this->task_description->Exportable) $Doc->ExportCaption($this->task_description);
					if ($this->task_key->Exportable) $Doc->ExportCaption($this->task_key);
					if ($this->task_file->Exportable) $Doc->ExportCaption($this->task_file);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->task_id->Exportable) $Doc->ExportField($this->task_id);
						if ($this->task_project_id->Exportable) $Doc->ExportField($this->task_project_id);
						if ($this->task_plan_id->Exportable) $Doc->ExportField($this->task_plan_id);
						if ($this->task_lab_id->Exportable) $Doc->ExportField($this->task_lab_id);
						if ($this->task_code->Exportable) $Doc->ExportField($this->task_code);
						if ($this->task_name->Exportable) $Doc->ExportField($this->task_name);
						if ($this->task_from->Exportable) $Doc->ExportField($this->task_from);
						if ($this->task_to->Exportable) $Doc->ExportField($this->task_to);
						if ($this->task_employee_id->Exportable) $Doc->ExportField($this->task_employee_id);
						if ($this->task_coordinator_id->Exportable) $Doc->ExportField($this->task_coordinator_id);
						if ($this->task_object->Exportable) $Doc->ExportField($this->task_object);
						if ($this->task_status_id->Exportable) $Doc->ExportField($this->task_status_id);
						if ($this->task_hours_planned->Exportable) $Doc->ExportField($this->task_hours_planned);
						if ($this->task_hours_actual->Exportable) $Doc->ExportField($this->task_hours_actual);
						if ($this->task_description->Exportable) $Doc->ExportField($this->task_description);
						if ($this->task_key->Exportable) $Doc->ExportField($this->task_key);
						if ($this->task_file->Exportable) $Doc->ExportField($this->task_file);
					} else {
						if ($this->task_id->Exportable) $Doc->ExportField($this->task_id);
						if ($this->task_project_id->Exportable) $Doc->ExportField($this->task_project_id);
						if ($this->task_plan_id->Exportable) $Doc->ExportField($this->task_plan_id);
						if ($this->task_lab_id->Exportable) $Doc->ExportField($this->task_lab_id);
						if ($this->task_code->Exportable) $Doc->ExportField($this->task_code);
						if ($this->task_name->Exportable) $Doc->ExportField($this->task_name);
						if ($this->task_from->Exportable) $Doc->ExportField($this->task_from);
						if ($this->task_to->Exportable) $Doc->ExportField($this->task_to);
						if ($this->task_employee_id->Exportable) $Doc->ExportField($this->task_employee_id);
						if ($this->task_coordinator_id->Exportable) $Doc->ExportField($this->task_coordinator_id);
						if ($this->task_object->Exportable) $Doc->ExportField($this->task_object);
						if ($this->task_status_id->Exportable) $Doc->ExportField($this->task_status_id);
						if ($this->task_hours_planned->Exportable) $Doc->ExportField($this->task_hours_planned);
						if ($this->task_hours_actual->Exportable) $Doc->ExportField($this->task_hours_actual);
						if ($this->task_description->Exportable) $Doc->ExportField($this->task_description);
						if ($this->task_key->Exportable) $Doc->ExportField($this->task_key);
						if ($this->task_file->Exportable) $Doc->ExportField($this->task_file);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {
		$code = intval(ew_ExecuteScalar("SELECT plan_code FROM plans WHERE plan_id = '{$rsnew['task_plan_id']}'"));
		$number = intval(ew_ExecuteScalar("SELECT plan_task_index FROM plans WHERE plan_id = '{$rsnew['task_plan_id']}'"));
		ew_Execute("UPDATE tasks SET task_code = CONCAT({$code}, '.', {$number}) WHERE task_id = '{$rsnew['task_id']}'");
		$number++;
		ew_Execute("UPDATE plans SET plan_task_index = {$number} WHERE plan_id = '{$rsnew['task_plan_id']}'");
		include("extensions/mail/email_task_inserted.php");
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {
		include("extensions/mail/email_task_updated.php");
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
