<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "tasksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$tasks_delete = NULL; // Initialize page object first

class ctasks_delete extends ctasks {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'tasks';

	// Page object name
	var $PageObjName = 'tasks_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = TRUE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (tasks)
		if (!isset($GLOBALS["tasks"]) || get_class($GLOBALS["tasks"]) == "ctasks") {
			$GLOBALS["tasks"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["tasks"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (plans)
		if (!isset($GLOBALS['plans'])) $GLOBALS['plans'] = new cplans();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'tasks', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("taskslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $tasks;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($tasks);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("taskslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in tasks class, tasksinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->CurrentAction = "I"; // Display record
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("taskslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->task_id->setDbValue($rs->fields('task_id'));
		$this->task_project_id->setDbValue($rs->fields('task_project_id'));
		$this->task_plan_id->setDbValue($rs->fields('task_plan_id'));
		$this->task_lab_id->setDbValue($rs->fields('task_lab_id'));
		$this->task_code->setDbValue($rs->fields('task_code'));
		$this->task_name->setDbValue($rs->fields('task_name'));
		$this->task_from->setDbValue($rs->fields('task_from'));
		$this->task_to->setDbValue($rs->fields('task_to'));
		$this->task_employee_id->setDbValue($rs->fields('task_employee_id'));
		$this->task_coordinator_id->setDbValue($rs->fields('task_coordinator_id'));
		$this->task_object->setDbValue($rs->fields('task_object'));
		$this->task_status_id->setDbValue($rs->fields('task_status_id'));
		$this->task_hours_planned->setDbValue($rs->fields('task_hours_planned'));
		$this->task_cof_planned->setDbValue($rs->fields('task_cof_planned'));
		$this->task_money_planned->setDbValue($rs->fields('task_money_planned'));
		$this->task_hours_actual->setDbValue($rs->fields('task_hours_actual'));
		$this->task_cof_actual->setDbValue($rs->fields('task_cof_actual'));
		$this->task_money_actual->setDbValue($rs->fields('task_money_actual'));
		$this->task_description->setDbValue($rs->fields('task_description'));
		$this->task_key->setDbValue($rs->fields('task_key'));
		$this->task_file->Upload->DbValue = $rs->fields('task_file');
		$this->task_file->CurrentValue = $this->task_file->Upload->DbValue;
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->task_id->DbValue = $row['task_id'];
		$this->task_project_id->DbValue = $row['task_project_id'];
		$this->task_plan_id->DbValue = $row['task_plan_id'];
		$this->task_lab_id->DbValue = $row['task_lab_id'];
		$this->task_code->DbValue = $row['task_code'];
		$this->task_name->DbValue = $row['task_name'];
		$this->task_from->DbValue = $row['task_from'];
		$this->task_to->DbValue = $row['task_to'];
		$this->task_employee_id->DbValue = $row['task_employee_id'];
		$this->task_coordinator_id->DbValue = $row['task_coordinator_id'];
		$this->task_object->DbValue = $row['task_object'];
		$this->task_status_id->DbValue = $row['task_status_id'];
		$this->task_hours_planned->DbValue = $row['task_hours_planned'];
		$this->task_cof_planned->DbValue = $row['task_cof_planned'];
		$this->task_money_planned->DbValue = $row['task_money_planned'];
		$this->task_hours_actual->DbValue = $row['task_hours_actual'];
		$this->task_cof_actual->DbValue = $row['task_cof_actual'];
		$this->task_money_actual->DbValue = $row['task_money_actual'];
		$this->task_description->DbValue = $row['task_description'];
		$this->task_key->DbValue = $row['task_key'];
		$this->task_file->Upload->DbValue = $row['task_file'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->task_hours_planned->FormValue == $this->task_hours_planned->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_planned->CurrentValue)))
			$this->task_hours_planned->CurrentValue = ew_StrToFloat($this->task_hours_planned->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_hours_actual->FormValue == $this->task_hours_actual->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_actual->CurrentValue)))
			$this->task_hours_actual->CurrentValue = ew_StrToFloat($this->task_hours_actual->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// task_id
		// task_project_id
		// task_plan_id

		$this->task_plan_id->CellCssStyle = "width: 240px;";

		// task_lab_id
		$this->task_lab_id->CellCssStyle = "white-space: nowrap;";

		// task_code
		$this->task_code->CellCssStyle = "white-space: nowrap;";

		// task_name
		$this->task_name->CellCssStyle = "width: 260px;";

		// task_from
		$this->task_from->CellCssStyle = "white-space: nowrap;";

		// task_to
		$this->task_to->CellCssStyle = "white-space: nowrap;";

		// task_employee_id
		$this->task_employee_id->CellCssStyle = "white-space: nowrap;";

		// task_coordinator_id
		$this->task_coordinator_id->CellCssStyle = "white-space: nowrap;";

		// task_object
		// task_status_id

		$this->task_status_id->CellCssStyle = "white-space: nowrap;";

		// task_hours_planned
		// task_cof_planned

		$this->task_cof_planned->CellCssStyle = "white-space: nowrap;";

		// task_money_planned
		$this->task_money_planned->CellCssStyle = "white-space: nowrap;";

		// task_hours_actual
		// task_cof_actual

		$this->task_cof_actual->CellCssStyle = "white-space: nowrap;";

		// task_money_actual
		$this->task_money_actual->CellCssStyle = "white-space: nowrap;";

		// task_description
		$this->task_description->CellCssStyle = "width: 260px;";

		// task_key
		// task_file

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// task_id
		$this->task_id->ViewValue = $this->task_id->CurrentValue;
		$this->task_id->ViewCustomAttributes = "";

		// task_project_id
		if (strval($this->task_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_project_id->ViewValue = $this->task_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_project_id->ViewValue = $this->task_project_id->CurrentValue;
			}
		} else {
			$this->task_project_id->ViewValue = NULL;
		}
		$this->task_project_id->ViewCustomAttributes = "";

		// task_plan_id
		if (strval($this->task_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->task_plan_id->ViewValue = $this->task_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_plan_id->ViewValue = $this->task_plan_id->CurrentValue;
			}
		} else {
			$this->task_plan_id->ViewValue = NULL;
		}
		$this->task_plan_id->ViewCustomAttributes = "";

		// task_lab_id
		if (strval($this->task_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_lab_id->ViewValue = $this->task_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_lab_id->ViewValue = $this->task_lab_id->CurrentValue;
			}
		} else {
			$this->task_lab_id->ViewValue = NULL;
		}
		$this->task_lab_id->ViewCustomAttributes = "";

		// task_code
		$this->task_code->ViewValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->ViewValue = $this->task_name->CurrentValue;
		$this->task_name->ViewCustomAttributes = "";

		// task_from
		$this->task_from->ViewValue = $this->task_from->CurrentValue;
		$this->task_from->ViewValue = ew_FormatDateTime($this->task_from->ViewValue, 7);
		$this->task_from->ViewCustomAttributes = "";

		// task_to
		$this->task_to->ViewValue = $this->task_to->CurrentValue;
		$this->task_to->ViewValue = ew_FormatDateTime($this->task_to->ViewValue, 7);
		$this->task_to->ViewCustomAttributes = "";

		// task_employee_id
		if (strval($this->task_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_employee_id->ViewValue = $this->task_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_employee_id->ViewValue = $this->task_employee_id->CurrentValue;
			}
		} else {
			$this->task_employee_id->ViewValue = NULL;
		}
		$this->task_employee_id->ViewCustomAttributes = "";

		// task_coordinator_id
		if (strval($this->task_coordinator_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->CurrentValue;
			}
		} else {
			$this->task_coordinator_id->ViewValue = NULL;
		}
		$this->task_coordinator_id->ViewCustomAttributes = "";

		// task_object
		$this->task_object->ViewValue = $this->task_object->CurrentValue;
		$this->task_object->ViewCustomAttributes = "";

		// task_status_id
		if (strval($this->task_status_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_status_id->ViewValue = $this->task_status_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_status_id->ViewValue = $this->task_status_id->CurrentValue;
			}
		} else {
			$this->task_status_id->ViewValue = NULL;
		}
		$this->task_status_id->ViewCustomAttributes = "";

		// task_hours_planned
		$this->task_hours_planned->ViewValue = $this->task_hours_planned->CurrentValue;
		$this->task_hours_planned->ViewCustomAttributes = "";

		// task_hours_actual
		$this->task_hours_actual->ViewValue = $this->task_hours_actual->CurrentValue;
		$this->task_hours_actual->ViewCustomAttributes = "";

		// task_description
		$this->task_description->ViewValue = $this->task_description->CurrentValue;
		$this->task_description->ViewCustomAttributes = "";

		// task_key
		$this->task_key->ViewValue = $this->task_key->CurrentValue;
		$this->task_key->ViewCustomAttributes = "";

		// task_file
		if (!ew_Empty($this->task_file->Upload->DbValue)) {
			$this->task_file->ViewValue = $this->task_file->Upload->DbValue;
		} else {
			$this->task_file->ViewValue = "";
		}
		$this->task_file->ViewCustomAttributes = "";

			// task_project_id
			$this->task_project_id->LinkCustomAttributes = "";
			$this->task_project_id->HrefValue = "";
			$this->task_project_id->TooltipValue = "";

			// task_plan_id
			$this->task_plan_id->LinkCustomAttributes = "";
			$this->task_plan_id->HrefValue = "";
			$this->task_plan_id->TooltipValue = "";

			// task_lab_id
			$this->task_lab_id->LinkCustomAttributes = "";
			$this->task_lab_id->HrefValue = "";
			$this->task_lab_id->TooltipValue = "";

			// task_code
			$this->task_code->LinkCustomAttributes = "";
			$this->task_code->HrefValue = "";
			$this->task_code->TooltipValue = "";

			// task_name
			$this->task_name->LinkCustomAttributes = "";
			$this->task_name->HrefValue = "";
			$this->task_name->TooltipValue = "";

			// task_from
			$this->task_from->LinkCustomAttributes = "";
			$this->task_from->HrefValue = "";
			$this->task_from->TooltipValue = "";

			// task_to
			$this->task_to->LinkCustomAttributes = "";
			$this->task_to->HrefValue = "";
			$this->task_to->TooltipValue = "";

			// task_employee_id
			$this->task_employee_id->LinkCustomAttributes = "";
			$this->task_employee_id->HrefValue = "";
			$this->task_employee_id->TooltipValue = "";

			// task_coordinator_id
			$this->task_coordinator_id->LinkCustomAttributes = "";
			$this->task_coordinator_id->HrefValue = "";
			$this->task_coordinator_id->TooltipValue = "";

			// task_object
			$this->task_object->LinkCustomAttributes = "";
			$this->task_object->HrefValue = "";
			$this->task_object->TooltipValue = "";

			// task_status_id
			$this->task_status_id->LinkCustomAttributes = "";
			$this->task_status_id->HrefValue = "";
			$this->task_status_id->TooltipValue = "";

			// task_hours_planned
			$this->task_hours_planned->LinkCustomAttributes = "";
			$this->task_hours_planned->HrefValue = "";
			$this->task_hours_planned->TooltipValue = "";

			// task_hours_actual
			$this->task_hours_actual->LinkCustomAttributes = "";
			$this->task_hours_actual->HrefValue = "";
			$this->task_hours_actual->TooltipValue = "";

			// task_description
			$this->task_description->LinkCustomAttributes = "";
			$this->task_description->HrefValue = "";
			$this->task_description->TooltipValue = "";

			// task_key
			$this->task_key->LinkCustomAttributes = "";
			$this->task_key->HrefValue = "";
			$this->task_key->TooltipValue = "";

			// task_file
			$this->task_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->task_file->Upload->DbValue)) {
				$this->task_file->HrefValue = ew_GetFileUploadUrl($this->task_file, $this->task_file->Upload->DbValue); // Add prefix/suffix
				$this->task_file->LinkAttrs["target"] = "_blank"; // Add target
				if ($this->Export <> "") $this->task_file->HrefValue = ew_ConvertFullUrl($this->task_file->HrefValue);
			} else {
				$this->task_file->HrefValue = "";
			}
			$this->task_file->HrefValue2 = $this->task_file->UploadPath . $this->task_file->Upload->DbValue;
			$this->task_file->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();
		if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteBegin")); // Batch delete begin

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['task_id'];
				$this->LoadDbValues($row);
				@unlink(ew_UploadPathEx(TRUE, $this->task_file->OldUploadPath) . $row['task_file']);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
			if ($DeleteRows) {
				foreach ($rsold as $row)
					$this->WriteAuditTrailOnDelete($row);
			}
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteSuccess")); // Batch delete success
		} else {
			$conn->RollbackTrans(); // Rollback changes
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteRollback")); // Batch delete rollback
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->task_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->task_employee_id->setSessionValue($this->task_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "plans") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_plan_id"] <> "") {
					$GLOBALS["plans"]->plan_id->setQueryStringValue($_GET["fk_plan_id"]);
					$this->task_plan_id->setQueryStringValue($GLOBALS["plans"]->plan_id->QueryStringValue);
					$this->task_plan_id->setSessionValue($this->task_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["plans"]->plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_plan_project_id"] <> "") {
					$GLOBALS["plans"]->plan_project_id->setQueryStringValue($_GET["fk_plan_project_id"]);
					$this->task_project_id->setQueryStringValue($GLOBALS["plans"]->plan_project_id->QueryStringValue);
					$this->task_project_id->setSessionValue($this->task_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["plans"]->plan_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->task_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->task_employee_id->setSessionValue($this->task_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "plans") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_plan_id"] <> "") {
					$GLOBALS["plans"]->plan_id->setFormValue($_POST["fk_plan_id"]);
					$this->task_plan_id->setFormValue($GLOBALS["plans"]->plan_id->FormValue);
					$this->task_plan_id->setSessionValue($this->task_plan_id->FormValue);
					if (!is_numeric($GLOBALS["plans"]->plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_plan_project_id"] <> "") {
					$GLOBALS["plans"]->plan_project_id->setFormValue($_POST["fk_plan_project_id"]);
					$this->task_project_id->setFormValue($GLOBALS["plans"]->plan_project_id->FormValue);
					$this->task_project_id->setSessionValue($this->task_project_id->FormValue);
					if (!is_numeric($GLOBALS["plans"]->plan_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "v_employees") {
				if ($this->task_employee_id->CurrentValue == "") $this->task_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "plans") {
				if ($this->task_plan_id->CurrentValue == "") $this->task_plan_id->setSessionValue("");
				if ($this->task_project_id->CurrentValue == "") $this->task_project_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("taskslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'tasks';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnDelete) return;
		$table = 'tasks';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['task_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$curUser = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$oldvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($tasks_delete)) $tasks_delete = new ctasks_delete();

// Page init
$tasks_delete->Page_Init();

// Page main
$tasks_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$tasks_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftasksdelete = new ew_Form("ftasksdelete", "delete");

// Form_CustomValidate event
ftasksdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftasksdelete.ValidateRequired = true;
<?php } else { ?>
ftasksdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftasksdelete.Lists["x_task_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_task_plan_id"],"FilterFields":[],"Options":[],"Template":""};
ftasksdelete.Lists["x_task_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftasksdelete.Lists["x_task_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_task_employee_id"],"FilterFields":[],"Options":[],"Template":""};
ftasksdelete.Lists["x_task_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftasksdelete.Lists["x_task_coordinator_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftasksdelete.Lists["x_task_status_id"] = {"LinkField":"x_task_status_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_status_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $tasks_delete->ShowPageHeader(); ?>
<?php
$tasks_delete->ShowMessage();
?>
<form name="ftasksdelete" id="ftasksdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($tasks_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $tasks_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="tasks">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($tasks_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $tasks->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($tasks->task_project_id->Visible) { // task_project_id ?>
		<th><span id="elh_tasks_task_project_id" class="tasks_task_project_id"><?php echo $tasks->task_project_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_plan_id->Visible) { // task_plan_id ?>
		<th><span id="elh_tasks_task_plan_id" class="tasks_task_plan_id"><?php echo $tasks->task_plan_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_lab_id->Visible) { // task_lab_id ?>
		<th><span id="elh_tasks_task_lab_id" class="tasks_task_lab_id"><?php echo $tasks->task_lab_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_code->Visible) { // task_code ?>
		<th><span id="elh_tasks_task_code" class="tasks_task_code"><?php echo $tasks->task_code->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_name->Visible) { // task_name ?>
		<th><span id="elh_tasks_task_name" class="tasks_task_name"><?php echo $tasks->task_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_from->Visible) { // task_from ?>
		<th><span id="elh_tasks_task_from" class="tasks_task_from"><?php echo $tasks->task_from->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_to->Visible) { // task_to ?>
		<th><span id="elh_tasks_task_to" class="tasks_task_to"><?php echo $tasks->task_to->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_employee_id->Visible) { // task_employee_id ?>
		<th><span id="elh_tasks_task_employee_id" class="tasks_task_employee_id"><?php echo $tasks->task_employee_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<th><span id="elh_tasks_task_coordinator_id" class="tasks_task_coordinator_id"><?php echo $tasks->task_coordinator_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_object->Visible) { // task_object ?>
		<th><span id="elh_tasks_task_object" class="tasks_task_object"><?php echo $tasks->task_object->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_status_id->Visible) { // task_status_id ?>
		<th><span id="elh_tasks_task_status_id" class="tasks_task_status_id"><?php echo $tasks->task_status_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<th><span id="elh_tasks_task_hours_planned" class="tasks_task_hours_planned"><?php echo $tasks->task_hours_planned->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<th><span id="elh_tasks_task_hours_actual" class="tasks_task_hours_actual"><?php echo $tasks->task_hours_actual->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_description->Visible) { // task_description ?>
		<th><span id="elh_tasks_task_description" class="tasks_task_description"><?php echo $tasks->task_description->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_key->Visible) { // task_key ?>
		<th><span id="elh_tasks_task_key" class="tasks_task_key"><?php echo $tasks->task_key->FldCaption() ?></span></th>
<?php } ?>
<?php if ($tasks->task_file->Visible) { // task_file ?>
		<th><span id="elh_tasks_task_file" class="tasks_task_file"><?php echo $tasks->task_file->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$tasks_delete->RecCnt = 0;
$i = 0;
while (!$tasks_delete->Recordset->EOF) {
	$tasks_delete->RecCnt++;
	$tasks_delete->RowCnt++;

	// Set row properties
	$tasks->ResetAttrs();
	$tasks->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$tasks_delete->LoadRowValues($tasks_delete->Recordset);

	// Render row
	$tasks_delete->RenderRow();
?>
	<tr<?php echo $tasks->RowAttributes() ?>>
<?php if ($tasks->task_project_id->Visible) { // task_project_id ?>
		<td<?php echo $tasks->task_project_id->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_project_id" class="tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<?php echo $tasks->task_project_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_plan_id->Visible) { // task_plan_id ?>
		<td<?php echo $tasks->task_plan_id->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_plan_id" class="tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<?php echo $tasks->task_plan_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_lab_id->Visible) { // task_lab_id ?>
		<td<?php echo $tasks->task_lab_id->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_lab_id" class="tasks_task_lab_id">
<span<?php echo $tasks->task_lab_id->ViewAttributes() ?>>
<?php echo $tasks->task_lab_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_code->Visible) { // task_code ?>
		<td<?php echo $tasks->task_code->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_code" class="tasks_task_code">
<span<?php echo $tasks->task_code->ViewAttributes() ?>>
<?php echo $tasks->task_code->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_name->Visible) { // task_name ?>
		<td<?php echo $tasks->task_name->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_name" class="tasks_task_name">
<span<?php echo $tasks->task_name->ViewAttributes() ?>>
<?php echo $tasks->task_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_from->Visible) { // task_from ?>
		<td<?php echo $tasks->task_from->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_from" class="tasks_task_from">
<span<?php echo $tasks->task_from->ViewAttributes() ?>>
<?php echo $tasks->task_from->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_to->Visible) { // task_to ?>
		<td<?php echo $tasks->task_to->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_to" class="tasks_task_to">
<span<?php echo $tasks->task_to->ViewAttributes() ?>>
<?php echo $tasks->task_to->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_employee_id->Visible) { // task_employee_id ?>
		<td<?php echo $tasks->task_employee_id->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_employee_id" class="tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<?php echo $tasks->task_employee_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<td<?php echo $tasks->task_coordinator_id->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_coordinator_id" class="tasks_task_coordinator_id">
<span<?php echo $tasks->task_coordinator_id->ViewAttributes() ?>>
<?php echo $tasks->task_coordinator_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_object->Visible) { // task_object ?>
		<td<?php echo $tasks->task_object->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_object" class="tasks_task_object">
<span<?php echo $tasks->task_object->ViewAttributes() ?>>
<?php echo $tasks->task_object->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_status_id->Visible) { // task_status_id ?>
		<td<?php echo $tasks->task_status_id->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_status_id" class="tasks_task_status_id">
<span<?php echo $tasks->task_status_id->ViewAttributes() ?>>
<?php echo $tasks->task_status_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<td<?php echo $tasks->task_hours_planned->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_hours_planned" class="tasks_task_hours_planned">
<span<?php echo $tasks->task_hours_planned->ViewAttributes() ?>>
<?php echo $tasks->task_hours_planned->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<td<?php echo $tasks->task_hours_actual->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_hours_actual" class="tasks_task_hours_actual">
<span<?php echo $tasks->task_hours_actual->ViewAttributes() ?>>
<?php echo $tasks->task_hours_actual->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_description->Visible) { // task_description ?>
		<td<?php echo $tasks->task_description->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_description" class="tasks_task_description">
<span<?php echo $tasks->task_description->ViewAttributes() ?>>
<?php echo $tasks->task_description->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_key->Visible) { // task_key ?>
		<td<?php echo $tasks->task_key->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_key" class="tasks_task_key">
<span<?php echo $tasks->task_key->ViewAttributes() ?>>
<?php echo $tasks->task_key->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($tasks->task_file->Visible) { // task_file ?>
		<td<?php echo $tasks->task_file->CellAttributes() ?>>
<span id="el<?php echo $tasks_delete->RowCnt ?>_tasks_task_file" class="tasks_task_file">
<span<?php echo $tasks->task_file->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($tasks->task_file, $tasks->task_file->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$tasks_delete->Recordset->MoveNext();
}
$tasks_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $tasks_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftasksdelete.Init();
</script>
<?php
$tasks_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$tasks_delete->Page_Terminate();
?>
