<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "e_tasks_financeinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "worksgridcls.php" ?>
<?php include_once "v_worksgridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$e_tasks_finance_view = NULL; // Initialize page object first

class ce_tasks_finance_view extends ce_tasks_finance {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'e_tasks_finance';

	// Page object name
	var $PageObjName = 'e_tasks_finance_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (e_tasks_finance)
		if (!isset($GLOBALS["e_tasks_finance"]) || get_class($GLOBALS["e_tasks_finance"]) == "ce_tasks_finance") {
			$GLOBALS["e_tasks_finance"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["e_tasks_finance"];
		}
		$KeyUrl = "";
		if (@$_GET["task_id"] <> "") {
			$this->RecKey["task_id"] = $_GET["task_id"];
			$KeyUrl .= "&amp;task_id=" . urlencode($this->RecKey["task_id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (plans)
		if (!isset($GLOBALS['plans'])) $GLOBALS['plans'] = new cplans();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'e_tasks_finance', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("e_tasks_financelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->task_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $e_tasks_finance;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($e_tasks_finance);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["task_id"] <> "") {
				$this->task_id->setQueryStringValue($_GET["task_id"]);
				$this->RecKey["task_id"] = $this->task_id->QueryStringValue;
			} elseif (@$_POST["task_id"] <> "") {
				$this->task_id->setFormValue($_POST["task_id"]);
				$this->RecKey["task_id"] = $this->task_id->FormValue;
			} else {
				$sReturnUrl = "e_tasks_financelist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "e_tasks_financelist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "e_tasks_financelist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();

		// Set up detail parameters
		$this->SetUpDetailParms();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());
		$option = &$options["detail"];
		$DetailTableLink = "";
		$DetailViewTblVar = "";
		$DetailCopyTblVar = "";
		$DetailEditTblVar = "";

		// "detail_works"
		$item = &$option->Add("detail_works");
		$body = $Language->Phrase("ViewPageDetailLink") . $Language->TablePhrase("works", "TblCaption");
		$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("workslist.php?" . EW_TABLE_SHOW_MASTER . "=e_tasks_finance&fk_task_project_id=" . urlencode(strval($this->task_project_id->CurrentValue)) . "&fk_task_plan_id=" . urlencode(strval($this->task_plan_id->CurrentValue)) . "&fk_task_lab_id=" . urlencode(strval($this->task_lab_id->CurrentValue)) . "&fk_task_id=" . urlencode(strval($this->task_id->CurrentValue)) . "") . "\">" . $body . "</a>";
		$links = "";
		if ($GLOBALS["works_grid"] && $GLOBALS["works_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'works')) {
			$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=works")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
			$DetailViewTblVar .= "works";
		}
		if ($GLOBALS["works_grid"] && $GLOBALS["works_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'works')) {
			$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=works")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
			$DetailEditTblVar .= "works";
		}
		if ($GLOBALS["works_grid"] && $GLOBALS["works_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'works')) {
			$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=works")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
			$DetailCopyTblVar .= "works";
		}
		if ($links <> "") {
			$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
			$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
		}
		$body = "<div class=\"btn-group\">" . $body . "</div>";
		$item->Body = $body;
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'works');
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "works";
		}
		if ($this->ShowMultipleDetails) $item->Visible = FALSE;

		// "detail_v_works"
		$item = &$option->Add("detail_v_works");
		$body = $Language->Phrase("ViewPageDetailLink") . $Language->TablePhrase("v_works", "TblCaption");
		$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("v_workslist.php?" . EW_TABLE_SHOW_MASTER . "=e_tasks_finance&fk_task_id=" . urlencode(strval($this->task_id->CurrentValue)) . "") . "\">" . $body . "</a>";
		$links = "";
		if ($GLOBALS["v_works_grid"] && $GLOBALS["v_works_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'v_works')) {
			$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=v_works")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
			$DetailViewTblVar .= "v_works";
		}
		if ($GLOBALS["v_works_grid"] && $GLOBALS["v_works_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'v_works')) {
			$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=v_works")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
			$DetailEditTblVar .= "v_works";
		}
		if ($GLOBALS["v_works_grid"] && $GLOBALS["v_works_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'v_works')) {
			$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=v_works")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
			$DetailCopyTblVar .= "v_works";
		}
		if ($links <> "") {
			$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
			$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
		}
		$body = "<div class=\"btn-group\">" . $body . "</div>";
		$item->Body = $body;
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'v_works');
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "v_works";
		}
		if ($this->ShowMultipleDetails) $item->Visible = FALSE;

		// Multiple details
		if ($this->ShowMultipleDetails) {
			$body = $Language->Phrase("MultipleMasterDetails");
			$body = "<div class=\"btn-group\">";
			$links = "";
			if ($DetailViewTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailViewTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			}
			if ($DetailEditTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailEditTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			}
			if ($DetailCopyTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailCopyTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewMasterDetail\" title=\"" . ew_HtmlTitle($Language->Phrase("MultipleMasterDetails")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("MultipleMasterDetails") . "<b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu ewMenu\">". $links . "</ul>";
			}
			$body .= "</div>";

			// Multiple details
			$oListOpt = &$option->Add("details");
			$oListOpt->Body = $body;
		}

		// Set up detail default
		$option = &$options["detail"];
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$option->UseImageAndText = TRUE;
		$ar = explode(",", $DetailTableLink);
		$cnt = count($ar);
		$option->UseDropDownButton = ($cnt > 1);
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		if ($this->AuditTrailOnView) $this->WriteAuditTrailOnView($row);
		$this->task_project_id->setDbValue($rs->fields('task_project_id'));
		$this->task_id->setDbValue($rs->fields('task_id'));
		$this->task_plan_id->setDbValue($rs->fields('task_plan_id'));
		$this->task_lab_id->setDbValue($rs->fields('task_lab_id'));
		$this->task_code->setDbValue($rs->fields('task_code'));
		$this->task_name->setDbValue($rs->fields('task_name'));
		$this->task_from->setDbValue($rs->fields('task_from'));
		$this->task_to->setDbValue($rs->fields('task_to'));
		$this->task_employee_id->setDbValue($rs->fields('task_employee_id'));
		$this->task_coordinator_id->setDbValue($rs->fields('task_coordinator_id'));
		$this->task_object->setDbValue($rs->fields('task_object'));
		$this->task_status_id->setDbValue($rs->fields('task_status_id'));
		$this->task_hours_planned->setDbValue($rs->fields('task_hours_planned'));
		$this->task_cof_planned->setDbValue($rs->fields('task_cof_planned'));
		$this->task_money_planned->setDbValue($rs->fields('task_money_planned'));
		$this->task_hours_actual->setDbValue($rs->fields('task_hours_actual'));
		$this->task_cof_actual->setDbValue($rs->fields('task_cof_actual'));
		$this->task_money_actual->setDbValue($rs->fields('task_money_actual'));
		$this->task_description->setDbValue($rs->fields('task_description'));
		$this->task_key->setDbValue($rs->fields('task_key'));
		$this->task_file->Upload->DbValue = $rs->fields('task_file');
		$this->task_file->CurrentValue = $this->task_file->Upload->DbValue;
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->task_project_id->DbValue = $row['task_project_id'];
		$this->task_id->DbValue = $row['task_id'];
		$this->task_plan_id->DbValue = $row['task_plan_id'];
		$this->task_lab_id->DbValue = $row['task_lab_id'];
		$this->task_code->DbValue = $row['task_code'];
		$this->task_name->DbValue = $row['task_name'];
		$this->task_from->DbValue = $row['task_from'];
		$this->task_to->DbValue = $row['task_to'];
		$this->task_employee_id->DbValue = $row['task_employee_id'];
		$this->task_coordinator_id->DbValue = $row['task_coordinator_id'];
		$this->task_object->DbValue = $row['task_object'];
		$this->task_status_id->DbValue = $row['task_status_id'];
		$this->task_hours_planned->DbValue = $row['task_hours_planned'];
		$this->task_cof_planned->DbValue = $row['task_cof_planned'];
		$this->task_money_planned->DbValue = $row['task_money_planned'];
		$this->task_hours_actual->DbValue = $row['task_hours_actual'];
		$this->task_cof_actual->DbValue = $row['task_cof_actual'];
		$this->task_money_actual->DbValue = $row['task_money_actual'];
		$this->task_description->DbValue = $row['task_description'];
		$this->task_key->DbValue = $row['task_key'];
		$this->task_file->Upload->DbValue = $row['task_file'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->task_hours_planned->FormValue == $this->task_hours_planned->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_planned->CurrentValue)))
			$this->task_hours_planned->CurrentValue = ew_StrToFloat($this->task_hours_planned->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_cof_planned->FormValue == $this->task_cof_planned->CurrentValue && is_numeric(ew_StrToFloat($this->task_cof_planned->CurrentValue)))
			$this->task_cof_planned->CurrentValue = ew_StrToFloat($this->task_cof_planned->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_money_planned->FormValue == $this->task_money_planned->CurrentValue && is_numeric(ew_StrToFloat($this->task_money_planned->CurrentValue)))
			$this->task_money_planned->CurrentValue = ew_StrToFloat($this->task_money_planned->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_hours_actual->FormValue == $this->task_hours_actual->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_actual->CurrentValue)))
			$this->task_hours_actual->CurrentValue = ew_StrToFloat($this->task_hours_actual->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_cof_actual->FormValue == $this->task_cof_actual->CurrentValue && is_numeric(ew_StrToFloat($this->task_cof_actual->CurrentValue)))
			$this->task_cof_actual->CurrentValue = ew_StrToFloat($this->task_cof_actual->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_money_actual->FormValue == $this->task_money_actual->CurrentValue && is_numeric(ew_StrToFloat($this->task_money_actual->CurrentValue)))
			$this->task_money_actual->CurrentValue = ew_StrToFloat($this->task_money_actual->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// task_project_id
		// task_id
		// task_plan_id
		// task_lab_id
		// task_code
		// task_name
		// task_from
		// task_to
		// task_employee_id
		// task_coordinator_id
		// task_object
		// task_status_id
		// task_hours_planned
		// task_cof_planned
		// task_money_planned
		// task_hours_actual
		// task_cof_actual
		// task_money_actual
		// task_description
		// task_key
		// task_file

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// task_project_id
		if (strval($this->task_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_project_id->ViewValue = $this->task_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_project_id->ViewValue = $this->task_project_id->CurrentValue;
			}
		} else {
			$this->task_project_id->ViewValue = NULL;
		}
		$this->task_project_id->ViewCustomAttributes = "";

		// task_id
		$this->task_id->ViewValue = $this->task_id->CurrentValue;
		$this->task_id->ViewCustomAttributes = "";

		// task_plan_id
		if (strval($this->task_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->task_plan_id->ViewValue = $this->task_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_plan_id->ViewValue = $this->task_plan_id->CurrentValue;
			}
		} else {
			$this->task_plan_id->ViewValue = NULL;
		}
		$this->task_plan_id->ViewCustomAttributes = "";

		// task_lab_id
		if (strval($this->task_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_lab_id->ViewValue = $this->task_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_lab_id->ViewValue = $this->task_lab_id->CurrentValue;
			}
		} else {
			$this->task_lab_id->ViewValue = NULL;
		}
		$this->task_lab_id->ViewCustomAttributes = "";

		// task_code
		$this->task_code->ViewValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->ViewValue = $this->task_name->CurrentValue;
		$this->task_name->ViewCustomAttributes = "";

		// task_from
		$this->task_from->ViewValue = $this->task_from->CurrentValue;
		$this->task_from->ViewValue = ew_FormatDateTime($this->task_from->ViewValue, 7);
		$this->task_from->ViewCustomAttributes = "";

		// task_to
		$this->task_to->ViewValue = $this->task_to->CurrentValue;
		$this->task_to->ViewValue = ew_FormatDateTime($this->task_to->ViewValue, 7);
		$this->task_to->ViewCustomAttributes = "";

		// task_employee_id
		if (strval($this->task_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_employee_id->ViewValue = $this->task_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_employee_id->ViewValue = $this->task_employee_id->CurrentValue;
			}
		} else {
			$this->task_employee_id->ViewValue = NULL;
		}
		$this->task_employee_id->ViewCustomAttributes = "";

		// task_coordinator_id
		if (strval($this->task_coordinator_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->CurrentValue;
			}
		} else {
			$this->task_coordinator_id->ViewValue = NULL;
		}
		$this->task_coordinator_id->ViewCustomAttributes = "";

		// task_object
		$this->task_object->ViewValue = $this->task_object->CurrentValue;
		$this->task_object->ViewCustomAttributes = "";

		// task_status_id
		if (strval($this->task_status_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_status_id->ViewValue = $this->task_status_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_status_id->ViewValue = $this->task_status_id->CurrentValue;
			}
		} else {
			$this->task_status_id->ViewValue = NULL;
		}
		$this->task_status_id->ViewCustomAttributes = "";

		// task_hours_planned
		$this->task_hours_planned->ViewValue = $this->task_hours_planned->CurrentValue;
		$this->task_hours_planned->ViewCustomAttributes = "";

		// task_cof_planned
		$this->task_cof_planned->ViewValue = $this->task_cof_planned->CurrentValue;
		$this->task_cof_planned->ViewCustomAttributes = "";

		// task_money_planned
		$this->task_money_planned->ViewValue = $this->task_money_planned->CurrentValue;
		$this->task_money_planned->ViewCustomAttributes = "";

		// task_hours_actual
		$this->task_hours_actual->ViewValue = $this->task_hours_actual->CurrentValue;
		$this->task_hours_actual->ViewCustomAttributes = "";

		// task_cof_actual
		$this->task_cof_actual->ViewValue = $this->task_cof_actual->CurrentValue;
		$this->task_cof_actual->ViewCustomAttributes = "";

		// task_money_actual
		$this->task_money_actual->ViewValue = $this->task_money_actual->CurrentValue;
		$this->task_money_actual->ViewCustomAttributes = "";

		// task_description
		$this->task_description->ViewValue = $this->task_description->CurrentValue;
		$this->task_description->ViewCustomAttributes = "";

		// task_key
		$this->task_key->ViewValue = $this->task_key->CurrentValue;
		$this->task_key->ViewCustomAttributes = "";

		// task_file
		if (!ew_Empty($this->task_file->Upload->DbValue)) {
			$this->task_file->ViewValue = $this->task_file->Upload->DbValue;
		} else {
			$this->task_file->ViewValue = "";
		}
		$this->task_file->ViewCustomAttributes = "";

			// task_project_id
			$this->task_project_id->LinkCustomAttributes = "";
			$this->task_project_id->HrefValue = "";
			$this->task_project_id->TooltipValue = "";

			// task_id
			$this->task_id->LinkCustomAttributes = "";
			$this->task_id->HrefValue = "";
			$this->task_id->TooltipValue = "";

			// task_plan_id
			$this->task_plan_id->LinkCustomAttributes = "";
			$this->task_plan_id->HrefValue = "";
			$this->task_plan_id->TooltipValue = "";

			// task_lab_id
			$this->task_lab_id->LinkCustomAttributes = "";
			$this->task_lab_id->HrefValue = "";
			$this->task_lab_id->TooltipValue = "";

			// task_code
			$this->task_code->LinkCustomAttributes = "";
			$this->task_code->HrefValue = "";
			$this->task_code->TooltipValue = "";

			// task_name
			$this->task_name->LinkCustomAttributes = "";
			$this->task_name->HrefValue = "";
			$this->task_name->TooltipValue = "";

			// task_from
			$this->task_from->LinkCustomAttributes = "";
			$this->task_from->HrefValue = "";
			$this->task_from->TooltipValue = "";

			// task_to
			$this->task_to->LinkCustomAttributes = "";
			$this->task_to->HrefValue = "";
			$this->task_to->TooltipValue = "";

			// task_employee_id
			$this->task_employee_id->LinkCustomAttributes = "";
			$this->task_employee_id->HrefValue = "";
			$this->task_employee_id->TooltipValue = "";

			// task_coordinator_id
			$this->task_coordinator_id->LinkCustomAttributes = "";
			$this->task_coordinator_id->HrefValue = "";
			$this->task_coordinator_id->TooltipValue = "";

			// task_object
			$this->task_object->LinkCustomAttributes = "";
			$this->task_object->HrefValue = "";
			$this->task_object->TooltipValue = "";

			// task_status_id
			$this->task_status_id->LinkCustomAttributes = "";
			$this->task_status_id->HrefValue = "";
			$this->task_status_id->TooltipValue = "";

			// task_hours_planned
			$this->task_hours_planned->LinkCustomAttributes = "";
			$this->task_hours_planned->HrefValue = "";
			$this->task_hours_planned->TooltipValue = "";

			// task_cof_planned
			$this->task_cof_planned->LinkCustomAttributes = "";
			$this->task_cof_planned->HrefValue = "";
			$this->task_cof_planned->TooltipValue = "";

			// task_money_planned
			$this->task_money_planned->LinkCustomAttributes = "";
			$this->task_money_planned->HrefValue = "";
			$this->task_money_planned->TooltipValue = "";

			// task_hours_actual
			$this->task_hours_actual->LinkCustomAttributes = "";
			$this->task_hours_actual->HrefValue = "";
			$this->task_hours_actual->TooltipValue = "";

			// task_cof_actual
			$this->task_cof_actual->LinkCustomAttributes = "";
			$this->task_cof_actual->HrefValue = "";
			$this->task_cof_actual->TooltipValue = "";

			// task_money_actual
			$this->task_money_actual->LinkCustomAttributes = "";
			$this->task_money_actual->HrefValue = "";
			$this->task_money_actual->TooltipValue = "";

			// task_description
			$this->task_description->LinkCustomAttributes = "";
			$this->task_description->HrefValue = "";
			$this->task_description->TooltipValue = "";

			// task_key
			$this->task_key->LinkCustomAttributes = "";
			$this->task_key->HrefValue = "";
			$this->task_key->TooltipValue = "";

			// task_file
			$this->task_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->task_file->Upload->DbValue)) {
				$this->task_file->HrefValue = ew_GetFileUploadUrl($this->task_file, $this->task_file->Upload->DbValue); // Add prefix/suffix
				$this->task_file->LinkAttrs["target"] = "_blank"; // Add target
				if ($this->Export <> "") $this->task_file->HrefValue = ew_ConvertFullUrl($this->task_file->HrefValue);
			} else {
				$this->task_file->HrefValue = "";
			}
			$this->task_file->HrefValue2 = $this->task_file->UploadPath . $this->task_file->Upload->DbValue;
			$this->task_file->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "plans") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_plan_id"] <> "") {
					$GLOBALS["plans"]->plan_id->setQueryStringValue($_GET["fk_plan_id"]);
					$this->task_plan_id->setQueryStringValue($GLOBALS["plans"]->plan_id->QueryStringValue);
					$this->task_plan_id->setSessionValue($this->task_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["plans"]->plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_plan_project_id"] <> "") {
					$GLOBALS["plans"]->plan_project_id->setQueryStringValue($_GET["fk_plan_project_id"]);
					$this->task_project_id->setQueryStringValue($GLOBALS["plans"]->plan_project_id->QueryStringValue);
					$this->task_project_id->setSessionValue($this->task_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["plans"]->plan_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->task_employee_id->setQueryStringValue($GLOBALS["e_employees"]->employee_id->QueryStringValue);
					$this->task_employee_id->setSessionValue($this->task_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "plans") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_plan_id"] <> "") {
					$GLOBALS["plans"]->plan_id->setFormValue($_POST["fk_plan_id"]);
					$this->task_plan_id->setFormValue($GLOBALS["plans"]->plan_id->FormValue);
					$this->task_plan_id->setSessionValue($this->task_plan_id->FormValue);
					if (!is_numeric($GLOBALS["plans"]->plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_plan_project_id"] <> "") {
					$GLOBALS["plans"]->plan_project_id->setFormValue($_POST["fk_plan_project_id"]);
					$this->task_project_id->setFormValue($GLOBALS["plans"]->plan_project_id->FormValue);
					$this->task_project_id->setSessionValue($this->task_project_id->FormValue);
					if (!is_numeric($GLOBALS["plans"]->plan_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->task_employee_id->setFormValue($GLOBALS["e_employees"]->employee_id->FormValue);
					$this->task_employee_id->setSessionValue($this->task_employee_id->FormValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);
			$this->setSessionWhere($this->GetDetailFilter());

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "plans") {
				if ($this->task_plan_id->CurrentValue == "") $this->task_plan_id->setSessionValue("");
				if ($this->task_project_id->CurrentValue == "") $this->task_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_employees") {
				if ($this->task_employee_id->CurrentValue == "") $this->task_employee_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("works", $DetailTblVar)) {
				if (!isset($GLOBALS["works_grid"]))
					$GLOBALS["works_grid"] = new cworks_grid;
				if ($GLOBALS["works_grid"]->DetailView) {
					$GLOBALS["works_grid"]->CurrentMode = "view";

					// Save current master table to detail table
					$GLOBALS["works_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["works_grid"]->setStartRecordNumber(1);
					$GLOBALS["works_grid"]->work_project_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_project_id->CurrentValue = $this->task_project_id->CurrentValue;
					$GLOBALS["works_grid"]->work_project_id->setSessionValue($GLOBALS["works_grid"]->work_project_id->CurrentValue);
					$GLOBALS["works_grid"]->work_plan_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_plan_id->CurrentValue = $this->task_plan_id->CurrentValue;
					$GLOBALS["works_grid"]->work_plan_id->setSessionValue($GLOBALS["works_grid"]->work_plan_id->CurrentValue);
					$GLOBALS["works_grid"]->work_lab_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_lab_id->CurrentValue = $this->task_lab_id->CurrentValue;
					$GLOBALS["works_grid"]->work_lab_id->setSessionValue($GLOBALS["works_grid"]->work_lab_id->CurrentValue);
					$GLOBALS["works_grid"]->work_task_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_task_id->CurrentValue = $this->task_id->CurrentValue;
					$GLOBALS["works_grid"]->work_task_id->setSessionValue($GLOBALS["works_grid"]->work_task_id->CurrentValue);
				}
			}
			if (in_array("v_works", $DetailTblVar)) {
				if (!isset($GLOBALS["v_works_grid"]))
					$GLOBALS["v_works_grid"] = new cv_works_grid;
				if ($GLOBALS["v_works_grid"]->DetailView) {
					$GLOBALS["v_works_grid"]->CurrentMode = "view";

					// Save current master table to detail table
					$GLOBALS["v_works_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["v_works_grid"]->setStartRecordNumber(1);
					$GLOBALS["v_works_grid"]->work_task_id->FldIsDetailKey = TRUE;
					$GLOBALS["v_works_grid"]->work_task_id->CurrentValue = $this->task_id->CurrentValue;
					$GLOBALS["v_works_grid"]->work_task_id->setSessionValue($GLOBALS["v_works_grid"]->work_task_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("e_tasks_financelist.php"), "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'e_tasks_finance';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($e_tasks_finance_view)) $e_tasks_finance_view = new ce_tasks_finance_view();

// Page init
$e_tasks_finance_view->Page_Init();

// Page main
$e_tasks_finance_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$e_tasks_finance_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fe_tasks_financeview = new ew_Form("fe_tasks_financeview", "view");

// Form_CustomValidate event
fe_tasks_financeview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fe_tasks_financeview.ValidateRequired = true;
<?php } else { ?>
fe_tasks_financeview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fe_tasks_financeview.Lists["x_task_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_task_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fe_tasks_financeview.Lists["x_task_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasks_financeview.Lists["x_task_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_task_employee_id"],"FilterFields":[],"Options":[],"Template":""};
fe_tasks_financeview.Lists["x_task_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasks_financeview.Lists["x_task_coordinator_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasks_financeview.Lists["x_task_status_id"] = {"LinkField":"x_task_status_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_status_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $e_tasks_finance_view->ExportOptions->Render("body") ?>
<?php
	foreach ($e_tasks_finance_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $e_tasks_finance_view->ShowPageHeader(); ?>
<?php
$e_tasks_finance_view->ShowMessage();
?>
<form name="fe_tasks_financeview" id="fe_tasks_financeview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($e_tasks_finance_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $e_tasks_finance_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="e_tasks_finance">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($e_tasks_finance->task_project_id->Visible) { // task_project_id ?>
	<tr id="r_task_project_id">
		<td><span id="elh_e_tasks_finance_task_project_id"><?php echo $e_tasks_finance->task_project_id->FldCaption() ?></span></td>
		<td data-name="task_project_id"<?php echo $e_tasks_finance->task_project_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_project_id">
<span<?php echo $e_tasks_finance->task_project_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_project_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_id->Visible) { // task_id ?>
	<tr id="r_task_id">
		<td><span id="elh_e_tasks_finance_task_id"><?php echo $e_tasks_finance->task_id->FldCaption() ?></span></td>
		<td data-name="task_id"<?php echo $e_tasks_finance->task_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_id">
<span<?php echo $e_tasks_finance->task_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_plan_id->Visible) { // task_plan_id ?>
	<tr id="r_task_plan_id">
		<td><span id="elh_e_tasks_finance_task_plan_id"><?php echo $e_tasks_finance->task_plan_id->FldCaption() ?></span></td>
		<td data-name="task_plan_id"<?php echo $e_tasks_finance->task_plan_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_plan_id">
<span<?php echo $e_tasks_finance->task_plan_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_plan_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_lab_id->Visible) { // task_lab_id ?>
	<tr id="r_task_lab_id">
		<td><span id="elh_e_tasks_finance_task_lab_id"><?php echo $e_tasks_finance->task_lab_id->FldCaption() ?></span></td>
		<td data-name="task_lab_id"<?php echo $e_tasks_finance->task_lab_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_lab_id">
<span<?php echo $e_tasks_finance->task_lab_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_lab_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_code->Visible) { // task_code ?>
	<tr id="r_task_code">
		<td><span id="elh_e_tasks_finance_task_code"><?php echo $e_tasks_finance->task_code->FldCaption() ?></span></td>
		<td data-name="task_code"<?php echo $e_tasks_finance->task_code->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_code">
<span<?php echo $e_tasks_finance->task_code->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_code->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_name->Visible) { // task_name ?>
	<tr id="r_task_name">
		<td><span id="elh_e_tasks_finance_task_name"><?php echo $e_tasks_finance->task_name->FldCaption() ?></span></td>
		<td data-name="task_name"<?php echo $e_tasks_finance->task_name->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_name">
<span<?php echo $e_tasks_finance->task_name->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_name->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_from->Visible) { // task_from ?>
	<tr id="r_task_from">
		<td><span id="elh_e_tasks_finance_task_from"><?php echo $e_tasks_finance->task_from->FldCaption() ?></span></td>
		<td data-name="task_from"<?php echo $e_tasks_finance->task_from->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_from">
<span<?php echo $e_tasks_finance->task_from->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_from->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_to->Visible) { // task_to ?>
	<tr id="r_task_to">
		<td><span id="elh_e_tasks_finance_task_to"><?php echo $e_tasks_finance->task_to->FldCaption() ?></span></td>
		<td data-name="task_to"<?php echo $e_tasks_finance->task_to->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_to">
<span<?php echo $e_tasks_finance->task_to->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_to->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_employee_id->Visible) { // task_employee_id ?>
	<tr id="r_task_employee_id">
		<td><span id="elh_e_tasks_finance_task_employee_id"><?php echo $e_tasks_finance->task_employee_id->FldCaption() ?></span></td>
		<td data-name="task_employee_id"<?php echo $e_tasks_finance->task_employee_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_employee_id">
<span<?php echo $e_tasks_finance->task_employee_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_employee_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_coordinator_id->Visible) { // task_coordinator_id ?>
	<tr id="r_task_coordinator_id">
		<td><span id="elh_e_tasks_finance_task_coordinator_id"><?php echo $e_tasks_finance->task_coordinator_id->FldCaption() ?></span></td>
		<td data-name="task_coordinator_id"<?php echo $e_tasks_finance->task_coordinator_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_coordinator_id">
<span<?php echo $e_tasks_finance->task_coordinator_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_coordinator_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_object->Visible) { // task_object ?>
	<tr id="r_task_object">
		<td><span id="elh_e_tasks_finance_task_object"><?php echo $e_tasks_finance->task_object->FldCaption() ?></span></td>
		<td data-name="task_object"<?php echo $e_tasks_finance->task_object->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_object">
<span<?php echo $e_tasks_finance->task_object->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_object->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_status_id->Visible) { // task_status_id ?>
	<tr id="r_task_status_id">
		<td><span id="elh_e_tasks_finance_task_status_id"><?php echo $e_tasks_finance->task_status_id->FldCaption() ?></span></td>
		<td data-name="task_status_id"<?php echo $e_tasks_finance->task_status_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_status_id">
<span<?php echo $e_tasks_finance->task_status_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_status_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_hours_planned->Visible) { // task_hours_planned ?>
	<tr id="r_task_hours_planned">
		<td><span id="elh_e_tasks_finance_task_hours_planned"><?php echo $e_tasks_finance->task_hours_planned->FldCaption() ?></span></td>
		<td data-name="task_hours_planned"<?php echo $e_tasks_finance->task_hours_planned->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_hours_planned">
<span<?php echo $e_tasks_finance->task_hours_planned->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_hours_planned->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_cof_planned->Visible) { // task_cof_planned ?>
	<tr id="r_task_cof_planned">
		<td><span id="elh_e_tasks_finance_task_cof_planned"><?php echo $e_tasks_finance->task_cof_planned->FldCaption() ?></span></td>
		<td data-name="task_cof_planned"<?php echo $e_tasks_finance->task_cof_planned->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_cof_planned">
<span<?php echo $e_tasks_finance->task_cof_planned->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_cof_planned->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_money_planned->Visible) { // task_money_planned ?>
	<tr id="r_task_money_planned">
		<td><span id="elh_e_tasks_finance_task_money_planned"><?php echo $e_tasks_finance->task_money_planned->FldCaption() ?></span></td>
		<td data-name="task_money_planned"<?php echo $e_tasks_finance->task_money_planned->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_money_planned">
<span<?php echo $e_tasks_finance->task_money_planned->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_money_planned->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_hours_actual->Visible) { // task_hours_actual ?>
	<tr id="r_task_hours_actual">
		<td><span id="elh_e_tasks_finance_task_hours_actual"><?php echo $e_tasks_finance->task_hours_actual->FldCaption() ?></span></td>
		<td data-name="task_hours_actual"<?php echo $e_tasks_finance->task_hours_actual->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_hours_actual">
<span<?php echo $e_tasks_finance->task_hours_actual->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_hours_actual->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_cof_actual->Visible) { // task_cof_actual ?>
	<tr id="r_task_cof_actual">
		<td><span id="elh_e_tasks_finance_task_cof_actual"><?php echo $e_tasks_finance->task_cof_actual->FldCaption() ?></span></td>
		<td data-name="task_cof_actual"<?php echo $e_tasks_finance->task_cof_actual->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_cof_actual">
<span<?php echo $e_tasks_finance->task_cof_actual->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_cof_actual->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_money_actual->Visible) { // task_money_actual ?>
	<tr id="r_task_money_actual">
		<td><span id="elh_e_tasks_finance_task_money_actual"><?php echo $e_tasks_finance->task_money_actual->FldCaption() ?></span></td>
		<td data-name="task_money_actual"<?php echo $e_tasks_finance->task_money_actual->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_money_actual">
<span<?php echo $e_tasks_finance->task_money_actual->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_money_actual->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_description->Visible) { // task_description ?>
	<tr id="r_task_description">
		<td><span id="elh_e_tasks_finance_task_description"><?php echo $e_tasks_finance->task_description->FldCaption() ?></span></td>
		<td data-name="task_description"<?php echo $e_tasks_finance->task_description->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_description">
<span<?php echo $e_tasks_finance->task_description->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_description->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_key->Visible) { // task_key ?>
	<tr id="r_task_key">
		<td><span id="elh_e_tasks_finance_task_key"><?php echo $e_tasks_finance->task_key->FldCaption() ?></span></td>
		<td data-name="task_key"<?php echo $e_tasks_finance->task_key->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_key">
<span<?php echo $e_tasks_finance->task_key->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_key->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_file->Visible) { // task_file ?>
	<tr id="r_task_file">
		<td><span id="elh_e_tasks_finance_task_file"><?php echo $e_tasks_finance->task_file->FldCaption() ?></span></td>
		<td data-name="task_file"<?php echo $e_tasks_finance->task_file->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_file">
<span<?php echo $e_tasks_finance->task_file->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($e_tasks_finance->task_file, $e_tasks_finance->task_file->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php
	if (in_array("works", explode(",", $e_tasks_finance->getCurrentDetailTable())) && $works->DetailView) {
?>
<?php if ($e_tasks_finance->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("works", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "worksgrid.php" ?>
<?php } ?>
<?php
	if (in_array("v_works", explode(",", $e_tasks_finance->getCurrentDetailTable())) && $v_works->DetailView) {
?>
<?php if ($e_tasks_finance->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("v_works", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "v_worksgrid.php" ?>
<?php } ?>
</form>
<script type="text/javascript">
fe_tasks_financeview.Init();
</script>
<?php
$e_tasks_finance_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$e_tasks_finance_view->Page_Terminate();
?>
