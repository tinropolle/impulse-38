<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$plans_search = NULL; // Initialize page object first

class cplans_search extends cplans {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'plans';

	// Page object name
	var $PageObjName = 'plans_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (plans)
		if (!isset($GLOBALS["plans"]) || get_class($GLOBALS["plans"]) == "cplans") {
			$GLOBALS["plans"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["plans"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (projects)
		if (!isset($GLOBALS['projects'])) $GLOBALS['projects'] = new cprojects();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'plans', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("planslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->plan_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $plans;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($plans);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "planslist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->plan_id); // plan_id
		$this->BuildSearchUrl($sSrchUrl, $this->plan_project_id); // plan_project_id
		$this->BuildSearchUrl($sSrchUrl, $this->plan_code); // plan_code
		$this->BuildSearchUrl($sSrchUrl, $this->plan_name); // plan_name
		$this->BuildSearchUrl($sSrchUrl, $this->plan_employee_id); // plan_employee_id
		$this->BuildSearchUrl($sSrchUrl, $this->plan_active); // plan_active
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// plan_id

		$this->plan_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plan_id"));
		$this->plan_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plan_id");

		// plan_project_id
		$this->plan_project_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plan_project_id"));
		$this->plan_project_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plan_project_id");

		// plan_code
		$this->plan_code->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plan_code"));
		$this->plan_code->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plan_code");

		// plan_name
		$this->plan_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plan_name"));
		$this->plan_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plan_name");

		// plan_employee_id
		$this->plan_employee_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plan_employee_id"));
		$this->plan_employee_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plan_employee_id");

		// plan_active
		$this->plan_active->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plan_active"));
		$this->plan_active->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plan_active");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// plan_id
		// plan_project_id
		// plan_code
		// plan_name
		// plan_employee_id
		// plan_active
		// plan_task_index

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// plan_id
		$this->plan_id->ViewValue = $this->plan_id->CurrentValue;
		$this->plan_id->ViewCustomAttributes = "";

		// plan_project_id
		if (strval($this->plan_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->plan_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->plan_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->plan_project_id->ViewValue = $this->plan_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->plan_project_id->ViewValue = $this->plan_project_id->CurrentValue;
			}
		} else {
			$this->plan_project_id->ViewValue = NULL;
		}
		$this->plan_project_id->ViewCustomAttributes = "";

		// plan_code
		if (strval($this->plan_code->CurrentValue) <> "") {
			$this->plan_code->ViewValue = $this->plan_code->OptionCaption($this->plan_code->CurrentValue);
		} else {
			$this->plan_code->ViewValue = NULL;
		}
		$this->plan_code->ViewCustomAttributes = "";

		// plan_name
		$this->plan_name->ViewValue = $this->plan_name->CurrentValue;
		$this->plan_name->ViewCustomAttributes = "";

		// plan_employee_id
		if (strval($this->plan_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->plan_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->plan_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->plan_employee_id->ViewValue = $this->plan_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->plan_employee_id->ViewValue = $this->plan_employee_id->CurrentValue;
			}
		} else {
			$this->plan_employee_id->ViewValue = NULL;
		}
		$this->plan_employee_id->ViewCustomAttributes = "";

		// plan_active
		if (strval($this->plan_active->CurrentValue) <> "") {
			$this->plan_active->ViewValue = $this->plan_active->OptionCaption($this->plan_active->CurrentValue);
		} else {
			$this->plan_active->ViewValue = NULL;
		}
		$this->plan_active->ViewCustomAttributes = "";

			// plan_id
			$this->plan_id->LinkCustomAttributes = "";
			$this->plan_id->HrefValue = "";
			$this->plan_id->TooltipValue = "";

			// plan_project_id
			$this->plan_project_id->LinkCustomAttributes = "";
			$this->plan_project_id->HrefValue = "";
			$this->plan_project_id->TooltipValue = "";

			// plan_code
			$this->plan_code->LinkCustomAttributes = "";
			$this->plan_code->HrefValue = "";
			$this->plan_code->TooltipValue = "";

			// plan_name
			$this->plan_name->LinkCustomAttributes = "";
			$this->plan_name->HrefValue = "";
			$this->plan_name->TooltipValue = "";

			// plan_employee_id
			$this->plan_employee_id->LinkCustomAttributes = "";
			$this->plan_employee_id->HrefValue = "";
			$this->plan_employee_id->TooltipValue = "";

			// plan_active
			$this->plan_active->LinkCustomAttributes = "";
			$this->plan_active->HrefValue = "";
			$this->plan_active->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// plan_id
			$this->plan_id->EditAttrs["class"] = "form-control";
			$this->plan_id->EditCustomAttributes = "";
			$this->plan_id->EditValue = ew_HtmlEncode($this->plan_id->AdvancedSearch->SearchValue);
			$this->plan_id->PlaceHolder = ew_RemoveHtml($this->plan_id->FldCaption());

			// plan_project_id
			$this->plan_project_id->EditAttrs["class"] = "form-control";
			$this->plan_project_id->EditCustomAttributes = "";
			if (trim(strval($this->plan_project_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->plan_project_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->plan_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->plan_project_id->EditValue = $arwrk;

			// plan_code
			$this->plan_code->EditAttrs["class"] = "form-control";
			$this->plan_code->EditCustomAttributes = "";
			$this->plan_code->EditValue = $this->plan_code->Options(TRUE);

			// plan_name
			$this->plan_name->EditAttrs["class"] = "form-control";
			$this->plan_name->EditCustomAttributes = "";
			$this->plan_name->EditValue = ew_HtmlEncode($this->plan_name->AdvancedSearch->SearchValue);
			$this->plan_name->PlaceHolder = ew_RemoveHtml($this->plan_name->FldCaption());

			// plan_employee_id
			$this->plan_employee_id->EditAttrs["class"] = "form-control";
			$this->plan_employee_id->EditCustomAttributes = "";
			if (trim(strval($this->plan_employee_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->plan_employee_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->plan_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->plan_employee_id->EditValue = $arwrk;

			// plan_active
			$this->plan_active->EditAttrs["class"] = "form-control";
			$this->plan_active->EditCustomAttributes = "";
			$this->plan_active->EditValue = $this->plan_active->Options(TRUE);
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->plan_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->plan_id->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->plan_id->AdvancedSearch->Load();
		$this->plan_project_id->AdvancedSearch->Load();
		$this->plan_code->AdvancedSearch->Load();
		$this->plan_name->AdvancedSearch->Load();
		$this->plan_employee_id->AdvancedSearch->Load();
		$this->plan_active->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("planslist.php"), "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($plans_search)) $plans_search = new cplans_search();

// Page init
$plans_search->Page_Init();

// Page main
$plans_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$plans_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($plans_search->IsModal) { ?>
var CurrentAdvancedSearchForm = fplanssearch = new ew_Form("fplanssearch", "search");
<?php } else { ?>
var CurrentForm = fplanssearch = new ew_Form("fplanssearch", "search");
<?php } ?>

// Form_CustomValidate event
fplanssearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fplanssearch.ValidateRequired = true;
<?php } else { ?>
fplanssearch.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fplanssearch.Lists["x_plan_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplanssearch.Lists["x_plan_code"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplanssearch.Lists["x_plan_code"].Options = <?php echo json_encode($plans->plan_code->Options()) ?>;
fplanssearch.Lists["x_plan_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplanssearch.Lists["x_plan_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplanssearch.Lists["x_plan_active"].Options = <?php echo json_encode($plans->plan_active->Options()) ?>;

// Form object for search
// Validate function for search

fplanssearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_plan_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($plans->plan_id->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$plans_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $plans_search->ShowPageHeader(); ?>
<?php
$plans_search->ShowMessage();
?>
<form name="fplanssearch" id="fplanssearch" class="<?php echo $plans_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($plans_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $plans_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="plans">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($plans_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($plans->plan_id->Visible) { // plan_id ?>
	<div id="r_plan_id" class="form-group">
		<label for="x_plan_id" class="<?php echo $plans_search->SearchLabelClass ?>"><span id="elh_plans_plan_id"><?php echo $plans->plan_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_plan_id" id="z_plan_id" value="="></p>
		</label>
		<div class="<?php echo $plans_search->SearchRightColumnClass ?>"><div<?php echo $plans->plan_id->CellAttributes() ?>>
			<span id="el_plans_plan_id">
<input type="text" data-table="plans" data-field="x_plan_id" name="x_plan_id" id="x_plan_id" placeholder="<?php echo ew_HtmlEncode($plans->plan_id->getPlaceHolder()) ?>" value="<?php echo $plans->plan_id->EditValue ?>"<?php echo $plans->plan_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_project_id->Visible) { // plan_project_id ?>
	<div id="r_plan_project_id" class="form-group">
		<label for="x_plan_project_id" class="<?php echo $plans_search->SearchLabelClass ?>"><span id="elh_plans_plan_project_id"><?php echo $plans->plan_project_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_plan_project_id" id="z_plan_project_id" value="="></p>
		</label>
		<div class="<?php echo $plans_search->SearchRightColumnClass ?>"><div<?php echo $plans->plan_project_id->CellAttributes() ?>>
			<span id="el_plans_plan_project_id">
<select data-table="plans" data-field="x_plan_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_project_id->DisplayValueSeparator) ? json_encode($plans->plan_project_id->DisplayValueSeparator) : $plans->plan_project_id->DisplayValueSeparator) ?>" id="x_plan_project_id" name="x_plan_project_id"<?php echo $plans->plan_project_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_project_id->EditValue)) {
	$arwrk = $plans->plan_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_project_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>" selected><?php echo $plans->plan_project_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$plans->plan_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $plans->plan_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_plan_project_id" id="s_x_plan_project_id" value="<?php echo $plans->plan_project_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_code->Visible) { // plan_code ?>
	<div id="r_plan_code" class="form-group">
		<label for="x_plan_code" class="<?php echo $plans_search->SearchLabelClass ?>"><span id="elh_plans_plan_code"><?php echo $plans->plan_code->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_plan_code" id="z_plan_code" value="="></p>
		</label>
		<div class="<?php echo $plans_search->SearchRightColumnClass ?>"><div<?php echo $plans->plan_code->CellAttributes() ?>>
			<span id="el_plans_plan_code">
<select data-table="plans" data-field="x_plan_code" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_code->DisplayValueSeparator) ? json_encode($plans->plan_code->DisplayValueSeparator) : $plans->plan_code->DisplayValueSeparator) ?>" id="x_plan_code" name="x_plan_code"<?php echo $plans->plan_code->EditAttributes() ?>>
<?php
if (is_array($plans->plan_code->EditValue)) {
	$arwrk = $plans->plan_code->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_code->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_code->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_code->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_code->CurrentValue) ?>" selected><?php echo $plans->plan_code->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_name->Visible) { // plan_name ?>
	<div id="r_plan_name" class="form-group">
		<label for="x_plan_name" class="<?php echo $plans_search->SearchLabelClass ?>"><span id="elh_plans_plan_name"><?php echo $plans->plan_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_plan_name" id="z_plan_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $plans_search->SearchRightColumnClass ?>"><div<?php echo $plans->plan_name->CellAttributes() ?>>
			<span id="el_plans_plan_name">
<input type="text" data-table="plans" data-field="x_plan_name" name="x_plan_name" id="x_plan_name" maxlength="255" placeholder="<?php echo ew_HtmlEncode($plans->plan_name->getPlaceHolder()) ?>" value="<?php echo $plans->plan_name->EditValue ?>"<?php echo $plans->plan_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_employee_id->Visible) { // plan_employee_id ?>
	<div id="r_plan_employee_id" class="form-group">
		<label for="x_plan_employee_id" class="<?php echo $plans_search->SearchLabelClass ?>"><span id="elh_plans_plan_employee_id"><?php echo $plans->plan_employee_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_plan_employee_id" id="z_plan_employee_id" value="="></p>
		</label>
		<div class="<?php echo $plans_search->SearchRightColumnClass ?>"><div<?php echo $plans->plan_employee_id->CellAttributes() ?>>
			<span id="el_plans_plan_employee_id">
<select data-table="plans" data-field="x_plan_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_employee_id->DisplayValueSeparator) ? json_encode($plans->plan_employee_id->DisplayValueSeparator) : $plans->plan_employee_id->DisplayValueSeparator) ?>" id="x_plan_employee_id" name="x_plan_employee_id"<?php echo $plans->plan_employee_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_employee_id->EditValue)) {
	$arwrk = $plans->plan_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_employee_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>" selected><?php echo $plans->plan_employee_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$plans->plan_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $plans->plan_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_plan_employee_id" id="s_x_plan_employee_id" value="<?php echo $plans->plan_employee_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_active->Visible) { // plan_active ?>
	<div id="r_plan_active" class="form-group">
		<label for="x_plan_active" class="<?php echo $plans_search->SearchLabelClass ?>"><span id="elh_plans_plan_active"><?php echo $plans->plan_active->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_plan_active" id="z_plan_active" value="="></p>
		</label>
		<div class="<?php echo $plans_search->SearchRightColumnClass ?>"><div<?php echo $plans->plan_active->CellAttributes() ?>>
			<span id="el_plans_plan_active">
<select data-table="plans" data-field="x_plan_active" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_active->DisplayValueSeparator) ? json_encode($plans->plan_active->DisplayValueSeparator) : $plans->plan_active->DisplayValueSeparator) ?>" id="x_plan_active" name="x_plan_active"<?php echo $plans->plan_active->EditAttributes() ?>>
<?php
if (is_array($plans->plan_active->EditValue)) {
	$arwrk = $plans->plan_active->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_active->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_active->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_active->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_active->CurrentValue) ?>" selected><?php echo $plans->plan_active->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
<?php if (!$plans_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fplanssearch.Init();
</script>
<?php
$plans_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$plans_search->Page_Terminate();
?>
