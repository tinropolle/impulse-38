<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "worksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "periodsinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "tasksinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "e_tasksinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "e_tasks_financeinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$works_view = NULL; // Initialize page object first

class cworks_view extends cworks {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'works';

	// Page object name
	var $PageObjName = 'works_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (works)
		if (!isset($GLOBALS["works"]) || get_class($GLOBALS["works"]) == "cworks") {
			$GLOBALS["works"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["works"];
		}
		$KeyUrl = "";
		if (@$_GET["work_id"] <> "") {
			$this->RecKey["work_id"] = $_GET["work_id"];
			$KeyUrl .= "&amp;work_id=" . urlencode($this->RecKey["work_id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (periods)
		if (!isset($GLOBALS['periods'])) $GLOBALS['periods'] = new cperiods();

		// Table object (projects)
		if (!isset($GLOBALS['projects'])) $GLOBALS['projects'] = new cprojects();

		// Table object (tasks)
		if (!isset($GLOBALS['tasks'])) $GLOBALS['tasks'] = new ctasks();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Table object (e_tasks)
		if (!isset($GLOBALS['e_tasks'])) $GLOBALS['e_tasks'] = new ce_tasks();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Table object (e_tasks_finance)
		if (!isset($GLOBALS['e_tasks_finance'])) $GLOBALS['e_tasks_finance'] = new ce_tasks_finance();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'works', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			}
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->work_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $works;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($works);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["work_id"] <> "") {
				$this->work_id->setQueryStringValue($_GET["work_id"]);
				$this->RecKey["work_id"] = $this->work_id->QueryStringValue;
			} elseif (@$_POST["work_id"] <> "") {
				$this->work_id->setFormValue($_POST["work_id"]);
				$this->RecKey["work_id"] = $this->work_id->FormValue;
			} else {
				$sReturnUrl = "workslist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "workslist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "workslist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit()&& $this->ShowOptionLink('edit'));

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd() && $this->ShowOptionLink('add'));

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete() && $this->ShowOptionLink('delete'));

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		if ($this->AuditTrailOnView) $this->WriteAuditTrailOnView($row);
		$this->work_id->setDbValue($rs->fields('work_id'));
		$this->work_period_id->setDbValue($rs->fields('work_period_id'));
		$this->work_project_id->setDbValue($rs->fields('work_project_id'));
		$this->work_plan_id->setDbValue($rs->fields('work_plan_id'));
		$this->work_lab_id->setDbValue($rs->fields('work_lab_id'));
		$this->work_task_id->setDbValue($rs->fields('work_task_id'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
		$this->work_started->setDbValue($rs->fields('work_started'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->work_id->DbValue = $row['work_id'];
		$this->work_period_id->DbValue = $row['work_period_id'];
		$this->work_project_id->DbValue = $row['work_project_id'];
		$this->work_plan_id->DbValue = $row['work_plan_id'];
		$this->work_lab_id->DbValue = $row['work_lab_id'];
		$this->work_task_id->DbValue = $row['work_task_id'];
		$this->work_description->DbValue = $row['work_description'];
		$this->work_progress->DbValue = $row['work_progress'];
		$this->work_time->DbValue = $row['work_time'];
		$this->work_employee_id->DbValue = $row['work_employee_id'];
		$this->work_started->DbValue = $row['work_started'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->work_time->FormValue == $this->work_time->CurrentValue && is_numeric(ew_StrToFloat($this->work_time->CurrentValue)))
			$this->work_time->CurrentValue = ew_StrToFloat($this->work_time->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// work_id
		$this->work_id->ViewValue = $this->work_id->CurrentValue;
		$this->work_id->ViewCustomAttributes = "";

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

			// work_id
			$this->work_id->LinkCustomAttributes = "";
			$this->work_id->HrefValue = "";
			$this->work_id->TooltipValue = "";

			// work_period_id
			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";
			$this->work_project_id->TooltipValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";
			$this->work_plan_id->TooltipValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";
			$this->work_lab_id->TooltipValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";
			$this->work_task_id->TooltipValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";
			$this->work_description->TooltipValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";
			$this->work_progress->TooltipValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";
			$this->work_time->TooltipValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
			$this->work_employee_id->TooltipValue = "";

			// work_started
			$this->work_started->LinkCustomAttributes = "";
			$this->work_started->HrefValue = "";
			$this->work_started->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->work_employee_id->CurrentValue);
		return TRUE;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setQueryStringValue($_GET["fk_period_id"]);
					$this->work_period_id->setQueryStringValue($GLOBALS["periods"]->period_id->QueryStringValue);
					$this->work_period_id->setSessionValue($this->work_period_id->QueryStringValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setQueryStringValue($_GET["fk_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["projects"]->project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["e_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setFormValue($_POST["fk_period_id"]);
					$this->work_period_id->setFormValue($GLOBALS["periods"]->period_id->FormValue);
					$this->work_period_id->setSessionValue($this->work_period_id->FormValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setFormValue($_POST["fk_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["projects"]->project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["e_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks_finance"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks_finance"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);
			$this->setSessionWhere($this->GetDetailFilter());

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "periods") {
				if ($this->work_period_id->CurrentValue == "") $this->work_period_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "projects") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "v_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks_finance") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("workslist.php"), "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'works';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($works_view)) $works_view = new cworks_view();

// Page init
$works_view->Page_Init();

// Page main
$works_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$works_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fworksview = new ew_Form("fworksview", "view");

// Form_CustomValidate event
fworksview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fworksview.ValidateRequired = true;
<?php } else { ?>
fworksview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fworksview.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksview.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fworksview.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworksview.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworksview.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksview.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksview.Lists["x_work_progress"].Options = <?php echo json_encode($works->work_progress->Options()) ?>;
fworksview.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php $works_view->ExportOptions->Render("body") ?>
<?php
	foreach ($works_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $works_view->ShowPageHeader(); ?>
<?php
$works_view->ShowMessage();
?>
<form name="fworksview" id="fworksview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($works_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $works_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="works">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($works->work_id->Visible) { // work_id ?>
	<tr id="r_work_id">
		<td><span id="elh_works_work_id"><?php echo $works->work_id->FldCaption() ?></span></td>
		<td data-name="work_id"<?php echo $works->work_id->CellAttributes() ?>>
<span id="el_works_work_id">
<span<?php echo $works->work_id->ViewAttributes() ?>>
<?php echo $works->work_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_period_id->Visible) { // work_period_id ?>
	<tr id="r_work_period_id">
		<td><span id="elh_works_work_period_id"><?php echo $works->work_period_id->FldCaption() ?></span></td>
		<td data-name="work_period_id"<?php echo $works->work_period_id->CellAttributes() ?>>
<span id="el_works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<?php echo $works->work_period_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_project_id->Visible) { // work_project_id ?>
	<tr id="r_work_project_id">
		<td><span id="elh_works_work_project_id"><?php echo $works->work_project_id->FldCaption() ?></span></td>
		<td data-name="work_project_id"<?php echo $works->work_project_id->CellAttributes() ?>>
<span id="el_works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<?php echo $works->work_project_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
	<tr id="r_work_plan_id">
		<td><span id="elh_works_work_plan_id"><?php echo $works->work_plan_id->FldCaption() ?></span></td>
		<td data-name="work_plan_id"<?php echo $works->work_plan_id->CellAttributes() ?>>
<span id="el_works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<?php echo $works->work_plan_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
	<tr id="r_work_lab_id">
		<td><span id="elh_works_work_lab_id"><?php echo $works->work_lab_id->FldCaption() ?></span></td>
		<td data-name="work_lab_id"<?php echo $works->work_lab_id->CellAttributes() ?>>
<span id="el_works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<?php echo $works->work_lab_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_task_id->Visible) { // work_task_id ?>
	<tr id="r_work_task_id">
		<td><span id="elh_works_work_task_id"><?php echo $works->work_task_id->FldCaption() ?></span></td>
		<td data-name="work_task_id"<?php echo $works->work_task_id->CellAttributes() ?>>
<span id="el_works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<?php echo $works->work_task_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_description->Visible) { // work_description ?>
	<tr id="r_work_description">
		<td><span id="elh_works_work_description"><?php echo $works->work_description->FldCaption() ?></span></td>
		<td data-name="work_description"<?php echo $works->work_description->CellAttributes() ?>>
<span id="el_works_work_description">
<span<?php echo $works->work_description->ViewAttributes() ?>>
<?php echo $works->work_description->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_progress->Visible) { // work_progress ?>
	<tr id="r_work_progress">
		<td><span id="elh_works_work_progress"><?php echo $works->work_progress->FldCaption() ?></span></td>
		<td data-name="work_progress"<?php echo $works->work_progress->CellAttributes() ?>>
<span id="el_works_work_progress">
<span<?php echo $works->work_progress->ViewAttributes() ?>>
<?php echo $works->work_progress->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_time->Visible) { // work_time ?>
	<tr id="r_work_time">
		<td><span id="elh_works_work_time"><?php echo $works->work_time->FldCaption() ?></span></td>
		<td data-name="work_time"<?php echo $works->work_time->CellAttributes() ?>>
<span id="el_works_work_time">
<span<?php echo $works->work_time->ViewAttributes() ?>>
<?php echo $works->work_time->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
	<tr id="r_work_employee_id">
		<td><span id="elh_works_work_employee_id"><?php echo $works->work_employee_id->FldCaption() ?></span></td>
		<td data-name="work_employee_id"<?php echo $works->work_employee_id->CellAttributes() ?>>
<span id="el_works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<?php echo $works->work_employee_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($works->work_started->Visible) { // work_started ?>
	<tr id="r_work_started">
		<td><span id="elh_works_work_started"><?php echo $works->work_started->FldCaption() ?></span></td>
		<td data-name="work_started"<?php echo $works->work_started->CellAttributes() ?>>
<span id="el_works_work_started">
<span<?php echo $works->work_started->ViewAttributes() ?>>
<?php echo $works->work_started->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</form>
<script type="text/javascript">
fworksview.Init();
</script>
<?php
$works_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$works_view->Page_Terminate();
?>
