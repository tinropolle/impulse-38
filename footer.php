<?php if (@$gsExport == "") { ?>
<?php if (@!$gbSkipHeaderFooter) { ?>
				<!-- right column (end) -->
				<?php if (isset($gTimer)) $gTimer->Stop() ?>
			</div>
		</div>
	</div>
	<!-- content (end) -->
	<!-- footer (begin) --><!-- ** Note: Only licensed users are allowed to remove or change the following copyright statement. ** -->
	<div id="ewFooterRow" class="ewFooterRow">	
		<div class="ewFooterText"><?php echo $Language->ProjectPhrase("FooterText") ?></div>
		<!-- Place other links, for example, disclaimer, here -->		
	</div>
	<!-- footer (end) -->	
</div>
<?php } ?>
<!-- search dialog -->
<div id="ewSearchDialog" class="modal" role="dialog" aria-labelledby="ewSearchDialogTitle" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="ewSearchDialogTitle"></h4></div><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-primary ewButton"><?php echo $Language->Phrase("Search") ?></button><button type="button" class="btn btn-default ewButton" data-dismiss="modal"><?php echo $Language->Phrase("CancelBtn") ?></button></div></div></div></div>
<!-- message box -->
<div id="ewMsgBox" class="modal" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-primary ewButton" data-dismiss="modal"><?php echo $Language->Phrase("MessageOK") ?></button></div></div></div></div>
<!-- prompt -->
<div id="ewPrompt" class="modal" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-primary ewButton"><?php echo $Language->Phrase("MessageOK") ?></button><button type="button" class="btn btn-default ewButton" data-dismiss="modal"><?php echo $Language->Phrase("CancelBtn") ?></button></div></div></div></div>
<!-- session timer -->
<div id="ewTimer" class="modal" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-primary ewButton" data-dismiss="modal"><?php echo $Language->Phrase("MessageOK") ?></button></div></div></div></div>
<!-- tooltip -->
<div id="ewTooltip"></div>
<?php } ?>
<?php if (@$gsExport == "") { ?>
<script type="text/javascript" src="<?php echo $EW_RELATIVE_PATH ?>phpjs/userevt12.js"></script>
<script type="text/javascript">
$("div.ewToolbar").append('<div class="hidden-xs" style="position: fixed; top: 28px; right: 24px;"><?php echo CurrentUserName() . " @ " . $Language->ProjectPhrase("BodyTitle") ?></div>');
if (typeof CurrentSearchForm !== 'undefined')
{
	var savedFilters = JSON.parse(localStorage.getItem(CurrentSearchForm.ID + "_filters"));
	if (savedFilters)
	{
		for (var i = 0; i < savedFilters.length; i++)
		{
			var obj = savedFilters[i];
			if (obj != null && JSON.stringify(obj[1]) === JSON.stringify(currentFilter))
			{
				$("div.ewFilterOption > div.ewButtonDropdown").append(
					'<div style=" background-color: #1565C0; border: 1px solid #0D47A1; border-top-right-radius: 4px; border-bottom-right-radius: 4px; color: white; float: left; margin-left: -4px; padding: 4px 16px;">' + obj[0] + '</div>'
				)
			}
		}
	}
}

// Check maintenance status
<?php
	$checkMaintenance = ew_ExecuteScalar(
		"SELECT 1 FROM config WHERE config_name = 'on_maintenance' AND config_value = 1");
	if ($checkMaintenance === "1")
	{
		echo "$('body').prepend('<div class=\"on-maintenance\">Ведутся технические работы</div>');";
	}
?>

function setStatus()
{

}

/* $("div.ewToolbar").append('<input type="checkbox" name="><label for="') */
</script>
<?php } ?>
</body>
</html>
