<?php

// plan_project_id
// plan_code
// plan_name
// plan_employee_id
// plan_active

?>
<?php if ($plans->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $plans->TableCaption() ?></h4> -->
<table id="tbl_plansmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $plans->TableCustomInnerHtml ?>
	<tbody>
<?php if ($plans->plan_project_id->Visible) { // plan_project_id ?>
		<tr id="r_plan_project_id">
			<td><?php echo $plans->plan_project_id->FldCaption() ?></td>
			<td<?php echo $plans->plan_project_id->CellAttributes() ?>>
<span id="el_plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<?php echo $plans->plan_project_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($plans->plan_code->Visible) { // plan_code ?>
		<tr id="r_plan_code">
			<td><?php echo $plans->plan_code->FldCaption() ?></td>
			<td<?php echo $plans->plan_code->CellAttributes() ?>>
<span id="el_plans_plan_code">
<span<?php echo $plans->plan_code->ViewAttributes() ?>>
<?php echo $plans->plan_code->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($plans->plan_name->Visible) { // plan_name ?>
		<tr id="r_plan_name">
			<td><?php echo $plans->plan_name->FldCaption() ?></td>
			<td<?php echo $plans->plan_name->CellAttributes() ?>>
<span id="el_plans_plan_name">
<span<?php echo $plans->plan_name->ViewAttributes() ?>>
<?php echo $plans->plan_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($plans->plan_employee_id->Visible) { // plan_employee_id ?>
		<tr id="r_plan_employee_id">
			<td><?php echo $plans->plan_employee_id->FldCaption() ?></td>
			<td<?php echo $plans->plan_employee_id->CellAttributes() ?>>
<span id="el_plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<?php echo $plans->plan_employee_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($plans->plan_active->Visible) { // plan_active ?>
		<tr id="r_plan_active">
			<td><?php echo $plans->plan_active->FldCaption() ?></td>
			<td<?php echo $plans->plan_active->CellAttributes() ?>>
<span id="el_plans_plan_active">
<span<?php echo $plans->plan_active->ViewAttributes() ?>>
<?php echo $plans->plan_active->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
