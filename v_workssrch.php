<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "v_worksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "labsinfo.php" ?>
<?php include_once "tasksinfo.php" ?>
<?php include_once "e_tasks_financeinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$v_works_search = NULL; // Initialize page object first

class cv_works_search extends cv_works {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'v_works';

	// Page object name
	var $PageObjName = 'v_works_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (v_works)
		if (!isset($GLOBALS["v_works"]) || get_class($GLOBALS["v_works"]) == "cv_works") {
			$GLOBALS["v_works"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["v_works"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (labs)
		if (!isset($GLOBALS['labs'])) $GLOBALS['labs'] = new clabs();

		// Table object (tasks)
		if (!isset($GLOBALS['tasks'])) $GLOBALS['tasks'] = new ctasks();

		// Table object (e_tasks_finance)
		if (!isset($GLOBALS['e_tasks_finance'])) $GLOBALS['e_tasks_finance'] = new ce_tasks_finance();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'v_works', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("v_workslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->work_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $v_works;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($v_works);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "v_workslist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->work_id); // work_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_period_id); // work_period_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_project_id); // work_project_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_plan_id); // work_plan_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_lab_id); // work_lab_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_task_id); // work_task_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_description); // work_description
		$this->BuildSearchUrl($sSrchUrl, $this->work_progress); // work_progress
		$this->BuildSearchUrl($sSrchUrl, $this->work_time); // work_time
		$this->BuildSearchUrl($sSrchUrl, $this->work_employee_id); // work_employee_id
		$this->BuildSearchUrl($sSrchUrl, $this->work_started); // work_started
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// work_id

		$this->work_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_id"));
		$this->work_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_id");

		// work_period_id
		$this->work_period_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_period_id"));
		$this->work_period_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_period_id");

		// work_project_id
		$this->work_project_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_project_id"));
		$this->work_project_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_project_id");

		// work_plan_id
		$this->work_plan_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_plan_id"));
		$this->work_plan_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_plan_id");

		// work_lab_id
		$this->work_lab_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_lab_id"));
		$this->work_lab_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_lab_id");

		// work_task_id
		$this->work_task_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_task_id"));
		$this->work_task_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_task_id");

		// work_description
		$this->work_description->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_description"));
		$this->work_description->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_description");

		// work_progress
		$this->work_progress->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_progress"));
		$this->work_progress->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_progress");

		// work_time
		$this->work_time->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_time"));
		$this->work_time->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_time");

		// work_employee_id
		$this->work_employee_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_employee_id"));
		$this->work_employee_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_employee_id");

		// work_started
		$this->work_started->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_work_started"));
		$this->work_started->AdvancedSearch->SearchOperator = $objForm->GetValue("z_work_started");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->work_time->FormValue == $this->work_time->CurrentValue && is_numeric(ew_StrToFloat($this->work_time->CurrentValue)))
			$this->work_time->CurrentValue = ew_StrToFloat($this->work_time->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// work_id
		$this->work_id->ViewValue = $this->work_id->CurrentValue;
		$this->work_id->ViewCustomAttributes = "";

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

			// work_id
			$this->work_id->LinkCustomAttributes = "";
			$this->work_id->HrefValue = "";
			$this->work_id->TooltipValue = "";

			// work_period_id
			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";
			$this->work_project_id->TooltipValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";
			$this->work_plan_id->TooltipValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";
			$this->work_lab_id->TooltipValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";
			$this->work_task_id->TooltipValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";
			$this->work_description->TooltipValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";
			$this->work_progress->TooltipValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";
			$this->work_time->TooltipValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
			$this->work_employee_id->TooltipValue = "";

			// work_started
			$this->work_started->LinkCustomAttributes = "";
			$this->work_started->HrefValue = "";
			$this->work_started->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// work_id
			$this->work_id->EditAttrs["class"] = "form-control";
			$this->work_id->EditCustomAttributes = "";
			$this->work_id->EditValue = ew_HtmlEncode($this->work_id->AdvancedSearch->SearchValue);
			$this->work_id->PlaceHolder = ew_RemoveHtml($this->work_id->FldCaption());

			// work_period_id
			$this->work_period_id->EditAttrs["class"] = "form-control";
			$this->work_period_id->EditCustomAttributes = "";
			if (trim(strval($this->work_period_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `periods`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_period_id->EditValue = $arwrk;

			// work_project_id
			$this->work_project_id->EditAttrs["class"] = "form-control";
			$this->work_project_id->EditCustomAttributes = "";
			if (trim(strval($this->work_project_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_project_id->EditValue = $arwrk;

			// work_plan_id
			$this->work_plan_id->EditAttrs["class"] = "form-control";
			$this->work_plan_id->EditCustomAttributes = "";
			if (trim(strval($this->work_plan_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `plan_project_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `plans`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_plan_id->EditValue = $arwrk;

			// work_lab_id
			$this->work_lab_id->EditAttrs["class"] = "form-control";
			$this->work_lab_id->EditCustomAttributes = "";
			if (trim(strval($this->work_lab_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_lab_id->EditValue = $arwrk;

			// work_task_id
			$this->work_task_id->EditAttrs["class"] = "form-control";
			$this->work_task_id->EditCustomAttributes = "";
			$this->work_task_id->EditValue = ew_HtmlEncode($this->work_task_id->AdvancedSearch->SearchValue);
			if (strval($this->work_task_id->AdvancedSearch->SearchValue) <> "") {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
					$arwrk[2] = ew_HtmlEncode($rswrk->fields('Disp2Fld'));
					$this->work_task_id->EditValue = $this->work_task_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_task_id->EditValue = ew_HtmlEncode($this->work_task_id->AdvancedSearch->SearchValue);
				}
			} else {
				$this->work_task_id->EditValue = NULL;
			}
			$this->work_task_id->PlaceHolder = ew_RemoveHtml($this->work_task_id->FldCaption());

			// work_description
			$this->work_description->EditAttrs["class"] = "form-control";
			$this->work_description->EditCustomAttributes = "";
			$this->work_description->EditValue = ew_HtmlEncode($this->work_description->AdvancedSearch->SearchValue);
			$this->work_description->PlaceHolder = ew_RemoveHtml($this->work_description->FldCaption());

			// work_progress
			$this->work_progress->EditAttrs["class"] = "form-control";
			$this->work_progress->EditCustomAttributes = "";
			$this->work_progress->EditValue = $this->work_progress->Options(TRUE);

			// work_time
			$this->work_time->EditAttrs["class"] = "form-control";
			$this->work_time->EditCustomAttributes = "";
			$this->work_time->EditValue = ew_HtmlEncode($this->work_time->AdvancedSearch->SearchValue);
			$this->work_time->PlaceHolder = ew_RemoveHtml($this->work_time->FldCaption());

			// work_employee_id
			$this->work_employee_id->EditAttrs["class"] = "form-control";
			$this->work_employee_id->EditCustomAttributes = "";
			if (trim(strval($this->work_employee_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_employee_id->EditValue = $arwrk;

			// work_started
			$this->work_started->EditAttrs["class"] = "form-control";
			$this->work_started->EditCustomAttributes = "";
			$this->work_started->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->work_started->AdvancedSearch->SearchValue, 7), 7));
			$this->work_started->PlaceHolder = ew_RemoveHtml($this->work_started->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->work_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->work_id->FldErrMsg());
		}
		if (!ew_CheckInteger($this->work_task_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->work_task_id->FldErrMsg());
		}
		if (!ew_CheckNumber($this->work_time->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->work_time->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->work_started->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->work_started->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->work_id->AdvancedSearch->Load();
		$this->work_period_id->AdvancedSearch->Load();
		$this->work_project_id->AdvancedSearch->Load();
		$this->work_plan_id->AdvancedSearch->Load();
		$this->work_lab_id->AdvancedSearch->Load();
		$this->work_task_id->AdvancedSearch->Load();
		$this->work_description->AdvancedSearch->Load();
		$this->work_progress->AdvancedSearch->Load();
		$this->work_time->AdvancedSearch->Load();
		$this->work_employee_id->AdvancedSearch->Load();
		$this->work_started->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("v_workslist.php"), "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($v_works_search)) $v_works_search = new cv_works_search();

// Page init
$v_works_search->Page_Init();

// Page main
$v_works_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$v_works_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($v_works_search->IsModal) { ?>
var CurrentAdvancedSearchForm = fv_workssearch = new ew_Form("fv_workssearch", "search");
<?php } else { ?>
var CurrentForm = fv_workssearch = new ew_Form("fv_workssearch", "search");
<?php } ?>

// Form_CustomValidate event
fv_workssearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fv_workssearch.ValidateRequired = true;
<?php } else { ?>
fv_workssearch.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fv_workssearch.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fv_workssearch.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fv_workssearch.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_work_project_id"],"ChildFields":["x_work_task_id"],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
fv_workssearch.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fv_workssearch.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":["x_work_plan_id","x_work_lab_id"],"ChildFields":[],"FilterFields":["x_task_plan_id","x_task_lab_id"],"Options":[],"Template":""};
fv_workssearch.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fv_workssearch.Lists["x_work_progress"].Options = <?php echo json_encode($v_works->work_progress->Options()) ?>;
fv_workssearch.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
// Validate function for search

fv_workssearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_work_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($v_works->work_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_work_task_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($v_works->work_task_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_work_time");
	if (elm && !ew_CheckNumber(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($v_works->work_time->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_work_started");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($v_works->work_started->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$v_works_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $v_works_search->ShowPageHeader(); ?>
<?php
$v_works_search->ShowMessage();
?>
<form name="fv_workssearch" id="fv_workssearch" class="<?php echo $v_works_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($v_works_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $v_works_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="v_works">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($v_works_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($v_works->work_id->Visible) { // work_id ?>
	<div id="r_work_id" class="form-group">
		<label for="x_work_id" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_id"><?php echo $v_works->work_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_id" id="z_work_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_id->CellAttributes() ?>>
			<span id="el_v_works_work_id">
<input type="text" data-table="v_works" data-field="x_work_id" name="x_work_id" id="x_work_id" placeholder="<?php echo ew_HtmlEncode($v_works->work_id->getPlaceHolder()) ?>" value="<?php echo $v_works->work_id->EditValue ?>"<?php echo $v_works->work_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_period_id->Visible) { // work_period_id ?>
	<div id="r_work_period_id" class="form-group">
		<label for="x_work_period_id" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_period_id"><?php echo $v_works->work_period_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_period_id" id="z_work_period_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_period_id->CellAttributes() ?>>
			<span id="el_v_works_work_period_id">
<select data-table="v_works" data-field="x_work_period_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_period_id->DisplayValueSeparator) ? json_encode($v_works->work_period_id->DisplayValueSeparator) : $v_works->work_period_id->DisplayValueSeparator) ?>" id="x_work_period_id" name="x_work_period_id"<?php echo $v_works->work_period_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_period_id->EditValue)) {
	$arwrk = $v_works->work_period_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_period_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_period_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_period_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_period_id->CurrentValue) ?>" selected><?php echo $v_works->work_period_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
$sWhereWrk = "";
$v_works->work_period_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_period_id->LookupFilters += array("f0" => "`period_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_period_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `period_from` DESC";
if ($sSqlWrk <> "") $v_works->work_period_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_period_id" id="s_x_work_period_id" value="<?php echo $v_works->work_period_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_project_id->Visible) { // work_project_id ?>
	<div id="r_work_project_id" class="form-group">
		<label for="x_work_project_id" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_project_id"><?php echo $v_works->work_project_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_project_id" id="z_work_project_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_project_id->CellAttributes() ?>>
			<span id="el_v_works_work_project_id">
<?php $v_works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_project_id->DisplayValueSeparator) ? json_encode($v_works->work_project_id->DisplayValueSeparator) : $v_works->work_project_id->DisplayValueSeparator) ?>" id="x_work_project_id" name="x_work_project_id"<?php echo $v_works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_project_id->EditValue)) {
	$arwrk = $v_works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_project_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_project_id->CurrentValue) ?>" selected><?php echo $v_works->work_project_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$v_works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_project_id" id="s_x_work_project_id" value="<?php echo $v_works->work_project_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_plan_id->Visible) { // work_plan_id ?>
	<div id="r_work_plan_id" class="form-group">
		<label for="x_work_plan_id" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_plan_id"><?php echo $v_works->work_plan_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_plan_id" id="z_work_plan_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_plan_id->CellAttributes() ?>>
			<span id="el_v_works_work_plan_id">
<?php $v_works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_plan_id->DisplayValueSeparator) ? json_encode($v_works->work_plan_id->DisplayValueSeparator) : $v_works->work_plan_id->DisplayValueSeparator) ?>" id="x_work_plan_id" name="x_work_plan_id"<?php echo $v_works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_plan_id->EditValue)) {
	$arwrk = $v_works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_plan_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_plan_id->CurrentValue) ?>" selected><?php echo $v_works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$v_works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$v_works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_plan_id" id="s_x_work_plan_id" value="<?php echo $v_works->work_plan_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_lab_id->Visible) { // work_lab_id ?>
	<div id="r_work_lab_id" class="form-group">
		<label for="x_work_lab_id" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_lab_id"><?php echo $v_works->work_lab_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_lab_id" id="z_work_lab_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_lab_id->CellAttributes() ?>>
			<span id="el_v_works_work_lab_id">
<?php $v_works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$v_works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="v_works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_lab_id->DisplayValueSeparator) ? json_encode($v_works->work_lab_id->DisplayValueSeparator) : $v_works->work_lab_id->DisplayValueSeparator) ?>" id="x_work_lab_id" name="x_work_lab_id"<?php echo $v_works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_lab_id->EditValue)) {
	$arwrk = $v_works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_lab_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_lab_id->CurrentValue) ?>" selected><?php echo $v_works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$v_works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $v_works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_lab_id" id="s_x_work_lab_id" value="<?php echo $v_works->work_lab_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_task_id->Visible) { // work_task_id ?>
	<div id="r_work_task_id" class="form-group">
		<label class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_task_id"><?php echo $v_works->work_task_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_task_id" id="z_work_task_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_task_id->CellAttributes() ?>>
			<span id="el_v_works_work_task_id">
<?php
$wrkonchange = trim(" " . @$v_works->work_task_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$v_works->work_task_id->EditAttrs["onchange"] = "";
?>
<span id="as_x_work_task_id" style="white-space: nowrap; z-index: 8940">
	<input type="text" name="sv_x_work_task_id" id="sv_x_work_task_id" value="<?php echo $v_works->work_task_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($v_works->work_task_id->getPlaceHolder()) ?>"<?php echo $v_works->work_task_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="v_works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_task_id->DisplayValueSeparator) ? json_encode($v_works->work_task_id->DisplayValueSeparator) : $v_works->work_task_id->DisplayValueSeparator) ?>" name="x_work_task_id" id="x_work_task_id" value="<?php echo ew_HtmlEncode($v_works->work_task_id->AdvancedSearch->SearchValue) ?>"<?php echo $wrkonchange ?>>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld` FROM `tasks`";
$sWhereWrk = "(`task_code` LIKE '{query_value}%' OR CONCAT(`task_code`,'" . ew_ValueSeparator(1, $Page->work_task_id) . "',`task_name`) LIKE '{query_value}%') AND ({filter})";
$v_works->Lookup_Selecting($v_works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x_work_task_id" id="q_x_work_task_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&f1=<?php echo ew_Encrypt("`task_plan_id` IN ({filter_value})"); ?>&t1=3&f2=<?php echo ew_Encrypt("`task_lab_id` IN ({filter_value})"); ?>&t2=3&d=">
<script type="text/javascript">
fv_workssearch.CreateAutoSuggest({"id":"x_work_task_id","forceSelect":false});
</script>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_description->Visible) { // work_description ?>
	<div id="r_work_description" class="form-group">
		<label for="x_work_description" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_description"><?php echo $v_works->work_description->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_work_description" id="z_work_description" value="LIKE"></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_description->CellAttributes() ?>>
			<span id="el_v_works_work_description">
<input type="text" data-table="v_works" data-field="x_work_description" name="x_work_description" id="x_work_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($v_works->work_description->getPlaceHolder()) ?>" value="<?php echo $v_works->work_description->EditValue ?>"<?php echo $v_works->work_description->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_progress->Visible) { // work_progress ?>
	<div id="r_work_progress" class="form-group">
		<label for="x_work_progress" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_progress"><?php echo $v_works->work_progress->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_progress" id="z_work_progress" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_progress->CellAttributes() ?>>
			<span id="el_v_works_work_progress">
<select data-table="v_works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_progress->DisplayValueSeparator) ? json_encode($v_works->work_progress->DisplayValueSeparator) : $v_works->work_progress->DisplayValueSeparator) ?>" id="x_work_progress" name="x_work_progress"<?php echo $v_works->work_progress->EditAttributes() ?>>
<?php
if (is_array($v_works->work_progress->EditValue)) {
	$arwrk = $v_works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_progress->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_progress->CurrentValue) ?>" selected><?php echo $v_works->work_progress->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_time->Visible) { // work_time ?>
	<div id="r_work_time" class="form-group">
		<label for="x_work_time" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_time"><?php echo $v_works->work_time->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_time" id="z_work_time" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_time->CellAttributes() ?>>
			<span id="el_v_works_work_time">
<input type="text" data-table="v_works" data-field="x_work_time" name="x_work_time" id="x_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($v_works->work_time->getPlaceHolder()) ?>" value="<?php echo $v_works->work_time->EditValue ?>"<?php echo $v_works->work_time->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_employee_id->Visible) { // work_employee_id ?>
	<div id="r_work_employee_id" class="form-group">
		<label for="x_work_employee_id" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_employee_id"><?php echo $v_works->work_employee_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_employee_id" id="z_work_employee_id" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_employee_id->CellAttributes() ?>>
			<span id="el_v_works_work_employee_id">
<select data-table="v_works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($v_works->work_employee_id->DisplayValueSeparator) ? json_encode($v_works->work_employee_id->DisplayValueSeparator) : $v_works->work_employee_id->DisplayValueSeparator) ?>" id="x_work_employee_id" name="x_work_employee_id"<?php echo $v_works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($v_works->work_employee_id->EditValue)) {
	$arwrk = $v_works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($v_works->work_employee_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $v_works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($v_works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($v_works->work_employee_id->CurrentValue) ?>" selected><?php echo $v_works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$v_works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$v_works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$v_works->Lookup_Selecting($v_works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $v_works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_employee_id" id="s_x_work_employee_id" value="<?php echo $v_works->work_employee_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($v_works->work_started->Visible) { // work_started ?>
	<div id="r_work_started" class="form-group">
		<label for="x_work_started" class="<?php echo $v_works_search->SearchLabelClass ?>"><span id="elh_v_works_work_started"><?php echo $v_works->work_started->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_work_started" id="z_work_started" value="="></p>
		</label>
		<div class="<?php echo $v_works_search->SearchRightColumnClass ?>"><div<?php echo $v_works->work_started->CellAttributes() ?>>
			<span id="el_v_works_work_started">
<input type="text" data-table="v_works" data-field="x_work_started" data-format="7" name="x_work_started" id="x_work_started" placeholder="<?php echo ew_HtmlEncode($v_works->work_started->getPlaceHolder()) ?>" value="<?php echo $v_works->work_started->EditValue ?>"<?php echo $v_works->work_started->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
<?php if (!$v_works_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fv_workssearch.Init();
</script>
<?php
$v_works_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$v_works_search->Page_Terminate();
?>
