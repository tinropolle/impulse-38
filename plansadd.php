<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "tasksgridcls.php" ?>
<?php include_once "e_tasksgridcls.php" ?>
<?php include_once "e_tasks_financegridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$plans_add = NULL; // Initialize page object first

class cplans_add extends cplans {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'plans';

	// Page object name
	var $PageObjName = 'plans_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = TRUE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (plans)
		if (!isset($GLOBALS["plans"]) || get_class($GLOBALS["plans"]) == "cplans") {
			$GLOBALS["plans"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["plans"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (projects)
		if (!isset($GLOBALS['projects'])) $GLOBALS['projects'] = new cprojects();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'plans', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("planslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'tasks'
			if (@$_POST["grid"] == "ftasksgrid") {
				if (!isset($GLOBALS["tasks_grid"])) $GLOBALS["tasks_grid"] = new ctasks_grid;
				$GLOBALS["tasks_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'e_tasks'
			if (@$_POST["grid"] == "fe_tasksgrid") {
				if (!isset($GLOBALS["e_tasks_grid"])) $GLOBALS["e_tasks_grid"] = new ce_tasks_grid;
				$GLOBALS["e_tasks_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'e_tasks_finance'
			if (@$_POST["grid"] == "fe_tasks_financegrid") {
				if (!isset($GLOBALS["e_tasks_finance_grid"])) $GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid;
				$GLOBALS["e_tasks_finance_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $plans;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($plans);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["plan_id"] != "") {
				$this->plan_id->setQueryStringValue($_GET["plan_id"]);
				$this->setKey("plan_id", $this->plan_id->CurrentValue); // Set up key
			} else {
				$this->setKey("plan_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Set up detail parameters
		$this->SetUpDetailParms();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		} else {
			if ($this->CurrentAction == "I") // Load default values for blank record
				$this->LoadDefaultValues();
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("planslist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					if ($this->getCurrentDetailTable() <> "") // Master/detail add
						$sReturnUrl = $this->GetDetailUrl();
					else
						$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "planslist.php")
						$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
					elseif (ew_GetPageName($sReturnUrl) == "plansview.php")
						$sReturnUrl = $this->GetViewUrl(); // View page, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->plan_project_id->CurrentValue = NULL;
		$this->plan_project_id->OldValue = $this->plan_project_id->CurrentValue;
		$this->plan_code->CurrentValue = NULL;
		$this->plan_code->OldValue = $this->plan_code->CurrentValue;
		$this->plan_name->CurrentValue = NULL;
		$this->plan_name->OldValue = $this->plan_name->CurrentValue;
		$this->plan_employee_id->CurrentValue = NULL;
		$this->plan_employee_id->OldValue = $this->plan_employee_id->CurrentValue;
		$this->plan_active->CurrentValue = 1;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->plan_project_id->FldIsDetailKey) {
			$this->plan_project_id->setFormValue($objForm->GetValue("x_plan_project_id"));
		}
		if (!$this->plan_code->FldIsDetailKey) {
			$this->plan_code->setFormValue($objForm->GetValue("x_plan_code"));
		}
		if (!$this->plan_name->FldIsDetailKey) {
			$this->plan_name->setFormValue($objForm->GetValue("x_plan_name"));
		}
		if (!$this->plan_employee_id->FldIsDetailKey) {
			$this->plan_employee_id->setFormValue($objForm->GetValue("x_plan_employee_id"));
		}
		if (!$this->plan_active->FldIsDetailKey) {
			$this->plan_active->setFormValue($objForm->GetValue("x_plan_active"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->plan_project_id->CurrentValue = $this->plan_project_id->FormValue;
		$this->plan_code->CurrentValue = $this->plan_code->FormValue;
		$this->plan_name->CurrentValue = $this->plan_name->FormValue;
		$this->plan_employee_id->CurrentValue = $this->plan_employee_id->FormValue;
		$this->plan_active->CurrentValue = $this->plan_active->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->plan_id->setDbValue($rs->fields('plan_id'));
		$this->plan_project_id->setDbValue($rs->fields('plan_project_id'));
		$this->plan_code->setDbValue($rs->fields('plan_code'));
		$this->plan_name->setDbValue($rs->fields('plan_name'));
		$this->plan_employee_id->setDbValue($rs->fields('plan_employee_id'));
		$this->plan_active->setDbValue($rs->fields('plan_active'));
		$this->plan_task_index->setDbValue($rs->fields('plan_task_index'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->plan_id->DbValue = $row['plan_id'];
		$this->plan_project_id->DbValue = $row['plan_project_id'];
		$this->plan_code->DbValue = $row['plan_code'];
		$this->plan_name->DbValue = $row['plan_name'];
		$this->plan_employee_id->DbValue = $row['plan_employee_id'];
		$this->plan_active->DbValue = $row['plan_active'];
		$this->plan_task_index->DbValue = $row['plan_task_index'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("plan_id")) <> "")
			$this->plan_id->CurrentValue = $this->getKey("plan_id"); // plan_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// plan_id
		// plan_project_id
		// plan_code
		// plan_name
		// plan_employee_id
		// plan_active
		// plan_task_index

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// plan_id
		$this->plan_id->ViewValue = $this->plan_id->CurrentValue;
		$this->plan_id->ViewCustomAttributes = "";

		// plan_project_id
		if (strval($this->plan_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->plan_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->plan_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->plan_project_id->ViewValue = $this->plan_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->plan_project_id->ViewValue = $this->plan_project_id->CurrentValue;
			}
		} else {
			$this->plan_project_id->ViewValue = NULL;
		}
		$this->plan_project_id->ViewCustomAttributes = "";

		// plan_code
		if (strval($this->plan_code->CurrentValue) <> "") {
			$this->plan_code->ViewValue = $this->plan_code->OptionCaption($this->plan_code->CurrentValue);
		} else {
			$this->plan_code->ViewValue = NULL;
		}
		$this->plan_code->ViewCustomAttributes = "";

		// plan_name
		$this->plan_name->ViewValue = $this->plan_name->CurrentValue;
		$this->plan_name->ViewCustomAttributes = "";

		// plan_employee_id
		if (strval($this->plan_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->plan_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->plan_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->plan_employee_id->ViewValue = $this->plan_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->plan_employee_id->ViewValue = $this->plan_employee_id->CurrentValue;
			}
		} else {
			$this->plan_employee_id->ViewValue = NULL;
		}
		$this->plan_employee_id->ViewCustomAttributes = "";

		// plan_active
		if (strval($this->plan_active->CurrentValue) <> "") {
			$this->plan_active->ViewValue = $this->plan_active->OptionCaption($this->plan_active->CurrentValue);
		} else {
			$this->plan_active->ViewValue = NULL;
		}
		$this->plan_active->ViewCustomAttributes = "";

			// plan_project_id
			$this->plan_project_id->LinkCustomAttributes = "";
			$this->plan_project_id->HrefValue = "";
			$this->plan_project_id->TooltipValue = "";

			// plan_code
			$this->plan_code->LinkCustomAttributes = "";
			$this->plan_code->HrefValue = "";
			$this->plan_code->TooltipValue = "";

			// plan_name
			$this->plan_name->LinkCustomAttributes = "";
			$this->plan_name->HrefValue = "";
			$this->plan_name->TooltipValue = "";

			// plan_employee_id
			$this->plan_employee_id->LinkCustomAttributes = "";
			$this->plan_employee_id->HrefValue = "";
			$this->plan_employee_id->TooltipValue = "";

			// plan_active
			$this->plan_active->LinkCustomAttributes = "";
			$this->plan_active->HrefValue = "";
			$this->plan_active->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// plan_project_id
			$this->plan_project_id->EditAttrs["class"] = "form-control";
			$this->plan_project_id->EditCustomAttributes = "";
			if ($this->plan_project_id->getSessionValue() <> "") {
				$this->plan_project_id->CurrentValue = $this->plan_project_id->getSessionValue();
			if (strval($this->plan_project_id->CurrentValue) <> "") {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->plan_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->plan_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->plan_project_id->ViewValue = $this->plan_project_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->plan_project_id->ViewValue = $this->plan_project_id->CurrentValue;
				}
			} else {
				$this->plan_project_id->ViewValue = NULL;
			}
			$this->plan_project_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->plan_project_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->plan_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->plan_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->plan_project_id->EditValue = $arwrk;
			}

			// plan_code
			$this->plan_code->EditAttrs["class"] = "form-control";
			$this->plan_code->EditCustomAttributes = "";
			$this->plan_code->EditValue = $this->plan_code->Options(TRUE);

			// plan_name
			$this->plan_name->EditAttrs["class"] = "form-control";
			$this->plan_name->EditCustomAttributes = "";
			$this->plan_name->EditValue = ew_HtmlEncode($this->plan_name->CurrentValue);
			$this->plan_name->PlaceHolder = ew_RemoveHtml($this->plan_name->FldCaption());

			// plan_employee_id
			$this->plan_employee_id->EditAttrs["class"] = "form-control";
			$this->plan_employee_id->EditCustomAttributes = "";
			if ($this->plan_employee_id->getSessionValue() <> "") {
				$this->plan_employee_id->CurrentValue = $this->plan_employee_id->getSessionValue();
			if (strval($this->plan_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->plan_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->plan_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->plan_employee_id->ViewValue = $this->plan_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->plan_employee_id->ViewValue = $this->plan_employee_id->CurrentValue;
				}
			} else {
				$this->plan_employee_id->ViewValue = NULL;
			}
			$this->plan_employee_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->plan_employee_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->plan_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->plan_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->plan_employee_id->EditValue = $arwrk;
			}

			// plan_active
			$this->plan_active->EditAttrs["class"] = "form-control";
			$this->plan_active->EditCustomAttributes = "";
			$this->plan_active->EditValue = $this->plan_active->Options(TRUE);

			// Add refer script
			// plan_project_id

			$this->plan_project_id->LinkCustomAttributes = "";
			$this->plan_project_id->HrefValue = "";

			// plan_code
			$this->plan_code->LinkCustomAttributes = "";
			$this->plan_code->HrefValue = "";

			// plan_name
			$this->plan_name->LinkCustomAttributes = "";
			$this->plan_name->HrefValue = "";

			// plan_employee_id
			$this->plan_employee_id->LinkCustomAttributes = "";
			$this->plan_employee_id->HrefValue = "";

			// plan_active
			$this->plan_active->LinkCustomAttributes = "";
			$this->plan_active->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->plan_project_id->FldIsDetailKey && !is_null($this->plan_project_id->FormValue) && $this->plan_project_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->plan_project_id->FldCaption(), $this->plan_project_id->ReqErrMsg));
		}
		if (!$this->plan_code->FldIsDetailKey && !is_null($this->plan_code->FormValue) && $this->plan_code->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->plan_code->FldCaption(), $this->plan_code->ReqErrMsg));
		}
		if (!$this->plan_name->FldIsDetailKey && !is_null($this->plan_name->FormValue) && $this->plan_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->plan_name->FldCaption(), $this->plan_name->ReqErrMsg));
		}
		if (!$this->plan_active->FldIsDetailKey && !is_null($this->plan_active->FormValue) && $this->plan_active->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->plan_active->FldCaption(), $this->plan_active->ReqErrMsg));
		}

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("tasks", $DetailTblVar) && $GLOBALS["tasks"]->DetailAdd) {
			if (!isset($GLOBALS["tasks_grid"])) $GLOBALS["tasks_grid"] = new ctasks_grid(); // get detail page object
			$GLOBALS["tasks_grid"]->ValidateGridForm();
		}
		if (in_array("e_tasks", $DetailTblVar) && $GLOBALS["e_tasks"]->DetailAdd) {
			if (!isset($GLOBALS["e_tasks_grid"])) $GLOBALS["e_tasks_grid"] = new ce_tasks_grid(); // get detail page object
			$GLOBALS["e_tasks_grid"]->ValidateGridForm();
		}
		if (in_array("e_tasks_finance", $DetailTblVar) && $GLOBALS["e_tasks_finance"]->DetailAdd) {
			if (!isset($GLOBALS["e_tasks_finance_grid"])) $GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid(); // get detail page object
			$GLOBALS["e_tasks_finance_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;

		// Check referential integrity for master table 'projects'
		$bValidMasterRecord = TRUE;
		$sMasterFilter = $this->SqlMasterFilter_projects();
		if (strval($this->plan_project_id->CurrentValue) <> "") {
			$sMasterFilter = str_replace("@project_id@", ew_AdjustSql($this->plan_project_id->CurrentValue, "DB"), $sMasterFilter);
		} else {
			$bValidMasterRecord = FALSE;
		}
		if ($bValidMasterRecord) {
			$rsmaster = $GLOBALS["projects"]->LoadRs($sMasterFilter);
			$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
			$rsmaster->Close();
		}
		if (!$bValidMasterRecord) {
			$sRelatedRecordMsg = str_replace("%t", "projects", $Language->Phrase("RelatedRecordRequired"));
			$this->setFailureMessage($sRelatedRecordMsg);
			return FALSE;
		}
		$conn = &$this->Connection();

		// Begin transaction
		if ($this->getCurrentDetailTable() <> "")
			$conn->BeginTrans();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// plan_project_id
		$this->plan_project_id->SetDbValueDef($rsnew, $this->plan_project_id->CurrentValue, 0, FALSE);

		// plan_code
		$this->plan_code->SetDbValueDef($rsnew, $this->plan_code->CurrentValue, 0, FALSE);

		// plan_name
		$this->plan_name->SetDbValueDef($rsnew, $this->plan_name->CurrentValue, "", FALSE);

		// plan_employee_id
		$this->plan_employee_id->SetDbValueDef($rsnew, $this->plan_employee_id->CurrentValue, NULL, FALSE);

		// plan_active
		$this->plan_active->SetDbValueDef($rsnew, $this->plan_active->CurrentValue, 0, strval($this->plan_active->CurrentValue) == "");

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->plan_id->setDbValue($conn->Insert_ID());
				$rsnew['plan_id'] = $this->plan_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Add detail records
		if ($AddRow) {
			$DetailTblVar = explode(",", $this->getCurrentDetailTable());
			if (in_array("tasks", $DetailTblVar) && $GLOBALS["tasks"]->DetailAdd) {
				$GLOBALS["tasks"]->task_plan_id->setSessionValue($this->plan_id->CurrentValue); // Set master key
				$GLOBALS["tasks"]->task_project_id->setSessionValue($this->plan_project_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["tasks_grid"])) $GLOBALS["tasks_grid"] = new ctasks_grid(); // Get detail page object
				$AddRow = $GLOBALS["tasks_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["tasks"]->task_project_id->setSessionValue(""); // Clear master key if insert failed
			}
			if (in_array("e_tasks", $DetailTblVar) && $GLOBALS["e_tasks"]->DetailAdd) {
				$GLOBALS["e_tasks"]->task_plan_id->setSessionValue($this->plan_id->CurrentValue); // Set master key
				$GLOBALS["e_tasks"]->task_project_id->setSessionValue($this->plan_project_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["e_tasks_grid"])) $GLOBALS["e_tasks_grid"] = new ce_tasks_grid(); // Get detail page object
				$AddRow = $GLOBALS["e_tasks_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["e_tasks"]->task_project_id->setSessionValue(""); // Clear master key if insert failed
			}
			if (in_array("e_tasks_finance", $DetailTblVar) && $GLOBALS["e_tasks_finance"]->DetailAdd) {
				$GLOBALS["e_tasks_finance"]->task_plan_id->setSessionValue($this->plan_id->CurrentValue); // Set master key
				$GLOBALS["e_tasks_finance"]->task_project_id->setSessionValue($this->plan_project_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["e_tasks_finance_grid"])) $GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid(); // Get detail page object
				$AddRow = $GLOBALS["e_tasks_finance_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["e_tasks_finance"]->task_project_id->setSessionValue(""); // Clear master key if insert failed
			}
		}

		// Commit/Rollback transaction
		if ($this->getCurrentDetailTable() <> "") {
			if ($AddRow) {
				$conn->CommitTrans(); // Commit transaction
			} else {
				$conn->RollbackTrans(); // Rollback transaction
			}
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->plan_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->plan_employee_id->setSessionValue($this->plan_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setQueryStringValue($_GET["fk_project_id"]);
					$this->plan_project_id->setQueryStringValue($GLOBALS["projects"]->project_id->QueryStringValue);
					$this->plan_project_id->setSessionValue($this->plan_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->plan_employee_id->setQueryStringValue($GLOBALS["e_employees"]->employee_id->QueryStringValue);
					$this->plan_employee_id->setSessionValue($this->plan_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->plan_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->plan_employee_id->setSessionValue($this->plan_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setFormValue($_POST["fk_project_id"]);
					$this->plan_project_id->setFormValue($GLOBALS["projects"]->project_id->FormValue);
					$this->plan_project_id->setSessionValue($this->plan_project_id->FormValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->plan_employee_id->setFormValue($GLOBALS["e_employees"]->employee_id->FormValue);
					$this->plan_employee_id->setSessionValue($this->plan_employee_id->FormValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "v_employees") {
				if ($this->plan_employee_id->CurrentValue == "") $this->plan_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "projects") {
				if ($this->plan_project_id->CurrentValue == "") $this->plan_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_employees") {
				if ($this->plan_employee_id->CurrentValue == "") $this->plan_employee_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("tasks", $DetailTblVar)) {
				if (!isset($GLOBALS["tasks_grid"]))
					$GLOBALS["tasks_grid"] = new ctasks_grid;
				if ($GLOBALS["tasks_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["tasks_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["tasks_grid"]->CurrentMode = "add";
					$GLOBALS["tasks_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["tasks_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["tasks_grid"]->setStartRecordNumber(1);
					$GLOBALS["tasks_grid"]->task_plan_id->FldIsDetailKey = TRUE;
					$GLOBALS["tasks_grid"]->task_plan_id->CurrentValue = $this->plan_id->CurrentValue;
					$GLOBALS["tasks_grid"]->task_plan_id->setSessionValue($GLOBALS["tasks_grid"]->task_plan_id->CurrentValue);
					$GLOBALS["tasks_grid"]->task_project_id->FldIsDetailKey = TRUE;
					$GLOBALS["tasks_grid"]->task_project_id->CurrentValue = $this->plan_project_id->CurrentValue;
					$GLOBALS["tasks_grid"]->task_project_id->setSessionValue($GLOBALS["tasks_grid"]->task_project_id->CurrentValue);
				}
			}
			if (in_array("e_tasks", $DetailTblVar)) {
				if (!isset($GLOBALS["e_tasks_grid"]))
					$GLOBALS["e_tasks_grid"] = new ce_tasks_grid;
				if ($GLOBALS["e_tasks_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["e_tasks_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["e_tasks_grid"]->CurrentMode = "add";
					$GLOBALS["e_tasks_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["e_tasks_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["e_tasks_grid"]->setStartRecordNumber(1);
					$GLOBALS["e_tasks_grid"]->task_plan_id->FldIsDetailKey = TRUE;
					$GLOBALS["e_tasks_grid"]->task_plan_id->CurrentValue = $this->plan_id->CurrentValue;
					$GLOBALS["e_tasks_grid"]->task_plan_id->setSessionValue($GLOBALS["e_tasks_grid"]->task_plan_id->CurrentValue);
					$GLOBALS["e_tasks_grid"]->task_project_id->FldIsDetailKey = TRUE;
					$GLOBALS["e_tasks_grid"]->task_project_id->CurrentValue = $this->plan_project_id->CurrentValue;
					$GLOBALS["e_tasks_grid"]->task_project_id->setSessionValue($GLOBALS["e_tasks_grid"]->task_project_id->CurrentValue);
				}
			}
			if (in_array("e_tasks_finance", $DetailTblVar)) {
				if (!isset($GLOBALS["e_tasks_finance_grid"]))
					$GLOBALS["e_tasks_finance_grid"] = new ce_tasks_finance_grid;
				if ($GLOBALS["e_tasks_finance_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["e_tasks_finance_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["e_tasks_finance_grid"]->CurrentMode = "add";
					$GLOBALS["e_tasks_finance_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["e_tasks_finance_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["e_tasks_finance_grid"]->setStartRecordNumber(1);
					$GLOBALS["e_tasks_finance_grid"]->task_plan_id->FldIsDetailKey = TRUE;
					$GLOBALS["e_tasks_finance_grid"]->task_plan_id->CurrentValue = $this->plan_id->CurrentValue;
					$GLOBALS["e_tasks_finance_grid"]->task_plan_id->setSessionValue($GLOBALS["e_tasks_finance_grid"]->task_plan_id->CurrentValue);
					$GLOBALS["e_tasks_finance_grid"]->task_project_id->FldIsDetailKey = TRUE;
					$GLOBALS["e_tasks_finance_grid"]->task_project_id->CurrentValue = $this->plan_project_id->CurrentValue;
					$GLOBALS["e_tasks_finance_grid"]->task_project_id->setSessionValue($GLOBALS["e_tasks_finance_grid"]->task_project_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("planslist.php"), "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'plans';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnAdd) return;
		$table = 'plans';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['plan_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$newvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($plans_add)) $plans_add = new cplans_add();

// Page init
$plans_add->Page_Init();

// Page main
$plans_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$plans_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fplansadd = new ew_Form("fplansadd", "add");

// Validate form
fplansadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_plan_project_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_project_id->FldCaption(), $plans->plan_project_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plan_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_code->FldCaption(), $plans->plan_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plan_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_name->FldCaption(), $plans->plan_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plan_active");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_active->FldCaption(), $plans->plan_active->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fplansadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fplansadd.ValidateRequired = true;
<?php } else { ?>
fplansadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fplansadd.Lists["x_plan_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansadd.Lists["x_plan_code"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansadd.Lists["x_plan_code"].Options = <?php echo json_encode($plans->plan_code->Options()) ?>;
fplansadd.Lists["x_plan_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansadd.Lists["x_plan_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansadd.Lists["x_plan_active"].Options = <?php echo json_encode($plans->plan_active->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $plans_add->ShowPageHeader(); ?>
<?php
$plans_add->ShowMessage();
?>
<form name="fplansadd" id="fplansadd" class="<?php echo $plans_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($plans_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $plans_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="plans">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($plans->getCurrentMasterTable() == "v_employees") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="v_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $plans->plan_employee_id->getSessionValue() ?>">
<?php } ?>
<?php if ($plans->getCurrentMasterTable() == "projects") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="projects">
<input type="hidden" name="fk_project_id" value="<?php echo $plans->plan_project_id->getSessionValue() ?>">
<?php } ?>
<?php if ($plans->getCurrentMasterTable() == "e_employees") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $plans->plan_employee_id->getSessionValue() ?>">
<?php } ?>
<div>
<?php if ($plans->plan_project_id->Visible) { // plan_project_id ?>
	<div id="r_plan_project_id" class="form-group">
		<label id="elh_plans_plan_project_id" for="x_plan_project_id" class="col-sm-2 control-label ewLabel"><?php echo $plans->plan_project_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $plans->plan_project_id->CellAttributes() ?>>
<?php if ($plans->plan_project_id->getSessionValue() <> "") { ?>
<span id="el_plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_plan_project_id" name="x_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_plans_plan_project_id">
<select data-table="plans" data-field="x_plan_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_project_id->DisplayValueSeparator) ? json_encode($plans->plan_project_id->DisplayValueSeparator) : $plans->plan_project_id->DisplayValueSeparator) ?>" id="x_plan_project_id" name="x_plan_project_id"<?php echo $plans->plan_project_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_project_id->EditValue)) {
	$arwrk = $plans->plan_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>" selected><?php echo $plans->plan_project_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$plans->plan_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $plans->plan_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_plan_project_id" id="s_x_plan_project_id" value="<?php echo $plans->plan_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $plans->plan_project_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_code->Visible) { // plan_code ?>
	<div id="r_plan_code" class="form-group">
		<label id="elh_plans_plan_code" for="x_plan_code" class="col-sm-2 control-label ewLabel"><?php echo $plans->plan_code->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $plans->plan_code->CellAttributes() ?>>
<span id="el_plans_plan_code">
<select data-table="plans" data-field="x_plan_code" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_code->DisplayValueSeparator) ? json_encode($plans->plan_code->DisplayValueSeparator) : $plans->plan_code->DisplayValueSeparator) ?>" id="x_plan_code" name="x_plan_code"<?php echo $plans->plan_code->EditAttributes() ?>>
<?php
if (is_array($plans->plan_code->EditValue)) {
	$arwrk = $plans->plan_code->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_code->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_code->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_code->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_code->CurrentValue) ?>" selected><?php echo $plans->plan_code->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php echo $plans->plan_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_name->Visible) { // plan_name ?>
	<div id="r_plan_name" class="form-group">
		<label id="elh_plans_plan_name" for="x_plan_name" class="col-sm-2 control-label ewLabel"><?php echo $plans->plan_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $plans->plan_name->CellAttributes() ?>>
<span id="el_plans_plan_name">
<textarea data-table="plans" data-field="x_plan_name" name="x_plan_name" id="x_plan_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($plans->plan_name->getPlaceHolder()) ?>"<?php echo $plans->plan_name->EditAttributes() ?>><?php echo $plans->plan_name->EditValue ?></textarea>
</span>
<?php echo $plans->plan_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_employee_id->Visible) { // plan_employee_id ?>
	<div id="r_plan_employee_id" class="form-group">
		<label id="elh_plans_plan_employee_id" for="x_plan_employee_id" class="col-sm-2 control-label ewLabel"><?php echo $plans->plan_employee_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $plans->plan_employee_id->CellAttributes() ?>>
<?php if ($plans->plan_employee_id->getSessionValue() <> "") { ?>
<span id="el_plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_plan_employee_id" name="x_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_plans_plan_employee_id">
<select data-table="plans" data-field="x_plan_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_employee_id->DisplayValueSeparator) ? json_encode($plans->plan_employee_id->DisplayValueSeparator) : $plans->plan_employee_id->DisplayValueSeparator) ?>" id="x_plan_employee_id" name="x_plan_employee_id"<?php echo $plans->plan_employee_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_employee_id->EditValue)) {
	$arwrk = $plans->plan_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>" selected><?php echo $plans->plan_employee_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$plans->plan_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $plans->plan_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_plan_employee_id" id="s_x_plan_employee_id" value="<?php echo $plans->plan_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $plans->plan_employee_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($plans->plan_active->Visible) { // plan_active ?>
	<div id="r_plan_active" class="form-group">
		<label id="elh_plans_plan_active" for="x_plan_active" class="col-sm-2 control-label ewLabel"><?php echo $plans->plan_active->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $plans->plan_active->CellAttributes() ?>>
<span id="el_plans_plan_active">
<select data-table="plans" data-field="x_plan_active" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_active->DisplayValueSeparator) ? json_encode($plans->plan_active->DisplayValueSeparator) : $plans->plan_active->DisplayValueSeparator) ?>" id="x_plan_active" name="x_plan_active"<?php echo $plans->plan_active->EditAttributes() ?>>
<?php
if (is_array($plans->plan_active->EditValue)) {
	$arwrk = $plans->plan_active->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_active->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_active->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_active->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_active->CurrentValue) ?>" selected><?php echo $plans->plan_active->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php echo $plans->plan_active->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php
	if (in_array("tasks", explode(",", $plans->getCurrentDetailTable())) && $tasks->DetailAdd) {
?>
<?php if ($plans->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("tasks", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "tasksgrid.php" ?>
<?php } ?>
<?php
	if (in_array("e_tasks", explode(",", $plans->getCurrentDetailTable())) && $e_tasks->DetailAdd) {
?>
<?php if ($plans->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("e_tasks", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "e_tasksgrid.php" ?>
<?php } ?>
<?php
	if (in_array("e_tasks_finance", explode(",", $plans->getCurrentDetailTable())) && $e_tasks_finance->DetailAdd) {
?>
<?php if ($plans->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("e_tasks_finance", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "e_tasks_financegrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $plans_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fplansadd.Init();
</script>
<?php
$plans_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$plans_add->Page_Terminate();
?>
