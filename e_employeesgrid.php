<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($e_employees_grid)) $e_employees_grid = new ce_employees_grid();

// Page init
$e_employees_grid->Page_Init();

// Page main
$e_employees_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$e_employees_grid->Page_Render();
?>
<?php if ($e_employees->Export == "") { ?>
<script type="text/javascript">

// Form object
var fe_employeesgrid = new ew_Form("fe_employeesgrid", "grid");
fe_employeesgrid.FormKeyCountName = '<?php echo $e_employees_grid->FormKeyCountName ?>';

// Validate form
fe_employeesgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_employee_login");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_login->FldCaption(), $e_employees->employee_login->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_level_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_level_id->FldCaption(), $e_employees->employee_level_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_lab_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_lab_id->FldCaption(), $e_employees->employee_lab_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_position_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_position_id->FldCaption(), $e_employees->employee_position_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_salary");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_employees->employee_salary->FldCaption(), $e_employees->employee_salary->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_employee_salary");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($e_employees->employee_salary->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fe_employeesgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "employee_login", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_level_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_first_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_last_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_telephone", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_lab_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_position_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "employee_salary", false)) return false;
	return true;
}

// Form_CustomValidate event
fe_employeesgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fe_employeesgrid.ValidateRequired = true;
<?php } else { ?>
fe_employeesgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fe_employeesgrid.Lists["x_employee_level_id"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_employeesgrid.Lists["x_employee_level_id"].Options = <?php echo json_encode($e_employees->employee_level_id->Options()) ?>;
fe_employeesgrid.Lists["x_employee_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_employeesgrid.Lists["x_employee_position_id"] = {"LinkField":"x_position_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_position_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<?php } ?>
<?php
if ($e_employees->CurrentAction == "gridadd") {
	if ($e_employees->CurrentMode == "copy") {
		$bSelectLimit = $e_employees_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$e_employees_grid->TotalRecs = $e_employees->SelectRecordCount();
			$e_employees_grid->Recordset = $e_employees_grid->LoadRecordset($e_employees_grid->StartRec-1, $e_employees_grid->DisplayRecs);
		} else {
			if ($e_employees_grid->Recordset = $e_employees_grid->LoadRecordset())
				$e_employees_grid->TotalRecs = $e_employees_grid->Recordset->RecordCount();
		}
		$e_employees_grid->StartRec = 1;
		$e_employees_grid->DisplayRecs = $e_employees_grid->TotalRecs;
	} else {
		$e_employees->CurrentFilter = "0=1";
		$e_employees_grid->StartRec = 1;
		$e_employees_grid->DisplayRecs = $e_employees->GridAddRowCount;
	}
	$e_employees_grid->TotalRecs = $e_employees_grid->DisplayRecs;
	$e_employees_grid->StopRec = $e_employees_grid->DisplayRecs;
} else {
	$bSelectLimit = $e_employees_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($e_employees_grid->TotalRecs <= 0)
			$e_employees_grid->TotalRecs = $e_employees->SelectRecordCount();
	} else {
		if (!$e_employees_grid->Recordset && ($e_employees_grid->Recordset = $e_employees_grid->LoadRecordset()))
			$e_employees_grid->TotalRecs = $e_employees_grid->Recordset->RecordCount();
	}
	$e_employees_grid->StartRec = 1;
	$e_employees_grid->DisplayRecs = $e_employees_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$e_employees_grid->Recordset = $e_employees_grid->LoadRecordset($e_employees_grid->StartRec-1, $e_employees_grid->DisplayRecs);

	// Set no record found message
	if ($e_employees->CurrentAction == "" && $e_employees_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$e_employees_grid->setWarningMessage(ew_DeniedMsg());
		if ($e_employees_grid->SearchWhere == "0=101")
			$e_employees_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$e_employees_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$e_employees_grid->RenderOtherOptions();
?>
<?php $e_employees_grid->ShowPageHeader(); ?>
<?php
$e_employees_grid->ShowMessage();
?>
<?php if ($e_employees_grid->TotalRecs > 0 || $e_employees->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="fe_employeesgrid" class="ewForm form-inline">
<?php if ($e_employees_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($e_employees_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_e_employees" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_e_employeesgrid" class="table ewTable">
<?php echo $e_employees->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$e_employees_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$e_employees_grid->RenderListOptions();

// Render list options (header, left)
$e_employees_grid->ListOptions->Render("header", "left");
?>
<?php if ($e_employees->employee_login->Visible) { // employee_login ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_login) == "") { ?>
		<th data-name="employee_login"><div id="elh_e_employees_employee_login" class="e_employees_employee_login"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_login->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_login"><div><div id="elh_e_employees_employee_login" class="e_employees_employee_login">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_login->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_login->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_login->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_level_id->Visible) { // employee_level_id ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_level_id) == "") { ?>
		<th data-name="employee_level_id"><div id="elh_e_employees_employee_level_id" class="e_employees_employee_level_id"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_level_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_level_id"><div><div id="elh_e_employees_employee_level_id" class="e_employees_employee_level_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_level_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_level_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_level_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_first_name->Visible) { // employee_first_name ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_first_name) == "") { ?>
		<th data-name="employee_first_name"><div id="elh_e_employees_employee_first_name" class="e_employees_employee_first_name"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_first_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_first_name"><div><div id="elh_e_employees_employee_first_name" class="e_employees_employee_first_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_first_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_first_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_first_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_last_name->Visible) { // employee_last_name ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_last_name) == "") { ?>
		<th data-name="employee_last_name"><div id="elh_e_employees_employee_last_name" class="e_employees_employee_last_name"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_last_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_last_name"><div><div id="elh_e_employees_employee_last_name" class="e_employees_employee_last_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_last_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_last_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_last_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_telephone->Visible) { // employee_telephone ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_telephone) == "") { ?>
		<th data-name="employee_telephone"><div id="elh_e_employees_employee_telephone" class="e_employees_employee_telephone"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_telephone->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_telephone"><div><div id="elh_e_employees_employee_telephone" class="e_employees_employee_telephone">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_telephone->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_telephone->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_telephone->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_lab_id->Visible) { // employee_lab_id ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_lab_id) == "") { ?>
		<th data-name="employee_lab_id"><div id="elh_e_employees_employee_lab_id" class="e_employees_employee_lab_id"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_lab_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_lab_id"><div><div id="elh_e_employees_employee_lab_id" class="e_employees_employee_lab_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_lab_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_lab_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_lab_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_position_id->Visible) { // employee_position_id ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_position_id) == "") { ?>
		<th data-name="employee_position_id"><div id="elh_e_employees_employee_position_id" class="e_employees_employee_position_id"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_position_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_position_id"><div><div id="elh_e_employees_employee_position_id" class="e_employees_employee_position_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_position_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_position_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_position_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_employees->employee_salary->Visible) { // employee_salary ?>
	<?php if ($e_employees->SortUrl($e_employees->employee_salary) == "") { ?>
		<th data-name="employee_salary"><div id="elh_e_employees_employee_salary" class="e_employees_employee_salary"><div class="ewTableHeaderCaption"><?php echo $e_employees->employee_salary->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="employee_salary"><div><div id="elh_e_employees_employee_salary" class="e_employees_employee_salary">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_employees->employee_salary->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_employees->employee_salary->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_employees->employee_salary->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$e_employees_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$e_employees_grid->StartRec = 1;
$e_employees_grid->StopRec = $e_employees_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($e_employees_grid->FormKeyCountName) && ($e_employees->CurrentAction == "gridadd" || $e_employees->CurrentAction == "gridedit" || $e_employees->CurrentAction == "F")) {
		$e_employees_grid->KeyCount = $objForm->GetValue($e_employees_grid->FormKeyCountName);
		$e_employees_grid->StopRec = $e_employees_grid->StartRec + $e_employees_grid->KeyCount - 1;
	}
}
$e_employees_grid->RecCnt = $e_employees_grid->StartRec - 1;
if ($e_employees_grid->Recordset && !$e_employees_grid->Recordset->EOF) {
	$e_employees_grid->Recordset->MoveFirst();
	$bSelectLimit = $e_employees_grid->UseSelectLimit;
	if (!$bSelectLimit && $e_employees_grid->StartRec > 1)
		$e_employees_grid->Recordset->Move($e_employees_grid->StartRec - 1);
} elseif (!$e_employees->AllowAddDeleteRow && $e_employees_grid->StopRec == 0) {
	$e_employees_grid->StopRec = $e_employees->GridAddRowCount;
}

// Initialize aggregate
$e_employees->RowType = EW_ROWTYPE_AGGREGATEINIT;
$e_employees->ResetAttrs();
$e_employees_grid->RenderRow();
if ($e_employees->CurrentAction == "gridadd")
	$e_employees_grid->RowIndex = 0;
if ($e_employees->CurrentAction == "gridedit")
	$e_employees_grid->RowIndex = 0;
while ($e_employees_grid->RecCnt < $e_employees_grid->StopRec) {
	$e_employees_grid->RecCnt++;
	if (intval($e_employees_grid->RecCnt) >= intval($e_employees_grid->StartRec)) {
		$e_employees_grid->RowCnt++;
		if ($e_employees->CurrentAction == "gridadd" || $e_employees->CurrentAction == "gridedit" || $e_employees->CurrentAction == "F") {
			$e_employees_grid->RowIndex++;
			$objForm->Index = $e_employees_grid->RowIndex;
			if ($objForm->HasValue($e_employees_grid->FormActionName))
				$e_employees_grid->RowAction = strval($objForm->GetValue($e_employees_grid->FormActionName));
			elseif ($e_employees->CurrentAction == "gridadd")
				$e_employees_grid->RowAction = "insert";
			else
				$e_employees_grid->RowAction = "";
		}

		// Set up key count
		$e_employees_grid->KeyCount = $e_employees_grid->RowIndex;

		// Init row class and style
		$e_employees->ResetAttrs();
		$e_employees->CssClass = "";
		if ($e_employees->CurrentAction == "gridadd") {
			if ($e_employees->CurrentMode == "copy") {
				$e_employees_grid->LoadRowValues($e_employees_grid->Recordset); // Load row values
				$e_employees_grid->SetRecordKey($e_employees_grid->RowOldKey, $e_employees_grid->Recordset); // Set old record key
			} else {
				$e_employees_grid->LoadDefaultValues(); // Load default values
				$e_employees_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$e_employees_grid->LoadRowValues($e_employees_grid->Recordset); // Load row values
		}
		$e_employees->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($e_employees->CurrentAction == "gridadd") // Grid add
			$e_employees->RowType = EW_ROWTYPE_ADD; // Render add
		if ($e_employees->CurrentAction == "gridadd" && $e_employees->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$e_employees_grid->RestoreCurrentRowFormValues($e_employees_grid->RowIndex); // Restore form values
		if ($e_employees->CurrentAction == "gridedit") { // Grid edit
			if ($e_employees->EventCancelled) {
				$e_employees_grid->RestoreCurrentRowFormValues($e_employees_grid->RowIndex); // Restore form values
			}
			if ($e_employees_grid->RowAction == "insert")
				$e_employees->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$e_employees->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($e_employees->CurrentAction == "gridedit" && ($e_employees->RowType == EW_ROWTYPE_EDIT || $e_employees->RowType == EW_ROWTYPE_ADD) && $e_employees->EventCancelled) // Update failed
			$e_employees_grid->RestoreCurrentRowFormValues($e_employees_grid->RowIndex); // Restore form values
		if ($e_employees->RowType == EW_ROWTYPE_EDIT) // Edit row
			$e_employees_grid->EditRowCnt++;
		if ($e_employees->CurrentAction == "F") // Confirm row
			$e_employees_grid->RestoreCurrentRowFormValues($e_employees_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$e_employees->RowAttrs = array_merge($e_employees->RowAttrs, array('data-rowindex'=>$e_employees_grid->RowCnt, 'id'=>'r' . $e_employees_grid->RowCnt . '_e_employees', 'data-rowtype'=>$e_employees->RowType));

		// Render row
		$e_employees_grid->RenderRow();

		// Render list options
		$e_employees_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($e_employees_grid->RowAction <> "delete" && $e_employees_grid->RowAction <> "insertdelete" && !($e_employees_grid->RowAction == "insert" && $e_employees->CurrentAction == "F" && $e_employees_grid->EmptyRow())) {
?>
	<tr<?php echo $e_employees->RowAttributes() ?>>
<?php

// Render list options (body, left)
$e_employees_grid->ListOptions->Render("body", "left", $e_employees_grid->RowCnt);
?>
	<?php if ($e_employees->employee_login->Visible) { // employee_login ?>
		<td data-name="employee_login"<?php echo $e_employees->employee_login->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_login" class="form-group e_employees_employee_login">
<input type="text" data-table="e_employees" data-field="x_employee_login" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_login->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_login->EditValue ?>"<?php echo $e_employees->employee_login->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_login" class="form-group e_employees_employee_login">
<span<?php echo $e_employees->employee_login->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_login->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->CurrentValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_login" class="e_employees_employee_login">
<span<?php echo $e_employees->employee_login->ViewAttributes() ?>>
<?php echo $e_employees->employee_login->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->OldValue) ?>">
<?php } ?>
<a id="<?php echo $e_employees_grid->PageObjName . "_row_" . $e_employees_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_id" value="<?php echo ew_HtmlEncode($e_employees->employee_id->CurrentValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_id" value="<?php echo ew_HtmlEncode($e_employees->employee_id->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT || $e_employees->CurrentMode == "edit") { ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_id" value="<?php echo ew_HtmlEncode($e_employees->employee_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($e_employees->employee_level_id->Visible) { // employee_level_id ?>
		<td data-name="employee_level_id"<?php echo $e_employees->employee_level_id->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_level_id" class="form-group e_employees_employee_level_id">
<select data-table="e_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_level_id->DisplayValueSeparator) ? json_encode($e_employees->employee_level_id->DisplayValueSeparator) : $e_employees->employee_level_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id"<?php echo $e_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_level_id->EditValue)) {
	$arwrk = $e_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_level_id->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_level_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_level_id" class="form-group e_employees_employee_level_id">
<select data-table="e_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_level_id->DisplayValueSeparator) ? json_encode($e_employees->employee_level_id->DisplayValueSeparator) : $e_employees->employee_level_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id"<?php echo $e_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_level_id->EditValue)) {
	$arwrk = $e_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_level_id->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_level_id" class="e_employees_employee_level_id">
<span<?php echo $e_employees->employee_level_id->ViewAttributes() ?>>
<?php echo $e_employees->employee_level_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_level_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_level_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_employees->employee_first_name->Visible) { // employee_first_name ?>
		<td data-name="employee_first_name"<?php echo $e_employees->employee_first_name->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_first_name" class="form-group e_employees_employee_first_name">
<input type="text" data-table="e_employees" data-field="x_employee_first_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_first_name->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_first_name->EditValue ?>"<?php echo $e_employees->employee_first_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_first_name" class="form-group e_employees_employee_first_name">
<span<?php echo $e_employees->employee_first_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_first_name->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->CurrentValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_first_name" class="e_employees_employee_first_name">
<span<?php echo $e_employees->employee_first_name->ViewAttributes() ?>>
<?php echo $e_employees->employee_first_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_employees->employee_last_name->Visible) { // employee_last_name ?>
		<td data-name="employee_last_name"<?php echo $e_employees->employee_last_name->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_last_name" class="form-group e_employees_employee_last_name">
<input type="text" data-table="e_employees" data-field="x_employee_last_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_last_name->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_last_name->EditValue ?>"<?php echo $e_employees->employee_last_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_last_name" class="form-group e_employees_employee_last_name">
<span<?php echo $e_employees->employee_last_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_last_name->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->CurrentValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_last_name" class="e_employees_employee_last_name">
<span<?php echo $e_employees->employee_last_name->ViewAttributes() ?>>
<?php echo $e_employees->employee_last_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_employees->employee_telephone->Visible) { // employee_telephone ?>
		<td data-name="employee_telephone"<?php echo $e_employees->employee_telephone->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_telephone" class="form-group e_employees_employee_telephone">
<input type="text" data-table="e_employees" data-field="x_employee_telephone" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_telephone->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_telephone->EditValue ?>"<?php echo $e_employees->employee_telephone->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_telephone" class="form-group e_employees_employee_telephone">
<span<?php echo $e_employees->employee_telephone->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_telephone->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->CurrentValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_telephone" class="e_employees_employee_telephone">
<span<?php echo $e_employees->employee_telephone->ViewAttributes() ?>>
<?php echo $e_employees->employee_telephone->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_employees->employee_lab_id->Visible) { // employee_lab_id ?>
		<td data-name="employee_lab_id"<?php echo $e_employees->employee_lab_id->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($e_employees->employee_lab_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<select data-table="e_employees" data-field="x_employee_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_lab_id->DisplayValueSeparator) ? json_encode($e_employees->employee_lab_id->DisplayValueSeparator) : $e_employees->employee_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id"<?php echo $e_employees->employee_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_lab_id->EditValue)) {
	$arwrk = $e_employees->employee_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_employees->employee_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo $e_employees->employee_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_lab_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($e_employees->employee_lab_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<select data-table="e_employees" data-field="x_employee_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_lab_id->DisplayValueSeparator) ? json_encode($e_employees->employee_lab_id->DisplayValueSeparator) : $e_employees->employee_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id"<?php echo $e_employees->employee_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_lab_id->EditValue)) {
	$arwrk = $e_employees->employee_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_employees->employee_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo $e_employees->employee_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_lab_id" class="e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<?php echo $e_employees->employee_lab_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_lab_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_employees->employee_position_id->Visible) { // employee_position_id ?>
		<td data-name="employee_position_id"<?php echo $e_employees->employee_position_id->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($e_employees->employee_position_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_position_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<select data-table="e_employees" data-field="x_employee_position_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_position_id->DisplayValueSeparator) ? json_encode($e_employees->employee_position_id->DisplayValueSeparator) : $e_employees->employee_position_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id"<?php echo $e_employees->employee_position_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_position_id->EditValue)) {
	$arwrk = $e_employees->employee_position_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_position_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_position_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_position_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_position_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_position_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
$sWhereWrk = "";
$e_employees->employee_position_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_position_id->LookupFilters += array("f0" => "`position_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_position_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_position_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo $e_employees->employee_position_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_position_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($e_employees->employee_position_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_position_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<select data-table="e_employees" data-field="x_employee_position_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_position_id->DisplayValueSeparator) ? json_encode($e_employees->employee_position_id->DisplayValueSeparator) : $e_employees->employee_position_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id"<?php echo $e_employees->employee_position_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_position_id->EditValue)) {
	$arwrk = $e_employees->employee_position_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_position_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_position_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_position_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_position_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_position_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
$sWhereWrk = "";
$e_employees->employee_position_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_position_id->LookupFilters += array("f0" => "`position_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_position_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_position_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo $e_employees->employee_position_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_position_id" class="e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<?php echo $e_employees->employee_position_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_position_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_employees->employee_salary->Visible) { // employee_salary ?>
		<td data-name="employee_salary"<?php echo $e_employees->employee_salary->CellAttributes() ?>>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_salary" class="form-group e_employees_employee_salary">
<input type="text" data-table="e_employees" data-field="x_employee_salary" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" size="30" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_salary->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_salary->EditValue ?>"<?php echo $e_employees->employee_salary->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_salary" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_salary" value="<?php echo ew_HtmlEncode($e_employees->employee_salary->OldValue) ?>">
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_salary" class="form-group e_employees_employee_salary">
<input type="text" data-table="e_employees" data-field="x_employee_salary" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" size="30" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_salary->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_salary->EditValue ?>"<?php echo $e_employees->employee_salary->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($e_employees->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_employees_grid->RowCnt ?>_e_employees_employee_salary" class="e_employees_employee_salary">
<span<?php echo $e_employees->employee_salary->ViewAttributes() ?>>
<?php echo $e_employees->employee_salary->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_salary" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" value="<?php echo ew_HtmlEncode($e_employees->employee_salary->FormValue) ?>">
<input type="hidden" data-table="e_employees" data-field="x_employee_salary" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_salary" value="<?php echo ew_HtmlEncode($e_employees->employee_salary->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$e_employees_grid->ListOptions->Render("body", "right", $e_employees_grid->RowCnt);
?>
	</tr>
<?php if ($e_employees->RowType == EW_ROWTYPE_ADD || $e_employees->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fe_employeesgrid.UpdateOpts(<?php echo $e_employees_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($e_employees->CurrentAction <> "gridadd" || $e_employees->CurrentMode == "copy")
		if (!$e_employees_grid->Recordset->EOF) $e_employees_grid->Recordset->MoveNext();
}
?>
<?php
	if ($e_employees->CurrentMode == "add" || $e_employees->CurrentMode == "copy" || $e_employees->CurrentMode == "edit") {
		$e_employees_grid->RowIndex = '$rowindex$';
		$e_employees_grid->LoadDefaultValues();

		// Set row properties
		$e_employees->ResetAttrs();
		$e_employees->RowAttrs = array_merge($e_employees->RowAttrs, array('data-rowindex'=>$e_employees_grid->RowIndex, 'id'=>'r0_e_employees', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($e_employees->RowAttrs["class"], "ewTemplate");
		$e_employees->RowType = EW_ROWTYPE_ADD;

		// Render row
		$e_employees_grid->RenderRow();

		// Render list options
		$e_employees_grid->RenderListOptions();
		$e_employees_grid->StartRowCnt = 0;
?>
	<tr<?php echo $e_employees->RowAttributes() ?>>
<?php

// Render list options (body, left)
$e_employees_grid->ListOptions->Render("body", "left", $e_employees_grid->RowIndex);
?>
	<?php if ($e_employees->employee_login->Visible) { // employee_login ?>
		<td data-name="employee_login">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_employees_employee_login" class="form-group e_employees_employee_login">
<input type="text" data-table="e_employees" data-field="x_employee_login" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_login->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_login->EditValue ?>"<?php echo $e_employees->employee_login->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_login" class="form-group e_employees_employee_login">
<span<?php echo $e_employees->employee_login->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_login->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_login" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_login" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_login" value="<?php echo ew_HtmlEncode($e_employees->employee_login->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_level_id->Visible) { // employee_level_id ?>
		<td data-name="employee_level_id">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_employees_employee_level_id" class="form-group e_employees_employee_level_id">
<select data-table="e_employees" data-field="x_employee_level_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_level_id->DisplayValueSeparator) ? json_encode($e_employees->employee_level_id->DisplayValueSeparator) : $e_employees->employee_level_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id"<?php echo $e_employees->employee_level_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_level_id->EditValue)) {
	$arwrk = $e_employees->employee_level_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_level_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_level_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_level_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_level_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_level_id->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_level_id" class="form-group e_employees_employee_level_id">
<span<?php echo $e_employees->employee_level_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_level_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_level_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_level_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_level_id" value="<?php echo ew_HtmlEncode($e_employees->employee_level_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_first_name->Visible) { // employee_first_name ?>
		<td data-name="employee_first_name">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_employees_employee_first_name" class="form-group e_employees_employee_first_name">
<input type="text" data-table="e_employees" data-field="x_employee_first_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_first_name->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_first_name->EditValue ?>"<?php echo $e_employees->employee_first_name->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_first_name" class="form-group e_employees_employee_first_name">
<span<?php echo $e_employees->employee_first_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_first_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_first_name" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_first_name" value="<?php echo ew_HtmlEncode($e_employees->employee_first_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_last_name->Visible) { // employee_last_name ?>
		<td data-name="employee_last_name">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_employees_employee_last_name" class="form-group e_employees_employee_last_name">
<input type="text" data-table="e_employees" data-field="x_employee_last_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_last_name->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_last_name->EditValue ?>"<?php echo $e_employees->employee_last_name->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_last_name" class="form-group e_employees_employee_last_name">
<span<?php echo $e_employees->employee_last_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_last_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_last_name" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_last_name" value="<?php echo ew_HtmlEncode($e_employees->employee_last_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_telephone->Visible) { // employee_telephone ?>
		<td data-name="employee_telephone">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_employees_employee_telephone" class="form-group e_employees_employee_telephone">
<input type="text" data-table="e_employees" data-field="x_employee_telephone" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_telephone->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_telephone->EditValue ?>"<?php echo $e_employees->employee_telephone->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_telephone" class="form-group e_employees_employee_telephone">
<span<?php echo $e_employees->employee_telephone->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_telephone->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_telephone" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_telephone" value="<?php echo ew_HtmlEncode($e_employees->employee_telephone->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_lab_id->Visible) { // employee_lab_id ?>
		<td data-name="employee_lab_id">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<?php if ($e_employees->employee_lab_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<select data-table="e_employees" data-field="x_employee_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_lab_id->DisplayValueSeparator) ? json_encode($e_employees->employee_lab_id->DisplayValueSeparator) : $e_employees->employee_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id"<?php echo $e_employees->employee_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_lab_id->EditValue)) {
	$arwrk = $e_employees->employee_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_employees->employee_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo $e_employees->employee_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_lab_id" class="form-group e_employees_employee_lab_id">
<span<?php echo $e_employees->employee_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_lab_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_lab_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_lab_id" value="<?php echo ew_HtmlEncode($e_employees->employee_lab_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_position_id->Visible) { // employee_position_id ?>
		<td data-name="employee_position_id">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<?php if ($e_employees->employee_position_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_position_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<select data-table="e_employees" data-field="x_employee_position_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_employees->employee_position_id->DisplayValueSeparator) ? json_encode($e_employees->employee_position_id->DisplayValueSeparator) : $e_employees->employee_position_id->DisplayValueSeparator) ?>" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id"<?php echo $e_employees->employee_position_id->EditAttributes() ?>>
<?php
if (is_array($e_employees->employee_position_id->EditValue)) {
	$arwrk = $e_employees->employee_position_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_employees->employee_position_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_employees->employee_position_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_employees->employee_position_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->CurrentValue) ?>" selected><?php echo $e_employees->employee_position_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_employees->employee_position_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
$sWhereWrk = "";
$e_employees->employee_position_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_employees->employee_position_id->LookupFilters += array("f0" => "`position_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_employees->Lookup_Selecting($e_employees->employee_position_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_employees->employee_position_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="s_x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo $e_employees->employee_position_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_position_id" class="form-group e_employees_employee_position_id">
<span<?php echo $e_employees->employee_position_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_position_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_position_id" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_position_id" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_position_id" value="<?php echo ew_HtmlEncode($e_employees->employee_position_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_employees->employee_salary->Visible) { // employee_salary ?>
		<td data-name="employee_salary">
<?php if ($e_employees->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_employees_employee_salary" class="form-group e_employees_employee_salary">
<input type="text" data-table="e_employees" data-field="x_employee_salary" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" size="30" placeholder="<?php echo ew_HtmlEncode($e_employees->employee_salary->getPlaceHolder()) ?>" value="<?php echo $e_employees->employee_salary->EditValue ?>"<?php echo $e_employees->employee_salary->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_employees_employee_salary" class="form-group e_employees_employee_salary">
<span<?php echo $e_employees->employee_salary->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_employees->employee_salary->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_employees" data-field="x_employee_salary" name="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="x<?php echo $e_employees_grid->RowIndex ?>_employee_salary" value="<?php echo ew_HtmlEncode($e_employees->employee_salary->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_employees" data-field="x_employee_salary" name="o<?php echo $e_employees_grid->RowIndex ?>_employee_salary" id="o<?php echo $e_employees_grid->RowIndex ?>_employee_salary" value="<?php echo ew_HtmlEncode($e_employees->employee_salary->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$e_employees_grid->ListOptions->Render("body", "right", $e_employees_grid->RowCnt);
?>
<script type="text/javascript">
fe_employeesgrid.UpdateOpts(<?php echo $e_employees_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($e_employees->CurrentMode == "add" || $e_employees->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $e_employees_grid->FormKeyCountName ?>" id="<?php echo $e_employees_grid->FormKeyCountName ?>" value="<?php echo $e_employees_grid->KeyCount ?>">
<?php echo $e_employees_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($e_employees->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $e_employees_grid->FormKeyCountName ?>" id="<?php echo $e_employees_grid->FormKeyCountName ?>" value="<?php echo $e_employees_grid->KeyCount ?>">
<?php echo $e_employees_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($e_employees->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="fe_employeesgrid">
</div>
<?php

// Close recordset
if ($e_employees_grid->Recordset)
	$e_employees_grid->Recordset->Close();
?>
<?php if ($e_employees_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($e_employees_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($e_employees_grid->TotalRecs == 0 && $e_employees->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($e_employees_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($e_employees->Export == "") { ?>
<script type="text/javascript">
fe_employeesgrid.Init();
</script>
<?php } ?>
<?php
$e_employees_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$e_employees_grid->Page_Terminate();
?>
