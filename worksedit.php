<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "worksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "periodsinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "tasksinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "e_tasksinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "e_tasks_financeinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$works_edit = NULL; // Initialize page object first

class cworks_edit extends cworks {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'works';

	// Page object name
	var $PageObjName = 'works_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = TRUE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (works)
		if (!isset($GLOBALS["works"]) || get_class($GLOBALS["works"]) == "cworks") {
			$GLOBALS["works"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["works"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (periods)
		if (!isset($GLOBALS['periods'])) $GLOBALS['periods'] = new cperiods();

		// Table object (projects)
		if (!isset($GLOBALS['projects'])) $GLOBALS['projects'] = new cprojects();

		// Table object (tasks)
		if (!isset($GLOBALS['tasks'])) $GLOBALS['tasks'] = new ctasks();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Table object (e_tasks)
		if (!isset($GLOBALS['e_tasks'])) $GLOBALS['e_tasks'] = new ce_tasks();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Table object (e_tasks_finance)
		if (!isset($GLOBALS['e_tasks_finance'])) $GLOBALS['e_tasks_finance'] = new ce_tasks_finance();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'works', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			}
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $works;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($works);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["work_id"] <> "") {
			$this->work_id->setQueryStringValue($_GET["work_id"]);
		}

		// Set up master detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->work_id->CurrentValue == "")
			$this->Page_Terminate("workslist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("workslist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "workslist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->work_period_id->FldIsDetailKey) {
			$this->work_period_id->setFormValue($objForm->GetValue("x_work_period_id"));
		}
		if (!$this->work_project_id->FldIsDetailKey) {
			$this->work_project_id->setFormValue($objForm->GetValue("x_work_project_id"));
		}
		if (!$this->work_plan_id->FldIsDetailKey) {
			$this->work_plan_id->setFormValue($objForm->GetValue("x_work_plan_id"));
		}
		if (!$this->work_lab_id->FldIsDetailKey) {
			$this->work_lab_id->setFormValue($objForm->GetValue("x_work_lab_id"));
		}
		if (!$this->work_task_id->FldIsDetailKey) {
			$this->work_task_id->setFormValue($objForm->GetValue("x_work_task_id"));
		}
		if (!$this->work_description->FldIsDetailKey) {
			$this->work_description->setFormValue($objForm->GetValue("x_work_description"));
		}
		if (!$this->work_progress->FldIsDetailKey) {
			$this->work_progress->setFormValue($objForm->GetValue("x_work_progress"));
		}
		if (!$this->work_time->FldIsDetailKey) {
			$this->work_time->setFormValue($objForm->GetValue("x_work_time"));
		}
		if (!$this->work_employee_id->FldIsDetailKey) {
			$this->work_employee_id->setFormValue($objForm->GetValue("x_work_employee_id"));
		}
		if (!$this->work_id->FldIsDetailKey)
			$this->work_id->setFormValue($objForm->GetValue("x_work_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->work_id->CurrentValue = $this->work_id->FormValue;
		$this->work_period_id->CurrentValue = $this->work_period_id->FormValue;
		$this->work_project_id->CurrentValue = $this->work_project_id->FormValue;
		$this->work_plan_id->CurrentValue = $this->work_plan_id->FormValue;
		$this->work_lab_id->CurrentValue = $this->work_lab_id->FormValue;
		$this->work_task_id->CurrentValue = $this->work_task_id->FormValue;
		$this->work_description->CurrentValue = $this->work_description->FormValue;
		$this->work_progress->CurrentValue = $this->work_progress->FormValue;
		$this->work_time->CurrentValue = $this->work_time->FormValue;
		$this->work_employee_id->CurrentValue = $this->work_employee_id->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}

		// Check if valid user id
		if ($res) {
			$res = $this->ShowOptionLink('edit');
			if (!$res) {
				$sUserIdMsg = ew_DeniedMsg();
				$this->setFailureMessage($sUserIdMsg);
			}
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->work_id->setDbValue($rs->fields('work_id'));
		$this->work_period_id->setDbValue($rs->fields('work_period_id'));
		$this->work_project_id->setDbValue($rs->fields('work_project_id'));
		$this->work_plan_id->setDbValue($rs->fields('work_plan_id'));
		$this->work_lab_id->setDbValue($rs->fields('work_lab_id'));
		$this->work_task_id->setDbValue($rs->fields('work_task_id'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
		$this->work_started->setDbValue($rs->fields('work_started'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->work_id->DbValue = $row['work_id'];
		$this->work_period_id->DbValue = $row['work_period_id'];
		$this->work_project_id->DbValue = $row['work_project_id'];
		$this->work_plan_id->DbValue = $row['work_plan_id'];
		$this->work_lab_id->DbValue = $row['work_lab_id'];
		$this->work_task_id->DbValue = $row['work_task_id'];
		$this->work_description->DbValue = $row['work_description'];
		$this->work_progress->DbValue = $row['work_progress'];
		$this->work_time->DbValue = $row['work_time'];
		$this->work_employee_id->DbValue = $row['work_employee_id'];
		$this->work_started->DbValue = $row['work_started'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->work_time->FormValue == $this->work_time->CurrentValue && is_numeric(ew_StrToFloat($this->work_time->CurrentValue)))
			$this->work_time->CurrentValue = ew_StrToFloat($this->work_time->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

			// work_period_id
			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";
			$this->work_project_id->TooltipValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";
			$this->work_plan_id->TooltipValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";
			$this->work_lab_id->TooltipValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";
			$this->work_task_id->TooltipValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";
			$this->work_description->TooltipValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";
			$this->work_progress->TooltipValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";
			$this->work_time->TooltipValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
			$this->work_employee_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// work_period_id
			$this->work_period_id->EditAttrs["class"] = "form-control";
			$this->work_period_id->EditCustomAttributes = "";
			if (strval($this->work_period_id->CurrentValue) <> "") {
				$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
			$sWhereWrk = "";
			$lookuptblfilter = "`period_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `period_from` DESC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_period_id->EditValue = $this->work_period_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_period_id->EditValue = $this->work_period_id->CurrentValue;
				}
			} else {
				$this->work_period_id->EditValue = NULL;
			}
			$this->work_period_id->ViewCustomAttributes = "";

			// work_project_id
			$this->work_project_id->EditAttrs["class"] = "form-control";
			$this->work_project_id->EditCustomAttributes = "";
			if ($this->work_project_id->getSessionValue() <> "") {
				$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
			if (strval($this->work_project_id->CurrentValue) <> "") {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
				}
			} else {
				$this->work_project_id->ViewValue = NULL;
			}
			$this->work_project_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_project_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_project_id->EditValue = $arwrk;
			}

			// work_plan_id
			$this->work_plan_id->EditAttrs["class"] = "form-control";
			$this->work_plan_id->EditCustomAttributes = "";
			if ($this->work_plan_id->getSessionValue() <> "") {
				$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
			if (strval($this->work_plan_id->CurrentValue) <> "") {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
				}
			} else {
				$this->work_plan_id->ViewValue = NULL;
			}
			$this->work_plan_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_plan_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `plan_project_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_plan_id->EditValue = $arwrk;
			}

			// work_lab_id
			$this->work_lab_id->EditAttrs["class"] = "form-control";
			$this->work_lab_id->EditCustomAttributes = "";
			if ($this->work_lab_id->getSessionValue() <> "") {
				$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
			if (strval($this->work_lab_id->CurrentValue) <> "") {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
				}
			} else {
				$this->work_lab_id->ViewValue = NULL;
			}
			$this->work_lab_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_lab_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_lab_id->EditValue = $arwrk;
			}

			// work_task_id
			$this->work_task_id->EditAttrs["class"] = "form-control";
			$this->work_task_id->EditCustomAttributes = "";
			if ($this->work_task_id->getSessionValue() <> "") {
				$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
			if (strval($this->work_task_id->CurrentValue) <> "") {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
				}
			} else {
				$this->work_task_id->ViewValue = NULL;
			}
			$this->work_task_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_task_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `task_plan_id` AS `SelectFilterFld`, `task_lab_id` AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tasks`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_task_id->EditValue = $arwrk;
			}

			// work_description
			$this->work_description->EditAttrs["class"] = "form-control";
			$this->work_description->EditCustomAttributes = "";
			$this->work_description->EditValue = ew_HtmlEncode($this->work_description->CurrentValue);
			$this->work_description->PlaceHolder = ew_RemoveHtml($this->work_description->FldCaption());

			// work_progress
			$this->work_progress->EditAttrs["class"] = "form-control";
			$this->work_progress->EditCustomAttributes = "";
			$this->work_progress->EditValue = $this->work_progress->Options(TRUE);

			// work_time
			$this->work_time->EditAttrs["class"] = "form-control";
			$this->work_time->EditCustomAttributes = "";
			$this->work_time->EditValue = ew_HtmlEncode($this->work_time->CurrentValue);
			$this->work_time->PlaceHolder = ew_RemoveHtml($this->work_time->FldCaption());
			if (strval($this->work_time->EditValue) <> "" && is_numeric($this->work_time->EditValue)) $this->work_time->EditValue = ew_FormatNumber($this->work_time->EditValue, -2, -1, -2, 0);

			// work_employee_id
			$this->work_employee_id->EditAttrs["class"] = "form-control";
			$this->work_employee_id->EditCustomAttributes = "";
			if ($this->work_employee_id->getSessionValue() <> "") {
				$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
			if (strval($this->work_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
				}
			} else {
				$this->work_employee_id->ViewValue = NULL;
			}
			$this->work_employee_id->ViewCustomAttributes = "";
			} elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("edit")) { // Non system admin
				$this->work_employee_id->CurrentValue = CurrentUserID();
			if (strval($this->work_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->work_employee_id->EditValue = $this->work_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->work_employee_id->EditValue = $this->work_employee_id->CurrentValue;
				}
			} else {
				$this->work_employee_id->EditValue = NULL;
			}
			$this->work_employee_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->work_employee_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["works"]->UserIDAllow("edit")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->work_employee_id->EditValue = $arwrk;
			}

			// Edit refer script
			// work_period_id

			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->work_task_id->FldIsDetailKey && !is_null($this->work_task_id->FormValue) && $this->work_task_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_task_id->FldCaption(), $this->work_task_id->ReqErrMsg));
		}
		if (!$this->work_progress->FldIsDetailKey && !is_null($this->work_progress->FormValue) && $this->work_progress->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_progress->FldCaption(), $this->work_progress->ReqErrMsg));
		}
		if (!$this->work_time->FldIsDetailKey && !is_null($this->work_time->FormValue) && $this->work_time->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_time->FldCaption(), $this->work_time->ReqErrMsg));
		}
		if (!ew_CheckNumber($this->work_time->FormValue)) {
			ew_AddMessage($gsFormError, $this->work_time->FldErrMsg());
		}
		if (!$this->work_employee_id->FldIsDetailKey && !is_null($this->work_employee_id->FormValue) && $this->work_employee_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->work_employee_id->FldCaption(), $this->work_employee_id->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// work_project_id
			$this->work_project_id->SetDbValueDef($rsnew, $this->work_project_id->CurrentValue, NULL, $this->work_project_id->ReadOnly);

			// work_plan_id
			$this->work_plan_id->SetDbValueDef($rsnew, $this->work_plan_id->CurrentValue, NULL, $this->work_plan_id->ReadOnly);

			// work_lab_id
			$this->work_lab_id->SetDbValueDef($rsnew, $this->work_lab_id->CurrentValue, NULL, $this->work_lab_id->ReadOnly);

			// work_task_id
			$this->work_task_id->SetDbValueDef($rsnew, $this->work_task_id->CurrentValue, NULL, $this->work_task_id->ReadOnly);

			// work_description
			$this->work_description->SetDbValueDef($rsnew, $this->work_description->CurrentValue, NULL, $this->work_description->ReadOnly);

			// work_progress
			$this->work_progress->SetDbValueDef($rsnew, $this->work_progress->CurrentValue, 0, $this->work_progress->ReadOnly);

			// work_time
			$this->work_time->SetDbValueDef($rsnew, $this->work_time->CurrentValue, 0, $this->work_time->ReadOnly);

			// work_employee_id
			$this->work_employee_id->SetDbValueDef($rsnew, $this->work_employee_id->CurrentValue, 0, $this->work_employee_id->ReadOnly);

			// Check referential integrity for master table 'periods'
			$bValidMasterRecord = TRUE;
			$sMasterFilter = $this->SqlMasterFilter_periods();
			$KeyValue = isset($rsnew['work_period_id']) ? $rsnew['work_period_id'] : $rsold['work_period_id'];
			if (strval($KeyValue) <> "") {
				$sMasterFilter = str_replace("@period_id@", ew_AdjustSql($KeyValue), $sMasterFilter);
			} else {
				$bValidMasterRecord = FALSE;
			}
			if ($bValidMasterRecord) {
				$rsmaster = $GLOBALS["periods"]->LoadRs($sMasterFilter);
				$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
				$rsmaster->Close();
			}
			if (!$bValidMasterRecord) {
				$sRelatedRecordMsg = str_replace("%t", "periods", $Language->Phrase("RelatedRecordRequired"));
				$this->setFailureMessage($sRelatedRecordMsg);
				$rs->Close();
				return FALSE;
			}

			// Check referential integrity for master table 'v_employees'
			$bValidMasterRecord = TRUE;
			$sMasterFilter = $this->SqlMasterFilter_v_employees();
			$KeyValue = isset($rsnew['work_employee_id']) ? $rsnew['work_employee_id'] : $rsold['work_employee_id'];
			if (strval($KeyValue) <> "") {
				$sMasterFilter = str_replace("@employee_id@", ew_AdjustSql($KeyValue), $sMasterFilter);
			} else {
				$bValidMasterRecord = FALSE;
			}
			if ($bValidMasterRecord) {
				$rsmaster = $GLOBALS["v_employees"]->LoadRs($sMasterFilter);
				$bValidMasterRecord = ($rsmaster && !$rsmaster->EOF);
				$rsmaster->Close();
			}
			if (!$bValidMasterRecord) {
				$sRelatedRecordMsg = str_replace("%t", "v_employees", $Language->Phrase("RelatedRecordRequired"));
				$this->setFailureMessage($sRelatedRecordMsg);
				$rs->Close();
				return FALSE;
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->work_employee_id->CurrentValue);
		return TRUE;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setQueryStringValue($_GET["fk_period_id"]);
					$this->work_period_id->setQueryStringValue($GLOBALS["periods"]->period_id->QueryStringValue);
					$this->work_period_id->setSessionValue($this->work_period_id->QueryStringValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setQueryStringValue($_GET["fk_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["projects"]->project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["e_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setFormValue($_POST["fk_period_id"]);
					$this->work_period_id->setFormValue($GLOBALS["periods"]->period_id->FormValue);
					$this->work_period_id->setSessionValue($this->work_period_id->FormValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setFormValue($_POST["fk_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["projects"]->project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["e_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks_finance"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks_finance"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);
			$this->setSessionWhere($this->GetDetailFilter());

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "periods") {
				if ($this->work_period_id->CurrentValue == "") $this->work_period_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "projects") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "v_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks_finance") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("workslist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'works';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		global $Language;
		if (!$this->AuditTrailOnEdit) return;
		$table = 'works';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['work_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") { // Password Field
						$oldvalue = $Language->Phrase("PasswordMask");
						$newvalue = $Language->Phrase("PasswordMask");
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {
		$taskEmployeeID = ew_ExecuteScalar(
			"SELECT task_employee_id FROM tasks WHERE task_id = '{$this->work_task_id->FormValue}'");
		$workEmployeeID = $this->work_employee_id->FormValue;
		if (intval($taskEmployeeID) > 0 && $taskEmployeeID != $workEmployeeID)
		{
			$CustomError = "Нельзя выполнять работу над задачей, в которой вы не являетесь исполнителем";
			return FALSE;
		}
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($works_edit)) $works_edit = new cworks_edit();

// Page init
$works_edit->Page_Init();

// Page main
$works_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$works_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fworksedit = new ew_Form("fworksedit", "edit");

// Validate form
fworksedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_work_task_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_task_id->FldCaption(), $works->work_task_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_progress");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_progress->FldCaption(), $works->work_progress->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_time->FldCaption(), $works->work_time->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($works->work_time->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_work_employee_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_employee_id->FldCaption(), $works->work_employee_id->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fworksedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fworksedit.ValidateRequired = true;
<?php } else { ?>
fworksedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fworksedit.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksedit.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fworksedit.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_work_project_id"],"ChildFields":["x_work_task_id"],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
fworksedit.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworksedit.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":["x_work_plan_id","x_work_lab_id"],"ChildFields":[],"FilterFields":["x_task_plan_id","x_task_lab_id"],"Options":[],"Template":""};
fworksedit.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksedit.Lists["x_work_progress"].Options = <?php echo json_encode($works->work_progress->Options()) ?>;
fworksedit.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $works_edit->ShowPageHeader(); ?>
<?php
$works_edit->ShowMessage();
?>
<form name="fworksedit" id="fworksedit" class="<?php echo $works_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($works_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $works_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="works">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($works->getCurrentMasterTable() == "periods") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="periods">
<input type="hidden" name="fk_period_id" value="<?php echo $works->work_period_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "projects") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="projects">
<input type="hidden" name="fk_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "tasks") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="tasks">
<input type="hidden" name="fk_task_id" value="<?php echo $works->work_task_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_plan_id" value="<?php echo $works->work_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_lab_id" value="<?php echo $works->work_lab_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "v_employees") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="v_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $works->work_employee_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "e_employees") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $works->work_employee_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "e_tasks") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_tasks">
<input type="hidden" name="fk_task_id" value="<?php echo $works->work_task_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_plan_id" value="<?php echo $works->work_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_lab_id" value="<?php echo $works->work_lab_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "e_tasks_finance") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_tasks_finance">
<input type="hidden" name="fk_task_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_plan_id" value="<?php echo $works->work_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_lab_id" value="<?php echo $works->work_lab_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_id" value="<?php echo $works->work_task_id->getSessionValue() ?>">
<?php } ?>
<div>
<?php if ($works->work_period_id->Visible) { // work_period_id ?>
	<div id="r_work_period_id" class="form-group">
		<label id="elh_works_work_period_id" for="x_work_period_id" class="col-sm-2 control-label ewLabel"><?php echo $works->work_period_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_period_id->CellAttributes() ?>>
<span id="el_works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_period_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_period_id" name="x_work_period_id" id="x_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->CurrentValue) ?>">
<?php echo $works->work_period_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_project_id->Visible) { // work_project_id ?>
	<div id="r_work_project_id" class="form-group">
		<label id="elh_works_work_project_id" for="x_work_project_id" class="col-sm-2 control-label ewLabel"><?php echo $works->work_project_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_project_id->CellAttributes() ?>>
<?php if ($works->work_project_id->getSessionValue() <> "") { ?>
<span id="el_works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_work_project_id" name="x_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_works_work_project_id">
<?php $works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_project_id->DisplayValueSeparator) ? json_encode($works->work_project_id->DisplayValueSeparator) : $works->work_project_id->DisplayValueSeparator) ?>" id="x_work_project_id" name="x_work_project_id"<?php echo $works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($works->work_project_id->EditValue)) {
	$arwrk = $works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>" selected><?php echo $works->work_project_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_project_id" id="s_x_work_project_id" value="<?php echo $works->work_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $works->work_project_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
	<div id="r_work_plan_id" class="form-group">
		<label id="elh_works_work_plan_id" for="x_work_plan_id" class="col-sm-2 control-label ewLabel"><?php echo $works->work_plan_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_plan_id->CellAttributes() ?>>
<?php if ($works->work_plan_id->getSessionValue() <> "") { ?>
<span id="el_works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_work_plan_id" name="x_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_works_work_plan_id">
<?php $works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_plan_id->DisplayValueSeparator) ? json_encode($works->work_plan_id->DisplayValueSeparator) : $works->work_plan_id->DisplayValueSeparator) ?>" id="x_work_plan_id" name="x_work_plan_id"<?php echo $works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($works->work_plan_id->EditValue)) {
	$arwrk = $works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>" selected><?php echo $works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_plan_id" id="s_x_work_plan_id" value="<?php echo $works->work_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $works->work_plan_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
	<div id="r_work_lab_id" class="form-group">
		<label id="elh_works_work_lab_id" for="x_work_lab_id" class="col-sm-2 control-label ewLabel"><?php echo $works->work_lab_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_lab_id->CellAttributes() ?>>
<?php if ($works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el_works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_work_lab_id" name="x_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_works_work_lab_id">
<?php $works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_lab_id->DisplayValueSeparator) ? json_encode($works->work_lab_id->DisplayValueSeparator) : $works->work_lab_id->DisplayValueSeparator) ?>" id="x_work_lab_id" name="x_work_lab_id"<?php echo $works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($works->work_lab_id->EditValue)) {
	$arwrk = $works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>" selected><?php echo $works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_lab_id" id="s_x_work_lab_id" value="<?php echo $works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $works->work_lab_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_task_id->Visible) { // work_task_id ?>
	<div id="r_work_task_id" class="form-group">
		<label id="elh_works_work_task_id" for="x_work_task_id" class="col-sm-2 control-label ewLabel"><?php echo $works->work_task_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_task_id->CellAttributes() ?>>
<?php if ($works->work_task_id->getSessionValue() <> "") { ?>
<span id="el_works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_work_task_id" name="x_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_works_work_task_id">
<select data-table="works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_task_id->DisplayValueSeparator) ? json_encode($works->work_task_id->DisplayValueSeparator) : $works->work_task_id->DisplayValueSeparator) ?>" id="x_work_task_id" name="x_work_task_id"<?php echo $works->work_task_id->EditAttributes() ?>>
<?php
if (is_array($works->work_task_id->EditValue)) {
	$arwrk = $works->work_task_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_task_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_task_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_task_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>" selected><?php echo $works->work_task_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
$sWhereWrk = "{filter}";
$works->work_task_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_task_id->LookupFilters += array("f0" => "`task_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_task_id->LookupFilters += array("f1" => "`task_plan_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$works->work_task_id->LookupFilters += array("f2" => "`task_lab_id` IN ({filter_value})", "t2" => "3", "fn2" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_task_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_task_id" id="s_x_work_task_id" value="<?php echo $works->work_task_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $works->work_task_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_description->Visible) { // work_description ?>
	<div id="r_work_description" class="form-group">
		<label id="elh_works_work_description" for="x_work_description" class="col-sm-2 control-label ewLabel"><?php echo $works->work_description->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_description->CellAttributes() ?>>
<span id="el_works_work_description">
<textarea data-table="works" data-field="x_work_description" name="x_work_description" id="x_work_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($works->work_description->getPlaceHolder()) ?>"<?php echo $works->work_description->EditAttributes() ?>><?php echo $works->work_description->EditValue ?></textarea>
</span>
<?php echo $works->work_description->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_progress->Visible) { // work_progress ?>
	<div id="r_work_progress" class="form-group">
		<label id="elh_works_work_progress" for="x_work_progress" class="col-sm-2 control-label ewLabel"><?php echo $works->work_progress->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_progress->CellAttributes() ?>>
<span id="el_works_work_progress">
<select data-table="works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_progress->DisplayValueSeparator) ? json_encode($works->work_progress->DisplayValueSeparator) : $works->work_progress->DisplayValueSeparator) ?>" id="x_work_progress" name="x_work_progress"<?php echo $works->work_progress->EditAttributes() ?>>
<?php
if (is_array($works->work_progress->EditValue)) {
	$arwrk = $works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_progress->CurrentValue) ?>" selected><?php echo $works->work_progress->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php echo $works->work_progress->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_time->Visible) { // work_time ?>
	<div id="r_work_time" class="form-group">
		<label id="elh_works_work_time" for="x_work_time" class="col-sm-2 control-label ewLabel"><?php echo $works->work_time->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_time->CellAttributes() ?>>
<span id="el_works_work_time">
<input type="text" data-table="works" data-field="x_work_time" name="x_work_time" id="x_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($works->work_time->getPlaceHolder()) ?>" value="<?php echo $works->work_time->EditValue ?>"<?php echo $works->work_time->EditAttributes() ?>>
</span>
<?php echo $works->work_time->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
	<div id="r_work_employee_id" class="form-group">
		<label id="elh_works_work_employee_id" for="x_work_employee_id" class="col-sm-2 control-label ewLabel"><?php echo $works->work_employee_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $works->work_employee_id->CellAttributes() ?>>
<?php if ($works->work_employee_id->getSessionValue() <> "") { ?>
<span id="el_works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_work_employee_id" name="x_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$works->UserIDAllow("edit")) { // Non system admin ?>
<span id="el_works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="x_work_employee_id" id="x_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_works_work_employee_id">
<select data-table="works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_employee_id->DisplayValueSeparator) ? json_encode($works->work_employee_id->DisplayValueSeparator) : $works->work_employee_id->DisplayValueSeparator) ?>" id="x_work_employee_id" name="x_work_employee_id"<?php echo $works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($works->work_employee_id->EditValue)) {
	$arwrk = $works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>" selected><?php echo $works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
$sWhereWrk = "";
if (!$GLOBALS["works"]->UserIDAllow("edit")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
$works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_work_employee_id" id="s_x_work_employee_id" value="<?php echo $works->work_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $works->work_employee_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<input type="hidden" data-table="works" data-field="x_work_id" name="x_work_id" id="x_work_id" value="<?php echo ew_HtmlEncode($works->work_id->CurrentValue) ?>">
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $works_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fworksedit.Init();
</script>
<?php
$works_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");
function increaseProgress(val)
{
	for (var i = 0; i < val; i++)
		$("select[data-field='x_work_progress'] option:selected").next().attr('selected', 'selected');
}

function increaseHours(val)
{
	$("input[data-field='x_work_time']").val(+(parseFloat($("input[data-field='x_work_time']").val()) + parseFloat(val)).toFixed(1));
}
$("select[data-field='x_work_progress']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseProgress(2)">+10</a>');
$("select[data-field='x_work_progress']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseProgress(1)">+5</a>');
$("input[data-field='x_work_time']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseHours(8)">+8</a>');
$("input[data-field='x_work_time']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseHours(4)">+4</a>');
$("input[data-field='x_work_time']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseHours(2)">+2</a>');
$("input[data-field='x_work_time']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseHours(1)">+1</a>');
$("input[data-field='x_work_time']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseHours(0.5)">+0.5</a>');
$("input[data-field='x_work_time']")
	.after('<a href="#elh_works_work_progress" style="padding: 7px 4px; margin-left: 4px;" onclick="increaseHours(0.1)">+0.1</a>');
</script>
<?php include_once "footer.php" ?>
<?php
$works_edit->Page_Terminate();
?>
