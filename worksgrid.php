<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($works_grid)) $works_grid = new cworks_grid();

// Page init
$works_grid->Page_Init();

// Page main
$works_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$works_grid->Page_Render();
?>
<?php if ($works->Export == "") { ?>
<script type="text/javascript">

// Form object
var fworksgrid = new ew_Form("fworksgrid", "grid");
fworksgrid.FormKeyCountName = '<?php echo $works_grid->FormKeyCountName ?>';

// Validate form
fworksgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_work_period_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_period_id->FldCaption(), $works->work_period_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_task_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_task_id->FldCaption(), $works->work_task_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_progress");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_progress->FldCaption(), $works->work_progress->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_time->FldCaption(), $works->work_time->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_work_time");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($works->work_time->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_work_employee_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $works->work_employee_id->FldCaption(), $works->work_employee_id->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fworksgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "work_period_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_project_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_plan_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_lab_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_task_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_description", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_progress", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_time", false)) return false;
	if (ew_ValueChanged(fobj, infix, "work_employee_id", false)) return false;
	return true;
}

// Form_CustomValidate event
fworksgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fworksgrid.ValidateRequired = true;
<?php } else { ?>
fworksgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fworksgrid.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksgrid.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fworksgrid.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_work_project_id"],"ChildFields":["x_work_task_id"],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
fworksgrid.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworksgrid.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":["x_work_plan_id","x_work_lab_id"],"ChildFields":[],"FilterFields":["x_task_plan_id","x_task_lab_id"],"Options":[],"Template":""};
fworksgrid.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksgrid.Lists["x_work_progress"].Options = <?php echo json_encode($works->work_progress->Options()) ?>;
fworksgrid.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<?php } ?>
<?php
if ($works->CurrentAction == "gridadd") {
	if ($works->CurrentMode == "copy") {
		$bSelectLimit = $works_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$works_grid->TotalRecs = $works->SelectRecordCount();
			$works_grid->Recordset = $works_grid->LoadRecordset($works_grid->StartRec-1, $works_grid->DisplayRecs);
		} else {
			if ($works_grid->Recordset = $works_grid->LoadRecordset())
				$works_grid->TotalRecs = $works_grid->Recordset->RecordCount();
		}
		$works_grid->StartRec = 1;
		$works_grid->DisplayRecs = $works_grid->TotalRecs;
	} else {
		$works->CurrentFilter = "0=1";
		$works_grid->StartRec = 1;
		$works_grid->DisplayRecs = $works->GridAddRowCount;
	}
	$works_grid->TotalRecs = $works_grid->DisplayRecs;
	$works_grid->StopRec = $works_grid->DisplayRecs;
} else {
	$bSelectLimit = $works_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($works_grid->TotalRecs <= 0)
			$works_grid->TotalRecs = $works->SelectRecordCount();
	} else {
		if (!$works_grid->Recordset && ($works_grid->Recordset = $works_grid->LoadRecordset()))
			$works_grid->TotalRecs = $works_grid->Recordset->RecordCount();
	}
	$works_grid->StartRec = 1;
	$works_grid->DisplayRecs = $works_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$works_grid->Recordset = $works_grid->LoadRecordset($works_grid->StartRec-1, $works_grid->DisplayRecs);

	// Set no record found message
	if ($works->CurrentAction == "" && $works_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$works_grid->setWarningMessage(ew_DeniedMsg());
		if ($works_grid->SearchWhere == "0=101")
			$works_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$works_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$works_grid->RenderOtherOptions();
?>
<?php $works_grid->ShowPageHeader(); ?>
<?php
$works_grid->ShowMessage();
?>
<?php if ($works_grid->TotalRecs > 0 || $works->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="fworksgrid" class="ewForm form-inline">
<?php if ($works_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($works_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_works" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_worksgrid" class="table ewTable">
<?php echo $works->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$works_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$works_grid->RenderListOptions();

// Render list options (header, left)
$works_grid->ListOptions->Render("header", "left");
?>
<?php if ($works->work_period_id->Visible) { // work_period_id ?>
	<?php if ($works->SortUrl($works->work_period_id) == "") { ?>
		<th data-name="work_period_id"><div id="elh_works_work_period_id" class="works_work_period_id"><div class="ewTableHeaderCaption"><?php echo $works->work_period_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_period_id"><div><div id="elh_works_work_period_id" class="works_work_period_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_period_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_period_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_period_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_project_id->Visible) { // work_project_id ?>
	<?php if ($works->SortUrl($works->work_project_id) == "") { ?>
		<th data-name="work_project_id"><div id="elh_works_work_project_id" class="works_work_project_id"><div class="ewTableHeaderCaption"><?php echo $works->work_project_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_project_id"><div><div id="elh_works_work_project_id" class="works_work_project_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_project_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_project_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_project_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
	<?php if ($works->SortUrl($works->work_plan_id) == "") { ?>
		<th data-name="work_plan_id"><div id="elh_works_work_plan_id" class="works_work_plan_id"><div class="ewTableHeaderCaption"><?php echo $works->work_plan_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_plan_id"><div><div id="elh_works_work_plan_id" class="works_work_plan_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_plan_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_plan_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_plan_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
	<?php if ($works->SortUrl($works->work_lab_id) == "") { ?>
		<th data-name="work_lab_id"><div id="elh_works_work_lab_id" class="works_work_lab_id"><div class="ewTableHeaderCaption"><?php echo $works->work_lab_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_lab_id"><div><div id="elh_works_work_lab_id" class="works_work_lab_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_lab_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_lab_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_lab_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_task_id->Visible) { // work_task_id ?>
	<?php if ($works->SortUrl($works->work_task_id) == "") { ?>
		<th data-name="work_task_id"><div id="elh_works_work_task_id" class="works_work_task_id"><div class="ewTableHeaderCaption"><?php echo $works->work_task_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_task_id"><div><div id="elh_works_work_task_id" class="works_work_task_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_task_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_task_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_task_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_description->Visible) { // work_description ?>
	<?php if ($works->SortUrl($works->work_description) == "") { ?>
		<th data-name="work_description"><div id="elh_works_work_description" class="works_work_description"><div class="ewTableHeaderCaption"><?php echo $works->work_description->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_description"><div><div id="elh_works_work_description" class="works_work_description">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_description->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_description->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_description->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_progress->Visible) { // work_progress ?>
	<?php if ($works->SortUrl($works->work_progress) == "") { ?>
		<th data-name="work_progress"><div id="elh_works_work_progress" class="works_work_progress"><div class="ewTableHeaderCaption"><?php echo $works->work_progress->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_progress"><div><div id="elh_works_work_progress" class="works_work_progress">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_progress->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_progress->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_progress->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_time->Visible) { // work_time ?>
	<?php if ($works->SortUrl($works->work_time) == "") { ?>
		<th data-name="work_time"><div id="elh_works_work_time" class="works_work_time"><div class="ewTableHeaderCaption"><?php echo $works->work_time->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_time"><div><div id="elh_works_work_time" class="works_work_time">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_time->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_time->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_time->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
	<?php if ($works->SortUrl($works->work_employee_id) == "") { ?>
		<th data-name="work_employee_id"><div id="elh_works_work_employee_id" class="works_work_employee_id"><div class="ewTableHeaderCaption"><?php echo $works->work_employee_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_employee_id"><div><div id="elh_works_work_employee_id" class="works_work_employee_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_employee_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_employee_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_employee_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$works_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$works_grid->StartRec = 1;
$works_grid->StopRec = $works_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($works_grid->FormKeyCountName) && ($works->CurrentAction == "gridadd" || $works->CurrentAction == "gridedit" || $works->CurrentAction == "F")) {
		$works_grid->KeyCount = $objForm->GetValue($works_grid->FormKeyCountName);
		$works_grid->StopRec = $works_grid->StartRec + $works_grid->KeyCount - 1;
	}
}
$works_grid->RecCnt = $works_grid->StartRec - 1;
if ($works_grid->Recordset && !$works_grid->Recordset->EOF) {
	$works_grid->Recordset->MoveFirst();
	$bSelectLimit = $works_grid->UseSelectLimit;
	if (!$bSelectLimit && $works_grid->StartRec > 1)
		$works_grid->Recordset->Move($works_grid->StartRec - 1);
} elseif (!$works->AllowAddDeleteRow && $works_grid->StopRec == 0) {
	$works_grid->StopRec = $works->GridAddRowCount;
}

// Initialize aggregate
$works->RowType = EW_ROWTYPE_AGGREGATEINIT;
$works->ResetAttrs();
$works_grid->RenderRow();
if ($works->CurrentAction == "gridadd")
	$works_grid->RowIndex = 0;
if ($works->CurrentAction == "gridedit")
	$works_grid->RowIndex = 0;
while ($works_grid->RecCnt < $works_grid->StopRec) {
	$works_grid->RecCnt++;
	if (intval($works_grid->RecCnt) >= intval($works_grid->StartRec)) {
		$works_grid->RowCnt++;
		if ($works->CurrentAction == "gridadd" || $works->CurrentAction == "gridedit" || $works->CurrentAction == "F") {
			$works_grid->RowIndex++;
			$objForm->Index = $works_grid->RowIndex;
			if ($objForm->HasValue($works_grid->FormActionName))
				$works_grid->RowAction = strval($objForm->GetValue($works_grid->FormActionName));
			elseif ($works->CurrentAction == "gridadd")
				$works_grid->RowAction = "insert";
			else
				$works_grid->RowAction = "";
		}

		// Set up key count
		$works_grid->KeyCount = $works_grid->RowIndex;

		// Init row class and style
		$works->ResetAttrs();
		$works->CssClass = "";
		if ($works->CurrentAction == "gridadd") {
			if ($works->CurrentMode == "copy") {
				$works_grid->LoadRowValues($works_grid->Recordset); // Load row values
				$works_grid->SetRecordKey($works_grid->RowOldKey, $works_grid->Recordset); // Set old record key
			} else {
				$works_grid->LoadDefaultValues(); // Load default values
				$works_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$works_grid->LoadRowValues($works_grid->Recordset); // Load row values
		}
		$works->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($works->CurrentAction == "gridadd") // Grid add
			$works->RowType = EW_ROWTYPE_ADD; // Render add
		if ($works->CurrentAction == "gridadd" && $works->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$works_grid->RestoreCurrentRowFormValues($works_grid->RowIndex); // Restore form values
		if ($works->CurrentAction == "gridedit") { // Grid edit
			if ($works->EventCancelled) {
				$works_grid->RestoreCurrentRowFormValues($works_grid->RowIndex); // Restore form values
			}
			if ($works_grid->RowAction == "insert")
				$works->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$works->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($works->CurrentAction == "gridedit" && ($works->RowType == EW_ROWTYPE_EDIT || $works->RowType == EW_ROWTYPE_ADD) && $works->EventCancelled) // Update failed
			$works_grid->RestoreCurrentRowFormValues($works_grid->RowIndex); // Restore form values
		if ($works->RowType == EW_ROWTYPE_EDIT) // Edit row
			$works_grid->EditRowCnt++;
		if ($works->CurrentAction == "F") // Confirm row
			$works_grid->RestoreCurrentRowFormValues($works_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$works->RowAttrs = array_merge($works->RowAttrs, array('data-rowindex'=>$works_grid->RowCnt, 'id'=>'r' . $works_grid->RowCnt . '_works', 'data-rowtype'=>$works->RowType));

		// Render row
		$works_grid->RenderRow();

		// Render list options
		$works_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($works_grid->RowAction <> "delete" && $works_grid->RowAction <> "insertdelete" && !($works_grid->RowAction == "insert" && $works->CurrentAction == "F" && $works_grid->EmptyRow())) {
?>
	<tr<?php echo $works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$works_grid->ListOptions->Render("body", "left", $works_grid->RowCnt);
?>
	<?php if ($works->work_period_id->Visible) { // work_period_id ?>
		<td data-name="work_period_id"<?php echo $works->work_period_id->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($works->work_period_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_period_id" class="form-group works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_period_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_period_id" class="form-group works_work_period_id">
<select data-table="works" data-field="x_work_period_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_period_id->DisplayValueSeparator) ? json_encode($works->work_period_id->DisplayValueSeparator) : $works->work_period_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id"<?php echo $works->work_period_id->EditAttributes() ?>>
<?php
if (is_array($works->work_period_id->EditValue)) {
	$arwrk = $works->work_period_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_period_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_period_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_period_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_period_id->CurrentValue) ?>" selected><?php echo $works->work_period_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_period_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
$sWhereWrk = "";
$lookuptblfilter = "`period_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$works->work_period_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_period_id->LookupFilters += array("f0" => "`period_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_period_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `period_from` DESC";
if ($sSqlWrk <> "") $works->work_period_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_period_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo $works->work_period_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_period_id" name="o<?php echo $works_grid->RowIndex ?>_work_period_id" id="o<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_period_id" class="form-group works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_period_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->CurrentValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_period_id" class="works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<?php echo $works->work_period_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_period_id" name="o<?php echo $works_grid->RowIndex ?>_work_period_id" id="o<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $works_grid->PageObjName . "_row_" . $works_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="works" data-field="x_work_id" name="x<?php echo $works_grid->RowIndex ?>_work_id" id="x<?php echo $works_grid->RowIndex ?>_work_id" value="<?php echo ew_HtmlEncode($works->work_id->CurrentValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_id" name="o<?php echo $works_grid->RowIndex ?>_work_id" id="o<?php echo $works_grid->RowIndex ?>_work_id" value="<?php echo ew_HtmlEncode($works->work_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT || $works->CurrentMode == "edit") { ?>
<input type="hidden" data-table="works" data-field="x_work_id" name="x<?php echo $works_grid->RowIndex ?>_work_id" id="x<?php echo $works_grid->RowIndex ?>_work_id" value="<?php echo ew_HtmlEncode($works->work_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($works->work_project_id->Visible) { // work_project_id ?>
		<td data-name="work_project_id"<?php echo $works->work_project_id->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($works->work_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_project_id" class="form-group works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_project_id" class="form-group works_work_project_id">
<?php $works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_project_id->DisplayValueSeparator) ? json_encode($works->work_project_id->DisplayValueSeparator) : $works->work_project_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id"<?php echo $works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($works->work_project_id->EditValue)) {
	$arwrk = $works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>" selected><?php echo $works->work_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_project_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo $works->work_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_project_id" name="o<?php echo $works_grid->RowIndex ?>_work_project_id" id="o<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($works->work_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_project_id" class="form-group works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_project_id" class="form-group works_work_project_id">
<?php $works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_project_id->DisplayValueSeparator) ? json_encode($works->work_project_id->DisplayValueSeparator) : $works->work_project_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id"<?php echo $works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($works->work_project_id->EditValue)) {
	$arwrk = $works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>" selected><?php echo $works->work_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_project_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo $works->work_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_project_id" class="works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<?php echo $works->work_project_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_project_id" name="o<?php echo $works_grid->RowIndex ?>_work_project_id" id="o<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
		<td data-name="work_plan_id"<?php echo $works->work_plan_id->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($works->work_plan_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_plan_id" class="form-group works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_plan_id" class="form-group works_work_plan_id">
<?php $works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_plan_id->DisplayValueSeparator) ? json_encode($works->work_plan_id->DisplayValueSeparator) : $works->work_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id"<?php echo $works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($works->work_plan_id->EditValue)) {
	$arwrk = $works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>" selected><?php echo $works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_plan_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo $works->work_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_plan_id" name="o<?php echo $works_grid->RowIndex ?>_work_plan_id" id="o<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($works->work_plan_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_plan_id" class="form-group works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_plan_id" class="form-group works_work_plan_id">
<?php $works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_plan_id->DisplayValueSeparator) ? json_encode($works->work_plan_id->DisplayValueSeparator) : $works->work_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id"<?php echo $works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($works->work_plan_id->EditValue)) {
	$arwrk = $works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>" selected><?php echo $works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_plan_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo $works->work_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_plan_id" class="works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<?php echo $works->work_plan_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_plan_id" name="o<?php echo $works_grid->RowIndex ?>_work_plan_id" id="o<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
		<td data-name="work_lab_id"<?php echo $works->work_lab_id->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_lab_id" class="form-group works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_lab_id" class="form-group works_work_lab_id">
<?php $works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_lab_id->DisplayValueSeparator) ? json_encode($works->work_lab_id->DisplayValueSeparator) : $works->work_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id"<?php echo $works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($works->work_lab_id->EditValue)) {
	$arwrk = $works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>" selected><?php echo $works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_lab_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo $works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_lab_id" name="o<?php echo $works_grid->RowIndex ?>_work_lab_id" id="o<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_lab_id" class="form-group works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_lab_id" class="form-group works_work_lab_id">
<?php $works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_lab_id->DisplayValueSeparator) ? json_encode($works->work_lab_id->DisplayValueSeparator) : $works->work_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id"<?php echo $works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($works->work_lab_id->EditValue)) {
	$arwrk = $works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>" selected><?php echo $works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_lab_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo $works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_lab_id" class="works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<?php echo $works->work_lab_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_lab_id" name="o<?php echo $works_grid->RowIndex ?>_work_lab_id" id="o<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_task_id->Visible) { // work_task_id ?>
		<td data-name="work_task_id"<?php echo $works->work_task_id->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($works->work_task_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_task_id" class="form-group works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_task_id" class="form-group works_work_task_id">
<select data-table="works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_task_id->DisplayValueSeparator) ? json_encode($works->work_task_id->DisplayValueSeparator) : $works->work_task_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id"<?php echo $works->work_task_id->EditAttributes() ?>>
<?php
if (is_array($works->work_task_id->EditValue)) {
	$arwrk = $works->work_task_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_task_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_task_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_task_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>" selected><?php echo $works->work_task_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_task_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
$sWhereWrk = "{filter}";
$works->work_task_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_task_id->LookupFilters += array("f0" => "`task_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_task_id->LookupFilters += array("f1" => "`task_plan_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$works->work_task_id->LookupFilters += array("f2" => "`task_lab_id` IN ({filter_value})", "t2" => "3", "fn2" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_task_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_task_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo $works->work_task_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_task_id" name="o<?php echo $works_grid->RowIndex ?>_work_task_id" id="o<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($works->work_task_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_task_id" class="form-group works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_task_id" class="form-group works_work_task_id">
<select data-table="works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_task_id->DisplayValueSeparator) ? json_encode($works->work_task_id->DisplayValueSeparator) : $works->work_task_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id"<?php echo $works->work_task_id->EditAttributes() ?>>
<?php
if (is_array($works->work_task_id->EditValue)) {
	$arwrk = $works->work_task_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_task_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_task_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_task_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>" selected><?php echo $works->work_task_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_task_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
$sWhereWrk = "{filter}";
$works->work_task_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_task_id->LookupFilters += array("f0" => "`task_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_task_id->LookupFilters += array("f1" => "`task_plan_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$works->work_task_id->LookupFilters += array("f2" => "`task_lab_id` IN ({filter_value})", "t2" => "3", "fn2" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_task_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_task_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo $works->work_task_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_task_id" class="works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<?php echo $works->work_task_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_task_id" name="o<?php echo $works_grid->RowIndex ?>_work_task_id" id="o<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_description->Visible) { // work_description ?>
		<td data-name="work_description"<?php echo $works->work_description->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_description" class="form-group works_work_description">
<textarea data-table="works" data-field="x_work_description" name="x<?php echo $works_grid->RowIndex ?>_work_description" id="x<?php echo $works_grid->RowIndex ?>_work_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($works->work_description->getPlaceHolder()) ?>"<?php echo $works->work_description->EditAttributes() ?>><?php echo $works->work_description->EditValue ?></textarea>
</span>
<input type="hidden" data-table="works" data-field="x_work_description" name="o<?php echo $works_grid->RowIndex ?>_work_description" id="o<?php echo $works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($works->work_description->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_description" class="form-group works_work_description">
<textarea data-table="works" data-field="x_work_description" name="x<?php echo $works_grid->RowIndex ?>_work_description" id="x<?php echo $works_grid->RowIndex ?>_work_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($works->work_description->getPlaceHolder()) ?>"<?php echo $works->work_description->EditAttributes() ?>><?php echo $works->work_description->EditValue ?></textarea>
</span>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_description" class="works_work_description">
<span<?php echo $works->work_description->ViewAttributes() ?>>
<?php echo $works->work_description->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_description" name="x<?php echo $works_grid->RowIndex ?>_work_description" id="x<?php echo $works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($works->work_description->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_description" name="o<?php echo $works_grid->RowIndex ?>_work_description" id="o<?php echo $works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($works->work_description->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress"<?php echo $works->work_progress->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_progress" class="form-group works_work_progress">
<select data-table="works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_progress->DisplayValueSeparator) ? json_encode($works->work_progress->DisplayValueSeparator) : $works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_progress" name="x<?php echo $works_grid->RowIndex ?>_work_progress"<?php echo $works->work_progress->EditAttributes() ?>>
<?php
if (is_array($works->work_progress->EditValue)) {
	$arwrk = $works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_progress->CurrentValue) ?>" selected><?php echo $works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_progress->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="works" data-field="x_work_progress" name="o<?php echo $works_grid->RowIndex ?>_work_progress" id="o<?php echo $works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($works->work_progress->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_progress" class="form-group works_work_progress">
<select data-table="works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_progress->DisplayValueSeparator) ? json_encode($works->work_progress->DisplayValueSeparator) : $works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_progress" name="x<?php echo $works_grid->RowIndex ?>_work_progress"<?php echo $works->work_progress->EditAttributes() ?>>
<?php
if (is_array($works->work_progress->EditValue)) {
	$arwrk = $works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_progress->CurrentValue) ?>" selected><?php echo $works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_progress->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_progress" class="works_work_progress">
<span<?php echo $works->work_progress->ViewAttributes() ?>>
<?php echo $works->work_progress->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_progress" name="x<?php echo $works_grid->RowIndex ?>_work_progress" id="x<?php echo $works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($works->work_progress->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_progress" name="o<?php echo $works_grid->RowIndex ?>_work_progress" id="o<?php echo $works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($works->work_progress->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_time->Visible) { // work_time ?>
		<td data-name="work_time"<?php echo $works->work_time->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_time" class="form-group works_work_time">
<input type="text" data-table="works" data-field="x_work_time" name="x<?php echo $works_grid->RowIndex ?>_work_time" id="x<?php echo $works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($works->work_time->getPlaceHolder()) ?>" value="<?php echo $works->work_time->EditValue ?>"<?php echo $works->work_time->EditAttributes() ?>>
</span>
<input type="hidden" data-table="works" data-field="x_work_time" name="o<?php echo $works_grid->RowIndex ?>_work_time" id="o<?php echo $works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($works->work_time->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_time" class="form-group works_work_time">
<input type="text" data-table="works" data-field="x_work_time" name="x<?php echo $works_grid->RowIndex ?>_work_time" id="x<?php echo $works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($works->work_time->getPlaceHolder()) ?>" value="<?php echo $works->work_time->EditValue ?>"<?php echo $works->work_time->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_time" class="works_work_time">
<span<?php echo $works->work_time->ViewAttributes() ?>>
<?php echo $works->work_time->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_time" name="x<?php echo $works_grid->RowIndex ?>_work_time" id="x<?php echo $works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($works->work_time->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_time" name="o<?php echo $works_grid->RowIndex ?>_work_time" id="o<?php echo $works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($works->work_time->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
		<td data-name="work_employee_id"<?php echo $works->work_employee_id->CellAttributes() ?>>
<?php if ($works->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($works->work_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$works->UserIDAllow("grid")) { // Non system admin ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="form-group works_work_employee_id">
<select data-table="works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_employee_id->DisplayValueSeparator) ? json_encode($works->work_employee_id->DisplayValueSeparator) : $works->work_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id"<?php echo $works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($works->work_employee_id->EditValue)) {
	$arwrk = $works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>" selected><?php echo $works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
$sWhereWrk = "";
if (!$GLOBALS["works"]->UserIDAllow("grid")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
$works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo $works->work_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="o<?php echo $works_grid->RowIndex ?>_work_employee_id" id="o<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->OldValue) ?>">
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($works->work_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$works->UserIDAllow("grid")) { // Non system admin ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="form-group works_work_employee_id">
<select data-table="works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_employee_id->DisplayValueSeparator) ? json_encode($works->work_employee_id->DisplayValueSeparator) : $works->work_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id"<?php echo $works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($works->work_employee_id->EditValue)) {
	$arwrk = $works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>" selected><?php echo $works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
$sWhereWrk = "";
if (!$GLOBALS["works"]->UserIDAllow("grid")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
$works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo $works->work_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($works->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $works_grid->RowCnt ?>_works_work_employee_id" class="works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<?php echo $works->work_employee_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->FormValue) ?>">
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="o<?php echo $works_grid->RowIndex ?>_work_employee_id" id="o<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$works_grid->ListOptions->Render("body", "right", $works_grid->RowCnt);
?>
	</tr>
<?php if ($works->RowType == EW_ROWTYPE_ADD || $works->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fworksgrid.UpdateOpts(<?php echo $works_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($works->CurrentAction <> "gridadd" || $works->CurrentMode == "copy")
		if (!$works_grid->Recordset->EOF) $works_grid->Recordset->MoveNext();
}
?>
<?php
	if ($works->CurrentMode == "add" || $works->CurrentMode == "copy" || $works->CurrentMode == "edit") {
		$works_grid->RowIndex = '$rowindex$';
		$works_grid->LoadDefaultValues();

		// Set row properties
		$works->ResetAttrs();
		$works->RowAttrs = array_merge($works->RowAttrs, array('data-rowindex'=>$works_grid->RowIndex, 'id'=>'r0_works', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($works->RowAttrs["class"], "ewTemplate");
		$works->RowType = EW_ROWTYPE_ADD;

		// Render row
		$works_grid->RenderRow();

		// Render list options
		$works_grid->RenderListOptions();
		$works_grid->StartRowCnt = 0;
?>
	<tr<?php echo $works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$works_grid->ListOptions->Render("body", "left", $works_grid->RowIndex);
?>
	<?php if ($works->work_period_id->Visible) { // work_period_id ?>
		<td data-name="work_period_id">
<?php if ($works->CurrentAction <> "F") { ?>
<?php if ($works->work_period_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_works_work_period_id" class="form-group works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_period_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_works_work_period_id" class="form-group works_work_period_id">
<select data-table="works" data-field="x_work_period_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_period_id->DisplayValueSeparator) ? json_encode($works->work_period_id->DisplayValueSeparator) : $works->work_period_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id"<?php echo $works->work_period_id->EditAttributes() ?>>
<?php
if (is_array($works->work_period_id->EditValue)) {
	$arwrk = $works->work_period_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_period_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_period_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_period_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_period_id->CurrentValue) ?>" selected><?php echo $works->work_period_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_period_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
$sWhereWrk = "";
$lookuptblfilter = "`period_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$works->work_period_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_period_id->LookupFilters += array("f0" => "`period_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_period_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `period_from` DESC";
if ($sSqlWrk <> "") $works->work_period_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_period_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo $works->work_period_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_works_work_period_id" class="form-group works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_period_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_period_id" name="x<?php echo $works_grid->RowIndex ?>_work_period_id" id="x<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_period_id" name="o<?php echo $works_grid->RowIndex ?>_work_period_id" id="o<?php echo $works_grid->RowIndex ?>_work_period_id" value="<?php echo ew_HtmlEncode($works->work_period_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_project_id->Visible) { // work_project_id ?>
		<td data-name="work_project_id">
<?php if ($works->CurrentAction <> "F") { ?>
<?php if ($works->work_project_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_works_work_project_id" class="form-group works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_works_work_project_id" class="form-group works_work_project_id">
<?php $works->work_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_project_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_project_id->DisplayValueSeparator) ? json_encode($works->work_project_id->DisplayValueSeparator) : $works->work_project_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id"<?php echo $works->work_project_id->EditAttributes() ?>>
<?php
if (is_array($works->work_project_id->EditValue)) {
	$arwrk = $works->work_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_project_id->CurrentValue) ?>" selected><?php echo $works->work_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$works->work_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_project_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo $works->work_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_works_work_project_id" class="form-group works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_project_id" name="x<?php echo $works_grid->RowIndex ?>_work_project_id" id="x<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_project_id" name="o<?php echo $works_grid->RowIndex ?>_work_project_id" id="o<?php echo $works_grid->RowIndex ?>_work_project_id" value="<?php echo ew_HtmlEncode($works->work_project_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
		<td data-name="work_plan_id">
<?php if ($works->CurrentAction <> "F") { ?>
<?php if ($works->work_plan_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_works_work_plan_id" class="form-group works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_works_work_plan_id" class="form-group works_work_plan_id">
<?php $works->work_plan_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_plan_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_plan_id->DisplayValueSeparator) ? json_encode($works->work_plan_id->DisplayValueSeparator) : $works->work_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id"<?php echo $works->work_plan_id->EditAttributes() ?>>
<?php
if (is_array($works->work_plan_id->EditValue)) {
	$arwrk = $works->work_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_plan_id->CurrentValue) ?>" selected><?php echo $works->work_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$works->work_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $works->work_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_plan_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo $works->work_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_works_work_plan_id" class="form-group works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_plan_id" name="x<?php echo $works_grid->RowIndex ?>_work_plan_id" id="x<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_plan_id" name="o<?php echo $works_grid->RowIndex ?>_work_plan_id" id="o<?php echo $works_grid->RowIndex ?>_work_plan_id" value="<?php echo ew_HtmlEncode($works->work_plan_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
		<td data-name="work_lab_id">
<?php if ($works->CurrentAction <> "F") { ?>
<?php if ($works->work_lab_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_works_work_lab_id" class="form-group works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_works_work_lab_id" class="form-group works_work_lab_id">
<?php $works->work_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$works->work_lab_id->EditAttrs["onchange"]; ?>
<select data-table="works" data-field="x_work_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_lab_id->DisplayValueSeparator) ? json_encode($works->work_lab_id->DisplayValueSeparator) : $works->work_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id"<?php echo $works->work_lab_id->EditAttributes() ?>>
<?php
if (is_array($works->work_lab_id->EditValue)) {
	$arwrk = $works->work_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_lab_id->CurrentValue) ?>" selected><?php echo $works->work_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$works->work_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_lab_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo $works->work_lab_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_works_work_lab_id" class="form-group works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_lab_id" name="x<?php echo $works_grid->RowIndex ?>_work_lab_id" id="x<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_lab_id" name="o<?php echo $works_grid->RowIndex ?>_work_lab_id" id="o<?php echo $works_grid->RowIndex ?>_work_lab_id" value="<?php echo ew_HtmlEncode($works->work_lab_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_task_id->Visible) { // work_task_id ?>
		<td data-name="work_task_id">
<?php if ($works->CurrentAction <> "F") { ?>
<?php if ($works->work_task_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_works_work_task_id" class="form-group works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_works_work_task_id" class="form-group works_work_task_id">
<select data-table="works" data-field="x_work_task_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_task_id->DisplayValueSeparator) ? json_encode($works->work_task_id->DisplayValueSeparator) : $works->work_task_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id"<?php echo $works->work_task_id->EditAttributes() ?>>
<?php
if (is_array($works->work_task_id->EditValue)) {
	$arwrk = $works->work_task_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_task_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_task_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_task_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_task_id->CurrentValue) ?>" selected><?php echo $works->work_task_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_task_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
$sWhereWrk = "{filter}";
$works->work_task_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_task_id->LookupFilters += array("f0" => "`task_id` = {filter_value}", "t0" => "3", "fn0" => "");
$works->work_task_id->LookupFilters += array("f1" => "`task_plan_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$works->work_task_id->LookupFilters += array("f2" => "`task_lab_id` IN ({filter_value})", "t2" => "3", "fn2" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_task_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $works->work_task_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_task_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo $works->work_task_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_works_work_task_id" class="form-group works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_task_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_task_id" name="x<?php echo $works_grid->RowIndex ?>_work_task_id" id="x<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_task_id" name="o<?php echo $works_grid->RowIndex ?>_work_task_id" id="o<?php echo $works_grid->RowIndex ?>_work_task_id" value="<?php echo ew_HtmlEncode($works->work_task_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_description->Visible) { // work_description ?>
		<td data-name="work_description">
<?php if ($works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_works_work_description" class="form-group works_work_description">
<textarea data-table="works" data-field="x_work_description" name="x<?php echo $works_grid->RowIndex ?>_work_description" id="x<?php echo $works_grid->RowIndex ?>_work_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($works->work_description->getPlaceHolder()) ?>"<?php echo $works->work_description->EditAttributes() ?>><?php echo $works->work_description->EditValue ?></textarea>
</span>
<?php } else { ?>
<span id="el$rowindex$_works_work_description" class="form-group works_work_description">
<span<?php echo $works->work_description->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_description->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_description" name="x<?php echo $works_grid->RowIndex ?>_work_description" id="x<?php echo $works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($works->work_description->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_description" name="o<?php echo $works_grid->RowIndex ?>_work_description" id="o<?php echo $works_grid->RowIndex ?>_work_description" value="<?php echo ew_HtmlEncode($works->work_description->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress">
<?php if ($works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_works_work_progress" class="form-group works_work_progress">
<select data-table="works" data-field="x_work_progress" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_progress->DisplayValueSeparator) ? json_encode($works->work_progress->DisplayValueSeparator) : $works->work_progress->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_progress" name="x<?php echo $works_grid->RowIndex ?>_work_progress"<?php echo $works->work_progress->EditAttributes() ?>>
<?php
if (is_array($works->work_progress->EditValue)) {
	$arwrk = $works->work_progress->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_progress->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_progress->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_progress->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_progress->CurrentValue) ?>" selected><?php echo $works->work_progress->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_progress->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_works_work_progress" class="form-group works_work_progress">
<span<?php echo $works->work_progress->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_progress->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_progress" name="x<?php echo $works_grid->RowIndex ?>_work_progress" id="x<?php echo $works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($works->work_progress->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_progress" name="o<?php echo $works_grid->RowIndex ?>_work_progress" id="o<?php echo $works_grid->RowIndex ?>_work_progress" value="<?php echo ew_HtmlEncode($works->work_progress->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_time->Visible) { // work_time ?>
		<td data-name="work_time">
<?php if ($works->CurrentAction <> "F") { ?>
<span id="el$rowindex$_works_work_time" class="form-group works_work_time">
<input type="text" data-table="works" data-field="x_work_time" name="x<?php echo $works_grid->RowIndex ?>_work_time" id="x<?php echo $works_grid->RowIndex ?>_work_time" size="30" placeholder="<?php echo ew_HtmlEncode($works->work_time->getPlaceHolder()) ?>" value="<?php echo $works->work_time->EditValue ?>"<?php echo $works->work_time->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_works_work_time" class="form-group works_work_time">
<span<?php echo $works->work_time->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_time->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_time" name="x<?php echo $works_grid->RowIndex ?>_work_time" id="x<?php echo $works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($works->work_time->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_time" name="o<?php echo $works_grid->RowIndex ?>_work_time" id="o<?php echo $works_grid->RowIndex ?>_work_time" value="<?php echo ew_HtmlEncode($works->work_time->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
		<td data-name="work_employee_id">
<?php if ($works->CurrentAction <> "F") { ?>
<?php if ($works->work_employee_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$works->UserIDAllow("grid")) { // Non system admin ?>
<span id="el$rowindex$_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_works_work_employee_id" class="form-group works_work_employee_id">
<select data-table="works" data-field="x_work_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($works->work_employee_id->DisplayValueSeparator) ? json_encode($works->work_employee_id->DisplayValueSeparator) : $works->work_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id"<?php echo $works->work_employee_id->EditAttributes() ?>>
<?php
if (is_array($works->work_employee_id->EditValue)) {
	$arwrk = $works->work_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($works->work_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $works->work_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($works->work_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($works->work_employee_id->CurrentValue) ?>" selected><?php echo $works->work_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $works->work_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
$sWhereWrk = "";
if (!$GLOBALS["works"]->UserIDAllow("grid")) $sWhereWrk = $GLOBALS["employees"]->AddUserIDFilter($sWhereWrk);
$works->work_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$works->work_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$works->Lookup_Selecting($works->work_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $works->work_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="s_x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo $works->work_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_works_work_employee_id" class="form-group works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $works->work_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="x<?php echo $works_grid->RowIndex ?>_work_employee_id" id="x<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="works" data-field="x_work_employee_id" name="o<?php echo $works_grid->RowIndex ?>_work_employee_id" id="o<?php echo $works_grid->RowIndex ?>_work_employee_id" value="<?php echo ew_HtmlEncode($works->work_employee_id->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$works_grid->ListOptions->Render("body", "right", $works_grid->RowCnt);
?>
<script type="text/javascript">
fworksgrid.UpdateOpts(<?php echo $works_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($works->CurrentMode == "add" || $works->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $works_grid->FormKeyCountName ?>" id="<?php echo $works_grid->FormKeyCountName ?>" value="<?php echo $works_grid->KeyCount ?>">
<?php echo $works_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($works->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $works_grid->FormKeyCountName ?>" id="<?php echo $works_grid->FormKeyCountName ?>" value="<?php echo $works_grid->KeyCount ?>">
<?php echo $works_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($works->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="fworksgrid">
</div>
<?php

// Close recordset
if ($works_grid->Recordset)
	$works_grid->Recordset->Close();
?>
<?php if ($works_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($works_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($works_grid->TotalRecs == 0 && $works->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($works_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($works->Export == "") { ?>
<script type="text/javascript">
fworksgrid.Init();
</script>
<?php } ?>
<?php
$works_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$works_grid->Page_Terminate();
?>
