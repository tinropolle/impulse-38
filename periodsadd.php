<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "periodsinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "worksgridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$periods_add = NULL; // Initialize page object first

class cperiods_add extends cperiods {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'periods';

	// Page object name
	var $PageObjName = 'periods_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = TRUE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (periods)
		if (!isset($GLOBALS["periods"]) || get_class($GLOBALS["periods"]) == "cperiods") {
			$GLOBALS["periods"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["periods"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'periods', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("periodslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'works'
			if (@$_POST["grid"] == "fworksgrid") {
				if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid;
				$GLOBALS["works_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $periods;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($periods);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["period_id"] != "") {
				$this->period_id->setQueryStringValue($_GET["period_id"]);
				$this->setKey("period_id", $this->period_id->CurrentValue); // Set up key
			} else {
				$this->setKey("period_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Set up detail parameters
		$this->SetUpDetailParms();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		} else {
			if ($this->CurrentAction == "I") // Load default values for blank record
				$this->LoadDefaultValues();
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("periodslist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					if ($this->getCurrentDetailTable() <> "") // Master/detail add
						$sReturnUrl = $this->GetDetailUrl();
					else
						$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "periodslist.php")
						$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
					elseif (ew_GetPageName($sReturnUrl) == "periodsview.php")
						$sReturnUrl = $this->GetViewUrl(); // View page, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->period_name->CurrentValue = NULL;
		$this->period_name->OldValue = $this->period_name->CurrentValue;
		$this->period_from->CurrentValue = NULL;
		$this->period_from->OldValue = $this->period_from->CurrentValue;
		$this->period_to->CurrentValue = NULL;
		$this->period_to->OldValue = $this->period_to->CurrentValue;
		$this->period_active->CurrentValue = 1;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->period_name->FldIsDetailKey) {
			$this->period_name->setFormValue($objForm->GetValue("x_period_name"));
		}
		if (!$this->period_from->FldIsDetailKey) {
			$this->period_from->setFormValue($objForm->GetValue("x_period_from"));
			$this->period_from->CurrentValue = ew_UnFormatDateTime($this->period_from->CurrentValue, 7);
		}
		if (!$this->period_to->FldIsDetailKey) {
			$this->period_to->setFormValue($objForm->GetValue("x_period_to"));
			$this->period_to->CurrentValue = ew_UnFormatDateTime($this->period_to->CurrentValue, 7);
		}
		if (!$this->period_active->FldIsDetailKey) {
			$this->period_active->setFormValue($objForm->GetValue("x_period_active"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->period_name->CurrentValue = $this->period_name->FormValue;
		$this->period_from->CurrentValue = $this->period_from->FormValue;
		$this->period_from->CurrentValue = ew_UnFormatDateTime($this->period_from->CurrentValue, 7);
		$this->period_to->CurrentValue = $this->period_to->FormValue;
		$this->period_to->CurrentValue = ew_UnFormatDateTime($this->period_to->CurrentValue, 7);
		$this->period_active->CurrentValue = $this->period_active->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->period_id->setDbValue($rs->fields('period_id'));
		$this->period_name->setDbValue($rs->fields('period_name'));
		$this->period_from->setDbValue($rs->fields('period_from'));
		$this->period_to->setDbValue($rs->fields('period_to'));
		$this->period_active->setDbValue($rs->fields('period_active'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->period_id->DbValue = $row['period_id'];
		$this->period_name->DbValue = $row['period_name'];
		$this->period_from->DbValue = $row['period_from'];
		$this->period_to->DbValue = $row['period_to'];
		$this->period_active->DbValue = $row['period_active'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("period_id")) <> "")
			$this->period_id->CurrentValue = $this->getKey("period_id"); // period_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// period_id
		// period_name
		// period_from
		// period_to
		// period_active

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// period_id
		$this->period_id->ViewValue = $this->period_id->CurrentValue;
		$this->period_id->ViewCustomAttributes = "";

		// period_name
		$this->period_name->ViewValue = $this->period_name->CurrentValue;
		$this->period_name->ViewCustomAttributes = "";

		// period_from
		$this->period_from->ViewValue = $this->period_from->CurrentValue;
		$this->period_from->ViewValue = ew_FormatDateTime($this->period_from->ViewValue, 7);
		$this->period_from->ViewCustomAttributes = "";

		// period_to
		$this->period_to->ViewValue = $this->period_to->CurrentValue;
		$this->period_to->ViewValue = ew_FormatDateTime($this->period_to->ViewValue, 7);
		$this->period_to->ViewCustomAttributes = "";

		// period_active
		if (strval($this->period_active->CurrentValue) <> "") {
			$this->period_active->ViewValue = $this->period_active->OptionCaption($this->period_active->CurrentValue);
		} else {
			$this->period_active->ViewValue = NULL;
		}
		$this->period_active->ViewCustomAttributes = "";

			// period_name
			$this->period_name->LinkCustomAttributes = "";
			$this->period_name->HrefValue = "";
			$this->period_name->TooltipValue = "";

			// period_from
			$this->period_from->LinkCustomAttributes = "";
			$this->period_from->HrefValue = "";
			$this->period_from->TooltipValue = "";

			// period_to
			$this->period_to->LinkCustomAttributes = "";
			$this->period_to->HrefValue = "";
			$this->period_to->TooltipValue = "";

			// period_active
			$this->period_active->LinkCustomAttributes = "";
			$this->period_active->HrefValue = "";
			$this->period_active->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// period_name
			$this->period_name->EditAttrs["class"] = "form-control";
			$this->period_name->EditCustomAttributes = "";
			$this->period_name->EditValue = ew_HtmlEncode($this->period_name->CurrentValue);
			$this->period_name->PlaceHolder = ew_RemoveHtml($this->period_name->FldCaption());

			// period_from
			$this->period_from->EditAttrs["class"] = "form-control";
			$this->period_from->EditCustomAttributes = "";
			$this->period_from->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->period_from->CurrentValue, 7));
			$this->period_from->PlaceHolder = ew_RemoveHtml($this->period_from->FldCaption());

			// period_to
			$this->period_to->EditAttrs["class"] = "form-control";
			$this->period_to->EditCustomAttributes = "";
			$this->period_to->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->period_to->CurrentValue, 7));
			$this->period_to->PlaceHolder = ew_RemoveHtml($this->period_to->FldCaption());

			// period_active
			$this->period_active->EditAttrs["class"] = "form-control";
			$this->period_active->EditCustomAttributes = "";
			$this->period_active->EditValue = $this->period_active->Options(TRUE);

			// Add refer script
			// period_name

			$this->period_name->LinkCustomAttributes = "";
			$this->period_name->HrefValue = "";

			// period_from
			$this->period_from->LinkCustomAttributes = "";
			$this->period_from->HrefValue = "";

			// period_to
			$this->period_to->LinkCustomAttributes = "";
			$this->period_to->HrefValue = "";

			// period_active
			$this->period_active->LinkCustomAttributes = "";
			$this->period_active->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->period_name->FldIsDetailKey && !is_null($this->period_name->FormValue) && $this->period_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->period_name->FldCaption(), $this->period_name->ReqErrMsg));
		}
		if (!$this->period_from->FldIsDetailKey && !is_null($this->period_from->FormValue) && $this->period_from->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->period_from->FldCaption(), $this->period_from->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->period_from->FormValue)) {
			ew_AddMessage($gsFormError, $this->period_from->FldErrMsg());
		}
		if (!$this->period_to->FldIsDetailKey && !is_null($this->period_to->FormValue) && $this->period_to->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->period_to->FldCaption(), $this->period_to->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->period_to->FormValue)) {
			ew_AddMessage($gsFormError, $this->period_to->FldErrMsg());
		}
		if (!$this->period_active->FldIsDetailKey && !is_null($this->period_active->FormValue) && $this->period_active->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->period_active->FldCaption(), $this->period_active->ReqErrMsg));
		}

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("works", $DetailTblVar) && $GLOBALS["works"]->DetailAdd) {
			if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid(); // get detail page object
			$GLOBALS["works_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->period_name->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(period_name = '" . ew_AdjustSql($this->period_name->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->period_name->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->period_name->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Begin transaction
		if ($this->getCurrentDetailTable() <> "")
			$conn->BeginTrans();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// period_name
		$this->period_name->SetDbValueDef($rsnew, $this->period_name->CurrentValue, "", FALSE);

		// period_from
		$this->period_from->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->period_from->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// period_to
		$this->period_to->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->period_to->CurrentValue, 7), ew_CurrentDate(), FALSE);

		// period_active
		$this->period_active->SetDbValueDef($rsnew, $this->period_active->CurrentValue, 0, strval($this->period_active->CurrentValue) == "");

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->period_id->setDbValue($conn->Insert_ID());
				$rsnew['period_id'] = $this->period_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Add detail records
		if ($AddRow) {
			$DetailTblVar = explode(",", $this->getCurrentDetailTable());
			if (in_array("works", $DetailTblVar) && $GLOBALS["works"]->DetailAdd) {
				$GLOBALS["works"]->work_period_id->setSessionValue($this->period_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid(); // Get detail page object
				$AddRow = $GLOBALS["works_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["works"]->work_period_id->setSessionValue(""); // Clear master key if insert failed
			}
		}

		// Commit/Rollback transaction
		if ($this->getCurrentDetailTable() <> "") {
			if ($AddRow) {
				$conn->CommitTrans(); // Commit transaction
			} else {
				$conn->RollbackTrans(); // Rollback transaction
			}
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("works", $DetailTblVar)) {
				if (!isset($GLOBALS["works_grid"]))
					$GLOBALS["works_grid"] = new cworks_grid;
				if ($GLOBALS["works_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["works_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["works_grid"]->CurrentMode = "add";
					$GLOBALS["works_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["works_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["works_grid"]->setStartRecordNumber(1);
					$GLOBALS["works_grid"]->work_period_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_period_id->CurrentValue = $this->period_id->CurrentValue;
					$GLOBALS["works_grid"]->work_period_id->setSessionValue($GLOBALS["works_grid"]->work_period_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("periodslist.php"), "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'periods';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnAdd) return;
		$table = 'periods';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['period_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$newvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($periods_add)) $periods_add = new cperiods_add();

// Page init
$periods_add->Page_Init();

// Page main
$periods_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$periods_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fperiodsadd = new ew_Form("fperiodsadd", "add");

// Validate form
fperiodsadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_period_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $periods->period_name->FldCaption(), $periods->period_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_period_from");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $periods->period_from->FldCaption(), $periods->period_from->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_period_from");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($periods->period_from->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_period_to");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $periods->period_to->FldCaption(), $periods->period_to->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_period_to");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($periods->period_to->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_period_active");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $periods->period_active->FldCaption(), $periods->period_active->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fperiodsadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fperiodsadd.ValidateRequired = true;
<?php } else { ?>
fperiodsadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fperiodsadd.Lists["x_period_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fperiodsadd.Lists["x_period_active"].Options = <?php echo json_encode($periods->period_active->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $periods_add->ShowPageHeader(); ?>
<?php
$periods_add->ShowMessage();
?>
<form name="fperiodsadd" id="fperiodsadd" class="<?php echo $periods_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($periods_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $periods_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="periods">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($periods->period_name->Visible) { // period_name ?>
	<div id="r_period_name" class="form-group">
		<label id="elh_periods_period_name" for="x_period_name" class="col-sm-2 control-label ewLabel"><?php echo $periods->period_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $periods->period_name->CellAttributes() ?>>
<span id="el_periods_period_name">
<input type="text" data-table="periods" data-field="x_period_name" name="x_period_name" id="x_period_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($periods->period_name->getPlaceHolder()) ?>" value="<?php echo $periods->period_name->EditValue ?>"<?php echo $periods->period_name->EditAttributes() ?>>
</span>
<?php echo $periods->period_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($periods->period_from->Visible) { // period_from ?>
	<div id="r_period_from" class="form-group">
		<label id="elh_periods_period_from" for="x_period_from" class="col-sm-2 control-label ewLabel"><?php echo $periods->period_from->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $periods->period_from->CellAttributes() ?>>
<span id="el_periods_period_from">
<input type="text" data-table="periods" data-field="x_period_from" data-format="7" name="x_period_from" id="x_period_from" placeholder="<?php echo ew_HtmlEncode($periods->period_from->getPlaceHolder()) ?>" value="<?php echo $periods->period_from->EditValue ?>"<?php echo $periods->period_from->EditAttributes() ?>>
<?php if (!$periods->period_from->ReadOnly && !$periods->period_from->Disabled && !isset($periods->period_from->EditAttrs["readonly"]) && !isset($periods->period_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fperiodsadd", "x_period_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php echo $periods->period_from->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($periods->period_to->Visible) { // period_to ?>
	<div id="r_period_to" class="form-group">
		<label id="elh_periods_period_to" for="x_period_to" class="col-sm-2 control-label ewLabel"><?php echo $periods->period_to->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $periods->period_to->CellAttributes() ?>>
<span id="el_periods_period_to">
<input type="text" data-table="periods" data-field="x_period_to" data-format="7" name="x_period_to" id="x_period_to" placeholder="<?php echo ew_HtmlEncode($periods->period_to->getPlaceHolder()) ?>" value="<?php echo $periods->period_to->EditValue ?>"<?php echo $periods->period_to->EditAttributes() ?>>
<?php if (!$periods->period_to->ReadOnly && !$periods->period_to->Disabled && !isset($periods->period_to->EditAttrs["readonly"]) && !isset($periods->period_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fperiodsadd", "x_period_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php echo $periods->period_to->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($periods->period_active->Visible) { // period_active ?>
	<div id="r_period_active" class="form-group">
		<label id="elh_periods_period_active" for="x_period_active" class="col-sm-2 control-label ewLabel"><?php echo $periods->period_active->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $periods->period_active->CellAttributes() ?>>
<span id="el_periods_period_active">
<select data-table="periods" data-field="x_period_active" data-value-separator="<?php echo ew_HtmlEncode(is_array($periods->period_active->DisplayValueSeparator) ? json_encode($periods->period_active->DisplayValueSeparator) : $periods->period_active->DisplayValueSeparator) ?>" id="x_period_active" name="x_period_active"<?php echo $periods->period_active->EditAttributes() ?>>
<?php
if (is_array($periods->period_active->EditValue)) {
	$arwrk = $periods->period_active->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($periods->period_active->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $periods->period_active->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($periods->period_active->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($periods->period_active->CurrentValue) ?>" selected><?php echo $periods->period_active->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php echo $periods->period_active->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php
	if (in_array("works", explode(",", $periods->getCurrentDetailTable())) && $works->DetailAdd) {
?>
<?php if ($periods->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("works", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "worksgrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $periods_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fperiodsadd.Init();
</script>
<?php
$periods_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$periods_add->Page_Terminate();
?>
