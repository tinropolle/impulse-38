<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($e_tasks_grid)) $e_tasks_grid = new ce_tasks_grid();

// Page init
$e_tasks_grid->Page_Init();

// Page main
$e_tasks_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$e_tasks_grid->Page_Render();
?>
<?php if ($e_tasks->Export == "") { ?>
<script type="text/javascript">

// Form object
var fe_tasksgrid = new ew_Form("fe_tasksgrid", "grid");
fe_tasksgrid.FormKeyCountName = '<?php echo $e_tasks_grid->FormKeyCountName ?>';

// Validate form
fe_tasksgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_task_project_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_project_id->FldCaption(), $e_tasks->task_project_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_plan_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_plan_id->FldCaption(), $e_tasks->task_plan_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_lab_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_lab_id->FldCaption(), $e_tasks->task_lab_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_code->FldCaption(), $e_tasks->task_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_name->FldCaption(), $e_tasks->task_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_hours_planned");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_hours_planned->FldCaption(), $e_tasks->task_hours_planned->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_task_hours_actual");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $e_tasks->task_hours_actual->FldCaption(), $e_tasks->task_hours_actual->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fe_tasksgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "task_project_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_plan_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_lab_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_code", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_from", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_to", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_employee_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_coordinator_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_object", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_status_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_hours_planned", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_hours_actual", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_description", false)) return false;
	if (ew_ValueChanged(fobj, infix, "task_key", false)) return false;
	return true;
}

// Form_CustomValidate event
fe_tasksgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fe_tasksgrid.ValidateRequired = true;
<?php } else { ?>
fe_tasksgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fe_tasksgrid.Lists["x_task_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_task_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fe_tasksgrid.Lists["x_task_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":["x_task_project_id"],"ChildFields":[],"FilterFields":["x_plan_project_id"],"Options":[],"Template":""};
fe_tasksgrid.Lists["x_task_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_task_employee_id"],"FilterFields":[],"Options":[],"Template":""};
fe_tasksgrid.Lists["x_task_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":["x_task_lab_id"],"ChildFields":[],"FilterFields":["x_employee_lab_id"],"Options":[],"Template":""};
fe_tasksgrid.Lists["x_task_coordinator_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasksgrid.Lists["x_task_status_id"] = {"LinkField":"x_task_status_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_status_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<?php } ?>
<?php
if ($e_tasks->CurrentAction == "gridadd") {
	if ($e_tasks->CurrentMode == "copy") {
		$bSelectLimit = $e_tasks_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$e_tasks_grid->TotalRecs = $e_tasks->SelectRecordCount();
			$e_tasks_grid->Recordset = $e_tasks_grid->LoadRecordset($e_tasks_grid->StartRec-1, $e_tasks_grid->DisplayRecs);
		} else {
			if ($e_tasks_grid->Recordset = $e_tasks_grid->LoadRecordset())
				$e_tasks_grid->TotalRecs = $e_tasks_grid->Recordset->RecordCount();
		}
		$e_tasks_grid->StartRec = 1;
		$e_tasks_grid->DisplayRecs = $e_tasks_grid->TotalRecs;
	} else {
		$e_tasks->CurrentFilter = "0=1";
		$e_tasks_grid->StartRec = 1;
		$e_tasks_grid->DisplayRecs = $e_tasks->GridAddRowCount;
	}
	$e_tasks_grid->TotalRecs = $e_tasks_grid->DisplayRecs;
	$e_tasks_grid->StopRec = $e_tasks_grid->DisplayRecs;
} else {
	$bSelectLimit = $e_tasks_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($e_tasks_grid->TotalRecs <= 0)
			$e_tasks_grid->TotalRecs = $e_tasks->SelectRecordCount();
	} else {
		if (!$e_tasks_grid->Recordset && ($e_tasks_grid->Recordset = $e_tasks_grid->LoadRecordset()))
			$e_tasks_grid->TotalRecs = $e_tasks_grid->Recordset->RecordCount();
	}
	$e_tasks_grid->StartRec = 1;
	$e_tasks_grid->DisplayRecs = $e_tasks_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$e_tasks_grid->Recordset = $e_tasks_grid->LoadRecordset($e_tasks_grid->StartRec-1, $e_tasks_grid->DisplayRecs);

	// Set no record found message
	if ($e_tasks->CurrentAction == "" && $e_tasks_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$e_tasks_grid->setWarningMessage(ew_DeniedMsg());
		if ($e_tasks_grid->SearchWhere == "0=101")
			$e_tasks_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$e_tasks_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$e_tasks_grid->RenderOtherOptions();
?>
<?php $e_tasks_grid->ShowPageHeader(); ?>
<?php
$e_tasks_grid->ShowMessage();
?>
<?php if ($e_tasks_grid->TotalRecs > 0 || $e_tasks->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="fe_tasksgrid" class="ewForm form-inline">
<?php if ($e_tasks_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($e_tasks_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_e_tasks" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_e_tasksgrid" class="table ewTable">
<?php echo $e_tasks->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$e_tasks_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$e_tasks_grid->RenderListOptions();

// Render list options (header, left)
$e_tasks_grid->ListOptions->Render("header", "left");
?>
<?php if ($e_tasks->task_project_id->Visible) { // task_project_id ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_project_id) == "") { ?>
		<th data-name="task_project_id"><div id="elh_e_tasks_task_project_id" class="e_tasks_task_project_id"><div class="ewTableHeaderCaption"><?php echo $e_tasks->task_project_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_project_id"><div><div id="elh_e_tasks_task_project_id" class="e_tasks_task_project_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_project_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_project_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_project_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_plan_id->Visible) { // task_plan_id ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_plan_id) == "") { ?>
		<th data-name="task_plan_id"><div id="elh_e_tasks_task_plan_id" class="e_tasks_task_plan_id"><div class="ewTableHeaderCaption" style="width: 240px;"><?php echo $e_tasks->task_plan_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_plan_id"><div><div id="elh_e_tasks_task_plan_id" class="e_tasks_task_plan_id">
			<div class="ewTableHeaderBtn" style="width: 240px;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_plan_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_plan_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_plan_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_lab_id->Visible) { // task_lab_id ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_lab_id) == "") { ?>
		<th data-name="task_lab_id"><div id="elh_e_tasks_task_lab_id" class="e_tasks_task_lab_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_lab_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_lab_id"><div><div id="elh_e_tasks_task_lab_id" class="e_tasks_task_lab_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_lab_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_lab_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_lab_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_code->Visible) { // task_code ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_code) == "") { ?>
		<th data-name="task_code"><div id="elh_e_tasks_task_code" class="e_tasks_task_code"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_code->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_code"><div><div id="elh_e_tasks_task_code" class="e_tasks_task_code">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_code->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_code->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_code->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_name->Visible) { // task_name ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_name) == "") { ?>
		<th data-name="task_name"><div id="elh_e_tasks_task_name" class="e_tasks_task_name"><div class="ewTableHeaderCaption" style="width: 260px;"><?php echo $e_tasks->task_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_name"><div><div id="elh_e_tasks_task_name" class="e_tasks_task_name">
			<div class="ewTableHeaderBtn" style="width: 260px;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_from->Visible) { // task_from ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_from) == "") { ?>
		<th data-name="task_from"><div id="elh_e_tasks_task_from" class="e_tasks_task_from"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_from->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_from"><div><div id="elh_e_tasks_task_from" class="e_tasks_task_from">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_from->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_from->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_from->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_to->Visible) { // task_to ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_to) == "") { ?>
		<th data-name="task_to"><div id="elh_e_tasks_task_to" class="e_tasks_task_to"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_to->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_to"><div><div id="elh_e_tasks_task_to" class="e_tasks_task_to">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_to->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_to->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_to->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_employee_id->Visible) { // task_employee_id ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_employee_id) == "") { ?>
		<th data-name="task_employee_id"><div id="elh_e_tasks_task_employee_id" class="e_tasks_task_employee_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_employee_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_employee_id"><div><div id="elh_e_tasks_task_employee_id" class="e_tasks_task_employee_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_employee_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_employee_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_employee_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_coordinator_id) == "") { ?>
		<th data-name="task_coordinator_id"><div id="elh_e_tasks_task_coordinator_id" class="e_tasks_task_coordinator_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_coordinator_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_coordinator_id"><div><div id="elh_e_tasks_task_coordinator_id" class="e_tasks_task_coordinator_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_coordinator_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_coordinator_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_coordinator_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_object->Visible) { // task_object ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_object) == "") { ?>
		<th data-name="task_object"><div id="elh_e_tasks_task_object" class="e_tasks_task_object"><div class="ewTableHeaderCaption"><?php echo $e_tasks->task_object->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_object"><div><div id="elh_e_tasks_task_object" class="e_tasks_task_object">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_object->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_object->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_object->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_status_id->Visible) { // task_status_id ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_status_id) == "") { ?>
		<th data-name="task_status_id"><div id="elh_e_tasks_task_status_id" class="e_tasks_task_status_id"><div class="ewTableHeaderCaption" style="white-space: nowrap;"><?php echo $e_tasks->task_status_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_status_id"><div><div id="elh_e_tasks_task_status_id" class="e_tasks_task_status_id">
			<div class="ewTableHeaderBtn" style="white-space: nowrap;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_status_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_status_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_status_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_hours_planned->Visible) { // task_hours_planned ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_hours_planned) == "") { ?>
		<th data-name="task_hours_planned"><div id="elh_e_tasks_task_hours_planned" class="e_tasks_task_hours_planned"><div class="ewTableHeaderCaption"><?php echo $e_tasks->task_hours_planned->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_hours_planned"><div><div id="elh_e_tasks_task_hours_planned" class="e_tasks_task_hours_planned">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_hours_planned->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_hours_planned->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_hours_planned->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_hours_actual->Visible) { // task_hours_actual ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_hours_actual) == "") { ?>
		<th data-name="task_hours_actual"><div id="elh_e_tasks_task_hours_actual" class="e_tasks_task_hours_actual"><div class="ewTableHeaderCaption"><?php echo $e_tasks->task_hours_actual->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_hours_actual"><div><div id="elh_e_tasks_task_hours_actual" class="e_tasks_task_hours_actual">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_hours_actual->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_hours_actual->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_hours_actual->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_description->Visible) { // task_description ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_description) == "") { ?>
		<th data-name="task_description"><div id="elh_e_tasks_task_description" class="e_tasks_task_description"><div class="ewTableHeaderCaption" style="width: 260px;"><?php echo $e_tasks->task_description->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_description"><div><div id="elh_e_tasks_task_description" class="e_tasks_task_description">
			<div class="ewTableHeaderBtn" style="width: 260px;"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_description->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_description->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_description->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($e_tasks->task_key->Visible) { // task_key ?>
	<?php if ($e_tasks->SortUrl($e_tasks->task_key) == "") { ?>
		<th data-name="task_key"><div id="elh_e_tasks_task_key" class="e_tasks_task_key"><div class="ewTableHeaderCaption"><?php echo $e_tasks->task_key->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="task_key"><div><div id="elh_e_tasks_task_key" class="e_tasks_task_key">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $e_tasks->task_key->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($e_tasks->task_key->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($e_tasks->task_key->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$e_tasks_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$e_tasks_grid->StartRec = 1;
$e_tasks_grid->StopRec = $e_tasks_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($e_tasks_grid->FormKeyCountName) && ($e_tasks->CurrentAction == "gridadd" || $e_tasks->CurrentAction == "gridedit" || $e_tasks->CurrentAction == "F")) {
		$e_tasks_grid->KeyCount = $objForm->GetValue($e_tasks_grid->FormKeyCountName);
		$e_tasks_grid->StopRec = $e_tasks_grid->StartRec + $e_tasks_grid->KeyCount - 1;
	}
}
$e_tasks_grid->RecCnt = $e_tasks_grid->StartRec - 1;
if ($e_tasks_grid->Recordset && !$e_tasks_grid->Recordset->EOF) {
	$e_tasks_grid->Recordset->MoveFirst();
	$bSelectLimit = $e_tasks_grid->UseSelectLimit;
	if (!$bSelectLimit && $e_tasks_grid->StartRec > 1)
		$e_tasks_grid->Recordset->Move($e_tasks_grid->StartRec - 1);
} elseif (!$e_tasks->AllowAddDeleteRow && $e_tasks_grid->StopRec == 0) {
	$e_tasks_grid->StopRec = $e_tasks->GridAddRowCount;
}

// Initialize aggregate
$e_tasks->RowType = EW_ROWTYPE_AGGREGATEINIT;
$e_tasks->ResetAttrs();
$e_tasks_grid->RenderRow();
if ($e_tasks->CurrentAction == "gridadd")
	$e_tasks_grid->RowIndex = 0;
if ($e_tasks->CurrentAction == "gridedit")
	$e_tasks_grid->RowIndex = 0;
while ($e_tasks_grid->RecCnt < $e_tasks_grid->StopRec) {
	$e_tasks_grid->RecCnt++;
	if (intval($e_tasks_grid->RecCnt) >= intval($e_tasks_grid->StartRec)) {
		$e_tasks_grid->RowCnt++;
		if ($e_tasks->CurrentAction == "gridadd" || $e_tasks->CurrentAction == "gridedit" || $e_tasks->CurrentAction == "F") {
			$e_tasks_grid->RowIndex++;
			$objForm->Index = $e_tasks_grid->RowIndex;
			if ($objForm->HasValue($e_tasks_grid->FormActionName))
				$e_tasks_grid->RowAction = strval($objForm->GetValue($e_tasks_grid->FormActionName));
			elseif ($e_tasks->CurrentAction == "gridadd")
				$e_tasks_grid->RowAction = "insert";
			else
				$e_tasks_grid->RowAction = "";
		}

		// Set up key count
		$e_tasks_grid->KeyCount = $e_tasks_grid->RowIndex;

		// Init row class and style
		$e_tasks->ResetAttrs();
		$e_tasks->CssClass = "";
		if ($e_tasks->CurrentAction == "gridadd") {
			if ($e_tasks->CurrentMode == "copy") {
				$e_tasks_grid->LoadRowValues($e_tasks_grid->Recordset); // Load row values
				$e_tasks_grid->SetRecordKey($e_tasks_grid->RowOldKey, $e_tasks_grid->Recordset); // Set old record key
			} else {
				$e_tasks_grid->LoadDefaultValues(); // Load default values
				$e_tasks_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$e_tasks_grid->LoadRowValues($e_tasks_grid->Recordset); // Load row values
		}
		$e_tasks->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($e_tasks->CurrentAction == "gridadd") // Grid add
			$e_tasks->RowType = EW_ROWTYPE_ADD; // Render add
		if ($e_tasks->CurrentAction == "gridadd" && $e_tasks->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$e_tasks_grid->RestoreCurrentRowFormValues($e_tasks_grid->RowIndex); // Restore form values
		if ($e_tasks->CurrentAction == "gridedit") { // Grid edit
			if ($e_tasks->EventCancelled) {
				$e_tasks_grid->RestoreCurrentRowFormValues($e_tasks_grid->RowIndex); // Restore form values
			}
			if ($e_tasks_grid->RowAction == "insert")
				$e_tasks->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$e_tasks->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($e_tasks->CurrentAction == "gridedit" && ($e_tasks->RowType == EW_ROWTYPE_EDIT || $e_tasks->RowType == EW_ROWTYPE_ADD) && $e_tasks->EventCancelled) // Update failed
			$e_tasks_grid->RestoreCurrentRowFormValues($e_tasks_grid->RowIndex); // Restore form values
		if ($e_tasks->RowType == EW_ROWTYPE_EDIT) // Edit row
			$e_tasks_grid->EditRowCnt++;
		if ($e_tasks->CurrentAction == "F") // Confirm row
			$e_tasks_grid->RestoreCurrentRowFormValues($e_tasks_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$e_tasks->RowAttrs = array_merge($e_tasks->RowAttrs, array('data-rowindex'=>$e_tasks_grid->RowCnt, 'id'=>'r' . $e_tasks_grid->RowCnt . '_e_tasks', 'data-rowtype'=>$e_tasks->RowType));

		// Render row
		$e_tasks_grid->RenderRow();

		// Render list options
		$e_tasks_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($e_tasks_grid->RowAction <> "delete" && $e_tasks_grid->RowAction <> "insertdelete" && !($e_tasks_grid->RowAction == "insert" && $e_tasks->CurrentAction == "F" && $e_tasks_grid->EmptyRow())) {
?>
	<tr<?php echo $e_tasks->RowAttributes() ?>>
<?php

// Render list options (body, left)
$e_tasks_grid->ListOptions->Render("body", "left", $e_tasks_grid->RowCnt);
?>
	<?php if ($e_tasks->task_project_id->Visible) { // task_project_id ?>
		<td data-name="task_project_id"<?php echo $e_tasks->task_project_id->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($e_tasks->task_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_project_id" class="form-group e_tasks_task_project_id">
<span<?php echo $e_tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_project_id" class="form-group e_tasks_task_project_id">
<?php $e_tasks->task_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$e_tasks->task_project_id->EditAttrs["onchange"]; ?>
<select data-table="e_tasks" data-field="x_task_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_project_id->DisplayValueSeparator) ? json_encode($e_tasks->task_project_id->DisplayValueSeparator) : $e_tasks->task_project_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id"<?php echo $e_tasks->task_project_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_project_id->EditValue)) {
	$arwrk = $e_tasks->task_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$e_tasks->task_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo $e_tasks->task_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_project_id" class="form-group e_tasks_task_project_id">
<span<?php echo $e_tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_project_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_project_id" class="e_tasks_task_project_id">
<span<?php echo $e_tasks->task_project_id->ViewAttributes() ?>>
<?php echo $e_tasks->task_project_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $e_tasks_grid->PageObjName . "_row_" . $e_tasks_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_id" value="<?php echo ew_HtmlEncode($e_tasks->task_id->CurrentValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_id" value="<?php echo ew_HtmlEncode($e_tasks->task_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT || $e_tasks->CurrentMode == "edit") { ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_id" value="<?php echo ew_HtmlEncode($e_tasks->task_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($e_tasks->task_plan_id->Visible) { // task_plan_id ?>
		<td data-name="task_plan_id"<?php echo $e_tasks->task_plan_id->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($e_tasks->task_plan_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_plan_id" class="form-group e_tasks_task_plan_id">
<span<?php echo $e_tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_plan_id" class="form-group e_tasks_task_plan_id">
<select data-table="e_tasks" data-field="x_task_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_plan_id->DisplayValueSeparator) ? json_encode($e_tasks->task_plan_id->DisplayValueSeparator) : $e_tasks->task_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id"<?php echo $e_tasks->task_plan_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_plan_id->EditValue)) {
	$arwrk = $e_tasks->task_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$e_tasks->task_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$e_tasks->task_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $e_tasks->task_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo $e_tasks->task_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_plan_id" class="form-group e_tasks_task_plan_id">
<span<?php echo $e_tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_plan_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_plan_id" class="e_tasks_task_plan_id">
<span<?php echo $e_tasks->task_plan_id->ViewAttributes() ?>>
<?php echo $e_tasks->task_plan_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_lab_id->Visible) { // task_lab_id ?>
		<td data-name="task_lab_id"<?php echo $e_tasks->task_lab_id->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_lab_id" class="form-group e_tasks_task_lab_id">
<?php $e_tasks->task_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$e_tasks->task_lab_id->EditAttrs["onchange"]; ?>
<select data-table="e_tasks" data-field="x_task_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_lab_id->DisplayValueSeparator) ? json_encode($e_tasks->task_lab_id->DisplayValueSeparator) : $e_tasks->task_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id"<?php echo $e_tasks->task_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_lab_id->EditValue)) {
	$arwrk = $e_tasks->task_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_tasks->task_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo $e_tasks->task_lab_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_lab_id" class="form-group e_tasks_task_lab_id">
<span<?php echo $e_tasks->task_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_lab_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_lab_id" class="e_tasks_task_lab_id">
<span<?php echo $e_tasks->task_lab_id->ViewAttributes() ?>>
<?php echo $e_tasks->task_lab_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_code->Visible) { // task_code ?>
		<td data-name="task_code"<?php echo $e_tasks->task_code->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_code" class="form-group e_tasks_task_code">
<input type="text" data-table="e_tasks" data-field="x_task_code" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_code->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_code->EditValue ?>"<?php echo $e_tasks->task_code->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_code" class="form-group e_tasks_task_code">
<span<?php echo $e_tasks->task_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_code->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_code" class="e_tasks_task_code">
<span<?php echo $e_tasks->task_code->ViewAttributes() ?>>
<?php echo $e_tasks->task_code->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_name->Visible) { // task_name ?>
		<td data-name="task_name"<?php echo $e_tasks->task_name->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_name" class="form-group e_tasks_task_name">
<textarea data-table="e_tasks" data-field="x_task_name" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_name->getPlaceHolder()) ?>"<?php echo $e_tasks->task_name->EditAttributes() ?>><?php echo $e_tasks->task_name->EditValue ?></textarea>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_name" class="form-group e_tasks_task_name">
<span<?php echo $e_tasks->task_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_name->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_name" class="e_tasks_task_name">
<span<?php echo $e_tasks->task_name->ViewAttributes() ?>>
<?php echo $e_tasks->task_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_from->Visible) { // task_from ?>
		<td data-name="task_from"<?php echo $e_tasks->task_from->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_from" class="form-group e_tasks_task_from">
<input type="text" data-table="e_tasks" data-field="x_task_from" data-format="7" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_from->EditValue ?>"<?php echo $e_tasks->task_from->EditAttributes() ?>>
<?php if (!$e_tasks->task_from->ReadOnly && !$e_tasks->task_from->Disabled && !isset($e_tasks->task_from->EditAttrs["readonly"]) && !isset($e_tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_tasksgrid", "x<?php echo $e_tasks_grid->RowIndex ?>_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_from" class="form-group e_tasks_task_from">
<span<?php echo $e_tasks->task_from->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_from->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_from" class="e_tasks_task_from">
<span<?php echo $e_tasks->task_from->ViewAttributes() ?>>
<?php echo $e_tasks->task_from->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_to->Visible) { // task_to ?>
		<td data-name="task_to"<?php echo $e_tasks->task_to->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_to" class="form-group e_tasks_task_to">
<input type="text" data-table="e_tasks" data-field="x_task_to" data-format="7" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_to->EditValue ?>"<?php echo $e_tasks->task_to->EditAttributes() ?>>
<?php if (!$e_tasks->task_to->ReadOnly && !$e_tasks->task_to->Disabled && !isset($e_tasks->task_to->EditAttrs["readonly"]) && !isset($e_tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_tasksgrid", "x<?php echo $e_tasks_grid->RowIndex ?>_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_to" class="form-group e_tasks_task_to">
<span<?php echo $e_tasks->task_to->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_to->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_to" class="e_tasks_task_to">
<span<?php echo $e_tasks->task_to->ViewAttributes() ?>>
<?php echo $e_tasks->task_to->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_employee_id->Visible) { // task_employee_id ?>
		<td data-name="task_employee_id"<?php echo $e_tasks->task_employee_id->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($e_tasks->task_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_employee_id" class="form-group e_tasks_task_employee_id">
<span<?php echo $e_tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_employee_id" class="form-group e_tasks_task_employee_id">
<select data-table="e_tasks" data-field="x_task_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_employee_id->DisplayValueSeparator) ? json_encode($e_tasks->task_employee_id->DisplayValueSeparator) : $e_tasks->task_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id"<?php echo $e_tasks->task_employee_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_employee_id->EditValue)) {
	$arwrk = $e_tasks->task_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "{filter}";
$e_tasks->task_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$e_tasks->task_employee_id->LookupFilters += array("f1" => "`employee_lab_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $e_tasks->task_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo $e_tasks->task_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_employee_id" class="form-group e_tasks_task_employee_id">
<span<?php echo $e_tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_employee_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_employee_id" class="e_tasks_task_employee_id">
<span<?php echo $e_tasks->task_employee_id->ViewAttributes() ?>>
<?php echo $e_tasks->task_employee_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<td data-name="task_coordinator_id"<?php echo $e_tasks->task_coordinator_id->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_coordinator_id" class="form-group e_tasks_task_coordinator_id">
<select data-table="e_tasks" data-field="x_task_coordinator_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_coordinator_id->DisplayValueSeparator) ? json_encode($e_tasks->task_coordinator_id->DisplayValueSeparator) : $e_tasks->task_coordinator_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id"<?php echo $e_tasks->task_coordinator_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_coordinator_id->EditValue)) {
	$arwrk = $e_tasks->task_coordinator_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_coordinator_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_coordinator_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_coordinator_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_coordinator_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_coordinator_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$e_tasks->task_coordinator_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_coordinator_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $e_tasks->task_coordinator_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo $e_tasks->task_coordinator_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_coordinator_id" class="form-group e_tasks_task_coordinator_id">
<span<?php echo $e_tasks->task_coordinator_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_coordinator_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_coordinator_id" class="e_tasks_task_coordinator_id">
<span<?php echo $e_tasks->task_coordinator_id->ViewAttributes() ?>>
<?php echo $e_tasks->task_coordinator_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_object->Visible) { // task_object ?>
		<td data-name="task_object"<?php echo $e_tasks->task_object->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_object" class="form-group e_tasks_task_object">
<input type="text" data-table="e_tasks" data-field="x_task_object" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_object->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_object->EditValue ?>"<?php echo $e_tasks->task_object->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_object" class="form-group e_tasks_task_object">
<span<?php echo $e_tasks->task_object->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_object->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_object" class="e_tasks_task_object">
<span<?php echo $e_tasks->task_object->ViewAttributes() ?>>
<?php echo $e_tasks->task_object->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_status_id->Visible) { // task_status_id ?>
		<td data-name="task_status_id"<?php echo $e_tasks->task_status_id->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_status_id" class="form-group e_tasks_task_status_id">
<select data-table="e_tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_status_id->DisplayValueSeparator) ? json_encode($e_tasks->task_status_id->DisplayValueSeparator) : $e_tasks->task_status_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id"<?php echo $e_tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_status_id->EditValue)) {
	$arwrk = $e_tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_status_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$e_tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo $e_tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_status_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_status_id" class="form-group e_tasks_task_status_id">
<select data-table="e_tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_status_id->DisplayValueSeparator) ? json_encode($e_tasks->task_status_id->DisplayValueSeparator) : $e_tasks->task_status_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id"<?php echo $e_tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_status_id->EditValue)) {
	$arwrk = $e_tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_status_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$e_tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo $e_tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_status_id" class="e_tasks_task_status_id">
<span<?php echo $e_tasks->task_status_id->ViewAttributes() ?>>
<?php echo $e_tasks->task_status_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_status_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_status_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<td data-name="task_hours_planned"<?php echo $e_tasks->task_hours_planned->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_hours_planned" class="form-group e_tasks_task_hours_planned">
<input type="text" data-table="e_tasks" data-field="x_task_hours_planned" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_planned->EditValue ?>"<?php echo $e_tasks->task_hours_planned->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_hours_planned" class="form-group e_tasks_task_hours_planned">
<span<?php echo $e_tasks->task_hours_planned->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_hours_planned->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_hours_planned" class="e_tasks_task_hours_planned">
<span<?php echo $e_tasks->task_hours_planned->ViewAttributes() ?>>
<?php echo $e_tasks->task_hours_planned->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<td data-name="task_hours_actual"<?php echo $e_tasks->task_hours_actual->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_hours_actual" class="form-group e_tasks_task_hours_actual">
<input type="text" data-table="e_tasks" data-field="x_task_hours_actual" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_actual->EditValue ?>"<?php echo $e_tasks->task_hours_actual->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_hours_actual" class="form-group e_tasks_task_hours_actual">
<span<?php echo $e_tasks->task_hours_actual->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_hours_actual->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_hours_actual" class="e_tasks_task_hours_actual">
<span<?php echo $e_tasks->task_hours_actual->ViewAttributes() ?>>
<?php echo $e_tasks->task_hours_actual->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_description->Visible) { // task_description ?>
		<td data-name="task_description"<?php echo $e_tasks->task_description->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_description" class="form-group e_tasks_task_description">
<textarea data-table="e_tasks" data-field="x_task_description" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_description->getPlaceHolder()) ?>"<?php echo $e_tasks->task_description->EditAttributes() ?>><?php echo $e_tasks->task_description->EditValue ?></textarea>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_description" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($e_tasks->task_description->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_description" class="form-group e_tasks_task_description">
<textarea data-table="e_tasks" data-field="x_task_description" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_description->getPlaceHolder()) ?>"<?php echo $e_tasks->task_description->EditAttributes() ?>><?php echo $e_tasks->task_description->EditValue ?></textarea>
</span>
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_description" class="e_tasks_task_description">
<span<?php echo $e_tasks->task_description->ViewAttributes() ?>>
<?php echo $e_tasks->task_description->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_description" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($e_tasks->task_description->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_description" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($e_tasks->task_description->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($e_tasks->task_key->Visible) { // task_key ?>
		<td data-name="task_key"<?php echo $e_tasks->task_key->CellAttributes() ?>>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_key" class="form-group e_tasks_task_key">
<input type="text" data-table="e_tasks" data-field="x_task_key" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_key->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_key->EditValue ?>"<?php echo $e_tasks->task_key->EditAttributes() ?>>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->OldValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_key" class="form-group e_tasks_task_key">
<span<?php echo $e_tasks->task_key->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_key->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->CurrentValue) ?>">
<?php } ?>
<?php if ($e_tasks->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $e_tasks_grid->RowCnt ?>_e_tasks_task_key" class="e_tasks_task_key">
<span<?php echo $e_tasks->task_key->ViewAttributes() ?>>
<?php echo $e_tasks->task_key->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->FormValue) ?>">
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$e_tasks_grid->ListOptions->Render("body", "right", $e_tasks_grid->RowCnt);
?>
	</tr>
<?php if ($e_tasks->RowType == EW_ROWTYPE_ADD || $e_tasks->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fe_tasksgrid.UpdateOpts(<?php echo $e_tasks_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($e_tasks->CurrentAction <> "gridadd" || $e_tasks->CurrentMode == "copy")
		if (!$e_tasks_grid->Recordset->EOF) $e_tasks_grid->Recordset->MoveNext();
}
?>
<?php
	if ($e_tasks->CurrentMode == "add" || $e_tasks->CurrentMode == "copy" || $e_tasks->CurrentMode == "edit") {
		$e_tasks_grid->RowIndex = '$rowindex$';
		$e_tasks_grid->LoadDefaultValues();

		// Set row properties
		$e_tasks->ResetAttrs();
		$e_tasks->RowAttrs = array_merge($e_tasks->RowAttrs, array('data-rowindex'=>$e_tasks_grid->RowIndex, 'id'=>'r0_e_tasks', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($e_tasks->RowAttrs["class"], "ewTemplate");
		$e_tasks->RowType = EW_ROWTYPE_ADD;

		// Render row
		$e_tasks_grid->RenderRow();

		// Render list options
		$e_tasks_grid->RenderListOptions();
		$e_tasks_grid->StartRowCnt = 0;
?>
	<tr<?php echo $e_tasks->RowAttributes() ?>>
<?php

// Render list options (body, left)
$e_tasks_grid->ListOptions->Render("body", "left", $e_tasks_grid->RowIndex);
?>
	<?php if ($e_tasks->task_project_id->Visible) { // task_project_id ?>
		<td data-name="task_project_id">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<?php if ($e_tasks->task_project_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_e_tasks_task_project_id" class="form-group e_tasks_task_project_id">
<span<?php echo $e_tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_project_id" class="form-group e_tasks_task_project_id">
<?php $e_tasks->task_project_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$e_tasks->task_project_id->EditAttrs["onchange"]; ?>
<select data-table="e_tasks" data-field="x_task_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_project_id->DisplayValueSeparator) ? json_encode($e_tasks->task_project_id->DisplayValueSeparator) : $e_tasks->task_project_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id"<?php echo $e_tasks->task_project_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_project_id->EditValue)) {
	$arwrk = $e_tasks->task_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$e_tasks->task_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo $e_tasks->task_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_project_id" class="form-group e_tasks_task_project_id">
<span<?php echo $e_tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_plan_id->Visible) { // task_plan_id ?>
		<td data-name="task_plan_id">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<?php if ($e_tasks->task_plan_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_e_tasks_task_plan_id" class="form-group e_tasks_task_plan_id">
<span<?php echo $e_tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_plan_id" class="form-group e_tasks_task_plan_id">
<select data-table="e_tasks" data-field="x_task_plan_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_plan_id->DisplayValueSeparator) ? json_encode($e_tasks->task_plan_id->DisplayValueSeparator) : $e_tasks->task_plan_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id"<?php echo $e_tasks->task_plan_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_plan_id->EditValue)) {
	$arwrk = $e_tasks->task_plan_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_plan_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_plan_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_plan_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_plan_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_plan_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
$sWhereWrk = "{filter}";
$lookuptblfilter = "`plan_active` = 1";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
$e_tasks->task_plan_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_plan_id->LookupFilters += array("f0" => "`plan_id` = {filter_value}", "t0" => "3", "fn0" => "");
$e_tasks->task_plan_id->LookupFilters += array("f1" => "`plan_project_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_plan_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `plan_code` ASC";
if ($sSqlWrk <> "") $e_tasks->task_plan_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo $e_tasks->task_plan_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_plan_id" class="form-group e_tasks_task_plan_id">
<span<?php echo $e_tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_plan_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_lab_id->Visible) { // task_lab_id ?>
		<td data-name="task_lab_id">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_lab_id" class="form-group e_tasks_task_lab_id">
<?php $e_tasks->task_lab_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$e_tasks->task_lab_id->EditAttrs["onchange"]; ?>
<select data-table="e_tasks" data-field="x_task_lab_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_lab_id->DisplayValueSeparator) ? json_encode($e_tasks->task_lab_id->DisplayValueSeparator) : $e_tasks->task_lab_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id"<?php echo $e_tasks->task_lab_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_lab_id->EditValue)) {
	$arwrk = $e_tasks->task_lab_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_lab_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_lab_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_lab_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_lab_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_lab_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
$sWhereWrk = "";
$e_tasks->task_lab_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_lab_id->LookupFilters += array("f0" => "`lab_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_lab_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_lab_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo $e_tasks->task_lab_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_lab_id" class="form-group e_tasks_task_lab_id">
<span<?php echo $e_tasks->task_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_lab_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_code->Visible) { // task_code ?>
		<td data-name="task_code">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_code" class="form-group e_tasks_task_code">
<input type="text" data-table="e_tasks" data-field="x_task_code" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_code->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_code->EditValue ?>"<?php echo $e_tasks->task_code->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_code" class="form-group e_tasks_task_code">
<span<?php echo $e_tasks->task_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_code->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_code" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_name->Visible) { // task_name ?>
		<td data-name="task_name">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_name" class="form-group e_tasks_task_name">
<textarea data-table="e_tasks" data-field="x_task_name" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_name->getPlaceHolder()) ?>"<?php echo $e_tasks->task_name->EditAttributes() ?>><?php echo $e_tasks->task_name->EditValue ?></textarea>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_name" class="form-group e_tasks_task_name">
<span<?php echo $e_tasks->task_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_name" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_from->Visible) { // task_from ?>
		<td data-name="task_from">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_from" class="form-group e_tasks_task_from">
<input type="text" data-table="e_tasks" data-field="x_task_from" data-format="7" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_from->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_from->EditValue ?>"<?php echo $e_tasks->task_from->EditAttributes() ?>>
<?php if (!$e_tasks->task_from->ReadOnly && !$e_tasks->task_from->Disabled && !isset($e_tasks->task_from->EditAttrs["readonly"]) && !isset($e_tasks->task_from->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_tasksgrid", "x<?php echo $e_tasks_grid->RowIndex ?>_task_from", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_from" class="form-group e_tasks_task_from">
<span<?php echo $e_tasks->task_from->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_from->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_from" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_to->Visible) { // task_to ?>
		<td data-name="task_to">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_to" class="form-group e_tasks_task_to">
<input type="text" data-table="e_tasks" data-field="x_task_to" data-format="7" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_to->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_to->EditValue ?>"<?php echo $e_tasks->task_to->EditAttributes() ?>>
<?php if (!$e_tasks->task_to->ReadOnly && !$e_tasks->task_to->Disabled && !isset($e_tasks->task_to->EditAttrs["readonly"]) && !isset($e_tasks->task_to->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fe_tasksgrid", "x<?php echo $e_tasks_grid->RowIndex ?>_task_to", "%d-%m-%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_to" class="form-group e_tasks_task_to">
<span<?php echo $e_tasks->task_to->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_to->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_to" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_employee_id->Visible) { // task_employee_id ?>
		<td data-name="task_employee_id">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<?php if ($e_tasks->task_employee_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_e_tasks_task_employee_id" class="form-group e_tasks_task_employee_id">
<span<?php echo $e_tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_employee_id" class="form-group e_tasks_task_employee_id">
<select data-table="e_tasks" data-field="x_task_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_employee_id->DisplayValueSeparator) ? json_encode($e_tasks->task_employee_id->DisplayValueSeparator) : $e_tasks->task_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id"<?php echo $e_tasks->task_employee_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_employee_id->EditValue)) {
	$arwrk = $e_tasks->task_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "{filter}";
$e_tasks->task_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$e_tasks->task_employee_id->LookupFilters += array("f1" => "`employee_lab_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $e_tasks->task_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo $e_tasks->task_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_employee_id" class="form-group e_tasks_task_employee_id">
<span<?php echo $e_tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<td data-name="task_coordinator_id">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_coordinator_id" class="form-group e_tasks_task_coordinator_id">
<select data-table="e_tasks" data-field="x_task_coordinator_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_coordinator_id->DisplayValueSeparator) ? json_encode($e_tasks->task_coordinator_id->DisplayValueSeparator) : $e_tasks->task_coordinator_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id"<?php echo $e_tasks->task_coordinator_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_coordinator_id->EditValue)) {
	$arwrk = $e_tasks->task_coordinator_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_coordinator_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_coordinator_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_coordinator_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_coordinator_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_coordinator_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$e_tasks->task_coordinator_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_coordinator_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $e_tasks->task_coordinator_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo $e_tasks->task_coordinator_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_coordinator_id" class="form-group e_tasks_task_coordinator_id">
<span<?php echo $e_tasks->task_coordinator_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_coordinator_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_object->Visible) { // task_object ?>
		<td data-name="task_object">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_object" class="form-group e_tasks_task_object">
<input type="text" data-table="e_tasks" data-field="x_task_object" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_object->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_object->EditValue ?>"<?php echo $e_tasks->task_object->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_object" class="form-group e_tasks_task_object">
<span<?php echo $e_tasks->task_object->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_object->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_object" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_status_id->Visible) { // task_status_id ?>
		<td data-name="task_status_id">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_status_id" class="form-group e_tasks_task_status_id">
<select data-table="e_tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_status_id->DisplayValueSeparator) ? json_encode($e_tasks->task_status_id->DisplayValueSeparator) : $e_tasks->task_status_id->DisplayValueSeparator) ?>" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id"<?php echo $e_tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_status_id->EditValue)) {
	$arwrk = $e_tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $e_tasks->task_status_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$e_tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="s_x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo $e_tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_status_id" class="form-group e_tasks_task_status_id">
<span<?php echo $e_tasks->task_status_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_status_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_status_id" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_status_id" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_status_id" value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<td data-name="task_hours_planned">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_hours_planned" class="form-group e_tasks_task_hours_planned">
<input type="text" data-table="e_tasks" data-field="x_task_hours_planned" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_planned->EditValue ?>"<?php echo $e_tasks->task_hours_planned->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_hours_planned" class="form-group e_tasks_task_hours_planned">
<span<?php echo $e_tasks->task_hours_planned->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_hours_planned->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<td data-name="task_hours_actual">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_hours_actual" class="form-group e_tasks_task_hours_actual">
<input type="text" data-table="e_tasks" data-field="x_task_hours_actual" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" size="30" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_hours_actual->EditValue ?>"<?php echo $e_tasks->task_hours_actual->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_hours_actual" class="form-group e_tasks_task_hours_actual">
<span<?php echo $e_tasks->task_hours_actual->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_hours_actual->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_description->Visible) { // task_description ?>
		<td data-name="task_description">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_description" class="form-group e_tasks_task_description">
<textarea data-table="e_tasks" data-field="x_task_description" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_description->getPlaceHolder()) ?>"<?php echo $e_tasks->task_description->EditAttributes() ?>><?php echo $e_tasks->task_description->EditValue ?></textarea>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_description" class="form-group e_tasks_task_description">
<span<?php echo $e_tasks->task_description->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_description->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_description" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($e_tasks->task_description->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_description" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_description" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_description" value="<?php echo ew_HtmlEncode($e_tasks->task_description->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($e_tasks->task_key->Visible) { // task_key ?>
		<td data-name="task_key">
<?php if ($e_tasks->CurrentAction <> "F") { ?>
<span id="el$rowindex$_e_tasks_task_key" class="form-group e_tasks_task_key">
<input type="text" data-table="e_tasks" data-field="x_task_key" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_key->getPlaceHolder()) ?>" value="<?php echo $e_tasks->task_key->EditValue ?>"<?php echo $e_tasks->task_key->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_e_tasks_task_key" class="form-group e_tasks_task_key">
<span<?php echo $e_tasks->task_key->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_key->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="x<?php echo $e_tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="o<?php echo $e_tasks_grid->RowIndex ?>_task_key" id="o<?php echo $e_tasks_grid->RowIndex ?>_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$e_tasks_grid->ListOptions->Render("body", "right", $e_tasks_grid->RowCnt);
?>
<script type="text/javascript">
fe_tasksgrid.UpdateOpts(<?php echo $e_tasks_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($e_tasks->CurrentMode == "add" || $e_tasks->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $e_tasks_grid->FormKeyCountName ?>" id="<?php echo $e_tasks_grid->FormKeyCountName ?>" value="<?php echo $e_tasks_grid->KeyCount ?>">
<?php echo $e_tasks_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($e_tasks->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $e_tasks_grid->FormKeyCountName ?>" id="<?php echo $e_tasks_grid->FormKeyCountName ?>" value="<?php echo $e_tasks_grid->KeyCount ?>">
<?php echo $e_tasks_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($e_tasks->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="fe_tasksgrid">
</div>
<?php

// Close recordset
if ($e_tasks_grid->Recordset)
	$e_tasks_grid->Recordset->Close();
?>
<?php if ($e_tasks_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($e_tasks_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($e_tasks_grid->TotalRecs == 0 && $e_tasks->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($e_tasks_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($e_tasks->Export == "") { ?>
<script type="text/javascript">
fe_tasksgrid.Init();
</script>
<?php } ?>
<?php
$e_tasks_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$e_tasks_grid->Page_Terminate();
?>
