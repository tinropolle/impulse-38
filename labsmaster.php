<?php

// lab_name
// lab_description

?>
<?php if ($labs->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $labs->TableCaption() ?></h4> -->
<table id="tbl_labsmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $labs->TableCustomInnerHtml ?>
	<tbody>
<?php if ($labs->lab_name->Visible) { // lab_name ?>
		<tr id="r_lab_name">
			<td><?php echo $labs->lab_name->FldCaption() ?></td>
			<td<?php echo $labs->lab_name->CellAttributes() ?>>
<span id="el_labs_lab_name">
<span<?php echo $labs->lab_name->ViewAttributes() ?>>
<?php echo $labs->lab_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($labs->lab_description->Visible) { // lab_description ?>
		<tr id="r_lab_description">
			<td><?php echo $labs->lab_description->FldCaption() ?></td>
			<td<?php echo $labs->lab_description->CellAttributes() ?>>
<span id="el_labs_lab_description">
<span<?php echo $labs->lab_description->ViewAttributes() ?>>
<?php echo $labs->lab_description->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
