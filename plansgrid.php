<?php include_once "employeesinfo.php" ?>
<?php

// Create page object
if (!isset($plans_grid)) $plans_grid = new cplans_grid();

// Page init
$plans_grid->Page_Init();

// Page main
$plans_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$plans_grid->Page_Render();
?>
<?php if ($plans->Export == "") { ?>
<script type="text/javascript">

// Form object
var fplansgrid = new ew_Form("fplansgrid", "grid");
fplansgrid.FormKeyCountName = '<?php echo $plans_grid->FormKeyCountName ?>';

// Validate form
fplansgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_plan_project_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_project_id->FldCaption(), $plans->plan_project_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plan_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_code->FldCaption(), $plans->plan_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plan_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_name->FldCaption(), $plans->plan_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plan_active");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $plans->plan_active->FldCaption(), $plans->plan_active->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fplansgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "plan_project_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "plan_code", false)) return false;
	if (ew_ValueChanged(fobj, infix, "plan_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "plan_employee_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "plan_active", false)) return false;
	return true;
}

// Form_CustomValidate event
fplansgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fplansgrid.ValidateRequired = true;
<?php } else { ?>
fplansgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fplansgrid.Lists["x_plan_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansgrid.Lists["x_plan_code"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansgrid.Lists["x_plan_code"].Options = <?php echo json_encode($plans->plan_code->Options()) ?>;
fplansgrid.Lists["x_plan_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansgrid.Lists["x_plan_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fplansgrid.Lists["x_plan_active"].Options = <?php echo json_encode($plans->plan_active->Options()) ?>;

// Form object for search
</script>
<?php } ?>
<?php
if ($plans->CurrentAction == "gridadd") {
	if ($plans->CurrentMode == "copy") {
		$bSelectLimit = $plans_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$plans_grid->TotalRecs = $plans->SelectRecordCount();
			$plans_grid->Recordset = $plans_grid->LoadRecordset($plans_grid->StartRec-1, $plans_grid->DisplayRecs);
		} else {
			if ($plans_grid->Recordset = $plans_grid->LoadRecordset())
				$plans_grid->TotalRecs = $plans_grid->Recordset->RecordCount();
		}
		$plans_grid->StartRec = 1;
		$plans_grid->DisplayRecs = $plans_grid->TotalRecs;
	} else {
		$plans->CurrentFilter = "0=1";
		$plans_grid->StartRec = 1;
		$plans_grid->DisplayRecs = $plans->GridAddRowCount;
	}
	$plans_grid->TotalRecs = $plans_grid->DisplayRecs;
	$plans_grid->StopRec = $plans_grid->DisplayRecs;
} else {
	$bSelectLimit = $plans_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($plans_grid->TotalRecs <= 0)
			$plans_grid->TotalRecs = $plans->SelectRecordCount();
	} else {
		if (!$plans_grid->Recordset && ($plans_grid->Recordset = $plans_grid->LoadRecordset()))
			$plans_grid->TotalRecs = $plans_grid->Recordset->RecordCount();
	}
	$plans_grid->StartRec = 1;
	$plans_grid->DisplayRecs = $plans_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$plans_grid->Recordset = $plans_grid->LoadRecordset($plans_grid->StartRec-1, $plans_grid->DisplayRecs);

	// Set no record found message
	if ($plans->CurrentAction == "" && $plans_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$plans_grid->setWarningMessage(ew_DeniedMsg());
		if ($plans_grid->SearchWhere == "0=101")
			$plans_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$plans_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$plans_grid->RenderOtherOptions();
?>
<?php $plans_grid->ShowPageHeader(); ?>
<?php
$plans_grid->ShowMessage();
?>
<?php if ($plans_grid->TotalRecs > 0 || $plans->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="fplansgrid" class="ewForm form-inline">
<?php if ($plans_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($plans_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_plans" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_plansgrid" class="table ewTable">
<?php echo $plans->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$plans_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$plans_grid->RenderListOptions();

// Render list options (header, left)
$plans_grid->ListOptions->Render("header", "left");
?>
<?php if ($plans->plan_project_id->Visible) { // plan_project_id ?>
	<?php if ($plans->SortUrl($plans->plan_project_id) == "") { ?>
		<th data-name="plan_project_id"><div id="elh_plans_plan_project_id" class="plans_plan_project_id"><div class="ewTableHeaderCaption"><?php echo $plans->plan_project_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plan_project_id"><div><div id="elh_plans_plan_project_id" class="plans_plan_project_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $plans->plan_project_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($plans->plan_project_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($plans->plan_project_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($plans->plan_code->Visible) { // plan_code ?>
	<?php if ($plans->SortUrl($plans->plan_code) == "") { ?>
		<th data-name="plan_code"><div id="elh_plans_plan_code" class="plans_plan_code"><div class="ewTableHeaderCaption"><?php echo $plans->plan_code->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plan_code"><div><div id="elh_plans_plan_code" class="plans_plan_code">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $plans->plan_code->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($plans->plan_code->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($plans->plan_code->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($plans->plan_name->Visible) { // plan_name ?>
	<?php if ($plans->SortUrl($plans->plan_name) == "") { ?>
		<th data-name="plan_name"><div id="elh_plans_plan_name" class="plans_plan_name"><div class="ewTableHeaderCaption"><?php echo $plans->plan_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plan_name"><div><div id="elh_plans_plan_name" class="plans_plan_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $plans->plan_name->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($plans->plan_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($plans->plan_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($plans->plan_employee_id->Visible) { // plan_employee_id ?>
	<?php if ($plans->SortUrl($plans->plan_employee_id) == "") { ?>
		<th data-name="plan_employee_id"><div id="elh_plans_plan_employee_id" class="plans_plan_employee_id"><div class="ewTableHeaderCaption"><?php echo $plans->plan_employee_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plan_employee_id"><div><div id="elh_plans_plan_employee_id" class="plans_plan_employee_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $plans->plan_employee_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($plans->plan_employee_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($plans->plan_employee_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($plans->plan_active->Visible) { // plan_active ?>
	<?php if ($plans->SortUrl($plans->plan_active) == "") { ?>
		<th data-name="plan_active"><div id="elh_plans_plan_active" class="plans_plan_active"><div class="ewTableHeaderCaption"><?php echo $plans->plan_active->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plan_active"><div><div id="elh_plans_plan_active" class="plans_plan_active">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $plans->plan_active->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($plans->plan_active->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($plans->plan_active->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$plans_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$plans_grid->StartRec = 1;
$plans_grid->StopRec = $plans_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($plans_grid->FormKeyCountName) && ($plans->CurrentAction == "gridadd" || $plans->CurrentAction == "gridedit" || $plans->CurrentAction == "F")) {
		$plans_grid->KeyCount = $objForm->GetValue($plans_grid->FormKeyCountName);
		$plans_grid->StopRec = $plans_grid->StartRec + $plans_grid->KeyCount - 1;
	}
}
$plans_grid->RecCnt = $plans_grid->StartRec - 1;
if ($plans_grid->Recordset && !$plans_grid->Recordset->EOF) {
	$plans_grid->Recordset->MoveFirst();
	$bSelectLimit = $plans_grid->UseSelectLimit;
	if (!$bSelectLimit && $plans_grid->StartRec > 1)
		$plans_grid->Recordset->Move($plans_grid->StartRec - 1);
} elseif (!$plans->AllowAddDeleteRow && $plans_grid->StopRec == 0) {
	$plans_grid->StopRec = $plans->GridAddRowCount;
}

// Initialize aggregate
$plans->RowType = EW_ROWTYPE_AGGREGATEINIT;
$plans->ResetAttrs();
$plans_grid->RenderRow();
if ($plans->CurrentAction == "gridadd")
	$plans_grid->RowIndex = 0;
if ($plans->CurrentAction == "gridedit")
	$plans_grid->RowIndex = 0;
while ($plans_grid->RecCnt < $plans_grid->StopRec) {
	$plans_grid->RecCnt++;
	if (intval($plans_grid->RecCnt) >= intval($plans_grid->StartRec)) {
		$plans_grid->RowCnt++;
		if ($plans->CurrentAction == "gridadd" || $plans->CurrentAction == "gridedit" || $plans->CurrentAction == "F") {
			$plans_grid->RowIndex++;
			$objForm->Index = $plans_grid->RowIndex;
			if ($objForm->HasValue($plans_grid->FormActionName))
				$plans_grid->RowAction = strval($objForm->GetValue($plans_grid->FormActionName));
			elseif ($plans->CurrentAction == "gridadd")
				$plans_grid->RowAction = "insert";
			else
				$plans_grid->RowAction = "";
		}

		// Set up key count
		$plans_grid->KeyCount = $plans_grid->RowIndex;

		// Init row class and style
		$plans->ResetAttrs();
		$plans->CssClass = "";
		if ($plans->CurrentAction == "gridadd") {
			if ($plans->CurrentMode == "copy") {
				$plans_grid->LoadRowValues($plans_grid->Recordset); // Load row values
				$plans_grid->SetRecordKey($plans_grid->RowOldKey, $plans_grid->Recordset); // Set old record key
			} else {
				$plans_grid->LoadDefaultValues(); // Load default values
				$plans_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$plans_grid->LoadRowValues($plans_grid->Recordset); // Load row values
		}
		$plans->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($plans->CurrentAction == "gridadd") // Grid add
			$plans->RowType = EW_ROWTYPE_ADD; // Render add
		if ($plans->CurrentAction == "gridadd" && $plans->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$plans_grid->RestoreCurrentRowFormValues($plans_grid->RowIndex); // Restore form values
		if ($plans->CurrentAction == "gridedit") { // Grid edit
			if ($plans->EventCancelled) {
				$plans_grid->RestoreCurrentRowFormValues($plans_grid->RowIndex); // Restore form values
			}
			if ($plans_grid->RowAction == "insert")
				$plans->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$plans->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($plans->CurrentAction == "gridedit" && ($plans->RowType == EW_ROWTYPE_EDIT || $plans->RowType == EW_ROWTYPE_ADD) && $plans->EventCancelled) // Update failed
			$plans_grid->RestoreCurrentRowFormValues($plans_grid->RowIndex); // Restore form values
		if ($plans->RowType == EW_ROWTYPE_EDIT) // Edit row
			$plans_grid->EditRowCnt++;
		if ($plans->CurrentAction == "F") // Confirm row
			$plans_grid->RestoreCurrentRowFormValues($plans_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$plans->RowAttrs = array_merge($plans->RowAttrs, array('data-rowindex'=>$plans_grid->RowCnt, 'id'=>'r' . $plans_grid->RowCnt . '_plans', 'data-rowtype'=>$plans->RowType));

		// Render row
		$plans_grid->RenderRow();

		// Render list options
		$plans_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($plans_grid->RowAction <> "delete" && $plans_grid->RowAction <> "insertdelete" && !($plans_grid->RowAction == "insert" && $plans->CurrentAction == "F" && $plans_grid->EmptyRow())) {
?>
	<tr<?php echo $plans->RowAttributes() ?>>
<?php

// Render list options (body, left)
$plans_grid->ListOptions->Render("body", "left", $plans_grid->RowCnt);
?>
	<?php if ($plans->plan_project_id->Visible) { // plan_project_id ?>
		<td data-name="plan_project_id"<?php echo $plans->plan_project_id->CellAttributes() ?>>
<?php if ($plans->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($plans->plan_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_project_id" class="form-group plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_project_id" class="form-group plans_plan_project_id">
<select data-table="plans" data-field="x_plan_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_project_id->DisplayValueSeparator) ? json_encode($plans->plan_project_id->DisplayValueSeparator) : $plans->plan_project_id->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id"<?php echo $plans->plan_project_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_project_id->EditValue)) {
	$arwrk = $plans->plan_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>" selected><?php echo $plans->plan_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$plans->plan_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $plans->plan_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="s_x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo $plans->plan_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_project_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->OldValue) ?>">
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($plans->plan_project_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_project_id" class="form-group plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_project_id" class="form-group plans_plan_project_id">
<select data-table="plans" data-field="x_plan_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_project_id->DisplayValueSeparator) ? json_encode($plans->plan_project_id->DisplayValueSeparator) : $plans->plan_project_id->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id"<?php echo $plans->plan_project_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_project_id->EditValue)) {
	$arwrk = $plans->plan_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>" selected><?php echo $plans->plan_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$plans->plan_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $plans->plan_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="s_x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo $plans->plan_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_project_id" class="plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<?php echo $plans->plan_project_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->FormValue) ?>">
<input type="hidden" data-table="plans" data-field="x_plan_project_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $plans_grid->PageObjName . "_row_" . $plans_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="plans" data-field="x_plan_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_id" id="x<?php echo $plans_grid->RowIndex ?>_plan_id" value="<?php echo ew_HtmlEncode($plans->plan_id->CurrentValue) ?>">
<input type="hidden" data-table="plans" data-field="x_plan_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_id" value="<?php echo ew_HtmlEncode($plans->plan_id->OldValue) ?>">
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_EDIT || $plans->CurrentMode == "edit") { ?>
<input type="hidden" data-table="plans" data-field="x_plan_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_id" id="x<?php echo $plans_grid->RowIndex ?>_plan_id" value="<?php echo ew_HtmlEncode($plans->plan_id->CurrentValue) ?>">
<?php } ?>
	<?php if ($plans->plan_code->Visible) { // plan_code ?>
		<td data-name="plan_code"<?php echo $plans->plan_code->CellAttributes() ?>>
<?php if ($plans->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_code" class="form-group plans_plan_code">
<select data-table="plans" data-field="x_plan_code" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_code->DisplayValueSeparator) ? json_encode($plans->plan_code->DisplayValueSeparator) : $plans->plan_code->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_code" name="x<?php echo $plans_grid->RowIndex ?>_plan_code"<?php echo $plans->plan_code->EditAttributes() ?>>
<?php
if (is_array($plans->plan_code->EditValue)) {
	$arwrk = $plans->plan_code->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_code->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_code->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_code->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_code->CurrentValue) ?>" selected><?php echo $plans->plan_code->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_code->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_code" name="o<?php echo $plans_grid->RowIndex ?>_plan_code" id="o<?php echo $plans_grid->RowIndex ?>_plan_code" value="<?php echo ew_HtmlEncode($plans->plan_code->OldValue) ?>">
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_code" class="form-group plans_plan_code">
<select data-table="plans" data-field="x_plan_code" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_code->DisplayValueSeparator) ? json_encode($plans->plan_code->DisplayValueSeparator) : $plans->plan_code->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_code" name="x<?php echo $plans_grid->RowIndex ?>_plan_code"<?php echo $plans->plan_code->EditAttributes() ?>>
<?php
if (is_array($plans->plan_code->EditValue)) {
	$arwrk = $plans->plan_code->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_code->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_code->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_code->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_code->CurrentValue) ?>" selected><?php echo $plans->plan_code->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_code->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_code" class="plans_plan_code">
<span<?php echo $plans->plan_code->ViewAttributes() ?>>
<?php echo $plans->plan_code->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_code" name="x<?php echo $plans_grid->RowIndex ?>_plan_code" id="x<?php echo $plans_grid->RowIndex ?>_plan_code" value="<?php echo ew_HtmlEncode($plans->plan_code->FormValue) ?>">
<input type="hidden" data-table="plans" data-field="x_plan_code" name="o<?php echo $plans_grid->RowIndex ?>_plan_code" id="o<?php echo $plans_grid->RowIndex ?>_plan_code" value="<?php echo ew_HtmlEncode($plans->plan_code->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($plans->plan_name->Visible) { // plan_name ?>
		<td data-name="plan_name"<?php echo $plans->plan_name->CellAttributes() ?>>
<?php if ($plans->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_name" class="form-group plans_plan_name">
<textarea data-table="plans" data-field="x_plan_name" name="x<?php echo $plans_grid->RowIndex ?>_plan_name" id="x<?php echo $plans_grid->RowIndex ?>_plan_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($plans->plan_name->getPlaceHolder()) ?>"<?php echo $plans->plan_name->EditAttributes() ?>><?php echo $plans->plan_name->EditValue ?></textarea>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_name" name="o<?php echo $plans_grid->RowIndex ?>_plan_name" id="o<?php echo $plans_grid->RowIndex ?>_plan_name" value="<?php echo ew_HtmlEncode($plans->plan_name->OldValue) ?>">
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_name" class="form-group plans_plan_name">
<textarea data-table="plans" data-field="x_plan_name" name="x<?php echo $plans_grid->RowIndex ?>_plan_name" id="x<?php echo $plans_grid->RowIndex ?>_plan_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($plans->plan_name->getPlaceHolder()) ?>"<?php echo $plans->plan_name->EditAttributes() ?>><?php echo $plans->plan_name->EditValue ?></textarea>
</span>
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_name" class="plans_plan_name">
<span<?php echo $plans->plan_name->ViewAttributes() ?>>
<?php echo $plans->plan_name->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_name" name="x<?php echo $plans_grid->RowIndex ?>_plan_name" id="x<?php echo $plans_grid->RowIndex ?>_plan_name" value="<?php echo ew_HtmlEncode($plans->plan_name->FormValue) ?>">
<input type="hidden" data-table="plans" data-field="x_plan_name" name="o<?php echo $plans_grid->RowIndex ?>_plan_name" id="o<?php echo $plans_grid->RowIndex ?>_plan_name" value="<?php echo ew_HtmlEncode($plans->plan_name->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($plans->plan_employee_id->Visible) { // plan_employee_id ?>
		<td data-name="plan_employee_id"<?php echo $plans->plan_employee_id->CellAttributes() ?>>
<?php if ($plans->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($plans->plan_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<select data-table="plans" data-field="x_plan_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_employee_id->DisplayValueSeparator) ? json_encode($plans->plan_employee_id->DisplayValueSeparator) : $plans->plan_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id"<?php echo $plans->plan_employee_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_employee_id->EditValue)) {
	$arwrk = $plans->plan_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>" selected><?php echo $plans->plan_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$plans->plan_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $plans->plan_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="s_x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo $plans->plan_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_employee_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->OldValue) ?>">
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($plans->plan_employee_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<select data-table="plans" data-field="x_plan_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_employee_id->DisplayValueSeparator) ? json_encode($plans->plan_employee_id->DisplayValueSeparator) : $plans->plan_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id"<?php echo $plans->plan_employee_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_employee_id->EditValue)) {
	$arwrk = $plans->plan_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>" selected><?php echo $plans->plan_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$plans->plan_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $plans->plan_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="s_x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo $plans->plan_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_employee_id" class="plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<?php echo $plans->plan_employee_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->FormValue) ?>">
<input type="hidden" data-table="plans" data-field="x_plan_employee_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($plans->plan_active->Visible) { // plan_active ?>
		<td data-name="plan_active"<?php echo $plans->plan_active->CellAttributes() ?>>
<?php if ($plans->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_active" class="form-group plans_plan_active">
<select data-table="plans" data-field="x_plan_active" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_active->DisplayValueSeparator) ? json_encode($plans->plan_active->DisplayValueSeparator) : $plans->plan_active->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_active" name="x<?php echo $plans_grid->RowIndex ?>_plan_active"<?php echo $plans->plan_active->EditAttributes() ?>>
<?php
if (is_array($plans->plan_active->EditValue)) {
	$arwrk = $plans->plan_active->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_active->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_active->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_active->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_active->CurrentValue) ?>" selected><?php echo $plans->plan_active->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_active->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_active" name="o<?php echo $plans_grid->RowIndex ?>_plan_active" id="o<?php echo $plans_grid->RowIndex ?>_plan_active" value="<?php echo ew_HtmlEncode($plans->plan_active->OldValue) ?>">
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_active" class="form-group plans_plan_active">
<select data-table="plans" data-field="x_plan_active" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_active->DisplayValueSeparator) ? json_encode($plans->plan_active->DisplayValueSeparator) : $plans->plan_active->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_active" name="x<?php echo $plans_grid->RowIndex ?>_plan_active"<?php echo $plans->plan_active->EditAttributes() ?>>
<?php
if (is_array($plans->plan_active->EditValue)) {
	$arwrk = $plans->plan_active->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_active->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_active->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_active->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_active->CurrentValue) ?>" selected><?php echo $plans->plan_active->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_active->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($plans->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $plans_grid->RowCnt ?>_plans_plan_active" class="plans_plan_active">
<span<?php echo $plans->plan_active->ViewAttributes() ?>>
<?php echo $plans->plan_active->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_active" name="x<?php echo $plans_grid->RowIndex ?>_plan_active" id="x<?php echo $plans_grid->RowIndex ?>_plan_active" value="<?php echo ew_HtmlEncode($plans->plan_active->FormValue) ?>">
<input type="hidden" data-table="plans" data-field="x_plan_active" name="o<?php echo $plans_grid->RowIndex ?>_plan_active" id="o<?php echo $plans_grid->RowIndex ?>_plan_active" value="<?php echo ew_HtmlEncode($plans->plan_active->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$plans_grid->ListOptions->Render("body", "right", $plans_grid->RowCnt);
?>
	</tr>
<?php if ($plans->RowType == EW_ROWTYPE_ADD || $plans->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fplansgrid.UpdateOpts(<?php echo $plans_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($plans->CurrentAction <> "gridadd" || $plans->CurrentMode == "copy")
		if (!$plans_grid->Recordset->EOF) $plans_grid->Recordset->MoveNext();
}
?>
<?php
	if ($plans->CurrentMode == "add" || $plans->CurrentMode == "copy" || $plans->CurrentMode == "edit") {
		$plans_grid->RowIndex = '$rowindex$';
		$plans_grid->LoadDefaultValues();

		// Set row properties
		$plans->ResetAttrs();
		$plans->RowAttrs = array_merge($plans->RowAttrs, array('data-rowindex'=>$plans_grid->RowIndex, 'id'=>'r0_plans', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($plans->RowAttrs["class"], "ewTemplate");
		$plans->RowType = EW_ROWTYPE_ADD;

		// Render row
		$plans_grid->RenderRow();

		// Render list options
		$plans_grid->RenderListOptions();
		$plans_grid->StartRowCnt = 0;
?>
	<tr<?php echo $plans->RowAttributes() ?>>
<?php

// Render list options (body, left)
$plans_grid->ListOptions->Render("body", "left", $plans_grid->RowIndex);
?>
	<?php if ($plans->plan_project_id->Visible) { // plan_project_id ?>
		<td data-name="plan_project_id">
<?php if ($plans->CurrentAction <> "F") { ?>
<?php if ($plans->plan_project_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_plans_plan_project_id" class="form-group plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_plans_plan_project_id" class="form-group plans_plan_project_id">
<select data-table="plans" data-field="x_plan_project_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_project_id->DisplayValueSeparator) ? json_encode($plans->plan_project_id->DisplayValueSeparator) : $plans->plan_project_id->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id"<?php echo $plans->plan_project_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_project_id->EditValue)) {
	$arwrk = $plans->plan_project_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_project_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_project_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_project_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_project_id->CurrentValue) ?>" selected><?php echo $plans->plan_project_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_project_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
$sWhereWrk = "";
$plans->plan_project_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_project_id->LookupFilters += array("f0" => "`project_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_project_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $plans->plan_project_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="s_x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo $plans->plan_project_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_plans_plan_project_id" class="form-group plans_plan_project_id">
<span<?php echo $plans->plan_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_project_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_project_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="x<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_project_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_project_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_project_id" value="<?php echo ew_HtmlEncode($plans->plan_project_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($plans->plan_code->Visible) { // plan_code ?>
		<td data-name="plan_code">
<?php if ($plans->CurrentAction <> "F") { ?>
<span id="el$rowindex$_plans_plan_code" class="form-group plans_plan_code">
<select data-table="plans" data-field="x_plan_code" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_code->DisplayValueSeparator) ? json_encode($plans->plan_code->DisplayValueSeparator) : $plans->plan_code->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_code" name="x<?php echo $plans_grid->RowIndex ?>_plan_code"<?php echo $plans->plan_code->EditAttributes() ?>>
<?php
if (is_array($plans->plan_code->EditValue)) {
	$arwrk = $plans->plan_code->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_code->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_code->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_code->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_code->CurrentValue) ?>" selected><?php echo $plans->plan_code->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_code->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_plans_plan_code" class="form-group plans_plan_code">
<span<?php echo $plans->plan_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_code->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_code" name="x<?php echo $plans_grid->RowIndex ?>_plan_code" id="x<?php echo $plans_grid->RowIndex ?>_plan_code" value="<?php echo ew_HtmlEncode($plans->plan_code->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_code" name="o<?php echo $plans_grid->RowIndex ?>_plan_code" id="o<?php echo $plans_grid->RowIndex ?>_plan_code" value="<?php echo ew_HtmlEncode($plans->plan_code->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($plans->plan_name->Visible) { // plan_name ?>
		<td data-name="plan_name">
<?php if ($plans->CurrentAction <> "F") { ?>
<span id="el$rowindex$_plans_plan_name" class="form-group plans_plan_name">
<textarea data-table="plans" data-field="x_plan_name" name="x<?php echo $plans_grid->RowIndex ?>_plan_name" id="x<?php echo $plans_grid->RowIndex ?>_plan_name" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($plans->plan_name->getPlaceHolder()) ?>"<?php echo $plans->plan_name->EditAttributes() ?>><?php echo $plans->plan_name->EditValue ?></textarea>
</span>
<?php } else { ?>
<span id="el$rowindex$_plans_plan_name" class="form-group plans_plan_name">
<span<?php echo $plans->plan_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_name->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_name" name="x<?php echo $plans_grid->RowIndex ?>_plan_name" id="x<?php echo $plans_grid->RowIndex ?>_plan_name" value="<?php echo ew_HtmlEncode($plans->plan_name->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_name" name="o<?php echo $plans_grid->RowIndex ?>_plan_name" id="o<?php echo $plans_grid->RowIndex ?>_plan_name" value="<?php echo ew_HtmlEncode($plans->plan_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($plans->plan_employee_id->Visible) { // plan_employee_id ?>
		<td data-name="plan_employee_id">
<?php if ($plans->CurrentAction <> "F") { ?>
<?php if ($plans->plan_employee_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<select data-table="plans" data-field="x_plan_employee_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_employee_id->DisplayValueSeparator) ? json_encode($plans->plan_employee_id->DisplayValueSeparator) : $plans->plan_employee_id->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id"<?php echo $plans->plan_employee_id->EditAttributes() ?>>
<?php
if (is_array($plans->plan_employee_id->EditValue)) {
	$arwrk = $plans->plan_employee_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_employee_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_employee_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_employee_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_employee_id->CurrentValue) ?>" selected><?php echo $plans->plan_employee_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_employee_id->OldValue = "";
?>
</select>
<?php
$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
$sWhereWrk = "";
$plans->plan_employee_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$plans->plan_employee_id->LookupFilters += array("f0" => "`employee_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$plans->Lookup_Selecting($plans->plan_employee_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
if ($sSqlWrk <> "") $plans->plan_employee_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="s_x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo $plans->plan_employee_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_plans_plan_employee_id" class="form-group plans_plan_employee_id">
<span<?php echo $plans->plan_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_employee_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_employee_id" name="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="x<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_employee_id" name="o<?php echo $plans_grid->RowIndex ?>_plan_employee_id" id="o<?php echo $plans_grid->RowIndex ?>_plan_employee_id" value="<?php echo ew_HtmlEncode($plans->plan_employee_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($plans->plan_active->Visible) { // plan_active ?>
		<td data-name="plan_active">
<?php if ($plans->CurrentAction <> "F") { ?>
<span id="el$rowindex$_plans_plan_active" class="form-group plans_plan_active">
<select data-table="plans" data-field="x_plan_active" data-value-separator="<?php echo ew_HtmlEncode(is_array($plans->plan_active->DisplayValueSeparator) ? json_encode($plans->plan_active->DisplayValueSeparator) : $plans->plan_active->DisplayValueSeparator) ?>" id="x<?php echo $plans_grid->RowIndex ?>_plan_active" name="x<?php echo $plans_grid->RowIndex ?>_plan_active"<?php echo $plans->plan_active->EditAttributes() ?>>
<?php
if (is_array($plans->plan_active->EditValue)) {
	$arwrk = $plans->plan_active->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($plans->plan_active->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $plans->plan_active->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($plans->plan_active->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($plans->plan_active->CurrentValue) ?>" selected><?php echo $plans->plan_active->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $plans->plan_active->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_plans_plan_active" class="form-group plans_plan_active">
<span<?php echo $plans->plan_active->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $plans->plan_active->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="plans" data-field="x_plan_active" name="x<?php echo $plans_grid->RowIndex ?>_plan_active" id="x<?php echo $plans_grid->RowIndex ?>_plan_active" value="<?php echo ew_HtmlEncode($plans->plan_active->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="plans" data-field="x_plan_active" name="o<?php echo $plans_grid->RowIndex ?>_plan_active" id="o<?php echo $plans_grid->RowIndex ?>_plan_active" value="<?php echo ew_HtmlEncode($plans->plan_active->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$plans_grid->ListOptions->Render("body", "right", $plans_grid->RowCnt);
?>
<script type="text/javascript">
fplansgrid.UpdateOpts(<?php echo $plans_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($plans->CurrentMode == "add" || $plans->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $plans_grid->FormKeyCountName ?>" id="<?php echo $plans_grid->FormKeyCountName ?>" value="<?php echo $plans_grid->KeyCount ?>">
<?php echo $plans_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($plans->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $plans_grid->FormKeyCountName ?>" id="<?php echo $plans_grid->FormKeyCountName ?>" value="<?php echo $plans_grid->KeyCount ?>">
<?php echo $plans_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($plans->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="fplansgrid">
</div>
<?php

// Close recordset
if ($plans_grid->Recordset)
	$plans_grid->Recordset->Close();
?>
<?php if ($plans_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($plans_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($plans_grid->TotalRecs == 0 && $plans->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($plans_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($plans->Export == "") { ?>
<script type="text/javascript">
fplansgrid.Init();
</script>
<?php } ?>
<?php
$plans_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$plans_grid->Page_Terminate();
?>
