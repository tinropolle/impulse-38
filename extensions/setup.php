<?php
	// tasks code sort
	$files = array
	(
		"../tasksinfo.php",
		"../e_tasksinfo.php",
		"../e_tasks_financeinfo.php"
	);
	foreach ($files as $file)
	{
		$i = file_get_contents($file);
		$s = <<<'EOT'
$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
EOT;
		$r = <<<'EOT'
if (strpos($sSortField, "task_code") !== false)
{
$this->setSessionOrderBy(
	" SUBSTR(task_code, 1, POSITION('.' IN task_code) - 1) " . $sThisSort . ", " .
	" CAST(SUBSTR(task_code, POSITION('.' IN task_code) + 1, LENGTH(task_code) - POSITION('.' IN task_code)) AS UNSIGNED) " . $sThisSort); // Save to Session
}
else
{
$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
}
EOT;
		$o = str_replace($s, $r, $i);
		file_put_contents($file, $o);
	}

	// Periods render
	$files = array
	(
		"../workssrch.php"
	);
	foreach ($files as $file)
	{
		$i = file_get_contents($file);
		$s = '$lookuptblfilter = "`period_active` = 1";';
		$r = '$lookuptblfilter = ""; // "`period_active` = 1";';
		$o = str_replace($s, $r, $i);
		file_put_contents($file, $o);
	}

	// Works only for specified employees
	$files = array
	(
		"../worksadd.php"
	);
	foreach ($files as $file)
	{
		$i = file_get_contents($file);
		$s = <<<'EOT'
$sWhereWrk = "{filter}";
$works->work_task_id'
EOT;
		$r = <<<'EOT'
$sWhereWrk = "{filter} AND (`task_employee_id` IS NULL OR `task_employee_id` = '" . CurrentUserID() . "')";
$works->work_task_id'
EOT;
		$o = str_replace($s, $r, $i);
		file_put_contents($file, $o);
	}
?>
