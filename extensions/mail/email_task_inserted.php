<?php

// Email notification
$curatorID = ew_ExecuteScalar(
    "SELECT e.employee_id
    FROM tasks t
    INNER JOIN
        plans p ON p.plan_id = t.task_plan_id
    INNER JOIN
        employees e ON e.employee_id = p.plan_employee_id
    WHERE t.task_id = '{$rsnew["task_id"]}' ; ");

// If different user than resposible
if (intval(CurrentUserID()) != intval($rsnew["task_employee_id"]) ||
    intval(CurrentUserID()) != intval($rsnew["task_coordinator_id"]) ||
    intval(CurrentUserID()) != intval($curatorID))
{
    $form = array();

    $userID = CurrentUserID();
    $form["user"] = ew_ExecuteScalar(
        "SELECT CONCAT(e.employee_first_name, ' ', e.employee_last_name)
        FROM employees e
        WHERE e.employee_id = '$userID' ; ");

    $form["task_action"] = "Задача добавлена:";

    // Тема
    $form["new_project_name"] = ew_ExecuteScalar(
        "SELECT project_name FROM projects WHERE project_id = '{$rsnew["task_project_id"]}'");

    // План
    $form["new_plan_name"] = ew_ExecuteScalar(
        "SELECT CONCAT(plan_code, ', ', plan_name) FROM plans WHERE plan_id = '{$rsnew["task_plan_id"]}'");

    // Лаб.
    $form["new_lab_name"] = ew_ExecuteScalar(
        "SELECT lab_name FROM labs WHERE lab_id = '{$rsnew["task_lab_id"]}'");

    // № п.
    $form["task_code"] = ew_ExecuteScalar(
        "SELECT task_code FROM tasks WHERE task_id = '{$rsnew["task_id"]}'");

    // Содержание
    $form["new_task_name"] = $rsnew["task_name"];

    // Начало
    $form["new_task_from"] = substr($rsnew["task_from"], 0, 10);

    // Окончание
    $form["new_task_to"] = substr($rsnew["task_to"], 0, 10);

    // Исполнитель
    $form["new_task_employee_last_name"] = ew_ExecuteScalar(
        "SELECT employee_last_name FROM employees WHERE employee_id = '{$rsnew["task_employee_id"]}'");

    // Координатор
    $form["new_task_coordinator_last_name"] = ew_ExecuteScalar(
        "SELECT employee_last_name FROM employees WHERE employee_id = '{$rsnew["task_coordinator_id"]}'");

    // Ож.рез.
    $form["new_task_object"] = $rsnew["task_object"];

    // Статус
    $form["new_task_status_name"] = ew_ExecuteScalar(
        "SELECT task_status_name FROM task_statuses WHERE task_status_id = '{$rsnew["task_status_id"]}'");

    // Т-з план. (ч)
    $form["new_task_hours_planned"] = $rsnew["task_hours_planned"];

    // Пункт ОКП
    $form["new_task_description"] = $rsnew["task_description"];

    // Пункт ОКП
    $form["new_task_key"] = $rsnew["task_key"];

    $template = file_get_contents("extensions/mail/email_task_template_insert.html");

    $content = $template;
    foreach ($form as $key => $value)
    {
        $content = str_replace("{{" . $key . "}}", $value, $content);
    }

    if (intval(CurrentUserID()) != intval($rsnew["task_employee_id"]))
    {
        $email = ew_ExecuteScalar("SELECT employee_email FROM employees WHERE employee_id = '{$rsnew["task_employee_id"]}' ; ");
        if ($email)
        {
            $content = str_replace("{{task_url}}", GetTaskEditURL($rsnew["task_employee_id"]) . $rsnew["task_id"], $content);
            ew_SendEmail("Impulse-38 Pilot <div38pilot@gmail.com>", $email, "", "", "{$form["task_action"]} {$form["new_task_name"]}", $content, "html", "utf-8", "ssl");
        }
    }
    if (intval(CurrentUserID()) != intval($rsnew["task_coordinator_id"]) &&
        intval($rsnew["task_coordinator_id"]) != intval($rsnew["task_employee_id"]))
    {
        $email = ew_ExecuteScalar("SELECT employee_email FROM employees WHERE employee_id = '{$rsnew["task_coordinator_id"]}' ; ");
        if ($email)
        {
            $content = str_replace("{{task_url}}", GetTaskEditURL($rsnew["task_coordinator_id"]) . $rsnew["task_id"], $content);
            ew_SendEmail("Impulse-38 Pilot <div38pilot@gmail.com>", $email, "", "", "{$form["task_action"]} {$form["new_task_name"]}", $content, "html", "utf-8", "ssl");
        }
    }
    if (intval(CurrentUserID()) != intval($curatorID) &&
        intval($curatorID) != intval($rsnew["task_employee_id"]) &&
        intval($curatorID) != intval($rsnew["task_coordinator_id"]))
    {
        $email = ew_ExecuteScalar("SELECT employee_email FROM employees WHERE employee_id = '{$curatorID}' ; ");
        if ($email)
        {
            $content = str_replace("{{task_url}}", GetTaskEditURL($curatorID) . $rsnew["task_id"], $content);
            ew_SendEmail("Impulse-38 Pilot <div38pilot@gmail.com>", $email, "", "", "{$form["task_action"]} {$form["new_task_name"]}", $content, "html", "utf-8", "ssl");
        }
    }
}

function GetTaskEditURL($employeeID)
{
    $levelID = intval(ew_ExecuteScalar("SELECT employee_level_id FROM employees WHERE employee_id = '{$employeeID}' ; "));
    switch ($levelID) {
        case 4:
            return "http://192.168.38.240/e_tasks_financeedit.php?showdetail=&task_id=";
        case 2:
            return "http://192.168.38.240/tasksedit.php?showdetail=&task_id=";
        default:
            return "http://192.168.38.240/e_tasksedit.php?showdetail=&task_id=";
    }
}