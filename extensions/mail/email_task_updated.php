<?php

$cssChanged = "changed";

// Email notification
$curatorID = ew_ExecuteScalar(
    "SELECT e.employee_id
    FROM tasks t
    INNER JOIN
        plans p ON p.plan_id = t.task_plan_id
    INNER JOIN
        employees e ON e.employee_id = p.plan_employee_id
    WHERE t.task_id = '{$rsold["task_id"]}' ; ");

// If different user in at least one category - send a mail
if (intval(CurrentUserID()) != intval($rsnew["task_employee_id"]) ||
    intval(CurrentUserID()) != intval($rsnew["task_coordinator_id"]) ||
    intval(CurrentUserID()) != intval($curatorID))
{
    $changed = false;

    $form = array();

    $userID = CurrentUserID();
    $form["user"] = ew_ExecuteScalar(
        "SELECT CONCAT(e.employee_first_name, ' ', e.employee_last_name)
        FROM employees e
        WHERE e.employee_id = '$userID' ; ");

    $form["task_action"] = "Задача изменена:";

    // Тема
    $form["old_project_name"] = ew_ExecuteScalar(
        "SELECT project_name FROM projects WHERE project_id = '{$rsold["task_project_id"]}'");
    $form["new_project_name"] = ew_ExecuteScalar(
        "SELECT project_name FROM projects WHERE project_id = '{$rsnew["task_project_id"]}'");
    $form["project_name_attr"] = ($form["old_project_name"] !== $form["new_project_name"]) ? $cssChanged : "";

    // План
    $form["old_plan_name"] = ew_ExecuteScalar(
        "SELECT CONCAT(plan_code, ', ', plan_name) FROM plans WHERE plan_id = '{$rsold["task_plan_id"]}'");
    $form["new_plan_name"] = ew_ExecuteScalar(
        "SELECT CONCAT(plan_code, ', ', plan_name) FROM plans WHERE plan_id = '{$rsnew["task_plan_id"]}'");
    $form["plan_name_attr"] = ($form["old_plan_name"] !== $form["new_plan_name"]) ? $cssChanged : "";

    // Лаб.
    $form["old_lab_name"] = ew_ExecuteScalar(
        "SELECT lab_name FROM labs WHERE lab_id = '{$rsold["task_lab_id"]}'");
    $form["new_lab_name"] = ew_ExecuteScalar(
        "SELECT lab_name FROM labs WHERE lab_id = '{$rsnew["task_lab_id"]}'");
    $form["lab_name_attr"] = ($form["old_lab_name"] !== $form["new_lab_name"]) ? $cssChanged : "";

    // № п.
    $form["task_code"] = ew_ExecuteScalar(
        "SELECT task_code FROM tasks WHERE task_id = '{$rsold["task_id"]}'");

    // Содержание
    $form["old_task_name"] = $rsold["task_name"];
    $form["new_task_name"] = $rsnew["task_name"];
    $form["task_name_attr"] = ($form["old_task_name"] !== $form["new_task_name"]) ? $cssChanged : "";

    // Начало
    $form["old_task_from"] = substr($rsold["task_from"], 0, 10);
    $form["new_task_from"] = substr($rsnew["task_from"], 0, 10);
    $form["task_from_attr"] = ($form["old_task_from"] !== $form["new_task_from"]) ? $cssChanged : "";

    // Окончание
    $form["old_task_to"] = substr($rsold["task_to"], 0, 10);
    $form["new_task_to"] = substr($rsnew["task_to"], 0, 10);
    $form["task_to_attr"] = ($form["old_task_to"] !== $form["new_task_to"]) ? $cssChanged : "";

    // Исполнитель
    $form["old_task_employee_last_name"] = ew_ExecuteScalar(
        "SELECT employee_last_name FROM employees WHERE employee_id = '{$rsold["task_employee_id"]}'");
    $form["new_task_employee_last_name"] = ew_ExecuteScalar(
        "SELECT employee_last_name FROM employees WHERE employee_id = '{$rsnew["task_employee_id"]}'");
    $form["task_employee_last_name_attr"] = ($form["old_task_employee_last_name"] !== $form["new_task_employee_last_name"]) ? $cssChanged : "";

    // Координатор
    $form["old_task_coordinator_last_name"] = ew_ExecuteScalar(
        "SELECT employee_last_name FROM employees WHERE employee_id = '{$rsold["task_coordinator_id"]}'");
    $form["new_task_coordinator_last_name"] = ew_ExecuteScalar(
        "SELECT employee_last_name FROM employees WHERE employee_id = '{$rsnew["task_coordinator_id"]}'");
    $form["task_coordinator_last_name_attr"] = ($form["old_task_coordinator_last_name"] !== $form["new_task_coordinator_last_name"]) ? $cssChanged : "";

    // Ож.рез.
    $form["old_task_object"] = $rsold["task_object"];
    $form["new_task_object"] = $rsnew["task_object"];
    $form["task_object_attr"] = ($form["old_task_object"] !== $form["new_task_object"]) ? $cssChanged : "";

    // Статус
    $form["old_task_status_name"] = ew_ExecuteScalar(
        "SELECT task_status_name FROM task_statuses WHERE task_status_id = '{$rsold["task_status_id"]}'");
    $form["new_task_status_name"] = ew_ExecuteScalar(
        "SELECT task_status_name FROM task_statuses WHERE task_status_id = '{$rsnew["task_status_id"]}'");
    $form["task_status_name_attr"] = ($form["old_task_status_name"] !== $form["new_task_status_name"]) ? $cssChanged : "";

    // Т-з план. (ч)
    $form["old_task_hours_planned"] = $rsold["task_hours_planned"];
    $form["new_task_hours_planned"] = $rsnew["task_hours_planned"];
    $form["task_hours_planned_attr"] = ($form["old_task_hours_planned"] !== $form["new_task_hours_planned"]) ? $cssChanged : "";

    // Пункт ОКП
    $form["old_task_description"] = $rsold["task_description"];
    $form["new_task_description"] = $rsnew["task_description"];
    $form["task_description_attr"] = ($form["old_task_description"] !== $form["new_task_description"]) ? $cssChanged : "";

    // Пункт ОКП
    $form["old_task_key"] = $rsold["task_key"];
    $form["new_task_key"] = $rsnew["task_key"];
    $form["task_key_attr"] = ($form["old_task_key"] !== $form["new_task_key"]) ? $cssChanged : "";

    $template = file_get_contents("extensions/mail/email_task_template_update.html");

    // Change "changed" flag
    foreach ($form as $key => $value)
    {
        if (strpos($key, "attr") >= 0 && $value === $cssChanged)
        {
            $changed = true;
        }
    }

    // Fill
    $content = $template;
    foreach ($form as $key => $value)
    {
        $content = str_replace("{{" . $key . "}}", $value, $content);
    }

    if ($changed &&
        intval(CurrentUserID()) != intval($rsnew["task_employee_id"]))
    {
        $email = ew_ExecuteScalar("SELECT employee_email FROM employees WHERE employee_id = '{$rsnew["task_employee_id"]}' ; ");
        if ($email)
        {
            $content = str_replace("{{task_url}}", GetTaskEditURL($rsnew["task_employee_id"]) . $rsold["task_id"], $content);
            ew_SendEmail("Impulse-38 Pilot <div38pilot@gmail.com>", $email, "", "", "{$form["task_action"]} {$form["new_task_name"]}", $content, "html", "utf-8", "ssl");
        }
    }
    if ($changed &&
        intval(CurrentUserID()) != intval($rsnew["task_coordinator_id"]) &&
        intval($rsnew["task_coordinator_id"]) != intval($rsnew["task_employee_id"]))
    {
        $email = ew_ExecuteScalar("SELECT employee_email FROM employees WHERE employee_id = '{$rsnew["task_coordinator_id"]}' ; ");
        if ($email)
        {
            $content = str_replace("{{task_url}}", GetTaskEditURL($rsnew["task_coordinator_id"]) . $rsold["task_id"], $content);
            ew_SendEmail("Impulse-38 Pilot <div38pilot@gmail.com>", $email, "", "", "{$form["task_action"]} {$form["new_task_name"]}", $content, "html", "utf-8", "ssl");
        }
    }
    if ($changed &&
        intval(CurrentUserID()) != intval($curatorID) &&
        intval($curatorID) != intval($rsnew["task_employee_id"]) &&
        intval($curatorID) != intval($rsnew["task_coordinator_id"]))
    {
        $email = ew_ExecuteScalar("SELECT employee_email FROM employees WHERE employee_id = '{$curatorID}' ; ");
        if ($email)
        {
            $content = str_replace("{{task_url}}", GetTaskEditURL($curatorID) . $rsold["task_id"], $content);
            ew_SendEmail("Impulse-38 Pilot <div38pilot@gmail.com>", $email, "", "", "{$form["task_action"]} {$form["new_task_name"]}", $content, "html", "utf-8", "ssl");
        }
    }
}

function GetTaskEditURL($employeeID)
{
    $levelID = intval(ew_ExecuteScalar("SELECT employee_level_id FROM employees WHERE employee_id = '{$employeeID}' ; "));
    switch ($levelID) {
        case 4:
            return "http://192.168.38.240/e_tasks_financeedit.php?showdetail=&task_id=";
        case 2:
            return "http://192.168.38.240/tasksedit.php?showdetail=&task_id=";
        default:
            return "http://192.168.38.240/e_tasksedit.php?showdetail=&task_id=";
    }
}