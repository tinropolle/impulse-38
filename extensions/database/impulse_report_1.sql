ALTER TABLE works ADD COLUMN work_lab_id INTEGER DEFAULT NULL AFTER work_period_id;
ALTER TABLE works ADD CONSTRAINT fk_work_lab_id FOREIGN KEY (work_lab_id) REFERENCES labs (lab_id);
ALTER TABLE works MODIFY work_time DOUBLE NOT NULL;