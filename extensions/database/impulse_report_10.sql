drop view v_employees;

CREATE ALGORITHM = UNDEFINED VIEW `v_employees` AS
SELECT
  employees.employee_level_id AS employee_level_id,
  employees.employee_first_name AS employee_first_name,
  employees.employee_last_name AS employee_last_name,
  employees.employee_telephone AS employee_telephone,
  employees.employee_lab_id AS employee_lab_id,
  labs.lab_name AS lab_name,
  positions.position_name AS position_name,
  employees.employee_id AS employee_id
FROM (employees
  JOIN labs ON labs.lab_id = employees.employee_lab_id)
  JOIN positions ON positions.position_id = employees.employee_position_id;