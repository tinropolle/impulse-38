ALTER TABLE `tasks`
	ADD COLUMN `task_project_id` INT(11) NOT NULL AFTER `task_id`;

UPDATE tasks t SET t.task_project_id = (SELECT plan_project_id FROM plans WHERE plans.plan_id = t.task_plan_id);

ALTER TABLE `tasks`
	ADD CONSTRAINT `fk_task_project_id` FOREIGN KEY (`task_project_id`) REFERENCES `projects` (`project_id`);
