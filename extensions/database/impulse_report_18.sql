CREATE TABLE config
(
	config_id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	config_name VARCHAR(255) NOT NULL,
	config_value VARCHAR(255)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

INSERT INTO config VALUES (NULL, 'on_maintenance', 1);