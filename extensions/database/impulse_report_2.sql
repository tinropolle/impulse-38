CREATE TABLE main_fund_invoices (
    main_fund_invoice_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    main_fund_invoice_description VARCHAR(255) NOT NULL,
    main_fund_invoice_amount DECIMAL(13,2) NOT NULL,
    main_fund_invoice_dt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;