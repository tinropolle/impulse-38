ALTER TABLE `employees`
	ADD COLUMN `employee_salary` INT NOT NULL AFTER `employee_position_id`;

CREATE ALGORITHM = UNDEFINED VIEW `e_employees` AS
SELECT employees.employee_id AS employee_id,
  employees.employee_login AS employee_login,
  employees.employee_level_id AS employee_level_id,
  employees.employee_first_name AS employee_first_name,
  employees.employee_last_name AS employee_last_name,
  employees.employee_telephone AS employee_telephone,
  employees.employee_lab_id AS employee_lab_id,
  employees.employee_position_id AS employee_position_id,
  employees.employee_salary AS employee_salary
FROM employees;

ALTER TABLE `tasks`
	ADD COLUMN `task_cof_planned` DOUBLE NOT NULL DEFAULT '0' AFTER `task_file`,
	ADD COLUMN `task_cof_actual` DOUBLE NOT NULL DEFAULT '0' AFTER `task_cof_planned`;

CREATE ALGORITHM = UNDEFINED VIEW `e_tasks_finance` AS
SELECT tasks.task_project_id AS task_project_id,
  tasks.task_id AS task_id,
  tasks.task_plan_id AS task_plan_id,
  tasks.task_lab_id AS task_lab_id,
  tasks.task_code AS task_code,
  tasks.task_name AS task_name,
  tasks.task_from AS task_from,
  tasks.task_to AS task_to,
  tasks.task_employee_id AS task_employee_id,
  tasks.task_coordinator_id AS task_coordinator_id,
  tasks.task_object AS task_object,
  tasks.task_status_id AS task_status_id,
  tasks.task_hours_planned AS task_hours_planned,
  tasks.task_description AS task_description,
  tasks.task_key AS task_key,
  tasks.task_file AS task_file,
  tasks.task_cof_planned AS task_cof_planned,
  tasks.task_cof_actual AS task_cof_actual
FROM tasks