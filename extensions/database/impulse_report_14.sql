ALTER TABLE `tasks`
	ADD COLUMN `task_hours_actual` DOUBLE NOT NULL DEFAULT '0' AFTER `task_hours_planned`,
	ADD COLUMN `task_money_planned` DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER `task_cof_actual`,
	ADD COLUMN `task_money_actual` DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER `task_money_planned`;

DROP VIEW e_tasks;
DROP VIEW e_tasks_finance;

CREATE ALGORITHM = UNDEFINED VIEW `e_tasks` AS
SELECT
  tasks.task_id AS task_id,
  tasks.task_project_id AS task_project_id,
  tasks.task_plan_id AS task_plan_id,
  tasks.task_lab_id AS task_lab_id,
  tasks.task_code AS task_code,
  tasks.task_name AS task_name,
  tasks.task_from AS task_from,
  tasks.task_to AS task_to,
  tasks.task_employee_id AS task_employee_id,
  tasks.task_coordinator_id AS task_coordinator_id,
  tasks.task_object AS task_object,
  tasks.task_status_id AS task_status_id,
  tasks.task_hours_planned AS task_hours_planned,
  tasks.task_description AS task_description,
  tasks.task_key AS task_key,
  tasks.task_hours_actual AS task_hours_actual
FROM tasks;

CREATE ALGORITHM = UNDEFINED VIEW `e_tasks_finance` AS
SELECT tasks.task_project_id AS task_project_id,
  tasks.task_id AS task_id,
  tasks.task_plan_id AS task_plan_id,
  tasks.task_lab_id AS task_lab_id,
  tasks.task_code AS task_code,
  tasks.task_name AS task_name,
  tasks.task_from AS task_from,
  tasks.task_to AS task_to,
  tasks.task_employee_id AS task_employee_id,
  tasks.task_coordinator_id AS task_coordinator_id,
  tasks.task_object AS task_object,
  tasks.task_status_id AS task_status_id,
  tasks.task_hours_planned AS task_hours_planned,
  tasks.task_description AS task_description,
  tasks.task_key AS task_key,
  tasks.task_file AS task_file,
  tasks.task_cof_planned AS task_cof_planned,
  tasks.task_cof_actual AS task_cof_actual,
  tasks.task_hours_actual AS task_hours_actual,
  tasks.task_money_planned AS task_money_planned,
  tasks.task_money_actual AS task_money_actual
FROM tasks;