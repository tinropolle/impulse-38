ALTER TABLE `works`
	ALTER `work_description` DROP DEFAULT;
ALTER TABLE `works`
	CHANGE COLUMN `work_description` `work_description` VARCHAR(255) NULL AFTER `work_task_id`;
