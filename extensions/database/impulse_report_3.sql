ALTER TABLE `plans`
	DROP COLUMN `plan_lab_id`,
	DROP FOREIGN KEY `fk_plan_lab_id`;

ALTER TABLE `plans`
	ALTER `plan_employee_id` DROP DEFAULT;
ALTER TABLE `plans`
	CHANGE COLUMN `plan_employee_id` `plan_employee_id` INT(11) NULL AFTER `plan_name`;

ALTER TABLE `tasks`
	CHANGE COLUMN `task_from` `task_from` DATETIME NULL DEFAULT NULL AFTER `task_description`,
	CHANGE COLUMN `task_to` `task_to` DATETIME NULL DEFAULT NULL AFTER `task_from`;

ALTER TABLE `tasks`
	ADD COLUMN `task_hours_planned` DOUBLE NOT NULL DEFAULT '0' AFTER `task_status_id`;

ALTER TABLE `tasks`
	ADD COLUMN `task_lab_id` INT(11) NOT NULL AFTER `task_id`;

ALTER TABLE `tasks`
	ADD CONSTRAINT `fk_task_lab_id` FOREIGN KEY (`task_lab_id`) REFERENCES `labs` (`lab_id`);

/* Авто-набор пунктов */
ALTER TABLE `tasks`
	ALTER `task_code` DROP DEFAULT;
ALTER TABLE `tasks`
	CHANGE COLUMN `task_code` `task_code` INT NOT NULL AFTER `task_lab_id`;
ALTER TABLE `tasks`
	ADD UNIQUE INDEX `task_code_task_plan_id` (`task_code`, `task_plan_id`);

ALTER TABLE `works`
	DROP FOREIGN KEY `fk_work_lab_id`;
ALTER TABLE `works`
	DROP COLUMN `work_lab_id`;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE `resps`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

ALTER TABLE `tasks`
	ADD COLUMN `task_employee_id` INT NULL DEFAULT NULL AFTER `task_to`,
	ADD COLUMN `task_coordinator_id` INT NULL DEFAULT NULL AFTER `task_employee_id`;

ALTER TABLE `tasks`
	ADD CONSTRAINT `fk_task_employee_id` FOREIGN KEY (`task_employee_id`) REFERENCES `employees` (`employee_id`),
	ADD CONSTRAINT `fk_task_coordinator_id` FOREIGN KEY (`task_coordinator_id`) REFERENCES `employees` (`employee_id`);

ALTER TABLE `tasks`
	ADD COLUMN `task_key` VARCHAR(255) NULL DEFAULT NULL AFTER `task_object`;

ALTER TABLE `plans`
	ADD COLUMN `plan_code` INT(11) NOT NULL AFTER `plan_id`;

ALTER TABLE `plans`
	ADD UNIQUE INDEX `plan_code_plan_project_id` (`plan_code`, `plan_project_id`);

ALTER TABLE `tasks`
	CHANGE COLUMN `task_code` `task_code` INT(11) NOT NULL DEFAULT '0' AFTER `task_lab_id`;

CREATE ALGORITHM = UNDEFINED VIEW `v_works` AS
select `works`.`work_id` AS `work_id`,`works`.`work_period_id` AS `work_period_id`,`works`.`work_project_id` AS `work_project_id`,`works`.`work_plan_id` AS `work_plan_id`,`works`.`work_task_id` AS `work_task_id`,`works`.`work_description` AS `work_description`,`works`.`work_progress` AS `work_progress`,`works`.`work_time` AS `work_time`,`works`.`work_employee_id` AS `work_employee_id`,`works`.`work_started` AS `work_started` from `works`
