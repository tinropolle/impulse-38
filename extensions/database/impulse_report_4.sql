ALTER TABLE `works`
	ADD COLUMN `work_lab_id` INT(11) NULL DEFAULT NULL AFTER `work_plan_id`;
ALTER TABLE `works`
	ADD CONSTRAINT `fk_work_lab_id` FOREIGN KEY (`work_lab_id`) REFERENCES `labs` (`lab_id`);
ALTER ALGORITHM = UNDEFINED DEFINER=`imprprt`@`localhost` VIEW `v_works` AS select `works`.`work_id` AS `work_id`,`works`.`work_period_id` AS `work_period_id`,`works`.`work_project_id` AS `work_project_id`,`works`.`work_plan_id` AS `work_plan_id`,`works`.`work_lab_id` AS `work_lab_id`,`works`.`work_task_id` AS `work_task_id`,`works`.`work_description` AS `work_description`,`works`.`work_progress` AS `work_progress`,`works`.`work_time` AS `work_time`,`works`.`work_employee_id` AS `work_employee_id`,`works`.`work_started` AS `work_started` from `works` ;

ALTER TABLE `tasks`
	CHANGE COLUMN `task_description` `task_description` VARCHAR(1000) NULL DEFAULT NULL;