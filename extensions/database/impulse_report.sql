CREATE DATABASE impulse_reports CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'imprprt'@'localhost' IDENTIFIED BY 'Zx8Fts4K';

GRANT ALL PRIVILEGES ON impulse_reports . * TO 'imprprt'@'localhost';

FLUSH PRIVILEGES;

USE impulse_reports;

CREATE TABLE labs (
    lab_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    lab_name VARCHAR(255) NOT NULL,
    lab_description VARCHAR(255) NOT NULL,
    UNIQUE KEY (lab_name)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE positions (
    position_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    position_name VARCHAR(255) NOT NULL,
    UNIQUE KEY (position_name)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE employees (
    employee_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    employee_login VARCHAR(255) NOT NULL,
    employee_password VARCHAR(255) NOT NULL,
    employee_level_id INT NOT NULL DEFAULT 5,
    employee_first_name VARCHAR(255),
    employee_last_name VARCHAR(255),
    employee_telephone VARCHAR(255),
    employee_lab_id INT NOT NULL,
    employee_position_id INT NOT NULL,
    UNIQUE KEY (employee_login),
    FOREIGN KEY (employee_lab_id) REFERENCES labs(lab_id),
    FOREIGN KEY (employee_position_id) REFERENCES positions(position_id)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE projects (
    project_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    project_name VARCHAR(255) NOT NULL,
    UNIQUE KEY (project_name)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE plans (
    plan_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    plan_project_id INT NOT NULL,
    plan_name VARCHAR(255) NOT NULL,
    plan_employee_id INT NOT NULL,
    plan_active INT NOT NULL DEFAULT 1,
    UNIQUE KEY (plan_name),
    FOREIGN KEY (plan_project_id) REFERENCES projects(project_id),
    FOREIGN KEY (plan_employee_id) REFERENCES employees(employee_id)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE tasks (
    task_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    task_code VARCHAR(255) NOT NULL,
    task_plan_id INT NOT NULL,
    task_name VARCHAR(255) NOT NULL,
    task_description VARCHAR(255),
    task_from DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    task_to DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (task_plan_id) REFERENCES plans(plan_id)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE resps (
    resp_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    task_id INT NOT NULL,
    employee_id INT NOT NULL,
    FOREIGN KEY (task_id) REFERENCES tasks(task_id),
    FOREIGN KEY (employee_id) REFERENCES employees(employee_id)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE periods (
    period_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    period_name VARCHAR(255) NOT NULL,
    period_from DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    period_to DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE KEY (period_name)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE TABLE works (
    work_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    work_period_id INT NOT NULL,
    work_project_id INT,
    work_plan_id INT,
    work_task_id INT,
    work_description VARCHAR(255) NOT NULL,
    work_progress INT NOT NULL,
    work_time INT NOT NULL,
    work_employee_id INT NOT NULL,
    work_started DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (work_period_id) REFERENCES periods(period_id),
    FOREIGN KEY (work_project_id) REFERENCES projects(project_id),
    FOREIGN KEY (work_plan_id) REFERENCES plans(plan_id),
    FOREIGN KEY (work_task_id) REFERENCES tasks(task_id),
    FOREIGN KEY (work_employee_id) REFERENCES employees(employee_id)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

CREATE ALGORITHM = UNDEFINED VIEW `reports` AS
SELECT DISTINCT e.employee_id, e.employee_last_name, e.employee_first_name, positions.position_name, periods.period_id, periods.period_name
FROM periods
INNER JOIN works ON works.work_period_id = periods.period_id
INNER JOIN employees e ON e.employee_id = works.work_employee_id
INNER JOIN positions ON positions.position_id = e.employee_position_id;

CREATE ALGORITHM = UNDEFINED VIEW `report_works` AS
SELECT
    periods.period_from AS period_from,
    periods.period_id AS period_id,
    projects.project_name AS project_name,
    tasks.task_code AS task_code,
    tasks.task_name AS task_name,
	works.work_time AS work_time,
    works.work_progress AS work_progress,
    works.work_description AS work_description,
    works.work_employee_id AS work_employee_id
FROM periods
INNER JOIN works ON works.work_period_id = periods.period_id
LEFT JOIN tasks ON tasks.task_id = works.work_task_id
LEFT JOIN plans ON plans.plan_id = works.work_plan_id
LEFT JOIN projects ON projects.project_id = works.work_project_id
WHERE works.work_task_id IS NOT NULL
UNION
SELECT
    periods.period_from AS period_from,
    periods.period_id AS period_id,
    projects.project_name AS project_name,
    NULL AS task_code,
    works.work_description AS task_name,
	works.work_time AS work_time,
    works.work_progress AS work_progress,
    NULL AS work_description,
    works.work_employee_id AS work_employee_id
FROM periods
INNER JOIN works ON works.work_period_id = periods.period_id
LEFT JOIN plans ON plans.plan_id = works.work_plan_id
LEFT JOIN projects ON projects.project_id = works.work_project_id
WHERE works.work_task_id IS NULL
UNION
SELECT
    periods.period_from AS period_from,
    periods.period_id AS period_id,
    CONCAT(projects.project_name, ' (всего)') AS project_name,
    NULL AS task_code,
    NULL AS task_name,
	CONCAT(SUM(works.work_time), ' (', ROUND(100 * SUM(works.work_time) / (
		SELECT SUM(w1.work_time)
		FROM works w1
		WHERE w1.work_employee_id = works.work_employee_id
		  AND w1.work_period_id = works.work_period_id
	)), ' %)') AS work_time,
    NULL AS work_progress,
    NULL AS work_description,
    works.work_employee_id AS work_employee_id
FROM periods
INNER JOIN works ON works.work_period_id = periods.period_id
LEFT JOIN tasks ON tasks.task_id = works.work_task_id
LEFT JOIN plans ON plans.plan_id = works.work_plan_id
LEFT JOIN projects ON projects.project_id = works.work_project_id
GROUP BY works.work_employee_id, periods.period_id, projects.project_name
ORDER BY 9 ASC, 1 DESC, 3 ASC, 4 ASC;

/*
INSERT INTO positions (position_name) VALUES ('Инженер по компьютерным системам 1 категории');					
INSERT INTO positions (position_name) VALUES ('Инженер по компьютерным системам 2 категории');					
INSERT INTO positions (position_name) VALUES ('Инженер-программист 1 категории');					
INSERT INTO positions (position_name) VALUES ('Инженер-программист 2 категории');					
INSERT INTO positions (position_name) VALUES ('Зав. лабораторией');					
INSERT INTO positions (position_name) VALUES ('Зав. отделом');					
INSERT INTO positions (position_name) VALUES ('Заместитель зав. отделом');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер по компьютерным системам');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер по подсистеме автоматической настройки (ПАН)');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер по подсистеме контроля и регистрации (ПКР)');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер по подсистеме обслуживания защит и блокировок (ПОЗБ)');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер по подсистеме прикладных задач (ППЗ)');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер по подсистеме поддержки функционирования');					
INSERT INTO positions (position_name) VALUES ('Ведущий инженер-программист');					
INSERT INTO positions (position_name) VALUES ('Делопрозводитель');

INSERT INTO labs (lab_name, lab_description) VALUES ('38-0', 'Отдел разработки информационных вычислительных систем');
INSERT INTO labs (lab_name, lab_description) VALUES ('38-1', 'Лаборатория разработки и сопровождения программного обеспечения функциональных подсистем АСУ ТП');
INSERT INTO labs (lab_name, lab_description) VALUES ('38-2', 'Лаборатория разработки и внедрения общесистемного программного обеспечения, тестирования и наладки');
INSERT INTO labs (lab_name, lab_description) VALUES ('38-3', 'Лаборатория информационной поддержки АСУ ТП, функционального тестирования и наладки');
*/

ALTER TABLE plans ADD COLUMN plan_lab_id INT DEFAULT NULL;

ALTER TABLE plans ADD CONSTRAINT fk_plan_lab_id FOREIGN KEY (plan_lab_id) REFERENCES labs (lab_id);

ALTER TABLE plans DROP INDEX plan_name;

ALTER TABLE tasks MODIFY `task_from` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE tasks MODIFY `task_to` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;

CREATE TABLE task_statuses (
    task_status_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    task_status_name VARCHAR(255) NOT NULL,
    UNIQUE KEY (task_status_name)
)
COLLATE=utf8_general_ci
ENGINE=InnoDB;

ALTER TABLE tasks ADD COLUMN task_status_id INT DEFAULT NULL;

ALTER TABLE tasks ADD CONSTRAINT fk_task_status_id FOREIGN KEY (task_status_id) REFERENCES task_statuses (task_status_id);

ALTER TABLE tasks ADD COLUMN task_object VARCHAR(255) DEFAULT NULL;

CREATE ALGORITHM = UNDEFINED VIEW `project_tasks` AS
select
    `tasks`.`task_id` AS `task_id`,
    `tasks`.`task_code` AS `task_code`,
    `tasks`.`task_name` AS `task_name`,
    `tasks`.`task_description` AS `task_description`,
    `tasks`.`task_from` AS `task_from`,
    `tasks`.`task_to` AS `task_to`,
    `tasks`.`task_object` AS `task_object`,
    `plans`.`plan_active` AS `plan_active`,
    `plans`.`plan_lab_id` AS `plan_lab_id`,
    `projects`.`project_name` AS `project_name`,
    `plans`.`plan_name` AS `plan_name`,
    `task_statuses`.`task_status_name` AS `task_status_name`,
    `tasks`.`task_status_id` AS `task_status_id`,
    `tasks`.`task_plan_id` AS `task_plan_id`,
    `plans`.`plan_project_id` AS `plan_project_id`,
    `plans`.`plan_employee_id` AS `plan_employee_id`,
    `labs`.`lab_name` AS `lab_name`
from ((((`tasks`
    join `plans` on((`plans`.`plan_id` = `tasks`.`task_plan_id`)))
    join `projects` on((`projects`.`project_id` = `plans`.`plan_project_id`)))
    left join `task_statuses` on((`task_statuses`.`task_status_id` = `tasks`.`task_status_id`)))
    left join `labs` on((`plans`.`plan_lab_id` = `labs`.`lab_id`)));

CREATE ALGORITHM = UNDEFINED VIEW `v_employees` AS
select
    `employees`.`employee_first_name` AS `employee_first_name`,
    `employees`.`employee_last_name` AS `employee_last_name`,
    `employees`.`employee_telephone` AS `employee_telephone`,
    `labs`.`lab_name` AS `lab_name`,`positions`.`position_name` AS `position_name`,
    `employees`.`employee_id` AS `employee_id`
from ((`employees`
    join `labs` on((`labs`.`lab_id` = `employees`.`employee_lab_id`)))
    join `positions` on((`positions`.`position_id` = `employees`.`employee_position_id`)));

DROP VIEW `project_tasks`;

CREATE ALGORITHM = UNDEFINED VIEW `project_tasks` AS
select
    `tasks`.`task_id` AS `task_id`,
    `tasks`.`task_code` AS `task_code`,
    `tasks`.`task_name` AS `task_name`,
    `tasks`.`task_description` AS `task_description`,
    `tasks`.`task_from` AS `task_from`,
    `tasks`.`task_to` AS `task_to`,
    `tasks`.`task_object` AS `task_object`,
    `plans`.`plan_active` AS `plan_active`,
    `plans`.`plan_lab_id` AS `plan_lab_id`,
    `projects`.`project_name` AS `project_name`,
    `plans`.`plan_name` AS `plan_name`,
    `tasks`.`task_status_id` AS `task_status_id`,
    `tasks`.`task_plan_id` AS `task_plan_id`,
    `plans`.`plan_project_id` AS `plan_project_id`,
    `plans`.`plan_employee_id` AS `plan_employee_id`
from ((`tasks`
    join `plans` on((`plans`.`plan_id` = `tasks`.`task_plan_id`)))
    join `projects` on((`projects`.`project_id` = `plans`.`plan_project_id`)));

CREATE TABLE `audit_trail` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`dt` DATETIME NOT NULL,
	`script` VARCHAR(255) NULL DEFAULT NULL,
	`user` VARCHAR(255) NULL DEFAULT NULL,
	`action` VARCHAR(255) NULL DEFAULT NULL,
	`table` VARCHAR(255) NULL DEFAULT NULL,
	`field` VARCHAR(255) NULL DEFAULT NULL,
	`key_value` LONGTEXT NULL,
	`old_value` LONGTEXT NULL,
	`new_value` LONGTEXT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

ALTER TABLE tasks MODIFY `task_name` VARCHAR(1000) NOT NULL;

ALTER TABLE tasks MODIFY `task_from` VARCHAR(255) NULL;

ALTER TABLE tasks MODIFY `task_to` VARCHAR(255) NULL;
