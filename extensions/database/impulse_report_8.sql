ALTER TABLE `tasks`
	CHANGE COLUMN `task_code` `task_code_old` INT(11) NOT NULL DEFAULT '0' AFTER `task_lab_id`;

ALTER TABLE `tasks`
	ADD COLUMN `task_code` VARCHAR(255) NOT NULL AFTER `task_lab_id`;

UPDATE tasks t JOIN plans p ON p.plan_id = t.task_plan_id SET t.task_code = CONCAT(p.plan_code, '.', t.task_code_old);

ALTER TABLE `tasks`
	ADD UNIQUE INDEX `uk_task_project_id_task_code` (`task_project_id`, `task_code`);

ALTER TABLE `tasks`
	DROP INDEX `task_code_task_plan_id`;

ALTER TABLE `plans`
	ADD COLUMN `plan_task_index` INT(11) NOT NULL DEFAULT '1' AFTER `plan_active`;

UPDATE plans p SET p.plan_task_index = (SELECT MAX(task_code_old) + 1 FROM tasks WHERE task_plan_id = p.plan_id);

ALTER TABLE `tasks`
	DROP COLUMN `task_code_old`;

drop view report_works;

CREATE ALGORITHM = UNDEFINED VIEW `report_works` AS
SELECT
  periods.period_from AS period_from,
  periods.period_id AS period_id,
  projects.project_name AS project_name,
  tasks.task_code AS task_code,
  tasks.task_name AS task_name,
  works.work_time AS work_time,
  works.work_progress AS work_progress,
  works.work_description AS work_description,
  works.work_employee_id AS work_employee_id
FROM (((periods
  JOIN works ON works.work_period_id = periods.period_id)
  LEFT JOIN tasks ON tasks.task_id = works.work_task_id)
  LEFT JOIN plans ON plans.plan_id = works.work_plan_id)
  LEFT JOIN projects ON projects.project_id = works.work_project_id
WHERE works.work_task_id IS NOT NULL
UNION
SELECT
  periods.period_from AS period_from,
  periods.period_id AS period_id,
  CONCAT(projects.project_name, ' (всего)') AS project_name,
  NULL AS task_code,
  NULL AS task_name,
  CONCAT(SUM(works.work_time), ' (', ROUND(((100 * SUM(works.work_time)) /
  (SELECT SUM(w1.work_time) FROM works w1
  WHERE w1.work_employee_id = works.work_employee_id AND w1.work_period_id = works.work_period_id)), 0), ' %)') AS work_time,
  NULL AS work_progress,
  NULL AS work_description,
  works.work_employee_id AS work_employee_id
FROM (((periods
  JOIN works ON works.work_period_id = periods.period_id)
  LEFT JOIN tasks ON tasks.task_id = works.work_task_id)
  LEFT JOIN plans ON plans.plan_id = works.work_plan_id)
  LEFT JOIN projects ON projects.project_id = works.work_project_id
WHERE works.work_task_id IS NOT NULL
GROUP BY
  periods.period_id,
  works.work_employee_id,
  projects.project_name
ORDER BY work_employee_id,
  period_from DESC,
  project_name,
  task_code;

ALTER TABLE `tasks`
	CHANGE COLUMN `task_code` `task_code` VARCHAR(255) NOT NULL DEFAULT '0.0' AFTER `task_lab_id`;
