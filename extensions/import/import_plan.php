<?php
chdir("../../");
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "tasksgridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php
    $fileStruct = array
    (
        "lab_name",
        "task_number",
        "task_name",
        "task_from",
        "task_to",
        "task_employee_last_name",
        "task_coordinator_last_name",
        "task_object",
        "task_status",
        "task_hours_planned",
        "task_hours_calc",
        "task_description",
        "task_key"
    );

    $debug = 0;
?>
<form action="" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="file" id="fileNameLabel">Выберите файл для загрузки:</label>
    <input type="file" class="form-control-file" name="fileName" id="fileName">
    <br>
    <button type="submit" class="btn btn-default btn-sm">Загрузить план</button>
  </div>
</form>
<?php
    $messages = "";
    $lastSQL = "";

    $isFileOk = true;
    if (!$debug)
    {
        if (isset($_FILES["fileName"]) && isset($_FILES["fileName"]["name"]))
        {
            $targetFile = $_FILES["fileName"]["tmp_name"];

            $fileType = strtolower(pathinfo(basename($_FILES["fileName"]["name"]), PATHINFO_EXTENSION));

            // Allow certain file formats
            if ($fileType != "csv" && $fileType != "txt")
            {
                $messages .= "<span class=\"text-danger\">Файл должен быть в формате текста с разделителями (csv или txt)!</span><br>";
                $isFileOk = false;
            }
        }
        else
        {
            $isFileOk = false;
        }
    }
    else
    {
        $targetFile = "test.csv";
    }

    if ($isFileOk && file_exists($targetFile))
    {
        $db = new mysqli("192.168.38.230", "imprprt", "Zx8Fts4K", "impulse_reports");
        $db->autocommit(FALSE);

        // Open file
        $src = fopen($targetFile, "r");

        // Loop vars
        $projectID = "NULL";
        $projectName = 'Неизвестно';
        $planID = -1;
        $planCode = 1;
        $planName = "Без названия";
        $lineID = -1;

        // Loop
        while (($line = fgetcsv($src, 0, "|")) !== false)
        {
            $lineID++;

            // Skip header + plan info
            if ($lineID < 2)
            {
                if ($lineID == 0)
                {
                    if (trim($line[0])) $projectName = $line[0];
                    if (trim($line[1])) $planCode = $line[1];
                    if (trim($line[2])) $planName = str_replace("'", "''", $line[2]);

                    // Project ID
                    $Q = $db->query(
                        "SELECT project_id ID FROM projects WHERE project_name = '{$projectName}' ;");
                    if ($R = $Q->fetch_assoc()) $projectID = $R["ID"];

                    // Create plan
                    $db->query($lastSQL = 
                        "INSERT INTO plans (plan_project_id, plan_code, plan_name) VALUES ($projectID, $planCode, '$planName') ;");
                    CheckSQLError($db, $lastSQL);
                    $planID = $db->insert_id;
                    $messages .= "<span class=\"text-info\">План '{$planName}' добавлен</span><br>";
                }
                continue;
            }

            if ($projectID === "NULL")
            {
                echo "Имя проекта не найдено";
                exit;
            }

            // Get values
            $lineArray = array();
            for ($i = 0; $i < count($fileStruct); $i++)
            {
                if ($i < count($line))
                {
                    $lineArray[$fileStruct[$i]] = str_replace("'", "''", trim($line[$i]));
                }
            }
            
            // Get fields
            $taskName = (isset($lineArray["task_name"]) && trim($lineArray["task_name"])) ? "'{$lineArray["task_name"]}'" : "NULL";
            $taskFrom = ParseDate($lineArray["task_from"]);
            $taskTo = ParseDate($lineArray["task_to"]);
            $taskDesc = (isset($lineArray["task_description"]) && trim($lineArray["task_description"])) ? "'{$lineArray["task_description"]}'" : "NULL";
            $taskObject = (isset($lineArray["task_object"]) && trim($lineArray["task_object"])) ? "'{$lineArray["task_object"]}'" : "NULL";
            $taskHoursPlanned = (isset($lineArray["task_hours_planned"]) && trim($lineArray["task_hours_planned"])) ? "'{$lineArray["task_hours_planned"]}'" : "0";
            $taskHoursCalc = (isset($lineArray["task_hours_calc"]) && trim($lineArray["task_hours_calc"])) ? "'{$lineArray["task_hours_calc"]}'" : "0";
            $taskKey = (isset($lineArray["task_key"]) && trim($lineArray["task_key"])) ? "'{$lineArray["task_key"]}'" : "NULL";
            
            // Lab ID
            $labID = "NULL";
            $Q = $db->query(
                "SELECT lab_id ID FROM labs WHERE lab_name = '{$lineArray["lab_name"]}' ;");
            if ($R = $Q->fetch_assoc()) $labID = "'{$R["ID"]}'";

            // Resps
            $taskEmployees = array();
            preg_match_all("/([А-Яа-я]+)/u", $lineArray["task_employee_last_name"], $matches, PREG_SET_ORDER);
            foreach ($matches as $m)
            {
                $Q = $db->query(
                    "SELECT employee_id ID FROM employees WHERE employee_last_name = '{$m[1]}' ;");
                if ($R = $Q->fetch_assoc()) array_push($taskEmployees, "'{$R["ID"]}'");
            }
            if (count($taskEmployees) === 0)
            {
                array_push($taskEmployees, 'NULL');
            }

            // Coordinator ID
            $taskCoordinatorID = "NULL";
            $Q = $db->query(
                "SELECT employee_id ID FROM employees WHERE employee_last_name = '{$lineArray["task_coordinator_last_name"]}' ;");
            if ($R = $Q->fetch_assoc()) $taskCoordinatorID = "'{$R["ID"]}'";

            // Status ID
            $taskStatusID = "NULL";
            $taskStatus = strtolower($lineArray["task_status"]);
            $Q = $db->query(
                "SELECT task_status_id ID FROM task_statuses WHERE LOWER(task_status_name) = '{$taskStatus}' ;");
            if ($R = $Q->fetch_assoc()) $taskStatusID = "'{$R["ID"]}'";

            // Insert task
            foreach ($taskEmployees as $employeeID)
            {
                // Task code
                $Q = $db->query(
                    "SELECT plan_task_index AS ID FROM plans WHERE plan_id = '{$planID}' ;");
                $taskCode = (($R = $Q->fetch_assoc()) && $R["ID"]) ? $R["ID"] : "1";
                $taskCode = $planCode . "." . $taskCode;
                $Q = $db->query(
                    "UPDATE plans SET plan_task_index = plan_task_index + 1 WHERE plan_id = '{$planID}' ;");

                $db->query($lastSQL = 
                    "INSERT INTO tasks (
                        task_project_id,
                        task_plan_id,
                        task_lab_id,
                        task_code,
                        task_name,
                        task_from,
                        task_to,
                        task_employee_id,
                        task_coordinator_id,
                        task_object,
                        task_status_id,
                        task_hours_planned,
                        task_description,
                        task_key)
                    VALUES (
                        $projectID ,
                        $planID,
                        $labID,
                        '$taskCode',
                        $taskName,
                        $taskFrom,
                        $taskTo,
                        $employeeID,
                        $taskCoordinatorID,
                        $taskObject,
                        $taskStatusID,
                        $taskHoursPlanned,
                        $taskDesc,
                        $taskKey) ;");
                CheckSQLError($db, $lastSQL);
                $messages .= "<span class=\"text-info\">Пункт плана {$taskCode} добавлен</span><br>";
            }
        }

        $db->commit();

        // $db->rollback();

        $db->close();
    }
?>
<div style="border: 1px solid #DDDDDD; box-sizing: border-box; height: 400px; overflow: scroll; width: 100%; padding: 8px;">
<?php echo ($messages) ? $messages : "Выберите план для загрузки"; ?>
</div>
<?php

    function CheckSQLError($db, $lastSQL)
    {
        if ($db->errno !== 0)
        {
            echo "Ошибка запроса<br>{$db->error}<br>";
            echo $lastSQL;
            exit;
        }
    }

    function ParseDate($dateStr)
    {
        $dateRes = "NULL";
        if (($date = DateTime::createFromFormat("d-m-Y", $dateStr)) !== false)
        {
            $dateRes = "'" . $date->format("Y-m-d") . "'";
        }
        else
        if (($date = DateTime::createFromFormat("Y-m-d", $dateStr)) !== false)
        {
            $dateRes = "'" . $date->format("Y-m-d") . "'";
        }
        else
        if (($date = DateTime::createFromFormat("d.m.Y", $dateStr)) !== false)
        {
            $dateRes = "'" . $date->format("Y-m-d") . "'";
        }
        else
        if (($date = DateTime::createFromFormat("d.m.y", $dateStr)) !== false)
        {
            $dateRes = "'" . $date->format("Y-m-d") . "'";
        }
        else
        if (($date = DateTime::createFromFormat("d-m-y", $dateStr)) !== false)
        {
            $dateRes = "'" . $date->format("Y-m-d") . "'";
        }
        return $dateRes;
    }
?>