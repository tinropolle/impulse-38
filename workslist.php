<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "worksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "periodsinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "tasksinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "e_tasksinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "e_tasks_financeinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$works_list = NULL; // Initialize page object first

class cworks_list extends cworks {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'works';

	// Page object name
	var $PageObjName = 'works_list';

	// Grid form hidden field names
	var $FormName = 'fworkslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (works)
		if (!isset($GLOBALS["works"]) || get_class($GLOBALS["works"]) == "cworks") {
			$GLOBALS["works"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["works"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "worksadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "worksdelete.php";
		$this->MultiUpdateUrl = "worksupdate.php";

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (periods)
		if (!isset($GLOBALS['periods'])) $GLOBALS['periods'] = new cperiods();

		// Table object (projects)
		if (!isset($GLOBALS['projects'])) $GLOBALS['projects'] = new cprojects();

		// Table object (tasks)
		if (!isset($GLOBALS['tasks'])) $GLOBALS['tasks'] = new ctasks();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Table object (e_tasks)
		if (!isset($GLOBALS['e_tasks'])) $GLOBALS['e_tasks'] = new ce_tasks();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Table object (e_tasks_finance)
		if (!isset($GLOBALS['e_tasks_finance'])) $GLOBALS['e_tasks_finance'] = new ce_tasks_finance();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'works', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fworkslistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate();
			}
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Set up master detail parameters
		$this->SetUpMasterParms();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $works;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($works);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 100;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Set up records per page
			$this->SetUpDisplayRecs();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Restore filter list
			$this->RestoreFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 100; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records

		// Restore master/detail filter
		$this->DbMasterFilter = $this->GetMasterFilter(); // Restore master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Restore detail filter
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "periods") {
			global $periods;
			$rsmaster = $periods->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("periodslist.php"); // Return to master page
			} else {
				$periods->LoadListRowValues($rsmaster);
				$periods->RowType = EW_ROWTYPE_MASTER; // Master row
				$periods->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "projects") {
			global $projects;
			$rsmaster = $projects->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("projectslist.php"); // Return to master page
			} else {
				$projects->LoadListRowValues($rsmaster);
				$projects->RowType = EW_ROWTYPE_MASTER; // Master row
				$projects->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "tasks") {
			global $tasks;
			$rsmaster = $tasks->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("taskslist.php"); // Return to master page
			} else {
				$tasks->LoadListRowValues($rsmaster);
				$tasks->RowType = EW_ROWTYPE_MASTER; // Master row
				$tasks->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "v_employees") {
			global $v_employees;
			$rsmaster = $v_employees->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("v_employeeslist.php"); // Return to master page
			} else {
				$v_employees->LoadListRowValues($rsmaster);
				$v_employees->RowType = EW_ROWTYPE_MASTER; // Master row
				$v_employees->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_employees") {
			global $e_employees;
			$rsmaster = $e_employees->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("e_employeeslist.php"); // Return to master page
			} else {
				$e_employees->LoadListRowValues($rsmaster);
				$e_employees->RowType = EW_ROWTYPE_MASTER; // Master row
				$e_employees->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_tasks") {
			global $e_tasks;
			$rsmaster = $e_tasks->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("e_taskslist.php"); // Return to master page
			} else {
				$e_tasks->LoadListRowValues($rsmaster);
				$e_tasks->RowType = EW_ROWTYPE_MASTER; // Master row
				$e_tasks->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_tasks_finance") {
			global $e_tasks_finance;
			$rsmaster = $e_tasks_finance->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("e_tasks_financelist.php"); // Return to master page
			} else {
				$e_tasks_finance->LoadListRowValues($rsmaster);
				$e_tasks_finance->RowType = EW_ROWTYPE_MASTER; // Master row
				$e_tasks_finance->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	// Set up number of records displayed per page
	function SetUpDisplayRecs() {
		$sWrk = @$_GET[EW_TABLE_REC_PER_PAGE];
		if ($sWrk <> "") {
			if (is_numeric($sWrk)) {
				$this->DisplayRecs = intval($sWrk);
			} else {
				if (strtolower($sWrk) == "all") { // Display all records
					$this->DisplayRecs = -1;
				} else {
					$this->DisplayRecs = 100; // Non-numeric, load default
				}
			}
			$this->setRecordsPerPage($this->DisplayRecs); // Save to Session

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->work_id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->work_id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->work_id->AdvancedSearch->ToJSON(), ","); // Field work_id
		$sFilterList = ew_Concat($sFilterList, $this->work_period_id->AdvancedSearch->ToJSON(), ","); // Field work_period_id
		$sFilterList = ew_Concat($sFilterList, $this->work_project_id->AdvancedSearch->ToJSON(), ","); // Field work_project_id
		$sFilterList = ew_Concat($sFilterList, $this->work_plan_id->AdvancedSearch->ToJSON(), ","); // Field work_plan_id
		$sFilterList = ew_Concat($sFilterList, $this->work_lab_id->AdvancedSearch->ToJSON(), ","); // Field work_lab_id
		$sFilterList = ew_Concat($sFilterList, $this->work_task_id->AdvancedSearch->ToJSON(), ","); // Field work_task_id
		$sFilterList = ew_Concat($sFilterList, $this->work_description->AdvancedSearch->ToJSON(), ","); // Field work_description
		$sFilterList = ew_Concat($sFilterList, $this->work_progress->AdvancedSearch->ToJSON(), ","); // Field work_progress
		$sFilterList = ew_Concat($sFilterList, $this->work_time->AdvancedSearch->ToJSON(), ","); // Field work_time
		$sFilterList = ew_Concat($sFilterList, $this->work_employee_id->AdvancedSearch->ToJSON(), ","); // Field work_employee_id
		$sFilterList = ew_Concat($sFilterList, $this->work_started->AdvancedSearch->ToJSON(), ","); // Field work_started

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field work_id
		$this->work_id->AdvancedSearch->SearchValue = @$filter["x_work_id"];
		$this->work_id->AdvancedSearch->SearchOperator = @$filter["z_work_id"];
		$this->work_id->AdvancedSearch->SearchCondition = @$filter["v_work_id"];
		$this->work_id->AdvancedSearch->SearchValue2 = @$filter["y_work_id"];
		$this->work_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_id"];
		$this->work_id->AdvancedSearch->Save();

		// Field work_period_id
		$this->work_period_id->AdvancedSearch->SearchValue = @$filter["x_work_period_id"];
		$this->work_period_id->AdvancedSearch->SearchOperator = @$filter["z_work_period_id"];
		$this->work_period_id->AdvancedSearch->SearchCondition = @$filter["v_work_period_id"];
		$this->work_period_id->AdvancedSearch->SearchValue2 = @$filter["y_work_period_id"];
		$this->work_period_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_period_id"];
		$this->work_period_id->AdvancedSearch->Save();

		// Field work_project_id
		$this->work_project_id->AdvancedSearch->SearchValue = @$filter["x_work_project_id"];
		$this->work_project_id->AdvancedSearch->SearchOperator = @$filter["z_work_project_id"];
		$this->work_project_id->AdvancedSearch->SearchCondition = @$filter["v_work_project_id"];
		$this->work_project_id->AdvancedSearch->SearchValue2 = @$filter["y_work_project_id"];
		$this->work_project_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_project_id"];
		$this->work_project_id->AdvancedSearch->Save();

		// Field work_plan_id
		$this->work_plan_id->AdvancedSearch->SearchValue = @$filter["x_work_plan_id"];
		$this->work_plan_id->AdvancedSearch->SearchOperator = @$filter["z_work_plan_id"];
		$this->work_plan_id->AdvancedSearch->SearchCondition = @$filter["v_work_plan_id"];
		$this->work_plan_id->AdvancedSearch->SearchValue2 = @$filter["y_work_plan_id"];
		$this->work_plan_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_plan_id"];
		$this->work_plan_id->AdvancedSearch->Save();

		// Field work_lab_id
		$this->work_lab_id->AdvancedSearch->SearchValue = @$filter["x_work_lab_id"];
		$this->work_lab_id->AdvancedSearch->SearchOperator = @$filter["z_work_lab_id"];
		$this->work_lab_id->AdvancedSearch->SearchCondition = @$filter["v_work_lab_id"];
		$this->work_lab_id->AdvancedSearch->SearchValue2 = @$filter["y_work_lab_id"];
		$this->work_lab_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_lab_id"];
		$this->work_lab_id->AdvancedSearch->Save();

		// Field work_task_id
		$this->work_task_id->AdvancedSearch->SearchValue = @$filter["x_work_task_id"];
		$this->work_task_id->AdvancedSearch->SearchOperator = @$filter["z_work_task_id"];
		$this->work_task_id->AdvancedSearch->SearchCondition = @$filter["v_work_task_id"];
		$this->work_task_id->AdvancedSearch->SearchValue2 = @$filter["y_work_task_id"];
		$this->work_task_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_task_id"];
		$this->work_task_id->AdvancedSearch->Save();

		// Field work_description
		$this->work_description->AdvancedSearch->SearchValue = @$filter["x_work_description"];
		$this->work_description->AdvancedSearch->SearchOperator = @$filter["z_work_description"];
		$this->work_description->AdvancedSearch->SearchCondition = @$filter["v_work_description"];
		$this->work_description->AdvancedSearch->SearchValue2 = @$filter["y_work_description"];
		$this->work_description->AdvancedSearch->SearchOperator2 = @$filter["w_work_description"];
		$this->work_description->AdvancedSearch->Save();

		// Field work_progress
		$this->work_progress->AdvancedSearch->SearchValue = @$filter["x_work_progress"];
		$this->work_progress->AdvancedSearch->SearchOperator = @$filter["z_work_progress"];
		$this->work_progress->AdvancedSearch->SearchCondition = @$filter["v_work_progress"];
		$this->work_progress->AdvancedSearch->SearchValue2 = @$filter["y_work_progress"];
		$this->work_progress->AdvancedSearch->SearchOperator2 = @$filter["w_work_progress"];
		$this->work_progress->AdvancedSearch->Save();

		// Field work_time
		$this->work_time->AdvancedSearch->SearchValue = @$filter["x_work_time"];
		$this->work_time->AdvancedSearch->SearchOperator = @$filter["z_work_time"];
		$this->work_time->AdvancedSearch->SearchCondition = @$filter["v_work_time"];
		$this->work_time->AdvancedSearch->SearchValue2 = @$filter["y_work_time"];
		$this->work_time->AdvancedSearch->SearchOperator2 = @$filter["w_work_time"];
		$this->work_time->AdvancedSearch->Save();

		// Field work_employee_id
		$this->work_employee_id->AdvancedSearch->SearchValue = @$filter["x_work_employee_id"];
		$this->work_employee_id->AdvancedSearch->SearchOperator = @$filter["z_work_employee_id"];
		$this->work_employee_id->AdvancedSearch->SearchCondition = @$filter["v_work_employee_id"];
		$this->work_employee_id->AdvancedSearch->SearchValue2 = @$filter["y_work_employee_id"];
		$this->work_employee_id->AdvancedSearch->SearchOperator2 = @$filter["w_work_employee_id"];
		$this->work_employee_id->AdvancedSearch->Save();

		// Field work_started
		$this->work_started->AdvancedSearch->SearchValue = @$filter["x_work_started"];
		$this->work_started->AdvancedSearch->SearchOperator = @$filter["z_work_started"];
		$this->work_started->AdvancedSearch->SearchCondition = @$filter["v_work_started"];
		$this->work_started->AdvancedSearch->SearchValue2 = @$filter["y_work_started"];
		$this->work_started->AdvancedSearch->SearchOperator2 = @$filter["w_work_started"];
		$this->work_started->AdvancedSearch->Save();
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->work_id, $Default, FALSE); // work_id
		$this->BuildSearchSql($sWhere, $this->work_period_id, $Default, FALSE); // work_period_id
		$this->BuildSearchSql($sWhere, $this->work_project_id, $Default, FALSE); // work_project_id
		$this->BuildSearchSql($sWhere, $this->work_plan_id, $Default, FALSE); // work_plan_id
		$this->BuildSearchSql($sWhere, $this->work_lab_id, $Default, FALSE); // work_lab_id
		$this->BuildSearchSql($sWhere, $this->work_task_id, $Default, FALSE); // work_task_id
		$this->BuildSearchSql($sWhere, $this->work_description, $Default, FALSE); // work_description
		$this->BuildSearchSql($sWhere, $this->work_progress, $Default, FALSE); // work_progress
		$this->BuildSearchSql($sWhere, $this->work_time, $Default, FALSE); // work_time
		$this->BuildSearchSql($sWhere, $this->work_employee_id, $Default, FALSE); // work_employee_id
		$this->BuildSearchSql($sWhere, $this->work_started, $Default, FALSE); // work_started

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->work_id->AdvancedSearch->Save(); // work_id
			$this->work_period_id->AdvancedSearch->Save(); // work_period_id
			$this->work_project_id->AdvancedSearch->Save(); // work_project_id
			$this->work_plan_id->AdvancedSearch->Save(); // work_plan_id
			$this->work_lab_id->AdvancedSearch->Save(); // work_lab_id
			$this->work_task_id->AdvancedSearch->Save(); // work_task_id
			$this->work_description->AdvancedSearch->Save(); // work_description
			$this->work_progress->AdvancedSearch->Save(); // work_progress
			$this->work_time->AdvancedSearch->Save(); // work_time
			$this->work_employee_id->AdvancedSearch->Save(); // work_employee_id
			$this->work_started->AdvancedSearch->Save(); // work_started
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1 || $FldOpr <> "LIKE" ||
			($FldOpr2 <> "LIKE" && $FldVal2 <> ""))
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Check if search parm exists
	function CheckSearchParms() {
		if ($this->work_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_period_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_project_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_plan_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_lab_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_task_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_description->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_progress->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_time->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_employee_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->work_started->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->work_id->AdvancedSearch->UnsetSession();
		$this->work_period_id->AdvancedSearch->UnsetSession();
		$this->work_project_id->AdvancedSearch->UnsetSession();
		$this->work_plan_id->AdvancedSearch->UnsetSession();
		$this->work_lab_id->AdvancedSearch->UnsetSession();
		$this->work_task_id->AdvancedSearch->UnsetSession();
		$this->work_description->AdvancedSearch->UnsetSession();
		$this->work_progress->AdvancedSearch->UnsetSession();
		$this->work_time->AdvancedSearch->UnsetSession();
		$this->work_employee_id->AdvancedSearch->UnsetSession();
		$this->work_started->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore advanced search values
		$this->work_id->AdvancedSearch->Load();
		$this->work_period_id->AdvancedSearch->Load();
		$this->work_project_id->AdvancedSearch->Load();
		$this->work_plan_id->AdvancedSearch->Load();
		$this->work_lab_id->AdvancedSearch->Load();
		$this->work_task_id->AdvancedSearch->Load();
		$this->work_description->AdvancedSearch->Load();
		$this->work_progress->AdvancedSearch->Load();
		$this->work_time->AdvancedSearch->Load();
		$this->work_employee_id->AdvancedSearch->Load();
		$this->work_started->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->work_period_id); // work_period_id
			$this->UpdateSort($this->work_project_id); // work_project_id
			$this->UpdateSort($this->work_plan_id); // work_plan_id
			$this->UpdateSort($this->work_lab_id); // work_lab_id
			$this->UpdateSort($this->work_task_id); // work_task_id
			$this->UpdateSort($this->work_description); // work_description
			$this->UpdateSort($this->work_progress); // work_progress
			$this->UpdateSort($this->work_time); // work_time
			$this->UpdateSort($this->work_employee_id); // work_employee_id
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset master/detail keys
			if ($this->Command == "resetall") {
				$this->setCurrentMasterTable(""); // Clear master table
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
				$this->work_period_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_task_id->setSessionValue("");
				$this->work_plan_id->setSessionValue("");
				$this->work_lab_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_employee_id->setSessionValue("");
				$this->work_employee_id->setSessionValue("");
				$this->work_task_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_plan_id->setSessionValue("");
				$this->work_lab_id->setSessionValue("");
				$this->work_project_id->setSessionValue("");
				$this->work_plan_id->setSessionValue("");
				$this->work_lab_id->setSessionValue("");
				$this->work_task_id->setSessionValue("");
			}

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->work_period_id->setSort("");
				$this->work_project_id->setSort("");
				$this->work_plan_id->setSort("");
				$this->work_lab_id->setSort("");
				$this->work_task_id->setSort("");
				$this->work_description->setSort("");
				$this->work_progress->setSort("");
				$this->work_time->setSort("");
				$this->work_employee_id->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = FALSE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView() && $this->ShowOptionLink('view'))
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit() && $this->ShowOptionLink('edit')) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd() && $this->ShowOptionLink('add')) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete() && $this->ShowOptionLink('delete'))
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt && $this->Export == "" && $this->CurrentAction == "") {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->work_id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fworkslistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fworkslistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fworkslist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			$this->CurrentAction = ""; // Clear action
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Advanced search button
		$item = &$this->SearchOptions->Add("advancedsearch");
		if (ew_IsMobile())
			$item->Body = "<a class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" href=\"workssrch.php\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		else
			$item->Body = "<button type=\"button\" class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" onclick=\"ew_SearchDialogShow({lnk:this,url:'workssrch.php'});\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		$item->Visible = TRUE;

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// work_id

		$this->work_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_id"]);
		if ($this->work_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_id->AdvancedSearch->SearchOperator = @$_GET["z_work_id"];

		// work_period_id
		$this->work_period_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_period_id"]);
		if ($this->work_period_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_period_id->AdvancedSearch->SearchOperator = @$_GET["z_work_period_id"];

		// work_project_id
		$this->work_project_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_project_id"]);
		if ($this->work_project_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_project_id->AdvancedSearch->SearchOperator = @$_GET["z_work_project_id"];

		// work_plan_id
		$this->work_plan_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_plan_id"]);
		if ($this->work_plan_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_plan_id->AdvancedSearch->SearchOperator = @$_GET["z_work_plan_id"];

		// work_lab_id
		$this->work_lab_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_lab_id"]);
		if ($this->work_lab_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_lab_id->AdvancedSearch->SearchOperator = @$_GET["z_work_lab_id"];

		// work_task_id
		$this->work_task_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_task_id"]);
		if ($this->work_task_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_task_id->AdvancedSearch->SearchOperator = @$_GET["z_work_task_id"];

		// work_description
		$this->work_description->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_description"]);
		if ($this->work_description->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_description->AdvancedSearch->SearchOperator = @$_GET["z_work_description"];

		// work_progress
		$this->work_progress->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_progress"]);
		if ($this->work_progress->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_progress->AdvancedSearch->SearchOperator = @$_GET["z_work_progress"];

		// work_time
		$this->work_time->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_time"]);
		if ($this->work_time->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_time->AdvancedSearch->SearchOperator = @$_GET["z_work_time"];

		// work_employee_id
		$this->work_employee_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_employee_id"]);
		if ($this->work_employee_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_employee_id->AdvancedSearch->SearchOperator = @$_GET["z_work_employee_id"];

		// work_started
		$this->work_started->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_work_started"]);
		if ($this->work_started->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->work_started->AdvancedSearch->SearchOperator = @$_GET["z_work_started"];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->work_id->setDbValue($rs->fields('work_id'));
		$this->work_period_id->setDbValue($rs->fields('work_period_id'));
		$this->work_project_id->setDbValue($rs->fields('work_project_id'));
		$this->work_plan_id->setDbValue($rs->fields('work_plan_id'));
		$this->work_lab_id->setDbValue($rs->fields('work_lab_id'));
		$this->work_task_id->setDbValue($rs->fields('work_task_id'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
		$this->work_started->setDbValue($rs->fields('work_started'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->work_id->DbValue = $row['work_id'];
		$this->work_period_id->DbValue = $row['work_period_id'];
		$this->work_project_id->DbValue = $row['work_project_id'];
		$this->work_plan_id->DbValue = $row['work_plan_id'];
		$this->work_lab_id->DbValue = $row['work_lab_id'];
		$this->work_task_id->DbValue = $row['work_task_id'];
		$this->work_description->DbValue = $row['work_description'];
		$this->work_progress->DbValue = $row['work_progress'];
		$this->work_time->DbValue = $row['work_time'];
		$this->work_employee_id->DbValue = $row['work_employee_id'];
		$this->work_started->DbValue = $row['work_started'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("work_id")) <> "")
			$this->work_id->CurrentValue = $this->getKey("work_id"); // work_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->work_time->FormValue == $this->work_time->CurrentValue && is_numeric(ew_StrToFloat($this->work_time->CurrentValue)))
			$this->work_time->CurrentValue = ew_StrToFloat($this->work_time->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

			// work_period_id
			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";
			$this->work_project_id->TooltipValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";
			$this->work_plan_id->TooltipValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";
			$this->work_lab_id->TooltipValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";
			$this->work_task_id->TooltipValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";
			$this->work_description->TooltipValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";
			$this->work_progress->TooltipValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";
			$this->work_time->TooltipValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
			$this->work_employee_id->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->work_id->AdvancedSearch->Load();
		$this->work_period_id->AdvancedSearch->Load();
		$this->work_project_id->AdvancedSearch->Load();
		$this->work_plan_id->AdvancedSearch->Load();
		$this->work_lab_id->AdvancedSearch->Load();
		$this->work_task_id->AdvancedSearch->Load();
		$this->work_description->AdvancedSearch->Load();
		$this->work_progress->AdvancedSearch->Load();
		$this->work_time->AdvancedSearch->Load();
		$this->work_employee_id->AdvancedSearch->Load();
		$this->work_started->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_works\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_works',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.fworkslist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "periods") {
			global $periods;
			if (!isset($periods)) $periods = new cperiods;
			$rsmaster = $periods->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$periods;
					$periods->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "projects") {
			global $projects;
			if (!isset($projects)) $projects = new cprojects;
			$rsmaster = $projects->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$projects;
					$projects->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "tasks") {
			global $tasks;
			if (!isset($tasks)) $tasks = new ctasks;
			$rsmaster = $tasks->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$tasks;
					$tasks->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "v_employees") {
			global $v_employees;
			if (!isset($v_employees)) $v_employees = new cv_employees;
			$rsmaster = $v_employees->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$v_employees;
					$v_employees->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_employees") {
			global $e_employees;
			if (!isset($e_employees)) $e_employees = new ce_employees;
			$rsmaster = $e_employees->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$e_employees;
					$e_employees->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_tasks") {
			global $e_tasks;
			if (!isset($e_tasks)) $e_tasks = new ce_tasks;
			$rsmaster = $e_tasks->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$e_tasks;
					$e_tasks->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "e_tasks_finance") {
			global $e_tasks_finance;
			if (!isset($e_tasks_finance)) $e_tasks_finance = new ce_tasks_finance;
			$rsmaster = $e_tasks_finance->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$Doc->Table = &$e_tasks_finance;
					$e_tasks_finance->ExportDocument($Doc, $rsmaster, 1, 1);
					$Doc->ExportEmptyRow();
					$Doc->Table = &$this;
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->work_employee_id->CurrentValue);
		return TRUE;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setQueryStringValue($_GET["fk_period_id"]);
					$this->work_period_id->setQueryStringValue($GLOBALS["periods"]->period_id->QueryStringValue);
					$this->work_period_id->setSessionValue($this->work_period_id->QueryStringValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setQueryStringValue($_GET["fk_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["projects"]->project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["e_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setFormValue($_POST["fk_period_id"]);
					$this->work_period_id->setFormValue($GLOBALS["periods"]->period_id->FormValue);
					$this->work_period_id->setSessionValue($this->work_period_id->FormValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setFormValue($_POST["fk_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["projects"]->project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["e_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks_finance"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks_finance"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Update URL
			$this->AddUrl = $this->AddMasterUrl($this->AddUrl);
			$this->InlineAddUrl = $this->AddMasterUrl($this->InlineAddUrl);
			$this->GridAddUrl = $this->AddMasterUrl($this->GridAddUrl);
			$this->GridEditUrl = $this->AddMasterUrl($this->GridEditUrl);

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "periods") {
				if ($this->work_period_id->CurrentValue == "") $this->work_period_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "projects") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "v_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks_finance") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'works';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($works_list)) $works_list = new cworks_list();

// Page init
$works_list->Page_Init();

// Page main
$works_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$works_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($works->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fworkslist = new ew_Form("fworkslist", "list");
fworkslist.FormKeyCountName = '<?php echo $works_list->FormKeyCountName ?>';

// Form_CustomValidate event
fworkslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fworkslist.ValidateRequired = true;
<?php } else { ?>
fworkslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fworkslist.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworkslist.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fworkslist.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworkslist.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworkslist.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworkslist.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworkslist.Lists["x_work_progress"].Options = <?php echo json_encode($works->work_progress->Options()) ?>;
fworkslist.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = fworkslistsrch = new ew_Form("fworkslistsrch");

// Init search panel as collapsed
if (fworkslistsrch) fworkslistsrch.InitSearchPanel = true;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($works->Export == "") { ?>
<div class="ewToolbar">
<?php if ($works->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($works_list->TotalRecs > 0 && $works_list->ExportOptions->Visible()) { ?>
<?php $works_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($works_list->SearchOptions->Visible()) { ?>
<?php $works_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($works_list->FilterOptions->Visible()) { ?>
<?php $works_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($works->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php if (($works->Export == "") || (EW_EXPORT_MASTER_RECORD && $works->Export == "print")) { ?>
<?php
$gsMasterReturnUrl = "periodslist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "periods") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "periodsmaster.php" ?>
<?php
	}
}
?>
<?php
$gsMasterReturnUrl = "projectslist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "projects") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "projectsmaster.php" ?>
<?php
	}
}
?>
<?php
$gsMasterReturnUrl = "taskslist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "tasks") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "tasksmaster.php" ?>
<?php
	}
}
?>
<?php
$gsMasterReturnUrl = "v_employeeslist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "v_employees") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "v_employeesmaster.php" ?>
<?php
	}
}
?>
<?php
$gsMasterReturnUrl = "e_employeeslist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "e_employees") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "e_employeesmaster.php" ?>
<?php
	}
}
?>
<?php
$gsMasterReturnUrl = "e_taskslist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "e_tasks") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "e_tasksmaster.php" ?>
<?php
	}
}
?>
<?php
$gsMasterReturnUrl = "e_tasks_financelist.php";
if ($works_list->DbMasterFilter <> "" && $works->getCurrentMasterTable() == "e_tasks_finance") {
	if ($works_list->MasterRecordExists) {
		if ($works->getCurrentMasterTable() == $works->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<?php include_once "e_tasks_financemaster.php" ?>
<?php
	}
}
?>
<?php } ?>
<?php
	$bSelectLimit = $works_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($works_list->TotalRecs <= 0)
			$works_list->TotalRecs = $works->SelectRecordCount();
	} else {
		if (!$works_list->Recordset && ($works_list->Recordset = $works_list->LoadRecordset()))
			$works_list->TotalRecs = $works_list->Recordset->RecordCount();
	}
	$works_list->StartRec = 1;
	if ($works_list->DisplayRecs <= 0 || ($works->Export <> "" && $works->ExportAll)) // Display all records
		$works_list->DisplayRecs = $works_list->TotalRecs;
	if (!($works->Export <> "" && $works->ExportAll))
		$works_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$works_list->Recordset = $works_list->LoadRecordset($works_list->StartRec-1, $works_list->DisplayRecs);

	// Set no record found message
	if ($works->CurrentAction == "" && $works_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$works_list->setWarningMessage(ew_DeniedMsg());
		if ($works_list->SearchWhere == "0=101")
			$works_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$works_list->setWarningMessage($Language->Phrase("NoRecord"));
	}

	// Audit trail on search
	if ($works_list->AuditTrailOnSearch && $works_list->Command == "search" && !$works_list->RestoreSearch) {
		$searchparm = ew_ServerVar("QUERY_STRING");
		$searchsql = $works_list->getSessionWhere();
		$works_list->WriteAuditTrailOnSearch($searchparm, $searchsql);
	}
$works_list->RenderOtherOptions();
?>
<?php $works_list->ShowPageHeader(); ?>
<?php
$works_list->ShowMessage();
?>
<?php if ($works_list->TotalRecs > 0 || $works->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<?php if ($works->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($works->CurrentAction <> "gridadd" && $works->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($works_list->Pager)) $works_list->Pager = new cNumericPager($works_list->StartRec, $works_list->DisplayRecs, $works_list->TotalRecs, $works_list->RecRange) ?>
<?php if ($works_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<div class="ewNumericPage"><ul class="pagination">
	<?php if ($works_list->Pager->FirstButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->FirstButton->Start ?>"><?php echo $Language->Phrase("PagerFirst") ?></a></li>
	<?php } ?>
	<?php if ($works_list->Pager->PrevButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->PrevButton->Start ?>"><?php echo $Language->Phrase("PagerPrevious") ?></a></li>
	<?php } ?>
	<?php foreach ($works_list->Pager->Items as $PagerItem) { ?>
		<li<?php if (!$PagerItem->Enabled) { echo " class=\" active\""; } ?>><a href="<?php if ($PagerItem->Enabled) { echo $works_list->PageUrl() . "start=" . $PagerItem->Start; } else { echo "#"; } ?>"><?php echo $PagerItem->Text ?></a></li>
	<?php } ?>
	<?php if ($works_list->Pager->NextButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->NextButton->Start ?>"><?php echo $Language->Phrase("PagerNext") ?></a></li>
	<?php } ?>
	<?php if ($works_list->Pager->LastButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->LastButton->Start ?>"><?php echo $Language->Phrase("PagerLast") ?></a></li>
	<?php } ?>
</ul></div>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $works_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $works_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $works_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($works_list->TotalRecs > 0) { ?>
<div class="ewPager">
<input type="hidden" name="t" value="works">
<select name="<?php echo EW_TABLE_REC_PER_PAGE ?>" class="form-control input-sm" onchange="this.form.submit();">
<option value="20"<?php if ($works_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($works_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($works_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
<option value="500"<?php if ($works_list->DisplayRecs == 500) { ?> selected<?php } ?>>500</option>
<option value="ALL"<?php if ($works->getRecordsPerPage() == -1) { ?> selected<?php } ?>><?php echo $Language->Phrase("AllRecords") ?></option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($works_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fworkslist" id="fworkslist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($works_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $works_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="works">
<?php if ($works->getCurrentMasterTable() == "periods" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="periods">
<input type="hidden" name="fk_period_id" value="<?php echo $works->work_period_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "projects" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="projects">
<input type="hidden" name="fk_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "tasks" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="tasks">
<input type="hidden" name="fk_task_id" value="<?php echo $works->work_task_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_plan_id" value="<?php echo $works->work_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_lab_id" value="<?php echo $works->work_lab_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "v_employees" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="v_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $works->work_employee_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "e_employees" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $works->work_employee_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "e_tasks" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_tasks">
<input type="hidden" name="fk_task_id" value="<?php echo $works->work_task_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_plan_id" value="<?php echo $works->work_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_lab_id" value="<?php echo $works->work_lab_id->getSessionValue() ?>">
<?php } ?>
<?php if ($works->getCurrentMasterTable() == "e_tasks_finance" && $works->CurrentAction <> "") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="e_tasks_finance">
<input type="hidden" name="fk_task_project_id" value="<?php echo $works->work_project_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_plan_id" value="<?php echo $works->work_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_lab_id" value="<?php echo $works->work_lab_id->getSessionValue() ?>">
<input type="hidden" name="fk_task_id" value="<?php echo $works->work_task_id->getSessionValue() ?>">
<?php } ?>
<div id="gmp_works" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($works_list->TotalRecs > 0) { ?>
<table id="tbl_workslist" class="table ewTable">
<?php echo $works->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$works_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$works_list->RenderListOptions();

// Render list options (header, left)
$works_list->ListOptions->Render("header", "left");
?>
<?php if ($works->work_period_id->Visible) { // work_period_id ?>
	<?php if ($works->SortUrl($works->work_period_id) == "") { ?>
		<th data-name="work_period_id"><div id="elh_works_work_period_id" class="works_work_period_id"><div class="ewTableHeaderCaption"><?php echo $works->work_period_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_period_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_period_id) ?>',1);"><div id="elh_works_work_period_id" class="works_work_period_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_period_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_period_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_period_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_project_id->Visible) { // work_project_id ?>
	<?php if ($works->SortUrl($works->work_project_id) == "") { ?>
		<th data-name="work_project_id"><div id="elh_works_work_project_id" class="works_work_project_id"><div class="ewTableHeaderCaption"><?php echo $works->work_project_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_project_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_project_id) ?>',1);"><div id="elh_works_work_project_id" class="works_work_project_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_project_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_project_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_project_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
	<?php if ($works->SortUrl($works->work_plan_id) == "") { ?>
		<th data-name="work_plan_id"><div id="elh_works_work_plan_id" class="works_work_plan_id"><div class="ewTableHeaderCaption"><?php echo $works->work_plan_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_plan_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_plan_id) ?>',1);"><div id="elh_works_work_plan_id" class="works_work_plan_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_plan_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_plan_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_plan_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
	<?php if ($works->SortUrl($works->work_lab_id) == "") { ?>
		<th data-name="work_lab_id"><div id="elh_works_work_lab_id" class="works_work_lab_id"><div class="ewTableHeaderCaption"><?php echo $works->work_lab_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_lab_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_lab_id) ?>',1);"><div id="elh_works_work_lab_id" class="works_work_lab_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_lab_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_lab_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_lab_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_task_id->Visible) { // work_task_id ?>
	<?php if ($works->SortUrl($works->work_task_id) == "") { ?>
		<th data-name="work_task_id"><div id="elh_works_work_task_id" class="works_work_task_id"><div class="ewTableHeaderCaption"><?php echo $works->work_task_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_task_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_task_id) ?>',1);"><div id="elh_works_work_task_id" class="works_work_task_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_task_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_task_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_task_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_description->Visible) { // work_description ?>
	<?php if ($works->SortUrl($works->work_description) == "") { ?>
		<th data-name="work_description"><div id="elh_works_work_description" class="works_work_description"><div class="ewTableHeaderCaption"><?php echo $works->work_description->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_description"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_description) ?>',1);"><div id="elh_works_work_description" class="works_work_description">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_description->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_description->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_description->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_progress->Visible) { // work_progress ?>
	<?php if ($works->SortUrl($works->work_progress) == "") { ?>
		<th data-name="work_progress"><div id="elh_works_work_progress" class="works_work_progress"><div class="ewTableHeaderCaption"><?php echo $works->work_progress->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_progress"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_progress) ?>',1);"><div id="elh_works_work_progress" class="works_work_progress">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_progress->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_progress->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_progress->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_time->Visible) { // work_time ?>
	<?php if ($works->SortUrl($works->work_time) == "") { ?>
		<th data-name="work_time"><div id="elh_works_work_time" class="works_work_time"><div class="ewTableHeaderCaption"><?php echo $works->work_time->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_time"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_time) ?>',1);"><div id="elh_works_work_time" class="works_work_time">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_time->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_time->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_time->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
	<?php if ($works->SortUrl($works->work_employee_id) == "") { ?>
		<th data-name="work_employee_id"><div id="elh_works_work_employee_id" class="works_work_employee_id"><div class="ewTableHeaderCaption"><?php echo $works->work_employee_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="work_employee_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $works->SortUrl($works->work_employee_id) ?>',1);"><div id="elh_works_work_employee_id" class="works_work_employee_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $works->work_employee_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($works->work_employee_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($works->work_employee_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$works_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($works->ExportAll && $works->Export <> "") {
	$works_list->StopRec = $works_list->TotalRecs;
} else {

	// Set the last record to display
	if ($works_list->TotalRecs > $works_list->StartRec + $works_list->DisplayRecs - 1)
		$works_list->StopRec = $works_list->StartRec + $works_list->DisplayRecs - 1;
	else
		$works_list->StopRec = $works_list->TotalRecs;
}
$works_list->RecCnt = $works_list->StartRec - 1;
if ($works_list->Recordset && !$works_list->Recordset->EOF) {
	$works_list->Recordset->MoveFirst();
	$bSelectLimit = $works_list->UseSelectLimit;
	if (!$bSelectLimit && $works_list->StartRec > 1)
		$works_list->Recordset->Move($works_list->StartRec - 1);
} elseif (!$works->AllowAddDeleteRow && $works_list->StopRec == 0) {
	$works_list->StopRec = $works->GridAddRowCount;
}

// Initialize aggregate
$works->RowType = EW_ROWTYPE_AGGREGATEINIT;
$works->ResetAttrs();
$works_list->RenderRow();
while ($works_list->RecCnt < $works_list->StopRec) {
	$works_list->RecCnt++;
	if (intval($works_list->RecCnt) >= intval($works_list->StartRec)) {
		$works_list->RowCnt++;

		// Set up key count
		$works_list->KeyCount = $works_list->RowIndex;

		// Init row class and style
		$works->ResetAttrs();
		$works->CssClass = "";
		if ($works->CurrentAction == "gridadd") {
		} else {
			$works_list->LoadRowValues($works_list->Recordset); // Load row values
		}
		$works->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$works->RowAttrs = array_merge($works->RowAttrs, array('data-rowindex'=>$works_list->RowCnt, 'id'=>'r' . $works_list->RowCnt . '_works', 'data-rowtype'=>$works->RowType));

		// Render row
		$works_list->RenderRow();

		// Render list options
		$works_list->RenderListOptions();
?>
	<tr<?php echo $works->RowAttributes() ?>>
<?php

// Render list options (body, left)
$works_list->ListOptions->Render("body", "left", $works_list->RowCnt);
?>
	<?php if ($works->work_period_id->Visible) { // work_period_id ?>
		<td data-name="work_period_id"<?php echo $works->work_period_id->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_period_id" class="works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<?php echo $works->work_period_id->ListViewValue() ?></span>
</span>
<a id="<?php echo $works_list->PageObjName . "_row_" . $works_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($works->work_project_id->Visible) { // work_project_id ?>
		<td data-name="work_project_id"<?php echo $works->work_project_id->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_project_id" class="works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<?php echo $works->work_project_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
		<td data-name="work_plan_id"<?php echo $works->work_plan_id->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_plan_id" class="works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<?php echo $works->work_plan_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
		<td data-name="work_lab_id"<?php echo $works->work_lab_id->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_lab_id" class="works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<?php echo $works->work_lab_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_task_id->Visible) { // work_task_id ?>
		<td data-name="work_task_id"<?php echo $works->work_task_id->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_task_id" class="works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<?php echo $works->work_task_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_description->Visible) { // work_description ?>
		<td data-name="work_description"<?php echo $works->work_description->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_description" class="works_work_description">
<span<?php echo $works->work_description->ViewAttributes() ?>>
<?php echo $works->work_description->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_progress->Visible) { // work_progress ?>
		<td data-name="work_progress"<?php echo $works->work_progress->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_progress" class="works_work_progress">
<span<?php echo $works->work_progress->ViewAttributes() ?>>
<?php echo $works->work_progress->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_time->Visible) { // work_time ?>
		<td data-name="work_time"<?php echo $works->work_time->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_time" class="works_work_time">
<span<?php echo $works->work_time->ViewAttributes() ?>>
<?php echo $works->work_time->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
		<td data-name="work_employee_id"<?php echo $works->work_employee_id->CellAttributes() ?>>
<span id="el<?php echo $works_list->RowCnt ?>_works_work_employee_id" class="works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<?php echo $works->work_employee_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$works_list->ListOptions->Render("body", "right", $works_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($works->CurrentAction <> "gridadd")
		$works_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($works->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($works_list->Recordset)
	$works_list->Recordset->Close();
?>
<?php if ($works->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($works->CurrentAction <> "gridadd" && $works->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($works_list->Pager)) $works_list->Pager = new cNumericPager($works_list->StartRec, $works_list->DisplayRecs, $works_list->TotalRecs, $works_list->RecRange) ?>
<?php if ($works_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<div class="ewNumericPage"><ul class="pagination">
	<?php if ($works_list->Pager->FirstButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->FirstButton->Start ?>"><?php echo $Language->Phrase("PagerFirst") ?></a></li>
	<?php } ?>
	<?php if ($works_list->Pager->PrevButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->PrevButton->Start ?>"><?php echo $Language->Phrase("PagerPrevious") ?></a></li>
	<?php } ?>
	<?php foreach ($works_list->Pager->Items as $PagerItem) { ?>
		<li<?php if (!$PagerItem->Enabled) { echo " class=\" active\""; } ?>><a href="<?php if ($PagerItem->Enabled) { echo $works_list->PageUrl() . "start=" . $PagerItem->Start; } else { echo "#"; } ?>"><?php echo $PagerItem->Text ?></a></li>
	<?php } ?>
	<?php if ($works_list->Pager->NextButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->NextButton->Start ?>"><?php echo $Language->Phrase("PagerNext") ?></a></li>
	<?php } ?>
	<?php if ($works_list->Pager->LastButton->Enabled) { ?>
	<li><a href="<?php echo $works_list->PageUrl() ?>start=<?php echo $works_list->Pager->LastButton->Start ?>"><?php echo $Language->Phrase("PagerLast") ?></a></li>
	<?php } ?>
</ul></div>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $works_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $works_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $works_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
<?php if ($works_list->TotalRecs > 0) { ?>
<div class="ewPager">
<input type="hidden" name="t" value="works">
<select name="<?php echo EW_TABLE_REC_PER_PAGE ?>" class="form-control input-sm" onchange="this.form.submit();">
<option value="20"<?php if ($works_list->DisplayRecs == 20) { ?> selected<?php } ?>>20</option>
<option value="50"<?php if ($works_list->DisplayRecs == 50) { ?> selected<?php } ?>>50</option>
<option value="100"<?php if ($works_list->DisplayRecs == 100) { ?> selected<?php } ?>>100</option>
<option value="500"<?php if ($works_list->DisplayRecs == 500) { ?> selected<?php } ?>>500</option>
<option value="ALL"<?php if ($works->getRecordsPerPage() == -1) { ?> selected<?php } ?>><?php echo $Language->Phrase("AllRecords") ?></option>
</select>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($works_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($works_list->TotalRecs == 0 && $works->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($works_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($works->Export == "") { ?>
<script type="text/javascript">
fworkslistsrch.Init();
fworkslistsrch.FilterList = <?php echo $works_list->GetFilterList() ?>;
fworkslist.Init();
</script>
<?php } ?>
<?php
$works_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($works->Export == "") { ?>
<script type="text/javascript">
	var e = 0.0;
	$('span.works_work_time span').each(function() {
		var f = parseFloat($(this).text());
		e += f;
	});
	var days = Math.trunc(e / 8);
	var hours = Math.trunc(e - (days * 8));
	var mins = Math.trunc(60 * (e - (days * 8) - hours));
	$('form#fworkslist').after('<div style="font-weight:bold;padding:3px;border-top:2px solid #ddd;">Часы &#931; ' + e.toFixed(2) + 
	  ' <font style="color: grey;" title="1 раб. день = 8 ч">(' + days + ' раб. дн. ' + hours + ' ч ' + mins + ' м)</font></div>');
	var editButton = $("span#el1_works_edit a");
	if (<?php echo ($works->work_task_id->getSessionValue()) ? "true" : "false" ?> && editButton.length == 1)
	{
		var url = editButton.attr('href');
		$("a.ewAdd").remove();
		$("div.ewAddEditOption div.ewButtonGroup").append(
			'<a class="btn btn-default ewAddEdit ewEdit btn-sm"  title="Редактировать существующую запись" data-caption="Редактировать существующую запись" href="' + url + '"><span data-phrase="EditLink" class="glyphicon glyphicon-edit ewIcon" data-caption="Редактировать существующую запись"></span></a>'
		);
		$("span#ewPageCaption").after('<a href="' + window.location.pathname + '?cmd=resetall"> (Показать все)</a>');
	}

	// Links from GitLab
	$('span.works_work_description span').each(function (i)
	{
		$(this).html($(this).text().replace(/(http:\/\/gitlab\.div38\/([^\/]+\/[^\/]+)\/issues\/(\d+))/g, '<a class="gitlab-link" href="$1" title="$2" target="_blank"><img src="extensions/gitlab.png">#$3</a>'));
	});

	// Done
	$('span.works_work_progress span').each(function (i)
	{
		if ($(this).text().indexOf('готово') >= 0)
		{
			$(this).parent().parent().parent().addClass('work-done');
		}
	});
var currentFilter = <?php echo isset($works_list) ? $works_list->GetFilterList() : "[]" ?>;
</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$works_list->Page_Terminate();
?>
