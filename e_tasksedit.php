<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "e_tasksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "plansinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "worksgridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$e_tasks_edit = NULL; // Initialize page object first

class ce_tasks_edit extends ce_tasks {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'e_tasks';

	// Page object name
	var $PageObjName = 'e_tasks_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = TRUE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (e_tasks)
		if (!isset($GLOBALS["e_tasks"]) || get_class($GLOBALS["e_tasks"]) == "ce_tasks") {
			$GLOBALS["e_tasks"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["e_tasks"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (plans)
		if (!isset($GLOBALS['plans'])) $GLOBALS['plans'] = new cplans();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'e_tasks', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("e_taskslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'works'
			if (@$_POST["grid"] == "fworksgrid") {
				if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid;
				$GLOBALS["works_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $e_tasks;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($e_tasks);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["task_id"] <> "") {
			$this->task_id->setQueryStringValue($_GET["task_id"]);
		}

		// Set up master detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values

			// Set up detail parameters
			$this->SetUpDetailParms();
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->task_id->CurrentValue == "")
			$this->Page_Terminate("e_taskslist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("e_taskslist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			Case "U": // Update
				if ($this->getCurrentDetailTable() <> "") // Master/detail edit
					$sReturnUrl = $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $this->getCurrentDetailTable()); // Master/Detail view page
				else
					$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "e_taskslist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->task_project_id->FldIsDetailKey) {
			$this->task_project_id->setFormValue($objForm->GetValue("x_task_project_id"));
		}
		if (!$this->task_plan_id->FldIsDetailKey) {
			$this->task_plan_id->setFormValue($objForm->GetValue("x_task_plan_id"));
		}
		if (!$this->task_lab_id->FldIsDetailKey) {
			$this->task_lab_id->setFormValue($objForm->GetValue("x_task_lab_id"));
		}
		if (!$this->task_code->FldIsDetailKey) {
			$this->task_code->setFormValue($objForm->GetValue("x_task_code"));
		}
		if (!$this->task_name->FldIsDetailKey) {
			$this->task_name->setFormValue($objForm->GetValue("x_task_name"));
		}
		if (!$this->task_from->FldIsDetailKey) {
			$this->task_from->setFormValue($objForm->GetValue("x_task_from"));
			$this->task_from->CurrentValue = ew_UnFormatDateTime($this->task_from->CurrentValue, 7);
		}
		if (!$this->task_to->FldIsDetailKey) {
			$this->task_to->setFormValue($objForm->GetValue("x_task_to"));
			$this->task_to->CurrentValue = ew_UnFormatDateTime($this->task_to->CurrentValue, 7);
		}
		if (!$this->task_employee_id->FldIsDetailKey) {
			$this->task_employee_id->setFormValue($objForm->GetValue("x_task_employee_id"));
		}
		if (!$this->task_coordinator_id->FldIsDetailKey) {
			$this->task_coordinator_id->setFormValue($objForm->GetValue("x_task_coordinator_id"));
		}
		if (!$this->task_object->FldIsDetailKey) {
			$this->task_object->setFormValue($objForm->GetValue("x_task_object"));
		}
		if (!$this->task_status_id->FldIsDetailKey) {
			$this->task_status_id->setFormValue($objForm->GetValue("x_task_status_id"));
		}
		if (!$this->task_hours_planned->FldIsDetailKey) {
			$this->task_hours_planned->setFormValue($objForm->GetValue("x_task_hours_planned"));
		}
		if (!$this->task_hours_actual->FldIsDetailKey) {
			$this->task_hours_actual->setFormValue($objForm->GetValue("x_task_hours_actual"));
		}
		if (!$this->task_description->FldIsDetailKey) {
			$this->task_description->setFormValue($objForm->GetValue("x_task_description"));
		}
		if (!$this->task_key->FldIsDetailKey) {
			$this->task_key->setFormValue($objForm->GetValue("x_task_key"));
		}
		if (!$this->task_id->FldIsDetailKey)
			$this->task_id->setFormValue($objForm->GetValue("x_task_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->task_id->CurrentValue = $this->task_id->FormValue;
		$this->task_project_id->CurrentValue = $this->task_project_id->FormValue;
		$this->task_plan_id->CurrentValue = $this->task_plan_id->FormValue;
		$this->task_lab_id->CurrentValue = $this->task_lab_id->FormValue;
		$this->task_code->CurrentValue = $this->task_code->FormValue;
		$this->task_name->CurrentValue = $this->task_name->FormValue;
		$this->task_from->CurrentValue = $this->task_from->FormValue;
		$this->task_from->CurrentValue = ew_UnFormatDateTime($this->task_from->CurrentValue, 7);
		$this->task_to->CurrentValue = $this->task_to->FormValue;
		$this->task_to->CurrentValue = ew_UnFormatDateTime($this->task_to->CurrentValue, 7);
		$this->task_employee_id->CurrentValue = $this->task_employee_id->FormValue;
		$this->task_coordinator_id->CurrentValue = $this->task_coordinator_id->FormValue;
		$this->task_object->CurrentValue = $this->task_object->FormValue;
		$this->task_status_id->CurrentValue = $this->task_status_id->FormValue;
		$this->task_hours_planned->CurrentValue = $this->task_hours_planned->FormValue;
		$this->task_hours_actual->CurrentValue = $this->task_hours_actual->FormValue;
		$this->task_description->CurrentValue = $this->task_description->FormValue;
		$this->task_key->CurrentValue = $this->task_key->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->task_id->setDbValue($rs->fields('task_id'));
		$this->task_project_id->setDbValue($rs->fields('task_project_id'));
		$this->task_plan_id->setDbValue($rs->fields('task_plan_id'));
		$this->task_lab_id->setDbValue($rs->fields('task_lab_id'));
		$this->task_code->setDbValue($rs->fields('task_code'));
		$this->task_name->setDbValue($rs->fields('task_name'));
		$this->task_from->setDbValue($rs->fields('task_from'));
		$this->task_to->setDbValue($rs->fields('task_to'));
		$this->task_employee_id->setDbValue($rs->fields('task_employee_id'));
		$this->task_coordinator_id->setDbValue($rs->fields('task_coordinator_id'));
		$this->task_object->setDbValue($rs->fields('task_object'));
		$this->task_status_id->setDbValue($rs->fields('task_status_id'));
		$this->task_hours_planned->setDbValue($rs->fields('task_hours_planned'));
		$this->task_hours_actual->setDbValue($rs->fields('task_hours_actual'));
		$this->task_description->setDbValue($rs->fields('task_description'));
		$this->task_key->setDbValue($rs->fields('task_key'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->task_id->DbValue = $row['task_id'];
		$this->task_project_id->DbValue = $row['task_project_id'];
		$this->task_plan_id->DbValue = $row['task_plan_id'];
		$this->task_lab_id->DbValue = $row['task_lab_id'];
		$this->task_code->DbValue = $row['task_code'];
		$this->task_name->DbValue = $row['task_name'];
		$this->task_from->DbValue = $row['task_from'];
		$this->task_to->DbValue = $row['task_to'];
		$this->task_employee_id->DbValue = $row['task_employee_id'];
		$this->task_coordinator_id->DbValue = $row['task_coordinator_id'];
		$this->task_object->DbValue = $row['task_object'];
		$this->task_status_id->DbValue = $row['task_status_id'];
		$this->task_hours_planned->DbValue = $row['task_hours_planned'];
		$this->task_hours_actual->DbValue = $row['task_hours_actual'];
		$this->task_description->DbValue = $row['task_description'];
		$this->task_key->DbValue = $row['task_key'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->task_hours_planned->FormValue == $this->task_hours_planned->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_planned->CurrentValue)))
			$this->task_hours_planned->CurrentValue = ew_StrToFloat($this->task_hours_planned->CurrentValue);

		// Convert decimal values if posted back
		if ($this->task_hours_actual->FormValue == $this->task_hours_actual->CurrentValue && is_numeric(ew_StrToFloat($this->task_hours_actual->CurrentValue)))
			$this->task_hours_actual->CurrentValue = ew_StrToFloat($this->task_hours_actual->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// task_id
		// task_project_id
		// task_plan_id
		// task_lab_id
		// task_code
		// task_name
		// task_from
		// task_to
		// task_employee_id
		// task_coordinator_id
		// task_object
		// task_status_id
		// task_hours_planned
		// task_hours_actual
		// task_description
		// task_key

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// task_id
		$this->task_id->ViewValue = $this->task_id->CurrentValue;
		$this->task_id->ViewCustomAttributes = "";

		// task_project_id
		if (strval($this->task_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_project_id->ViewValue = $this->task_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_project_id->ViewValue = $this->task_project_id->CurrentValue;
			}
		} else {
			$this->task_project_id->ViewValue = NULL;
		}
		$this->task_project_id->ViewCustomAttributes = "";

		// task_plan_id
		if (strval($this->task_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->task_plan_id->ViewValue = $this->task_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_plan_id->ViewValue = $this->task_plan_id->CurrentValue;
			}
		} else {
			$this->task_plan_id->ViewValue = NULL;
		}
		$this->task_plan_id->ViewCustomAttributes = "";

		// task_lab_id
		if (strval($this->task_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_lab_id->ViewValue = $this->task_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_lab_id->ViewValue = $this->task_lab_id->CurrentValue;
			}
		} else {
			$this->task_lab_id->ViewValue = NULL;
		}
		$this->task_lab_id->ViewCustomAttributes = "";

		// task_code
		$this->task_code->ViewValue = $this->task_code->CurrentValue;
		$this->task_code->ViewCustomAttributes = "";

		// task_name
		$this->task_name->ViewValue = $this->task_name->CurrentValue;
		$this->task_name->ViewCustomAttributes = "";

		// task_from
		$this->task_from->ViewValue = $this->task_from->CurrentValue;
		$this->task_from->ViewValue = ew_FormatDateTime($this->task_from->ViewValue, 7);
		$this->task_from->ViewCustomAttributes = "";

		// task_to
		$this->task_to->ViewValue = $this->task_to->CurrentValue;
		$this->task_to->ViewValue = ew_FormatDateTime($this->task_to->ViewValue, 7);
		$this->task_to->ViewCustomAttributes = "";

		// task_employee_id
		if (strval($this->task_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_employee_id->ViewValue = $this->task_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_employee_id->ViewValue = $this->task_employee_id->CurrentValue;
			}
		} else {
			$this->task_employee_id->ViewValue = NULL;
		}
		$this->task_employee_id->ViewCustomAttributes = "";

		// task_coordinator_id
		if (strval($this->task_coordinator_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_coordinator_id->ViewValue = $this->task_coordinator_id->CurrentValue;
			}
		} else {
			$this->task_coordinator_id->ViewValue = NULL;
		}
		$this->task_coordinator_id->ViewCustomAttributes = "";

		// task_object
		$this->task_object->ViewValue = $this->task_object->CurrentValue;
		$this->task_object->ViewCustomAttributes = "";

		// task_status_id
		if (strval($this->task_status_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->task_status_id->ViewValue = $this->task_status_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->task_status_id->ViewValue = $this->task_status_id->CurrentValue;
			}
		} else {
			$this->task_status_id->ViewValue = NULL;
		}
		$this->task_status_id->ViewCustomAttributes = "";

		// task_hours_planned
		$this->task_hours_planned->ViewValue = $this->task_hours_planned->CurrentValue;
		$this->task_hours_planned->ViewCustomAttributes = "";

		// task_hours_actual
		$this->task_hours_actual->ViewValue = $this->task_hours_actual->CurrentValue;
		$this->task_hours_actual->ViewCustomAttributes = "";

		// task_description
		$this->task_description->ViewValue = $this->task_description->CurrentValue;
		$this->task_description->ViewCustomAttributes = "";

		// task_key
		$this->task_key->ViewValue = $this->task_key->CurrentValue;
		$this->task_key->ViewCustomAttributes = "";

			// task_project_id
			$this->task_project_id->LinkCustomAttributes = "";
			$this->task_project_id->HrefValue = "";
			$this->task_project_id->TooltipValue = "";

			// task_plan_id
			$this->task_plan_id->LinkCustomAttributes = "";
			$this->task_plan_id->HrefValue = "";
			$this->task_plan_id->TooltipValue = "";

			// task_lab_id
			$this->task_lab_id->LinkCustomAttributes = "";
			$this->task_lab_id->HrefValue = "";
			$this->task_lab_id->TooltipValue = "";

			// task_code
			$this->task_code->LinkCustomAttributes = "";
			$this->task_code->HrefValue = "";
			$this->task_code->TooltipValue = "";

			// task_name
			$this->task_name->LinkCustomAttributes = "";
			$this->task_name->HrefValue = "";
			$this->task_name->TooltipValue = "";

			// task_from
			$this->task_from->LinkCustomAttributes = "";
			$this->task_from->HrefValue = "";
			$this->task_from->TooltipValue = "";

			// task_to
			$this->task_to->LinkCustomAttributes = "";
			$this->task_to->HrefValue = "";
			$this->task_to->TooltipValue = "";

			// task_employee_id
			$this->task_employee_id->LinkCustomAttributes = "";
			$this->task_employee_id->HrefValue = "";
			$this->task_employee_id->TooltipValue = "";

			// task_coordinator_id
			$this->task_coordinator_id->LinkCustomAttributes = "";
			$this->task_coordinator_id->HrefValue = "";
			$this->task_coordinator_id->TooltipValue = "";

			// task_object
			$this->task_object->LinkCustomAttributes = "";
			$this->task_object->HrefValue = "";
			$this->task_object->TooltipValue = "";

			// task_status_id
			$this->task_status_id->LinkCustomAttributes = "";
			$this->task_status_id->HrefValue = "";
			$this->task_status_id->TooltipValue = "";

			// task_hours_planned
			$this->task_hours_planned->LinkCustomAttributes = "";
			$this->task_hours_planned->HrefValue = "";
			$this->task_hours_planned->TooltipValue = "";

			// task_hours_actual
			$this->task_hours_actual->LinkCustomAttributes = "";
			$this->task_hours_actual->HrefValue = "";
			$this->task_hours_actual->TooltipValue = "";

			// task_description
			$this->task_description->LinkCustomAttributes = "";
			$this->task_description->HrefValue = "";
			$this->task_description->TooltipValue = "";

			// task_key
			$this->task_key->LinkCustomAttributes = "";
			$this->task_key->HrefValue = "";
			$this->task_key->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// task_project_id
			$this->task_project_id->EditAttrs["class"] = "form-control";
			$this->task_project_id->EditCustomAttributes = "";
			if (strval($this->task_project_id->CurrentValue) <> "") {
				$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->task_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_project_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->task_project_id->EditValue = $this->task_project_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->task_project_id->EditValue = $this->task_project_id->CurrentValue;
				}
			} else {
				$this->task_project_id->EditValue = NULL;
			}
			$this->task_project_id->ViewCustomAttributes = "";

			// task_plan_id
			$this->task_plan_id->EditAttrs["class"] = "form-control";
			$this->task_plan_id->EditCustomAttributes = "";
			if (strval($this->task_plan_id->CurrentValue) <> "") {
				$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->task_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
			$sWhereWrk = "";
			$lookuptblfilter = "`plan_active` = 1";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_plan_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `plan_code` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->task_plan_id->EditValue = $this->task_plan_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->task_plan_id->EditValue = $this->task_plan_id->CurrentValue;
				}
			} else {
				$this->task_plan_id->EditValue = NULL;
			}
			$this->task_plan_id->ViewCustomAttributes = "";

			// task_lab_id
			$this->task_lab_id->EditAttrs["class"] = "form-control";
			$this->task_lab_id->EditCustomAttributes = "";
			if (strval($this->task_lab_id->CurrentValue) <> "") {
				$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->task_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_lab_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->task_lab_id->EditValue = $this->task_lab_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->task_lab_id->EditValue = $this->task_lab_id->CurrentValue;
				}
			} else {
				$this->task_lab_id->EditValue = NULL;
			}
			$this->task_lab_id->ViewCustomAttributes = "";

			// task_code
			$this->task_code->EditAttrs["class"] = "form-control";
			$this->task_code->EditCustomAttributes = "";
			$this->task_code->EditValue = $this->task_code->CurrentValue;
			$this->task_code->ViewCustomAttributes = "";

			// task_name
			$this->task_name->EditAttrs["class"] = "form-control";
			$this->task_name->EditCustomAttributes = "";
			$this->task_name->EditValue = $this->task_name->CurrentValue;
			$this->task_name->ViewCustomAttributes = "";

			// task_from
			$this->task_from->EditAttrs["class"] = "form-control";
			$this->task_from->EditCustomAttributes = "";
			$this->task_from->EditValue = $this->task_from->CurrentValue;
			$this->task_from->EditValue = ew_FormatDateTime($this->task_from->EditValue, 7);
			$this->task_from->ViewCustomAttributes = "";

			// task_to
			$this->task_to->EditAttrs["class"] = "form-control";
			$this->task_to->EditCustomAttributes = "";
			$this->task_to->EditValue = $this->task_to->CurrentValue;
			$this->task_to->EditValue = ew_FormatDateTime($this->task_to->EditValue, 7);
			$this->task_to->ViewCustomAttributes = "";

			// task_employee_id
			$this->task_employee_id->EditAttrs["class"] = "form-control";
			$this->task_employee_id->EditCustomAttributes = "";
			if (strval($this->task_employee_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_employee_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->task_employee_id->EditValue = $this->task_employee_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->task_employee_id->EditValue = $this->task_employee_id->CurrentValue;
				}
			} else {
				$this->task_employee_id->EditValue = NULL;
			}
			$this->task_employee_id->ViewCustomAttributes = "";

			// task_coordinator_id
			$this->task_coordinator_id->EditAttrs["class"] = "form-control";
			$this->task_coordinator_id->EditCustomAttributes = "";
			if (strval($this->task_coordinator_id->CurrentValue) <> "") {
				$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->task_coordinator_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `v_employees`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_coordinator_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->task_coordinator_id->EditValue = $this->task_coordinator_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->task_coordinator_id->EditValue = $this->task_coordinator_id->CurrentValue;
				}
			} else {
				$this->task_coordinator_id->EditValue = NULL;
			}
			$this->task_coordinator_id->ViewCustomAttributes = "";

			// task_object
			$this->task_object->EditAttrs["class"] = "form-control";
			$this->task_object->EditCustomAttributes = "";
			$this->task_object->EditValue = $this->task_object->CurrentValue;
			$this->task_object->ViewCustomAttributes = "";

			// task_status_id
			$this->task_status_id->EditAttrs["class"] = "form-control";
			$this->task_status_id->EditCustomAttributes = "";
			if (trim(strval($this->task_status_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`task_status_id`" . ew_SearchString("=", $this->task_status_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `task_statuses`";
			$sWhereWrk = "";
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->task_status_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->task_status_id->EditValue = $arwrk;

			// task_hours_planned
			$this->task_hours_planned->EditAttrs["class"] = "form-control";
			$this->task_hours_planned->EditCustomAttributes = "";
			$this->task_hours_planned->EditValue = $this->task_hours_planned->CurrentValue;
			$this->task_hours_planned->ViewCustomAttributes = "";

			// task_hours_actual
			$this->task_hours_actual->EditAttrs["class"] = "form-control";
			$this->task_hours_actual->EditCustomAttributes = "";
			$this->task_hours_actual->EditValue = $this->task_hours_actual->CurrentValue;
			$this->task_hours_actual->ViewCustomAttributes = "";

			// task_description
			$this->task_description->EditAttrs["class"] = "form-control";
			$this->task_description->EditCustomAttributes = "";
			$this->task_description->EditValue = ew_HtmlEncode($this->task_description->CurrentValue);
			$this->task_description->PlaceHolder = ew_RemoveHtml($this->task_description->FldCaption());

			// task_key
			$this->task_key->EditAttrs["class"] = "form-control";
			$this->task_key->EditCustomAttributes = "";
			$this->task_key->EditValue = $this->task_key->CurrentValue;
			$this->task_key->ViewCustomAttributes = "";

			// Edit refer script
			// task_project_id

			$this->task_project_id->LinkCustomAttributes = "";
			$this->task_project_id->HrefValue = "";
			$this->task_project_id->TooltipValue = "";

			// task_plan_id
			$this->task_plan_id->LinkCustomAttributes = "";
			$this->task_plan_id->HrefValue = "";
			$this->task_plan_id->TooltipValue = "";

			// task_lab_id
			$this->task_lab_id->LinkCustomAttributes = "";
			$this->task_lab_id->HrefValue = "";
			$this->task_lab_id->TooltipValue = "";

			// task_code
			$this->task_code->LinkCustomAttributes = "";
			$this->task_code->HrefValue = "";
			$this->task_code->TooltipValue = "";

			// task_name
			$this->task_name->LinkCustomAttributes = "";
			$this->task_name->HrefValue = "";
			$this->task_name->TooltipValue = "";

			// task_from
			$this->task_from->LinkCustomAttributes = "";
			$this->task_from->HrefValue = "";
			$this->task_from->TooltipValue = "";

			// task_to
			$this->task_to->LinkCustomAttributes = "";
			$this->task_to->HrefValue = "";
			$this->task_to->TooltipValue = "";

			// task_employee_id
			$this->task_employee_id->LinkCustomAttributes = "";
			$this->task_employee_id->HrefValue = "";
			$this->task_employee_id->TooltipValue = "";

			// task_coordinator_id
			$this->task_coordinator_id->LinkCustomAttributes = "";
			$this->task_coordinator_id->HrefValue = "";
			$this->task_coordinator_id->TooltipValue = "";

			// task_object
			$this->task_object->LinkCustomAttributes = "";
			$this->task_object->HrefValue = "";
			$this->task_object->TooltipValue = "";

			// task_status_id
			$this->task_status_id->LinkCustomAttributes = "";
			$this->task_status_id->HrefValue = "";

			// task_hours_planned
			$this->task_hours_planned->LinkCustomAttributes = "";
			$this->task_hours_planned->HrefValue = "";
			$this->task_hours_planned->TooltipValue = "";

			// task_hours_actual
			$this->task_hours_actual->LinkCustomAttributes = "";
			$this->task_hours_actual->HrefValue = "";
			$this->task_hours_actual->TooltipValue = "";

			// task_description
			$this->task_description->LinkCustomAttributes = "";
			$this->task_description->HrefValue = "";

			// task_key
			$this->task_key->LinkCustomAttributes = "";
			$this->task_key->HrefValue = "";
			$this->task_key->TooltipValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("works", $DetailTblVar) && $GLOBALS["works"]->DetailEdit) {
			if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid(); // get detail page object
			$GLOBALS["works_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Begin transaction
			if ($this->getCurrentDetailTable() <> "")
				$conn->BeginTrans();

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// task_status_id
			$this->task_status_id->SetDbValueDef($rsnew, $this->task_status_id->CurrentValue, NULL, $this->task_status_id->ReadOnly);

			// task_description
			$this->task_description->SetDbValueDef($rsnew, $this->task_description->CurrentValue, NULL, $this->task_description->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}

				// Update detail records
				$DetailTblVar = explode(",", $this->getCurrentDetailTable());
				if ($EditRow) {
					if (in_array("works", $DetailTblVar) && $GLOBALS["works"]->DetailEdit) {
						if (!isset($GLOBALS["works_grid"])) $GLOBALS["works_grid"] = new cworks_grid(); // Get detail page object
						$EditRow = $GLOBALS["works_grid"]->GridUpdate();
					}
				}

				// Commit/Rollback transaction
				if ($this->getCurrentDetailTable() <> "") {
					if ($EditRow) {
						$conn->CommitTrans(); // Commit transaction
					} else {
						$conn->RollbackTrans(); // Rollback transaction
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->task_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->task_employee_id->setSessionValue($this->task_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "plans") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_plan_id"] <> "") {
					$GLOBALS["plans"]->plan_id->setQueryStringValue($_GET["fk_plan_id"]);
					$this->task_plan_id->setQueryStringValue($GLOBALS["plans"]->plan_id->QueryStringValue);
					$this->task_plan_id->setSessionValue($this->task_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["plans"]->plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_plan_project_id"] <> "") {
					$GLOBALS["plans"]->plan_project_id->setQueryStringValue($_GET["fk_plan_project_id"]);
					$this->task_project_id->setQueryStringValue($GLOBALS["plans"]->plan_project_id->QueryStringValue);
					$this->task_project_id->setSessionValue($this->task_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["plans"]->plan_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->task_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->task_employee_id->setSessionValue($this->task_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "plans") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_plan_id"] <> "") {
					$GLOBALS["plans"]->plan_id->setFormValue($_POST["fk_plan_id"]);
					$this->task_plan_id->setFormValue($GLOBALS["plans"]->plan_id->FormValue);
					$this->task_plan_id->setSessionValue($this->task_plan_id->FormValue);
					if (!is_numeric($GLOBALS["plans"]->plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_plan_project_id"] <> "") {
					$GLOBALS["plans"]->plan_project_id->setFormValue($_POST["fk_plan_project_id"]);
					$this->task_project_id->setFormValue($GLOBALS["plans"]->plan_project_id->FormValue);
					$this->task_project_id->setSessionValue($this->task_project_id->FormValue);
					if (!is_numeric($GLOBALS["plans"]->plan_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);
			$this->setSessionWhere($this->GetDetailFilter());

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "v_employees") {
				if ($this->task_employee_id->CurrentValue == "") $this->task_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "plans") {
				if ($this->task_plan_id->CurrentValue == "") $this->task_plan_id->setSessionValue("");
				if ($this->task_project_id->CurrentValue == "") $this->task_project_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("works", $DetailTblVar)) {
				if (!isset($GLOBALS["works_grid"]))
					$GLOBALS["works_grid"] = new cworks_grid;
				if ($GLOBALS["works_grid"]->DetailEdit) {
					$GLOBALS["works_grid"]->CurrentMode = "edit";
					$GLOBALS["works_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["works_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["works_grid"]->setStartRecordNumber(1);
					$GLOBALS["works_grid"]->work_task_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_task_id->CurrentValue = $this->task_id->CurrentValue;
					$GLOBALS["works_grid"]->work_task_id->setSessionValue($GLOBALS["works_grid"]->work_task_id->CurrentValue);
					$GLOBALS["works_grid"]->work_project_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_project_id->CurrentValue = $this->task_project_id->CurrentValue;
					$GLOBALS["works_grid"]->work_project_id->setSessionValue($GLOBALS["works_grid"]->work_project_id->CurrentValue);
					$GLOBALS["works_grid"]->work_plan_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_plan_id->CurrentValue = $this->task_plan_id->CurrentValue;
					$GLOBALS["works_grid"]->work_plan_id->setSessionValue($GLOBALS["works_grid"]->work_plan_id->CurrentValue);
					$GLOBALS["works_grid"]->work_lab_id->FldIsDetailKey = TRUE;
					$GLOBALS["works_grid"]->work_lab_id->CurrentValue = $this->task_lab_id->CurrentValue;
					$GLOBALS["works_grid"]->work_lab_id->setSessionValue($GLOBALS["works_grid"]->work_lab_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("e_taskslist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'e_tasks';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		global $Language;
		if (!$this->AuditTrailOnEdit) return;
		$table = 'e_tasks';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['task_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") { // Password Field
						$oldvalue = $Language->Phrase("PasswordMask");
						$newvalue = $Language->Phrase("PasswordMask");
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($e_tasks_edit)) $e_tasks_edit = new ce_tasks_edit();

// Page init
$e_tasks_edit->Page_Init();

// Page main
$e_tasks_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$e_tasks_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fe_tasksedit = new ew_Form("fe_tasksedit", "edit");

// Validate form
fe_tasksedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fe_tasksedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fe_tasksedit.ValidateRequired = true;
<?php } else { ?>
fe_tasksedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fe_tasksedit.Lists["x_task_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_task_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fe_tasksedit.Lists["x_task_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasksedit.Lists["x_task_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_task_employee_id"],"FilterFields":[],"Options":[],"Template":""};
fe_tasksedit.Lists["x_task_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasksedit.Lists["x_task_coordinator_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fe_tasksedit.Lists["x_task_status_id"] = {"LinkField":"x_task_status_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_status_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $e_tasks_edit->ShowPageHeader(); ?>
<?php
$e_tasks_edit->ShowMessage();
?>
<form name="fe_tasksedit" id="fe_tasksedit" class="<?php echo $e_tasks_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($e_tasks_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $e_tasks_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="e_tasks">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($e_tasks->getCurrentMasterTable() == "v_employees") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="v_employees">
<input type="hidden" name="fk_employee_id" value="<?php echo $e_tasks->task_employee_id->getSessionValue() ?>">
<?php } ?>
<?php if ($e_tasks->getCurrentMasterTable() == "plans") { ?>
<input type="hidden" name="<?php echo EW_TABLE_SHOW_MASTER ?>" value="plans">
<input type="hidden" name="fk_plan_id" value="<?php echo $e_tasks->task_plan_id->getSessionValue() ?>">
<input type="hidden" name="fk_plan_project_id" value="<?php echo $e_tasks->task_project_id->getSessionValue() ?>">
<?php } ?>
<div>
<?php if ($e_tasks->task_project_id->Visible) { // task_project_id ?>
	<div id="r_task_project_id" class="form-group">
		<label id="elh_e_tasks_task_project_id" for="x_task_project_id" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_project_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_project_id->CellAttributes() ?>>
<span id="el_e_tasks_task_project_id">
<span<?php echo $e_tasks->task_project_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_project_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_project_id" name="x_task_project_id" id="x_task_project_id" value="<?php echo ew_HtmlEncode($e_tasks->task_project_id->CurrentValue) ?>">
<?php echo $e_tasks->task_project_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_plan_id->Visible) { // task_plan_id ?>
	<div id="r_task_plan_id" class="form-group">
		<label id="elh_e_tasks_task_plan_id" for="x_task_plan_id" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_plan_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_plan_id->CellAttributes() ?>>
<span id="el_e_tasks_task_plan_id">
<span<?php echo $e_tasks->task_plan_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_plan_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_plan_id" name="x_task_plan_id" id="x_task_plan_id" value="<?php echo ew_HtmlEncode($e_tasks->task_plan_id->CurrentValue) ?>">
<?php echo $e_tasks->task_plan_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_lab_id->Visible) { // task_lab_id ?>
	<div id="r_task_lab_id" class="form-group">
		<label id="elh_e_tasks_task_lab_id" for="x_task_lab_id" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_lab_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_lab_id->CellAttributes() ?>>
<span id="el_e_tasks_task_lab_id">
<span<?php echo $e_tasks->task_lab_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_lab_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_lab_id" name="x_task_lab_id" id="x_task_lab_id" value="<?php echo ew_HtmlEncode($e_tasks->task_lab_id->CurrentValue) ?>">
<?php echo $e_tasks->task_lab_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_code->Visible) { // task_code ?>
	<div id="r_task_code" class="form-group">
		<label id="elh_e_tasks_task_code" for="x_task_code" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_code->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_code->CellAttributes() ?>>
<span id="el_e_tasks_task_code">
<span<?php echo $e_tasks->task_code->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_code->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_code" name="x_task_code" id="x_task_code" value="<?php echo ew_HtmlEncode($e_tasks->task_code->CurrentValue) ?>">
<?php echo $e_tasks->task_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_name->Visible) { // task_name ?>
	<div id="r_task_name" class="form-group">
		<label id="elh_e_tasks_task_name" for="x_task_name" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_name->CellAttributes() ?>>
<span id="el_e_tasks_task_name">
<span<?php echo $e_tasks->task_name->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_name->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_name" name="x_task_name" id="x_task_name" value="<?php echo ew_HtmlEncode($e_tasks->task_name->CurrentValue) ?>">
<?php echo $e_tasks->task_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_from->Visible) { // task_from ?>
	<div id="r_task_from" class="form-group">
		<label id="elh_e_tasks_task_from" for="x_task_from" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_from->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_from->CellAttributes() ?>>
<span id="el_e_tasks_task_from">
<span<?php echo $e_tasks->task_from->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_from->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_from" name="x_task_from" id="x_task_from" value="<?php echo ew_HtmlEncode($e_tasks->task_from->CurrentValue) ?>">
<?php echo $e_tasks->task_from->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_to->Visible) { // task_to ?>
	<div id="r_task_to" class="form-group">
		<label id="elh_e_tasks_task_to" for="x_task_to" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_to->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_to->CellAttributes() ?>>
<span id="el_e_tasks_task_to">
<span<?php echo $e_tasks->task_to->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_to->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_to" name="x_task_to" id="x_task_to" value="<?php echo ew_HtmlEncode($e_tasks->task_to->CurrentValue) ?>">
<?php echo $e_tasks->task_to->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_employee_id->Visible) { // task_employee_id ?>
	<div id="r_task_employee_id" class="form-group">
		<label id="elh_e_tasks_task_employee_id" for="x_task_employee_id" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_employee_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_employee_id->CellAttributes() ?>>
<span id="el_e_tasks_task_employee_id">
<span<?php echo $e_tasks->task_employee_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_employee_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_employee_id" name="x_task_employee_id" id="x_task_employee_id" value="<?php echo ew_HtmlEncode($e_tasks->task_employee_id->CurrentValue) ?>">
<?php echo $e_tasks->task_employee_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
	<div id="r_task_coordinator_id" class="form-group">
		<label id="elh_e_tasks_task_coordinator_id" for="x_task_coordinator_id" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_coordinator_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_coordinator_id->CellAttributes() ?>>
<span id="el_e_tasks_task_coordinator_id">
<span<?php echo $e_tasks->task_coordinator_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_coordinator_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_coordinator_id" name="x_task_coordinator_id" id="x_task_coordinator_id" value="<?php echo ew_HtmlEncode($e_tasks->task_coordinator_id->CurrentValue) ?>">
<?php echo $e_tasks->task_coordinator_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_object->Visible) { // task_object ?>
	<div id="r_task_object" class="form-group">
		<label id="elh_e_tasks_task_object" for="x_task_object" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_object->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_object->CellAttributes() ?>>
<span id="el_e_tasks_task_object">
<span<?php echo $e_tasks->task_object->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_object->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_object" name="x_task_object" id="x_task_object" value="<?php echo ew_HtmlEncode($e_tasks->task_object->CurrentValue) ?>">
<?php echo $e_tasks->task_object->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_status_id->Visible) { // task_status_id ?>
	<div id="r_task_status_id" class="form-group">
		<label id="elh_e_tasks_task_status_id" for="x_task_status_id" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_status_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_status_id->CellAttributes() ?>>
<span id="el_e_tasks_task_status_id">
<select data-table="e_tasks" data-field="x_task_status_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($e_tasks->task_status_id->DisplayValueSeparator) ? json_encode($e_tasks->task_status_id->DisplayValueSeparator) : $e_tasks->task_status_id->DisplayValueSeparator) ?>" id="x_task_status_id" name="x_task_status_id"<?php echo $e_tasks->task_status_id->EditAttributes() ?>>
<?php
if (is_array($e_tasks->task_status_id->EditValue)) {
	$arwrk = $e_tasks->task_status_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($e_tasks->task_status_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $e_tasks->task_status_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($e_tasks->task_status_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($e_tasks->task_status_id->CurrentValue) ?>" selected><?php echo $e_tasks->task_status_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
$sSqlWrk = "SELECT `task_status_id`, `task_status_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `task_statuses`";
$sWhereWrk = "";
$e_tasks->task_status_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$e_tasks->task_status_id->LookupFilters += array("f0" => "`task_status_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$e_tasks->Lookup_Selecting($e_tasks->task_status_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $e_tasks->task_status_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_task_status_id" id="s_x_task_status_id" value="<?php echo $e_tasks->task_status_id->LookupFilterQuery() ?>">
</span>
<?php echo $e_tasks->task_status_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_hours_planned->Visible) { // task_hours_planned ?>
	<div id="r_task_hours_planned" class="form-group">
		<label id="elh_e_tasks_task_hours_planned" for="x_task_hours_planned" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_hours_planned->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_hours_planned->CellAttributes() ?>>
<span id="el_e_tasks_task_hours_planned">
<span<?php echo $e_tasks->task_hours_planned->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_hours_planned->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_planned" name="x_task_hours_planned" id="x_task_hours_planned" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_planned->CurrentValue) ?>">
<?php echo $e_tasks->task_hours_planned->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_hours_actual->Visible) { // task_hours_actual ?>
	<div id="r_task_hours_actual" class="form-group">
		<label id="elh_e_tasks_task_hours_actual" for="x_task_hours_actual" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_hours_actual->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_hours_actual->CellAttributes() ?>>
<span id="el_e_tasks_task_hours_actual">
<span<?php echo $e_tasks->task_hours_actual->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_hours_actual->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_hours_actual" name="x_task_hours_actual" id="x_task_hours_actual" value="<?php echo ew_HtmlEncode($e_tasks->task_hours_actual->CurrentValue) ?>">
<?php echo $e_tasks->task_hours_actual->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_description->Visible) { // task_description ?>
	<div id="r_task_description" class="form-group">
		<label id="elh_e_tasks_task_description" for="x_task_description" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_description->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_description->CellAttributes() ?>>
<span id="el_e_tasks_task_description">
<textarea data-table="e_tasks" data-field="x_task_description" name="x_task_description" id="x_task_description" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($e_tasks->task_description->getPlaceHolder()) ?>"<?php echo $e_tasks->task_description->EditAttributes() ?>><?php echo $e_tasks->task_description->EditValue ?></textarea>
</span>
<?php echo $e_tasks->task_description->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($e_tasks->task_key->Visible) { // task_key ?>
	<div id="r_task_key" class="form-group">
		<label id="elh_e_tasks_task_key" for="x_task_key" class="col-sm-2 control-label ewLabel"><?php echo $e_tasks->task_key->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $e_tasks->task_key->CellAttributes() ?>>
<span id="el_e_tasks_task_key">
<span<?php echo $e_tasks->task_key->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $e_tasks->task_key->EditValue ?></p></span>
</span>
<input type="hidden" data-table="e_tasks" data-field="x_task_key" name="x_task_key" id="x_task_key" value="<?php echo ew_HtmlEncode($e_tasks->task_key->CurrentValue) ?>">
<?php echo $e_tasks->task_key->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<input type="hidden" data-table="e_tasks" data-field="x_task_id" name="x_task_id" id="x_task_id" value="<?php echo ew_HtmlEncode($e_tasks->task_id->CurrentValue) ?>">
<?php
	if (in_array("works", explode(",", $e_tasks->getCurrentDetailTable())) && $works->DetailEdit) {
?>
<?php if ($e_tasks->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("works", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "worksgrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $e_tasks_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fe_tasksedit.Init();
</script>
<?php
$e_tasks_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$e_tasks_edit->Page_Terminate();
?>
