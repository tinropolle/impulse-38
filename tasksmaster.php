<?php

// task_project_id
// task_plan_id
// task_lab_id
// task_code
// task_name
// task_from
// task_to
// task_employee_id
// task_coordinator_id
// task_object
// task_status_id
// task_hours_planned
// task_hours_actual
// task_description
// task_key
// task_file

?>
<?php if ($tasks->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $tasks->TableCaption() ?></h4> -->
<table id="tbl_tasksmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $tasks->TableCustomInnerHtml ?>
	<tbody>
<?php if ($tasks->task_project_id->Visible) { // task_project_id ?>
		<tr id="r_task_project_id">
			<td><?php echo $tasks->task_project_id->FldCaption() ?></td>
			<td<?php echo $tasks->task_project_id->CellAttributes() ?>>
<span id="el_tasks_task_project_id">
<span<?php echo $tasks->task_project_id->ViewAttributes() ?>>
<?php echo $tasks->task_project_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_plan_id->Visible) { // task_plan_id ?>
		<tr id="r_task_plan_id">
			<td><?php echo $tasks->task_plan_id->FldCaption() ?></td>
			<td<?php echo $tasks->task_plan_id->CellAttributes() ?>>
<span id="el_tasks_task_plan_id">
<span<?php echo $tasks->task_plan_id->ViewAttributes() ?>>
<?php echo $tasks->task_plan_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_lab_id->Visible) { // task_lab_id ?>
		<tr id="r_task_lab_id">
			<td><?php echo $tasks->task_lab_id->FldCaption() ?></td>
			<td<?php echo $tasks->task_lab_id->CellAttributes() ?>>
<span id="el_tasks_task_lab_id">
<span<?php echo $tasks->task_lab_id->ViewAttributes() ?>>
<?php echo $tasks->task_lab_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_code->Visible) { // task_code ?>
		<tr id="r_task_code">
			<td><?php echo $tasks->task_code->FldCaption() ?></td>
			<td<?php echo $tasks->task_code->CellAttributes() ?>>
<span id="el_tasks_task_code">
<span<?php echo $tasks->task_code->ViewAttributes() ?>>
<?php echo $tasks->task_code->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_name->Visible) { // task_name ?>
		<tr id="r_task_name">
			<td><?php echo $tasks->task_name->FldCaption() ?></td>
			<td<?php echo $tasks->task_name->CellAttributes() ?>>
<span id="el_tasks_task_name">
<span<?php echo $tasks->task_name->ViewAttributes() ?>>
<?php echo $tasks->task_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_from->Visible) { // task_from ?>
		<tr id="r_task_from">
			<td><?php echo $tasks->task_from->FldCaption() ?></td>
			<td<?php echo $tasks->task_from->CellAttributes() ?>>
<span id="el_tasks_task_from">
<span<?php echo $tasks->task_from->ViewAttributes() ?>>
<?php echo $tasks->task_from->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_to->Visible) { // task_to ?>
		<tr id="r_task_to">
			<td><?php echo $tasks->task_to->FldCaption() ?></td>
			<td<?php echo $tasks->task_to->CellAttributes() ?>>
<span id="el_tasks_task_to">
<span<?php echo $tasks->task_to->ViewAttributes() ?>>
<?php echo $tasks->task_to->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_employee_id->Visible) { // task_employee_id ?>
		<tr id="r_task_employee_id">
			<td><?php echo $tasks->task_employee_id->FldCaption() ?></td>
			<td<?php echo $tasks->task_employee_id->CellAttributes() ?>>
<span id="el_tasks_task_employee_id">
<span<?php echo $tasks->task_employee_id->ViewAttributes() ?>>
<?php echo $tasks->task_employee_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<tr id="r_task_coordinator_id">
			<td><?php echo $tasks->task_coordinator_id->FldCaption() ?></td>
			<td<?php echo $tasks->task_coordinator_id->CellAttributes() ?>>
<span id="el_tasks_task_coordinator_id">
<span<?php echo $tasks->task_coordinator_id->ViewAttributes() ?>>
<?php echo $tasks->task_coordinator_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_object->Visible) { // task_object ?>
		<tr id="r_task_object">
			<td><?php echo $tasks->task_object->FldCaption() ?></td>
			<td<?php echo $tasks->task_object->CellAttributes() ?>>
<span id="el_tasks_task_object">
<span<?php echo $tasks->task_object->ViewAttributes() ?>>
<?php echo $tasks->task_object->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_status_id->Visible) { // task_status_id ?>
		<tr id="r_task_status_id">
			<td><?php echo $tasks->task_status_id->FldCaption() ?></td>
			<td<?php echo $tasks->task_status_id->CellAttributes() ?>>
<span id="el_tasks_task_status_id">
<span<?php echo $tasks->task_status_id->ViewAttributes() ?>>
<?php echo $tasks->task_status_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_hours_planned->Visible) { // task_hours_planned ?>
		<tr id="r_task_hours_planned">
			<td><?php echo $tasks->task_hours_planned->FldCaption() ?></td>
			<td<?php echo $tasks->task_hours_planned->CellAttributes() ?>>
<span id="el_tasks_task_hours_planned">
<span<?php echo $tasks->task_hours_planned->ViewAttributes() ?>>
<?php echo $tasks->task_hours_planned->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_hours_actual->Visible) { // task_hours_actual ?>
		<tr id="r_task_hours_actual">
			<td><?php echo $tasks->task_hours_actual->FldCaption() ?></td>
			<td<?php echo $tasks->task_hours_actual->CellAttributes() ?>>
<span id="el_tasks_task_hours_actual">
<span<?php echo $tasks->task_hours_actual->ViewAttributes() ?>>
<?php echo $tasks->task_hours_actual->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_description->Visible) { // task_description ?>
		<tr id="r_task_description">
			<td><?php echo $tasks->task_description->FldCaption() ?></td>
			<td<?php echo $tasks->task_description->CellAttributes() ?>>
<span id="el_tasks_task_description">
<span<?php echo $tasks->task_description->ViewAttributes() ?>>
<?php echo $tasks->task_description->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_key->Visible) { // task_key ?>
		<tr id="r_task_key">
			<td><?php echo $tasks->task_key->FldCaption() ?></td>
			<td<?php echo $tasks->task_key->CellAttributes() ?>>
<span id="el_tasks_task_key">
<span<?php echo $tasks->task_key->ViewAttributes() ?>>
<?php echo $tasks->task_key->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($tasks->task_file->Visible) { // task_file ?>
		<tr id="r_task_file">
			<td><?php echo $tasks->task_file->FldCaption() ?></td>
			<td<?php echo $tasks->task_file->CellAttributes() ?>>
<span id="el_tasks_task_file">
<span<?php echo $tasks->task_file->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($tasks->task_file, $tasks->task_file->ListViewValue()) ?>
</span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
