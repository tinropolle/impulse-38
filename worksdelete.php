<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "worksinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "periodsinfo.php" ?>
<?php include_once "projectsinfo.php" ?>
<?php include_once "tasksinfo.php" ?>
<?php include_once "v_employeesinfo.php" ?>
<?php include_once "e_tasksinfo.php" ?>
<?php include_once "e_employeesinfo.php" ?>
<?php include_once "e_tasks_financeinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$works_delete = NULL; // Initialize page object first

class cworks_delete extends cworks {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'works';

	// Page object name
	var $PageObjName = 'works_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = TRUE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (works)
		if (!isset($GLOBALS["works"]) || get_class($GLOBALS["works"]) == "cworks") {
			$GLOBALS["works"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["works"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Table object (periods)
		if (!isset($GLOBALS['periods'])) $GLOBALS['periods'] = new cperiods();

		// Table object (projects)
		if (!isset($GLOBALS['projects'])) $GLOBALS['projects'] = new cprojects();

		// Table object (tasks)
		if (!isset($GLOBALS['tasks'])) $GLOBALS['tasks'] = new ctasks();

		// Table object (v_employees)
		if (!isset($GLOBALS['v_employees'])) $GLOBALS['v_employees'] = new cv_employees();

		// Table object (e_tasks)
		if (!isset($GLOBALS['e_tasks'])) $GLOBALS['e_tasks'] = new ce_tasks();

		// Table object (e_employees)
		if (!isset($GLOBALS['e_employees'])) $GLOBALS['e_employees'] = new ce_employees();

		// Table object (e_tasks_finance)
		if (!isset($GLOBALS['e_tasks_finance'])) $GLOBALS['e_tasks_finance'] = new ce_tasks_finance();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'works', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate(ew_GetUrl("workslist.php"));
			}
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $works;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($works);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("workslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in works class, worksinfo.php

		$this->CurrentFilter = $sFilter;

		// Check if valid user id
		$conn = &$this->Connection();
		$sql = $this->GetSQL($this->CurrentFilter, "");
		if ($this->Recordset = ew_LoadRecordset($sql, $conn)) {
			$res = TRUE;
			while (!$this->Recordset->EOF) {
				$this->LoadRowValues($this->Recordset);
				if (!$this->ShowOptionLink('delete')) {
					$sUserIdMsg = $Language->Phrase("NoDeletePermission");
					$this->setFailureMessage($sUserIdMsg);
					$res = FALSE;
					break;
				}
				$this->Recordset->MoveNext();
			}
			$this->Recordset->Close();
			if (!$res) $this->Page_Terminate("workslist.php"); // Return to list
		}

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->CurrentAction = "I"; // Display record
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("workslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->work_id->setDbValue($rs->fields('work_id'));
		$this->work_period_id->setDbValue($rs->fields('work_period_id'));
		$this->work_project_id->setDbValue($rs->fields('work_project_id'));
		$this->work_plan_id->setDbValue($rs->fields('work_plan_id'));
		$this->work_lab_id->setDbValue($rs->fields('work_lab_id'));
		$this->work_task_id->setDbValue($rs->fields('work_task_id'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
		$this->work_started->setDbValue($rs->fields('work_started'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->work_id->DbValue = $row['work_id'];
		$this->work_period_id->DbValue = $row['work_period_id'];
		$this->work_project_id->DbValue = $row['work_project_id'];
		$this->work_plan_id->DbValue = $row['work_plan_id'];
		$this->work_lab_id->DbValue = $row['work_lab_id'];
		$this->work_task_id->DbValue = $row['work_task_id'];
		$this->work_description->DbValue = $row['work_description'];
		$this->work_progress->DbValue = $row['work_progress'];
		$this->work_time->DbValue = $row['work_time'];
		$this->work_employee_id->DbValue = $row['work_employee_id'];
		$this->work_started->DbValue = $row['work_started'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->work_time->FormValue == $this->work_time->CurrentValue && is_numeric(ew_StrToFloat($this->work_time->CurrentValue)))
			$this->work_time->CurrentValue = ew_StrToFloat($this->work_time->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

			// work_period_id
			$this->work_period_id->LinkCustomAttributes = "";
			$this->work_period_id->HrefValue = "";
			$this->work_period_id->TooltipValue = "";

			// work_project_id
			$this->work_project_id->LinkCustomAttributes = "";
			$this->work_project_id->HrefValue = "";
			$this->work_project_id->TooltipValue = "";

			// work_plan_id
			$this->work_plan_id->LinkCustomAttributes = "";
			$this->work_plan_id->HrefValue = "";
			$this->work_plan_id->TooltipValue = "";

			// work_lab_id
			$this->work_lab_id->LinkCustomAttributes = "";
			$this->work_lab_id->HrefValue = "";
			$this->work_lab_id->TooltipValue = "";

			// work_task_id
			$this->work_task_id->LinkCustomAttributes = "";
			$this->work_task_id->HrefValue = "";
			$this->work_task_id->TooltipValue = "";

			// work_description
			$this->work_description->LinkCustomAttributes = "";
			$this->work_description->HrefValue = "";
			$this->work_description->TooltipValue = "";

			// work_progress
			$this->work_progress->LinkCustomAttributes = "";
			$this->work_progress->HrefValue = "";
			$this->work_progress->TooltipValue = "";

			// work_time
			$this->work_time->LinkCustomAttributes = "";
			$this->work_time->HrefValue = "";
			$this->work_time->TooltipValue = "";

			// work_employee_id
			$this->work_employee_id->LinkCustomAttributes = "";
			$this->work_employee_id->HrefValue = "";
			$this->work_employee_id->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();
		if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteBegin")); // Batch delete begin

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['work_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
			if ($DeleteRows) {
				foreach ($rsold as $row)
					$this->WriteAuditTrailOnDelete($row);
			}
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteSuccess")); // Batch delete success
		} else {
			$conn->RollbackTrans(); // Rollback changes
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteRollback")); // Batch delete rollback
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->work_employee_id->CurrentValue);
		return TRUE;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setQueryStringValue($_GET["fk_period_id"]);
					$this->work_period_id->setQueryStringValue($GLOBALS["periods"]->period_id->QueryStringValue);
					$this->work_period_id->setSessionValue($this->work_period_id->QueryStringValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setQueryStringValue($_GET["fk_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["projects"]->project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["v_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setQueryStringValue($_GET["fk_employee_id"]);
					$this->work_employee_id->setQueryStringValue($GLOBALS["e_employees"]->employee_id->QueryStringValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setQueryStringValue($_GET["fk_task_project_id"]);
					$this->work_project_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue);
					$this->work_project_id->setSessionValue($this->work_project_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setQueryStringValue($_GET["fk_task_plan_id"]);
					$this->work_plan_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setQueryStringValue($_GET["fk_task_lab_id"]);
					$this->work_lab_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_GET["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setQueryStringValue($_GET["fk_task_id"]);
					$this->work_task_id->setQueryStringValue($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue);
					$this->work_task_id->setSessionValue($this->work_task_id->QueryStringValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "periods") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_period_id"] <> "") {
					$GLOBALS["periods"]->period_id->setFormValue($_POST["fk_period_id"]);
					$this->work_period_id->setFormValue($GLOBALS["periods"]->period_id->FormValue);
					$this->work_period_id->setSessionValue($this->work_period_id->FormValue);
					if (!is_numeric($GLOBALS["periods"]->period_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "projects") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_project_id"] <> "") {
					$GLOBALS["projects"]->project_id->setFormValue($_POST["fk_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["projects"]->project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["projects"]->project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "v_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["v_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["v_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["v_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_employees") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_employee_id"] <> "") {
					$GLOBALS["e_employees"]->employee_id->setFormValue($_POST["fk_employee_id"]);
					$this->work_employee_id->setFormValue($GLOBALS["e_employees"]->employee_id->FormValue);
					$this->work_employee_id->setSessionValue($this->work_employee_id->FormValue);
					if (!is_numeric($GLOBALS["e_employees"]->employee_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "e_tasks_finance") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_task_project_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_project_id->setFormValue($_POST["fk_task_project_id"]);
					$this->work_project_id->setFormValue($GLOBALS["e_tasks_finance"]->task_project_id->FormValue);
					$this->work_project_id->setSessionValue($this->work_project_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_project_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_plan_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_plan_id->setFormValue($_POST["fk_task_plan_id"]);
					$this->work_plan_id->setFormValue($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue);
					$this->work_plan_id->setSessionValue($this->work_plan_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_plan_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_lab_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_lab_id->setFormValue($_POST["fk_task_lab_id"]);
					$this->work_lab_id->setFormValue($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue);
					$this->work_lab_id->setSessionValue($this->work_lab_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_lab_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
				if (@$_POST["fk_task_id"] <> "") {
					$GLOBALS["e_tasks_finance"]->task_id->setFormValue($_POST["fk_task_id"]);
					$this->work_task_id->setFormValue($GLOBALS["e_tasks_finance"]->task_id->FormValue);
					$this->work_task_id->setSessionValue($this->work_task_id->FormValue);
					if (!is_numeric($GLOBALS["e_tasks_finance"]->task_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "periods") {
				if ($this->work_period_id->CurrentValue == "") $this->work_period_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "projects") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "v_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_employees") {
				if ($this->work_employee_id->CurrentValue == "") $this->work_employee_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks") {
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "e_tasks_finance") {
				if ($this->work_project_id->CurrentValue == "") $this->work_project_id->setSessionValue("");
				if ($this->work_plan_id->CurrentValue == "") $this->work_plan_id->setSessionValue("");
				if ($this->work_lab_id->CurrentValue == "") $this->work_lab_id->setSessionValue("");
				if ($this->work_task_id->CurrentValue == "") $this->work_task_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("workslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'works';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnDelete) return;
		$table = 'works';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['work_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$curUser = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$oldvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($works_delete)) $works_delete = new cworks_delete();

// Page init
$works_delete->Page_Init();

// Page main
$works_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$works_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fworksdelete = new ew_Form("fworksdelete", "delete");

// Form_CustomValidate event
fworksdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fworksdelete.ValidateRequired = true;
<?php } else { ?>
fworksdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fworksdelete.Lists["x_work_period_id"] = {"LinkField":"x_period_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_period_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksdelete.Lists["x_work_project_id"] = {"LinkField":"x_project_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_project_name","","",""],"ParentFields":[],"ChildFields":["x_work_plan_id"],"FilterFields":[],"Options":[],"Template":""};
fworksdelete.Lists["x_work_plan_id"] = {"LinkField":"x_plan_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_plan_code","x_plan_name","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworksdelete.Lists["x_work_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":["x_work_task_id"],"FilterFields":[],"Options":[],"Template":""};
fworksdelete.Lists["x_work_task_id"] = {"LinkField":"x_task_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_task_code","x_task_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksdelete.Lists["x_work_progress"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fworksdelete.Lists["x_work_progress"].Options = <?php echo json_encode($works->work_progress->Options()) ?>;
fworksdelete.Lists["x_work_employee_id"] = {"LinkField":"x_employee_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_employee_last_name","x_employee_first_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $works_delete->ShowPageHeader(); ?>
<?php
$works_delete->ShowMessage();
?>
<form name="fworksdelete" id="fworksdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($works_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $works_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="works">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($works_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $works->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($works->work_period_id->Visible) { // work_period_id ?>
		<th><span id="elh_works_work_period_id" class="works_work_period_id"><?php echo $works->work_period_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_project_id->Visible) { // work_project_id ?>
		<th><span id="elh_works_work_project_id" class="works_work_project_id"><?php echo $works->work_project_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
		<th><span id="elh_works_work_plan_id" class="works_work_plan_id"><?php echo $works->work_plan_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
		<th><span id="elh_works_work_lab_id" class="works_work_lab_id"><?php echo $works->work_lab_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_task_id->Visible) { // work_task_id ?>
		<th><span id="elh_works_work_task_id" class="works_work_task_id"><?php echo $works->work_task_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_description->Visible) { // work_description ?>
		<th><span id="elh_works_work_description" class="works_work_description"><?php echo $works->work_description->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_progress->Visible) { // work_progress ?>
		<th><span id="elh_works_work_progress" class="works_work_progress"><?php echo $works->work_progress->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_time->Visible) { // work_time ?>
		<th><span id="elh_works_work_time" class="works_work_time"><?php echo $works->work_time->FldCaption() ?></span></th>
<?php } ?>
<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
		<th><span id="elh_works_work_employee_id" class="works_work_employee_id"><?php echo $works->work_employee_id->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$works_delete->RecCnt = 0;
$i = 0;
while (!$works_delete->Recordset->EOF) {
	$works_delete->RecCnt++;
	$works_delete->RowCnt++;

	// Set row properties
	$works->ResetAttrs();
	$works->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$works_delete->LoadRowValues($works_delete->Recordset);

	// Render row
	$works_delete->RenderRow();
?>
	<tr<?php echo $works->RowAttributes() ?>>
<?php if ($works->work_period_id->Visible) { // work_period_id ?>
		<td<?php echo $works->work_period_id->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_period_id" class="works_work_period_id">
<span<?php echo $works->work_period_id->ViewAttributes() ?>>
<?php echo $works->work_period_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_project_id->Visible) { // work_project_id ?>
		<td<?php echo $works->work_project_id->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_project_id" class="works_work_project_id">
<span<?php echo $works->work_project_id->ViewAttributes() ?>>
<?php echo $works->work_project_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_plan_id->Visible) { // work_plan_id ?>
		<td<?php echo $works->work_plan_id->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_plan_id" class="works_work_plan_id">
<span<?php echo $works->work_plan_id->ViewAttributes() ?>>
<?php echo $works->work_plan_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_lab_id->Visible) { // work_lab_id ?>
		<td<?php echo $works->work_lab_id->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_lab_id" class="works_work_lab_id">
<span<?php echo $works->work_lab_id->ViewAttributes() ?>>
<?php echo $works->work_lab_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_task_id->Visible) { // work_task_id ?>
		<td<?php echo $works->work_task_id->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_task_id" class="works_work_task_id">
<span<?php echo $works->work_task_id->ViewAttributes() ?>>
<?php echo $works->work_task_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_description->Visible) { // work_description ?>
		<td<?php echo $works->work_description->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_description" class="works_work_description">
<span<?php echo $works->work_description->ViewAttributes() ?>>
<?php echo $works->work_description->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_progress->Visible) { // work_progress ?>
		<td<?php echo $works->work_progress->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_progress" class="works_work_progress">
<span<?php echo $works->work_progress->ViewAttributes() ?>>
<?php echo $works->work_progress->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_time->Visible) { // work_time ?>
		<td<?php echo $works->work_time->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_time" class="works_work_time">
<span<?php echo $works->work_time->ViewAttributes() ?>>
<?php echo $works->work_time->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($works->work_employee_id->Visible) { // work_employee_id ?>
		<td<?php echo $works->work_employee_id->CellAttributes() ?>>
<span id="el<?php echo $works_delete->RowCnt ?>_works_work_employee_id" class="works_work_employee_id">
<span<?php echo $works->work_employee_id->ViewAttributes() ?>>
<?php echo $works->work_employee_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$works_delete->Recordset->MoveNext();
}
$works_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $works_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fworksdelete.Init();
</script>
<?php
$works_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$works_delete->Page_Terminate();
?>
