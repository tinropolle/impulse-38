<?php

// position_name
?>
<?php if ($positions->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $positions->TableCaption() ?></h4> -->
<table id="tbl_positionsmaster" class="table table-bordered table-striped ewViewTable">
<?php echo $positions->TableCustomInnerHtml ?>
	<tbody>
<?php if ($positions->position_name->Visible) { // position_name ?>
		<tr id="r_position_name">
			<td><?php echo $positions->position_name->FldCaption() ?></td>
			<td<?php echo $positions->position_name->CellAttributes() ?>>
<span id="el_positions_position_name">
<span<?php echo $positions->position_name->ViewAttributes() ?>>
<?php echo $positions->position_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
