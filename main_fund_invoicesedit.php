<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "main_fund_invoicesinfo.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$main_fund_invoices_edit = NULL; // Initialize page object first

class cmain_fund_invoices_edit extends cmain_fund_invoices {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'main_fund_invoices';

	// Page object name
	var $PageObjName = 'main_fund_invoices_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = TRUE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (main_fund_invoices)
		if (!isset($GLOBALS["main_fund_invoices"]) || get_class($GLOBALS["main_fund_invoices"]) == "cmain_fund_invoices") {
			$GLOBALS["main_fund_invoices"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["main_fund_invoices"];
		}

		// Table object (employees)
		if (!isset($GLOBALS['employees'])) $GLOBALS['employees'] = new cemployees();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'main_fund_invoices', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("main_fund_invoiceslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->main_fund_invoice_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $main_fund_invoices;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($main_fund_invoices);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["main_fund_invoice_id"] <> "") {
			$this->main_fund_invoice_id->setQueryStringValue($_GET["main_fund_invoice_id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->main_fund_invoice_id->CurrentValue == "")
			$this->Page_Terminate("main_fund_invoiceslist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("main_fund_invoiceslist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "main_fund_invoiceslist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->main_fund_invoice_id->FldIsDetailKey)
			$this->main_fund_invoice_id->setFormValue($objForm->GetValue("x_main_fund_invoice_id"));
		if (!$this->main_fund_invoice_description->FldIsDetailKey) {
			$this->main_fund_invoice_description->setFormValue($objForm->GetValue("x_main_fund_invoice_description"));
		}
		if (!$this->main_fund_invoice_amount->FldIsDetailKey) {
			$this->main_fund_invoice_amount->setFormValue($objForm->GetValue("x_main_fund_invoice_amount"));
		}
		if (!$this->main_fund_invoice_dt->FldIsDetailKey) {
			$this->main_fund_invoice_dt->setFormValue($objForm->GetValue("x_main_fund_invoice_dt"));
			$this->main_fund_invoice_dt->CurrentValue = ew_UnFormatDateTime($this->main_fund_invoice_dt->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->main_fund_invoice_id->CurrentValue = $this->main_fund_invoice_id->FormValue;
		$this->main_fund_invoice_description->CurrentValue = $this->main_fund_invoice_description->FormValue;
		$this->main_fund_invoice_amount->CurrentValue = $this->main_fund_invoice_amount->FormValue;
		$this->main_fund_invoice_dt->CurrentValue = $this->main_fund_invoice_dt->FormValue;
		$this->main_fund_invoice_dt->CurrentValue = ew_UnFormatDateTime($this->main_fund_invoice_dt->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->main_fund_invoice_id->setDbValue($rs->fields('main_fund_invoice_id'));
		$this->main_fund_invoice_description->setDbValue($rs->fields('main_fund_invoice_description'));
		$this->main_fund_invoice_amount->setDbValue($rs->fields('main_fund_invoice_amount'));
		$this->main_fund_invoice_dt->setDbValue($rs->fields('main_fund_invoice_dt'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->main_fund_invoice_id->DbValue = $row['main_fund_invoice_id'];
		$this->main_fund_invoice_description->DbValue = $row['main_fund_invoice_description'];
		$this->main_fund_invoice_amount->DbValue = $row['main_fund_invoice_amount'];
		$this->main_fund_invoice_dt->DbValue = $row['main_fund_invoice_dt'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->main_fund_invoice_amount->FormValue == $this->main_fund_invoice_amount->CurrentValue && is_numeric(ew_StrToFloat($this->main_fund_invoice_amount->CurrentValue)))
			$this->main_fund_invoice_amount->CurrentValue = ew_StrToFloat($this->main_fund_invoice_amount->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// main_fund_invoice_id
		// main_fund_invoice_description
		// main_fund_invoice_amount
		// main_fund_invoice_dt

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// main_fund_invoice_id
		$this->main_fund_invoice_id->ViewValue = $this->main_fund_invoice_id->CurrentValue;
		$this->main_fund_invoice_id->ViewCustomAttributes = "";

		// main_fund_invoice_description
		$this->main_fund_invoice_description->ViewValue = $this->main_fund_invoice_description->CurrentValue;
		$this->main_fund_invoice_description->ViewCustomAttributes = "";

		// main_fund_invoice_amount
		$this->main_fund_invoice_amount->ViewValue = $this->main_fund_invoice_amount->CurrentValue;
		$this->main_fund_invoice_amount->ViewCustomAttributes = "";

		// main_fund_invoice_dt
		$this->main_fund_invoice_dt->ViewValue = $this->main_fund_invoice_dt->CurrentValue;
		$this->main_fund_invoice_dt->ViewValue = ew_FormatDateTime($this->main_fund_invoice_dt->ViewValue, 7);
		$this->main_fund_invoice_dt->ViewCustomAttributes = "";

			// main_fund_invoice_id
			$this->main_fund_invoice_id->LinkCustomAttributes = "";
			$this->main_fund_invoice_id->HrefValue = "";
			$this->main_fund_invoice_id->TooltipValue = "";

			// main_fund_invoice_description
			$this->main_fund_invoice_description->LinkCustomAttributes = "";
			$this->main_fund_invoice_description->HrefValue = "";
			$this->main_fund_invoice_description->TooltipValue = "";

			// main_fund_invoice_amount
			$this->main_fund_invoice_amount->LinkCustomAttributes = "";
			$this->main_fund_invoice_amount->HrefValue = "";
			$this->main_fund_invoice_amount->TooltipValue = "";

			// main_fund_invoice_dt
			$this->main_fund_invoice_dt->LinkCustomAttributes = "";
			$this->main_fund_invoice_dt->HrefValue = "";
			$this->main_fund_invoice_dt->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// main_fund_invoice_id
			$this->main_fund_invoice_id->EditAttrs["class"] = "form-control";
			$this->main_fund_invoice_id->EditCustomAttributes = "";
			$this->main_fund_invoice_id->EditValue = $this->main_fund_invoice_id->CurrentValue;
			$this->main_fund_invoice_id->ViewCustomAttributes = "";

			// main_fund_invoice_description
			$this->main_fund_invoice_description->EditAttrs["class"] = "form-control";
			$this->main_fund_invoice_description->EditCustomAttributes = "";
			$this->main_fund_invoice_description->EditValue = ew_HtmlEncode($this->main_fund_invoice_description->CurrentValue);
			$this->main_fund_invoice_description->PlaceHolder = ew_RemoveHtml($this->main_fund_invoice_description->FldCaption());

			// main_fund_invoice_amount
			$this->main_fund_invoice_amount->EditAttrs["class"] = "form-control";
			$this->main_fund_invoice_amount->EditCustomAttributes = "";
			$this->main_fund_invoice_amount->EditValue = $this->main_fund_invoice_amount->CurrentValue;
			$this->main_fund_invoice_amount->ViewCustomAttributes = "";

			// main_fund_invoice_dt
			$this->main_fund_invoice_dt->EditAttrs["class"] = "form-control";
			$this->main_fund_invoice_dt->EditCustomAttributes = "";
			$this->main_fund_invoice_dt->EditValue = $this->main_fund_invoice_dt->CurrentValue;
			$this->main_fund_invoice_dt->EditValue = ew_FormatDateTime($this->main_fund_invoice_dt->EditValue, 7);
			$this->main_fund_invoice_dt->ViewCustomAttributes = "";

			// Edit refer script
			// main_fund_invoice_id

			$this->main_fund_invoice_id->LinkCustomAttributes = "";
			$this->main_fund_invoice_id->HrefValue = "";

			// main_fund_invoice_description
			$this->main_fund_invoice_description->LinkCustomAttributes = "";
			$this->main_fund_invoice_description->HrefValue = "";

			// main_fund_invoice_amount
			$this->main_fund_invoice_amount->LinkCustomAttributes = "";
			$this->main_fund_invoice_amount->HrefValue = "";
			$this->main_fund_invoice_amount->TooltipValue = "";

			// main_fund_invoice_dt
			$this->main_fund_invoice_dt->LinkCustomAttributes = "";
			$this->main_fund_invoice_dt->HrefValue = "";
			$this->main_fund_invoice_dt->TooltipValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->main_fund_invoice_description->FldIsDetailKey && !is_null($this->main_fund_invoice_description->FormValue) && $this->main_fund_invoice_description->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->main_fund_invoice_description->FldCaption(), $this->main_fund_invoice_description->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// main_fund_invoice_description
			$this->main_fund_invoice_description->SetDbValueDef($rsnew, $this->main_fund_invoice_description->CurrentValue, "", $this->main_fund_invoice_description->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("main_fund_invoiceslist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'main_fund_invoices';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		global $Language;
		if (!$this->AuditTrailOnEdit) return;
		$table = 'main_fund_invoices';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['main_fund_invoice_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") { // Password Field
						$oldvalue = $Language->Phrase("PasswordMask");
						$newvalue = $Language->Phrase("PasswordMask");
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($main_fund_invoices_edit)) $main_fund_invoices_edit = new cmain_fund_invoices_edit();

// Page init
$main_fund_invoices_edit->Page_Init();

// Page main
$main_fund_invoices_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$main_fund_invoices_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fmain_fund_invoicesedit = new ew_Form("fmain_fund_invoicesedit", "edit");

// Validate form
fmain_fund_invoicesedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_main_fund_invoice_description");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $main_fund_invoices->main_fund_invoice_description->FldCaption(), $main_fund_invoices->main_fund_invoice_description->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fmain_fund_invoicesedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fmain_fund_invoicesedit.ValidateRequired = true;
<?php } else { ?>
fmain_fund_invoicesedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $main_fund_invoices_edit->ShowPageHeader(); ?>
<?php
$main_fund_invoices_edit->ShowMessage();
?>
<form name="fmain_fund_invoicesedit" id="fmain_fund_invoicesedit" class="<?php echo $main_fund_invoices_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($main_fund_invoices_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $main_fund_invoices_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="main_fund_invoices">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div>
<?php if ($main_fund_invoices->main_fund_invoice_id->Visible) { // main_fund_invoice_id ?>
	<div id="r_main_fund_invoice_id" class="form-group">
		<label id="elh_main_fund_invoices_main_fund_invoice_id" class="col-sm-2 control-label ewLabel"><?php echo $main_fund_invoices->main_fund_invoice_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $main_fund_invoices->main_fund_invoice_id->CellAttributes() ?>>
<span id="el_main_fund_invoices_main_fund_invoice_id">
<span<?php echo $main_fund_invoices->main_fund_invoice_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $main_fund_invoices->main_fund_invoice_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="main_fund_invoices" data-field="x_main_fund_invoice_id" name="x_main_fund_invoice_id" id="x_main_fund_invoice_id" value="<?php echo ew_HtmlEncode($main_fund_invoices->main_fund_invoice_id->CurrentValue) ?>">
<?php echo $main_fund_invoices->main_fund_invoice_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($main_fund_invoices->main_fund_invoice_description->Visible) { // main_fund_invoice_description ?>
	<div id="r_main_fund_invoice_description" class="form-group">
		<label id="elh_main_fund_invoices_main_fund_invoice_description" for="x_main_fund_invoice_description" class="col-sm-2 control-label ewLabel"><?php echo $main_fund_invoices->main_fund_invoice_description->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $main_fund_invoices->main_fund_invoice_description->CellAttributes() ?>>
<span id="el_main_fund_invoices_main_fund_invoice_description">
<input type="text" data-table="main_fund_invoices" data-field="x_main_fund_invoice_description" name="x_main_fund_invoice_description" id="x_main_fund_invoice_description" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($main_fund_invoices->main_fund_invoice_description->getPlaceHolder()) ?>" value="<?php echo $main_fund_invoices->main_fund_invoice_description->EditValue ?>"<?php echo $main_fund_invoices->main_fund_invoice_description->EditAttributes() ?>>
</span>
<?php echo $main_fund_invoices->main_fund_invoice_description->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($main_fund_invoices->main_fund_invoice_amount->Visible) { // main_fund_invoice_amount ?>
	<div id="r_main_fund_invoice_amount" class="form-group">
		<label id="elh_main_fund_invoices_main_fund_invoice_amount" for="x_main_fund_invoice_amount" class="col-sm-2 control-label ewLabel"><?php echo $main_fund_invoices->main_fund_invoice_amount->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $main_fund_invoices->main_fund_invoice_amount->CellAttributes() ?>>
<span id="el_main_fund_invoices_main_fund_invoice_amount">
<span<?php echo $main_fund_invoices->main_fund_invoice_amount->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $main_fund_invoices->main_fund_invoice_amount->EditValue ?></p></span>
</span>
<input type="hidden" data-table="main_fund_invoices" data-field="x_main_fund_invoice_amount" name="x_main_fund_invoice_amount" id="x_main_fund_invoice_amount" value="<?php echo ew_HtmlEncode($main_fund_invoices->main_fund_invoice_amount->CurrentValue) ?>">
<?php echo $main_fund_invoices->main_fund_invoice_amount->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($main_fund_invoices->main_fund_invoice_dt->Visible) { // main_fund_invoice_dt ?>
	<div id="r_main_fund_invoice_dt" class="form-group">
		<label id="elh_main_fund_invoices_main_fund_invoice_dt" for="x_main_fund_invoice_dt" class="col-sm-2 control-label ewLabel"><?php echo $main_fund_invoices->main_fund_invoice_dt->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $main_fund_invoices->main_fund_invoice_dt->CellAttributes() ?>>
<span id="el_main_fund_invoices_main_fund_invoice_dt">
<span<?php echo $main_fund_invoices->main_fund_invoice_dt->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $main_fund_invoices->main_fund_invoice_dt->EditValue ?></p></span>
</span>
<input type="hidden" data-table="main_fund_invoices" data-field="x_main_fund_invoice_dt" name="x_main_fund_invoice_dt" id="x_main_fund_invoice_dt" value="<?php echo ew_HtmlEncode($main_fund_invoices->main_fund_invoice_dt->CurrentValue) ?>">
<?php echo $main_fund_invoices->main_fund_invoice_dt->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $main_fund_invoices_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
fmain_fund_invoicesedit.Init();
</script>
<?php
$main_fund_invoices_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$main_fund_invoices_edit->Page_Terminate();
?>
