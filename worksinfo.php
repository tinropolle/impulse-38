<?php

// Global variable for table object
$works = NULL;

//
// Table class for works
//
class cworks extends cTable {
	var $work_id;
	var $work_period_id;
	var $work_project_id;
	var $work_plan_id;
	var $work_lab_id;
	var $work_task_id;
	var $work_description;
	var $work_progress;
	var $work_time;
	var $work_employee_id;
	var $work_started;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'works';
		$this->TableName = 'works';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`works`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// work_id
		$this->work_id = new cField('works', 'works', 'x_work_id', 'work_id', '`work_id`', '`work_id`', 3, -1, FALSE, '`work_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->work_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_id'] = &$this->work_id;

		// work_period_id
		$this->work_period_id = new cField('works', 'works', 'x_work_period_id', 'work_period_id', '`work_period_id`', '`work_period_id`', 3, -1, FALSE, '`work_period_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_period_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_period_id'] = &$this->work_period_id;

		// work_project_id
		$this->work_project_id = new cField('works', 'works', 'x_work_project_id', 'work_project_id', '`work_project_id`', '`work_project_id`', 3, -1, FALSE, '`work_project_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_project_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_project_id'] = &$this->work_project_id;

		// work_plan_id
		$this->work_plan_id = new cField('works', 'works', 'x_work_plan_id', 'work_plan_id', '`work_plan_id`', '`work_plan_id`', 3, -1, FALSE, '`work_plan_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_plan_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_plan_id'] = &$this->work_plan_id;

		// work_lab_id
		$this->work_lab_id = new cField('works', 'works', 'x_work_lab_id', 'work_lab_id', '`work_lab_id`', '`work_lab_id`', 3, -1, FALSE, '`work_lab_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_lab_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_lab_id'] = &$this->work_lab_id;

		// work_task_id
		$this->work_task_id = new cField('works', 'works', 'x_work_task_id', 'work_task_id', '`work_task_id`', '`work_task_id`', 3, -1, FALSE, '`work_task_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_task_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_task_id'] = &$this->work_task_id;

		// work_description
		$this->work_description = new cField('works', 'works', 'x_work_description', 'work_description', '`work_description`', '`work_description`', 201, -1, FALSE, '`work_description`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->fields['work_description'] = &$this->work_description;

		// work_progress
		$this->work_progress = new cField('works', 'works', 'x_work_progress', 'work_progress', '`work_progress`', '`work_progress`', 3, -1, FALSE, '`work_progress`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_progress->OptionCount = 22;
		$this->work_progress->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_progress'] = &$this->work_progress;

		// work_time
		$this->work_time = new cField('works', 'works', 'x_work_time', 'work_time', '`work_time`', '`work_time`', 5, -1, FALSE, '`work_time`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->work_time->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['work_time'] = &$this->work_time;

		// work_employee_id
		$this->work_employee_id = new cField('works', 'works', 'x_work_employee_id', 'work_employee_id', '`work_employee_id`', '`work_employee_id`', 3, -1, FALSE, '`work_employee_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->work_employee_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['work_employee_id'] = &$this->work_employee_id;

		// work_started
		$this->work_started = new cField('works', 'works', 'x_work_started', 'work_started', '`work_started`', 'DATE_FORMAT(`work_started`, \'%d-%m-%Y\')', 135, 7, FALSE, '`work_started`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->work_started->FldDefaultErrMsg = str_replace("%s", "-", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['work_started'] = &$this->work_started;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Current master table name
	function getCurrentMasterTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE];
	}

	function setCurrentMasterTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE] = $v;
	}

	// Session master WHERE clause
	function GetMasterFilter() {

		// Master filter
		$sMasterFilter = "";
		if ($this->getCurrentMasterTable() == "periods") {
			if ($this->work_period_id->getSessionValue() <> "")
				$sMasterFilter .= "`period_id`=" . ew_QuotedValue($this->work_period_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "projects") {
			if ($this->work_project_id->getSessionValue() <> "")
				$sMasterFilter .= "`project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "tasks") {
			if ($this->work_task_id->getSessionValue() <> "")
				$sMasterFilter .= "`task_id`=" . ew_QuotedValue($this->work_task_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_plan_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_plan_id`=" . ew_QuotedValue($this->work_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_lab_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_lab_id`=" . ew_QuotedValue($this->work_lab_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_project_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "v_employees") {
			if ($this->work_employee_id->getSessionValue() <> "")
				$sMasterFilter .= "`employee_id`=" . ew_QuotedValue($this->work_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "e_employees") {
			if ($this->work_employee_id->getSessionValue() <> "")
				$sMasterFilter .= "`employee_id`=" . ew_QuotedValue($this->work_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "e_tasks") {
			if ($this->work_task_id->getSessionValue() <> "")
				$sMasterFilter .= "`task_id`=" . ew_QuotedValue($this->work_task_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_project_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_plan_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_plan_id`=" . ew_QuotedValue($this->work_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_lab_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_lab_id`=" . ew_QuotedValue($this->work_lab_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "e_tasks_finance") {
			if ($this->work_project_id->getSessionValue() <> "")
				$sMasterFilter .= "`task_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_plan_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_plan_id`=" . ew_QuotedValue($this->work_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_lab_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_lab_id`=" . ew_QuotedValue($this->work_lab_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_task_id->getSessionValue() <> "")
				$sMasterFilter .= " AND `task_id`=" . ew_QuotedValue($this->work_task_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sMasterFilter;
	}

	// Session detail WHERE clause
	function GetDetailFilter() {

		// Detail filter
		$sDetailFilter = "";
		if ($this->getCurrentMasterTable() == "periods") {
			if ($this->work_period_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_period_id`=" . ew_QuotedValue($this->work_period_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "projects") {
			if ($this->work_project_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "tasks") {
			if ($this->work_task_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_task_id`=" . ew_QuotedValue($this->work_task_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_plan_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_plan_id`=" . ew_QuotedValue($this->work_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_lab_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_lab_id`=" . ew_QuotedValue($this->work_lab_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_project_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "v_employees") {
			if ($this->work_employee_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_employee_id`=" . ew_QuotedValue($this->work_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "e_employees") {
			if ($this->work_employee_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_employee_id`=" . ew_QuotedValue($this->work_employee_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "e_tasks") {
			if ($this->work_task_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_task_id`=" . ew_QuotedValue($this->work_task_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_project_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_plan_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_plan_id`=" . ew_QuotedValue($this->work_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_lab_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_lab_id`=" . ew_QuotedValue($this->work_lab_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "e_tasks_finance") {
			if ($this->work_project_id->getSessionValue() <> "")
				$sDetailFilter .= "`work_project_id`=" . ew_QuotedValue($this->work_project_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_plan_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_plan_id`=" . ew_QuotedValue($this->work_plan_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_lab_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_lab_id`=" . ew_QuotedValue($this->work_lab_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
			if ($this->work_task_id->getSessionValue() <> "")
				$sDetailFilter .= " AND `work_task_id`=" . ew_QuotedValue($this->work_task_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sDetailFilter;
	}

	// Master filter
	function SqlMasterFilter_periods() {
		return "`period_id`=@period_id@";
	}

	// Detail filter
	function SqlDetailFilter_periods() {
		return "`work_period_id`=@work_period_id@";
	}

	// Master filter
	function SqlMasterFilter_projects() {
		return "`project_id`=@project_id@";
	}

	// Detail filter
	function SqlDetailFilter_projects() {
		return "`work_project_id`=@work_project_id@";
	}

	// Master filter
	function SqlMasterFilter_tasks() {
		return "`task_id`=@task_id@ AND `task_plan_id`=@task_plan_id@ AND `task_lab_id`=@task_lab_id@ AND `task_project_id`=@task_project_id@";
	}

	// Detail filter
	function SqlDetailFilter_tasks() {
		return "`work_task_id`=@work_task_id@ AND `work_plan_id`=@work_plan_id@ AND `work_lab_id`=@work_lab_id@ AND `work_project_id`=@work_project_id@";
	}

	// Master filter
	function SqlMasterFilter_v_employees() {
		return "`employee_id`=@employee_id@";
	}

	// Detail filter
	function SqlDetailFilter_v_employees() {
		return "`work_employee_id`=@work_employee_id@";
	}

	// Master filter
	function SqlMasterFilter_e_employees() {
		return "`employee_id`=@employee_id@";
	}

	// Detail filter
	function SqlDetailFilter_e_employees() {
		return "`work_employee_id`=@work_employee_id@";
	}

	// Master filter
	function SqlMasterFilter_e_tasks() {
		return "`task_id`=@task_id@ AND `task_project_id`=@task_project_id@ AND `task_plan_id`=@task_plan_id@ AND `task_lab_id`=@task_lab_id@";
	}

	// Detail filter
	function SqlDetailFilter_e_tasks() {
		return "`work_task_id`=@work_task_id@ AND `work_project_id`=@work_project_id@ AND `work_plan_id`=@work_plan_id@ AND `work_lab_id`=@work_lab_id@";
	}

	// Master filter
	function SqlMasterFilter_e_tasks_finance() {
		return "`task_project_id`=@task_project_id@ AND `task_plan_id`=@task_plan_id@ AND `task_lab_id`=@task_lab_id@ AND `task_id`=@task_id@";
	}

	// Detail filter
	function SqlDetailFilter_e_tasks_finance() {
		return "`work_project_id`=@work_project_id@ AND `work_plan_id`=@work_plan_id@ AND `work_lab_id`=@work_lab_id@ AND `work_task_id`=@work_task_id@";
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`works`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "`work_period_id` DESC, `work_project_id` ASC";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		global $Security;

		// Add User ID filter
		if ($Security->CurrentUserID() <> "" && !$Security->IsAdmin()) { // Non system admin
			$sFilter = $this->AddUserIDFilter($sFilter);
		}
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = $this->UserIDAllowSecurity;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('work_id', $rs))
				ew_AddFilter($where, ew_QuotedName('work_id', $this->DBID) . '=' . ew_QuotedValue($rs['work_id'], $this->work_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`work_id` = @work_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->work_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@work_id@", ew_AdjustSql($this->work_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "workslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "workslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("worksview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("worksview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "worksadd.php?" . $this->UrlParm($parm);
		else
			$url = "worksadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("worksedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("worksadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("worksdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		if ($this->getCurrentMasterTable() == "periods" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_period_id=" . urlencode($this->work_period_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "projects" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_project_id=" . urlencode($this->work_project_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "tasks" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_task_id=" . urlencode($this->work_task_id->CurrentValue);
			$url .= "&fk_task_plan_id=" . urlencode($this->work_plan_id->CurrentValue);
			$url .= "&fk_task_lab_id=" . urlencode($this->work_lab_id->CurrentValue);
			$url .= "&fk_task_project_id=" . urlencode($this->work_project_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "v_employees" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_employee_id=" . urlencode($this->work_employee_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "e_employees" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_employee_id=" . urlencode($this->work_employee_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "e_tasks" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_task_id=" . urlencode($this->work_task_id->CurrentValue);
			$url .= "&fk_task_project_id=" . urlencode($this->work_project_id->CurrentValue);
			$url .= "&fk_task_plan_id=" . urlencode($this->work_plan_id->CurrentValue);
			$url .= "&fk_task_lab_id=" . urlencode($this->work_lab_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "e_tasks_finance" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_task_project_id=" . urlencode($this->work_project_id->CurrentValue);
			$url .= "&fk_task_plan_id=" . urlencode($this->work_plan_id->CurrentValue);
			$url .= "&fk_task_lab_id=" . urlencode($this->work_lab_id->CurrentValue);
			$url .= "&fk_task_id=" . urlencode($this->work_task_id->CurrentValue);
		}
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "work_id:" . ew_VarToJson($this->work_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->work_id->CurrentValue)) {
			$sUrl .= "work_id=" . urlencode($this->work_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["work_id"]))
				$arKeys[] = ew_StripSlashes($_POST["work_id"]);
			elseif (isset($_GET["work_id"]))
				$arKeys[] = ew_StripSlashes($_GET["work_id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->work_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->work_id->setDbValue($rs->fields('work_id'));
		$this->work_period_id->setDbValue($rs->fields('work_period_id'));
		$this->work_project_id->setDbValue($rs->fields('work_project_id'));
		$this->work_plan_id->setDbValue($rs->fields('work_plan_id'));
		$this->work_lab_id->setDbValue($rs->fields('work_lab_id'));
		$this->work_task_id->setDbValue($rs->fields('work_task_id'));
		$this->work_description->setDbValue($rs->fields('work_description'));
		$this->work_progress->setDbValue($rs->fields('work_progress'));
		$this->work_time->setDbValue($rs->fields('work_time'));
		$this->work_employee_id->setDbValue($rs->fields('work_employee_id'));
		$this->work_started->setDbValue($rs->fields('work_started'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// work_id
		// work_period_id
		// work_project_id
		// work_plan_id
		// work_lab_id
		// work_task_id
		// work_description
		// work_progress
		// work_time
		// work_employee_id
		// work_started
		// work_id

		$this->work_id->ViewValue = $this->work_id->CurrentValue;
		$this->work_id->ViewCustomAttributes = "";

		// work_period_id
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->ViewValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->ViewValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->ViewValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";

		// work_plan_id
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";

		// work_lab_id
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";

		// work_task_id
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";

		// work_description
		$this->work_description->ViewValue = $this->work_description->CurrentValue;
		$this->work_description->ViewCustomAttributes = "";

		// work_progress
		if (strval($this->work_progress->CurrentValue) <> "") {
			$this->work_progress->ViewValue = $this->work_progress->OptionCaption($this->work_progress->CurrentValue);
		} else {
			$this->work_progress->ViewValue = NULL;
		}
		$this->work_progress->ViewCustomAttributes = "";

		// work_time
		$this->work_time->ViewValue = $this->work_time->CurrentValue;
		$this->work_time->ViewCustomAttributes = "";

		// work_employee_id
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";

		// work_started
		$this->work_started->ViewValue = $this->work_started->CurrentValue;
		$this->work_started->ViewValue = ew_FormatDateTime($this->work_started->ViewValue, 7);
		$this->work_started->ViewCustomAttributes = "";

		// work_id
		$this->work_id->LinkCustomAttributes = "";
		$this->work_id->HrefValue = "";
		$this->work_id->TooltipValue = "";

		// work_period_id
		$this->work_period_id->LinkCustomAttributes = "";
		$this->work_period_id->HrefValue = "";
		$this->work_period_id->TooltipValue = "";

		// work_project_id
		$this->work_project_id->LinkCustomAttributes = "";
		$this->work_project_id->HrefValue = "";
		$this->work_project_id->TooltipValue = "";

		// work_plan_id
		$this->work_plan_id->LinkCustomAttributes = "";
		$this->work_plan_id->HrefValue = "";
		$this->work_plan_id->TooltipValue = "";

		// work_lab_id
		$this->work_lab_id->LinkCustomAttributes = "";
		$this->work_lab_id->HrefValue = "";
		$this->work_lab_id->TooltipValue = "";

		// work_task_id
		$this->work_task_id->LinkCustomAttributes = "";
		$this->work_task_id->HrefValue = "";
		$this->work_task_id->TooltipValue = "";

		// work_description
		$this->work_description->LinkCustomAttributes = "";
		$this->work_description->HrefValue = "";
		$this->work_description->TooltipValue = "";

		// work_progress
		$this->work_progress->LinkCustomAttributes = "";
		$this->work_progress->HrefValue = "";
		$this->work_progress->TooltipValue = "";

		// work_time
		$this->work_time->LinkCustomAttributes = "";
		$this->work_time->HrefValue = "";
		$this->work_time->TooltipValue = "";

		// work_employee_id
		$this->work_employee_id->LinkCustomAttributes = "";
		$this->work_employee_id->HrefValue = "";
		$this->work_employee_id->TooltipValue = "";

		// work_started
		$this->work_started->LinkCustomAttributes = "";
		$this->work_started->HrefValue = "";
		$this->work_started->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// work_id
		$this->work_id->EditAttrs["class"] = "form-control";
		$this->work_id->EditCustomAttributes = "";
		$this->work_id->EditValue = $this->work_id->CurrentValue;
		$this->work_id->ViewCustomAttributes = "";

		// work_period_id
		$this->work_period_id->EditAttrs["class"] = "form-control";
		$this->work_period_id->EditCustomAttributes = "";
		if (strval($this->work_period_id->CurrentValue) <> "") {
			$sFilterWrk = "`period_id`" . ew_SearchString("=", $this->work_period_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `period_id`, `period_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `periods`";
		$sWhereWrk = "";
		$lookuptblfilter = "`period_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_period_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `period_from` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_period_id->EditValue = $this->work_period_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_period_id->EditValue = $this->work_period_id->CurrentValue;
			}
		} else {
			$this->work_period_id->EditValue = NULL;
		}
		$this->work_period_id->ViewCustomAttributes = "";

		// work_project_id
		$this->work_project_id->EditAttrs["class"] = "form-control";
		$this->work_project_id->EditCustomAttributes = "";
		if ($this->work_project_id->getSessionValue() <> "") {
			$this->work_project_id->CurrentValue = $this->work_project_id->getSessionValue();
		if (strval($this->work_project_id->CurrentValue) <> "") {
			$sFilterWrk = "`project_id`" . ew_SearchString("=", $this->work_project_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `project_id`, `project_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `projects`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_project_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_project_id->ViewValue = $this->work_project_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_project_id->ViewValue = $this->work_project_id->CurrentValue;
			}
		} else {
			$this->work_project_id->ViewValue = NULL;
		}
		$this->work_project_id->ViewCustomAttributes = "";
		} else {
		}

		// work_plan_id
		$this->work_plan_id->EditAttrs["class"] = "form-control";
		$this->work_plan_id->EditCustomAttributes = "";
		if ($this->work_plan_id->getSessionValue() <> "") {
			$this->work_plan_id->CurrentValue = $this->work_plan_id->getSessionValue();
		if (strval($this->work_plan_id->CurrentValue) <> "") {
			$sFilterWrk = "`plan_id`" . ew_SearchString("=", $this->work_plan_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `plan_id`, `plan_code` AS `DispFld`, `plan_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `plans`";
		$sWhereWrk = "";
		$lookuptblfilter = "`plan_active` = 1";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_plan_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `plan_code` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_plan_id->ViewValue = $this->work_plan_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_plan_id->ViewValue = $this->work_plan_id->CurrentValue;
			}
		} else {
			$this->work_plan_id->ViewValue = NULL;
		}
		$this->work_plan_id->ViewCustomAttributes = "";
		} else {
		}

		// work_lab_id
		$this->work_lab_id->EditAttrs["class"] = "form-control";
		$this->work_lab_id->EditCustomAttributes = "";
		if ($this->work_lab_id->getSessionValue() <> "") {
			$this->work_lab_id->CurrentValue = $this->work_lab_id->getSessionValue();
		if (strval($this->work_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->work_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->work_lab_id->ViewValue = $this->work_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_lab_id->ViewValue = $this->work_lab_id->CurrentValue;
			}
		} else {
			$this->work_lab_id->ViewValue = NULL;
		}
		$this->work_lab_id->ViewCustomAttributes = "";
		} else {
		}

		// work_task_id
		$this->work_task_id->EditAttrs["class"] = "form-control";
		$this->work_task_id->EditCustomAttributes = "";
		if ($this->work_task_id->getSessionValue() <> "") {
			$this->work_task_id->CurrentValue = $this->work_task_id->getSessionValue();
		if (strval($this->work_task_id->CurrentValue) <> "") {
			$sFilterWrk = "`task_id`" . ew_SearchString("=", $this->work_task_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `task_id`, `task_code` AS `DispFld`, `task_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tasks`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_task_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_task_id->ViewValue = $this->work_task_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_task_id->ViewValue = $this->work_task_id->CurrentValue;
			}
		} else {
			$this->work_task_id->ViewValue = NULL;
		}
		$this->work_task_id->ViewCustomAttributes = "";
		} else {
		}

		// work_description
		$this->work_description->EditAttrs["class"] = "form-control";
		$this->work_description->EditCustomAttributes = "";
		$this->work_description->EditValue = $this->work_description->CurrentValue;
		$this->work_description->PlaceHolder = ew_RemoveHtml($this->work_description->FldCaption());

		// work_progress
		$this->work_progress->EditAttrs["class"] = "form-control";
		$this->work_progress->EditCustomAttributes = "";
		$this->work_progress->EditValue = $this->work_progress->Options(TRUE);

		// work_time
		$this->work_time->EditAttrs["class"] = "form-control";
		$this->work_time->EditCustomAttributes = "";
		$this->work_time->EditValue = $this->work_time->CurrentValue;
		$this->work_time->PlaceHolder = ew_RemoveHtml($this->work_time->FldCaption());
		if (strval($this->work_time->EditValue) <> "" && is_numeric($this->work_time->EditValue)) $this->work_time->EditValue = ew_FormatNumber($this->work_time->EditValue, -2, -1, -2, 0);

		// work_employee_id
		$this->work_employee_id->EditAttrs["class"] = "form-control";
		$this->work_employee_id->EditCustomAttributes = "";
		if ($this->work_employee_id->getSessionValue() <> "") {
			$this->work_employee_id->CurrentValue = $this->work_employee_id->getSessionValue();
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->ViewValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->ViewValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->ViewValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";
		} elseif (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("info")) { // Non system admin
			$this->work_employee_id->CurrentValue = CurrentUserID();
		if (strval($this->work_employee_id->CurrentValue) <> "") {
			$sFilterWrk = "`employee_id`" . ew_SearchString("=", $this->work_employee_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `employee_id`, `employee_last_name` AS `DispFld`, `employee_first_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `employees`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->work_employee_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `employee_last_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->work_employee_id->EditValue = $this->work_employee_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->work_employee_id->EditValue = $this->work_employee_id->CurrentValue;
			}
		} else {
			$this->work_employee_id->EditValue = NULL;
		}
		$this->work_employee_id->ViewCustomAttributes = "";
		} else {
		}

		// work_started
		$this->work_started->EditAttrs["class"] = "form-control";
		$this->work_started->EditCustomAttributes = "";
		$this->work_started->EditValue = $this->work_started->CurrentValue;
		$this->work_started->PlaceHolder = ew_RemoveHtml($this->work_started->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->work_id->Exportable) $Doc->ExportCaption($this->work_id);
					if ($this->work_period_id->Exportable) $Doc->ExportCaption($this->work_period_id);
					if ($this->work_project_id->Exportable) $Doc->ExportCaption($this->work_project_id);
					if ($this->work_plan_id->Exportable) $Doc->ExportCaption($this->work_plan_id);
					if ($this->work_lab_id->Exportable) $Doc->ExportCaption($this->work_lab_id);
					if ($this->work_task_id->Exportable) $Doc->ExportCaption($this->work_task_id);
					if ($this->work_description->Exportable) $Doc->ExportCaption($this->work_description);
					if ($this->work_progress->Exportable) $Doc->ExportCaption($this->work_progress);
					if ($this->work_time->Exportable) $Doc->ExportCaption($this->work_time);
					if ($this->work_employee_id->Exportable) $Doc->ExportCaption($this->work_employee_id);
					if ($this->work_started->Exportable) $Doc->ExportCaption($this->work_started);
				} else {
					if ($this->work_period_id->Exportable) $Doc->ExportCaption($this->work_period_id);
					if ($this->work_project_id->Exportable) $Doc->ExportCaption($this->work_project_id);
					if ($this->work_plan_id->Exportable) $Doc->ExportCaption($this->work_plan_id);
					if ($this->work_lab_id->Exportable) $Doc->ExportCaption($this->work_lab_id);
					if ($this->work_task_id->Exportable) $Doc->ExportCaption($this->work_task_id);
					if ($this->work_description->Exportable) $Doc->ExportCaption($this->work_description);
					if ($this->work_progress->Exportable) $Doc->ExportCaption($this->work_progress);
					if ($this->work_time->Exportable) $Doc->ExportCaption($this->work_time);
					if ($this->work_employee_id->Exportable) $Doc->ExportCaption($this->work_employee_id);
					if ($this->work_started->Exportable) $Doc->ExportCaption($this->work_started);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->work_id->Exportable) $Doc->ExportField($this->work_id);
						if ($this->work_period_id->Exportable) $Doc->ExportField($this->work_period_id);
						if ($this->work_project_id->Exportable) $Doc->ExportField($this->work_project_id);
						if ($this->work_plan_id->Exportable) $Doc->ExportField($this->work_plan_id);
						if ($this->work_lab_id->Exportable) $Doc->ExportField($this->work_lab_id);
						if ($this->work_task_id->Exportable) $Doc->ExportField($this->work_task_id);
						if ($this->work_description->Exportable) $Doc->ExportField($this->work_description);
						if ($this->work_progress->Exportable) $Doc->ExportField($this->work_progress);
						if ($this->work_time->Exportable) $Doc->ExportField($this->work_time);
						if ($this->work_employee_id->Exportable) $Doc->ExportField($this->work_employee_id);
						if ($this->work_started->Exportable) $Doc->ExportField($this->work_started);
					} else {
						if ($this->work_period_id->Exportable) $Doc->ExportField($this->work_period_id);
						if ($this->work_project_id->Exportable) $Doc->ExportField($this->work_project_id);
						if ($this->work_plan_id->Exportable) $Doc->ExportField($this->work_plan_id);
						if ($this->work_lab_id->Exportable) $Doc->ExportField($this->work_lab_id);
						if ($this->work_task_id->Exportable) $Doc->ExportField($this->work_task_id);
						if ($this->work_description->Exportable) $Doc->ExportField($this->work_description);
						if ($this->work_progress->Exportable) $Doc->ExportField($this->work_progress);
						if ($this->work_time->Exportable) $Doc->ExportField($this->work_time);
						if ($this->work_employee_id->Exportable) $Doc->ExportField($this->work_employee_id);
						if ($this->work_started->Exportable) $Doc->ExportField($this->work_started);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Add User ID filter
	function AddUserIDFilter($sFilter) {
		global $Security;
		$sFilterWrk = "";
		$id = (CurrentPageID() == "list") ? $this->CurrentAction : CurrentPageID();
		if (!$this->UserIDAllow($id) && !$Security->IsAdmin()) {
			$sFilterWrk = $Security->UserIDList();
			if ($sFilterWrk <> "")
				$sFilterWrk = '`work_employee_id` IN (' . $sFilterWrk . ')';
		}

		// Call User ID Filtering event
		$this->UserID_Filtering($sFilterWrk);
		ew_AddFilter($sFilter, $sFilterWrk);
		return $sFilter;
	}

	// User ID subquery
	function GetUserIDSubquery(&$fld, &$masterfld) {
		global $UserTableConn;
		$sWrk = "";
		$sSql = "SELECT " . $masterfld->FldExpression . " FROM `works`";
		$sFilter = $this->AddUserIDFilter("");
		if ($sFilter <> "") $sSql .= " WHERE " . $sFilter;

		// Use subquery
		if (EW_USE_SUBQUERY_FOR_MASTER_USER_ID) {
			$sWrk = $sSql;
		} else {

			// List all values
			if ($rs = $UserTableConn->Execute($sSql)) {
				while (!$rs->EOF) {
					if ($sWrk <> "") $sWrk .= ",";
					$sWrk .= ew_QuotedValue($rs->fields[0], $masterfld->FldDataType, EW_USER_TABLE_DBID);
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		if ($sWrk <> "") {
			$sWrk = $fld->FldExpression . " IN (" . $sWrk . ")";
		}
		return $sWrk;
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {
		$periodName = ew_ExecuteScalar(
			"SELECT period_name FROM periods WHERE period_id = '{$this->work_period_id->DbValue}'");
		$this->work_period_id->EditValue = $periodName;
		$this->work_period_id->ViewValue = $periodName;
	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
