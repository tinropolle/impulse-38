<?php

// task_project_id
// task_plan_id
// task_lab_id
// task_code
// task_name
// task_from
// task_to
// task_employee_id
// task_coordinator_id
// task_object
// task_status_id
// task_hours_planned
// task_cof_planned
// task_money_planned
// task_hours_actual
// task_cof_actual
// task_money_actual
// task_description
// task_key
// task_file

?>
<?php if ($e_tasks_finance->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $e_tasks_finance->TableCaption() ?></h4> -->
<table id="tbl_e_tasks_financemaster" class="table table-bordered table-striped ewViewTable">
<?php echo $e_tasks_finance->TableCustomInnerHtml ?>
	<tbody>
<?php if ($e_tasks_finance->task_project_id->Visible) { // task_project_id ?>
		<tr id="r_task_project_id">
			<td><?php echo $e_tasks_finance->task_project_id->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_project_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_project_id">
<span<?php echo $e_tasks_finance->task_project_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_project_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_plan_id->Visible) { // task_plan_id ?>
		<tr id="r_task_plan_id">
			<td><?php echo $e_tasks_finance->task_plan_id->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_plan_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_plan_id">
<span<?php echo $e_tasks_finance->task_plan_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_plan_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_lab_id->Visible) { // task_lab_id ?>
		<tr id="r_task_lab_id">
			<td><?php echo $e_tasks_finance->task_lab_id->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_lab_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_lab_id">
<span<?php echo $e_tasks_finance->task_lab_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_lab_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_code->Visible) { // task_code ?>
		<tr id="r_task_code">
			<td><?php echo $e_tasks_finance->task_code->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_code->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_code">
<span<?php echo $e_tasks_finance->task_code->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_code->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_name->Visible) { // task_name ?>
		<tr id="r_task_name">
			<td><?php echo $e_tasks_finance->task_name->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_name->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_name">
<span<?php echo $e_tasks_finance->task_name->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_from->Visible) { // task_from ?>
		<tr id="r_task_from">
			<td><?php echo $e_tasks_finance->task_from->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_from->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_from">
<span<?php echo $e_tasks_finance->task_from->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_from->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_to->Visible) { // task_to ?>
		<tr id="r_task_to">
			<td><?php echo $e_tasks_finance->task_to->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_to->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_to">
<span<?php echo $e_tasks_finance->task_to->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_to->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_employee_id->Visible) { // task_employee_id ?>
		<tr id="r_task_employee_id">
			<td><?php echo $e_tasks_finance->task_employee_id->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_employee_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_employee_id">
<span<?php echo $e_tasks_finance->task_employee_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_employee_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_coordinator_id->Visible) { // task_coordinator_id ?>
		<tr id="r_task_coordinator_id">
			<td><?php echo $e_tasks_finance->task_coordinator_id->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_coordinator_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_coordinator_id">
<span<?php echo $e_tasks_finance->task_coordinator_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_coordinator_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_object->Visible) { // task_object ?>
		<tr id="r_task_object">
			<td><?php echo $e_tasks_finance->task_object->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_object->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_object">
<span<?php echo $e_tasks_finance->task_object->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_object->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_status_id->Visible) { // task_status_id ?>
		<tr id="r_task_status_id">
			<td><?php echo $e_tasks_finance->task_status_id->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_status_id->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_status_id">
<span<?php echo $e_tasks_finance->task_status_id->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_status_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_hours_planned->Visible) { // task_hours_planned ?>
		<tr id="r_task_hours_planned">
			<td><?php echo $e_tasks_finance->task_hours_planned->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_hours_planned->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_hours_planned">
<span<?php echo $e_tasks_finance->task_hours_planned->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_hours_planned->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_cof_planned->Visible) { // task_cof_planned ?>
		<tr id="r_task_cof_planned">
			<td><?php echo $e_tasks_finance->task_cof_planned->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_cof_planned->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_cof_planned">
<span<?php echo $e_tasks_finance->task_cof_planned->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_cof_planned->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_money_planned->Visible) { // task_money_planned ?>
		<tr id="r_task_money_planned">
			<td><?php echo $e_tasks_finance->task_money_planned->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_money_planned->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_money_planned">
<span<?php echo $e_tasks_finance->task_money_planned->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_money_planned->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_hours_actual->Visible) { // task_hours_actual ?>
		<tr id="r_task_hours_actual">
			<td><?php echo $e_tasks_finance->task_hours_actual->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_hours_actual->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_hours_actual">
<span<?php echo $e_tasks_finance->task_hours_actual->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_hours_actual->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_cof_actual->Visible) { // task_cof_actual ?>
		<tr id="r_task_cof_actual">
			<td><?php echo $e_tasks_finance->task_cof_actual->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_cof_actual->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_cof_actual">
<span<?php echo $e_tasks_finance->task_cof_actual->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_cof_actual->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_money_actual->Visible) { // task_money_actual ?>
		<tr id="r_task_money_actual">
			<td><?php echo $e_tasks_finance->task_money_actual->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_money_actual->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_money_actual">
<span<?php echo $e_tasks_finance->task_money_actual->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_money_actual->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_description->Visible) { // task_description ?>
		<tr id="r_task_description">
			<td><?php echo $e_tasks_finance->task_description->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_description->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_description">
<span<?php echo $e_tasks_finance->task_description->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_description->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_key->Visible) { // task_key ?>
		<tr id="r_task_key">
			<td><?php echo $e_tasks_finance->task_key->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_key->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_key">
<span<?php echo $e_tasks_finance->task_key->ViewAttributes() ?>>
<?php echo $e_tasks_finance->task_key->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($e_tasks_finance->task_file->Visible) { // task_file ?>
		<tr id="r_task_file">
			<td><?php echo $e_tasks_finance->task_file->FldCaption() ?></td>
			<td<?php echo $e_tasks_finance->task_file->CellAttributes() ?>>
<span id="el_e_tasks_finance_task_file">
<span<?php echo $e_tasks_finance->task_file->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($e_tasks_finance->task_file, $e_tasks_finance->task_file->ListViewValue()) ?>
</span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
