<!-- Begin Main Menu -->
<?php $RootMenu = new cMenu(EW_MENUBAR_ID) ?>
<?php

// Generate all menu items
$RootMenu->IsRoot = TRUE;
$RootMenu->AddMenuItem(2, "mi_employees", $Language->MenuPhrase("2", "MenuText"), "employeeslist.php", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}employees'), FALSE);
$RootMenu->AddMenuItem(20, "mci_42144244044343a442443440430", $Language->MenuPhrase("20", "MenuText"), "", -1, "", IsLoggedIn(), FALSE, TRUE);
$RootMenu->AddMenuItem(6, "mi_positions", $Language->MenuPhrase("6", "MenuText"), "positionslist.php", 20, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}positions'), FALSE);
$RootMenu->AddMenuItem(3, "mi_labs", $Language->MenuPhrase("3", "MenuText"), "labslist.php", 20, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}labs'), FALSE);
$RootMenu->AddMenuItem(22, "mi_v_employees", $Language->MenuPhrase("22", "MenuText"), "v_employeeslist.php?cmd=resetall", 20, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}v_employees'), FALSE);
$RootMenu->AddMenuItem(28, "mi_e_employees", $Language->MenuPhrase("28", "MenuText"), "e_employeeslist.php?cmd=resetall", 20, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}e_employees'), FALSE);
$RootMenu->AddMenuItem(7, "mi_projects", $Language->MenuPhrase("7", "MenuText"), "projectslist.php", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}projects'), FALSE);
$RootMenu->AddMenuItem(5, "mi_plans", $Language->MenuPhrase("5", "MenuText"), "planslist.php?cmd=resetall", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}plans'), FALSE);
$RootMenu->AddMenuItem(27, "mi_e_tasks", $Language->MenuPhrase("27", "MenuText"), "e_taskslist.php?cmd=resetall", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}e_tasks'), FALSE);
$RootMenu->AddMenuItem(9, "mi_tasks", $Language->MenuPhrase("9", "MenuText"), "taskslist.php?cmd=resetall", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}tasks'), FALSE);
$RootMenu->AddMenuItem(29, "mi_e_tasks_finance", $Language->MenuPhrase("29", "MenuText"), "e_tasks_financelist.php?cmd=resetall", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}e_tasks_finance'), FALSE);
$RootMenu->AddMenuItem(14, "mi_task_statuses", $Language->MenuPhrase("14", "MenuText"), "task_statuseslist.php", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}task_statuses'), FALSE);
$RootMenu->AddMenuItem(4, "mi_periods", $Language->MenuPhrase("4", "MenuText"), "periodslist.php", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}periods'), FALSE);
$RootMenu->AddMenuItem(11, "mi_works", $Language->MenuPhrase("11", "MenuText"), "workslist.php?cmd=resetall", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}works'), FALSE);
$RootMenu->AddMenuItem(25, "mi_v_works", $Language->MenuPhrase("25", "MenuText"), "v_workslist.php?cmd=resetall", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}v_works'), FALSE);
$RootMenu->AddMenuItem(12, "mi_reports", $Language->MenuPhrase("12", "MenuText"), "reportslist.php", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}reports'), FALSE);
$RootMenu->AddMenuItem(24, "mi_main_fund_invoices", $Language->MenuPhrase("24", "MenuText"), "main_fund_invoiceslist.php", -1, "", AllowListMenu('{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}main_fund_invoices'), FALSE);
$RootMenu->AddMenuItem(-1, "mi_logout", $Language->Phrase("Logout"), "logout.php", -1, "", IsLoggedIn());
$RootMenu->AddMenuItem(-1, "mi_login", $Language->Phrase("Login"), "login.php", -1, "", !IsLoggedIn() && substr(@$_SERVER["URL"], -1 * strlen("login.php")) <> "login.php");
$RootMenu->Render();
?>
<!-- End Main Menu -->
