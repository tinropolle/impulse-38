<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "employeesinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$employees_delete = NULL; // Initialize page object first

class cemployees_delete extends cemployees {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{E29E16BF-0E45-492D-B9CD-B0C1AFCB635D}";

	// Table name
	var $TableName = 'employees';

	// Page object name
	var $PageObjName = 'employees_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = TRUE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (employees)
		if (!isset($GLOBALS["employees"]) || get_class($GLOBALS["employees"]) == "cemployees") {
			$GLOBALS["employees"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["employees"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'employees', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (employees)
		if (!isset($UserTable)) {
			$UserTable = new cemployees();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("employeeslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
				$this->Page_Terminate(ew_GetUrl("employeeslist.php"));
			}
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $employees;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($employees);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("employeeslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in employees class, employeesinfo.php

		$this->CurrentFilter = $sFilter;

		// Check if valid user id
		$conn = &$this->Connection();
		$sql = $this->GetSQL($this->CurrentFilter, "");
		if ($this->Recordset = ew_LoadRecordset($sql, $conn)) {
			$res = TRUE;
			while (!$this->Recordset->EOF) {
				$this->LoadRowValues($this->Recordset);
				if (!$this->ShowOptionLink('delete')) {
					$sUserIdMsg = $Language->Phrase("NoDeletePermission");
					$this->setFailureMessage($sUserIdMsg);
					$res = FALSE;
					break;
				}
				$this->Recordset->MoveNext();
			}
			$this->Recordset->Close();
			if (!$res) $this->Page_Terminate("employeeslist.php"); // Return to list
		}

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->CurrentAction = "I"; // Display record
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("employeeslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->employee_id->setDbValue($rs->fields('employee_id'));
		$this->employee_login->setDbValue($rs->fields('employee_login'));
		$this->employee_password->setDbValue($rs->fields('employee_password'));
		$this->employee_level_id->setDbValue($rs->fields('employee_level_id'));
		$this->employee_first_name->setDbValue($rs->fields('employee_first_name'));
		$this->employee_last_name->setDbValue($rs->fields('employee_last_name'));
		$this->employee_telephone->setDbValue($rs->fields('employee_telephone'));
		$this->employee_lab_id->setDbValue($rs->fields('employee_lab_id'));
		$this->employee_position_id->setDbValue($rs->fields('employee_position_id'));
		$this->employee_salary->setDbValue($rs->fields('employee_salary'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->employee_id->DbValue = $row['employee_id'];
		$this->employee_login->DbValue = $row['employee_login'];
		$this->employee_password->DbValue = $row['employee_password'];
		$this->employee_level_id->DbValue = $row['employee_level_id'];
		$this->employee_first_name->DbValue = $row['employee_first_name'];
		$this->employee_last_name->DbValue = $row['employee_last_name'];
		$this->employee_telephone->DbValue = $row['employee_telephone'];
		$this->employee_lab_id->DbValue = $row['employee_lab_id'];
		$this->employee_position_id->DbValue = $row['employee_position_id'];
		$this->employee_salary->DbValue = $row['employee_salary'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// employee_id
		// employee_login
		// employee_password

		$this->employee_password->CellCssStyle = "white-space: nowrap;";

		// employee_level_id
		// employee_first_name
		// employee_last_name
		// employee_telephone
		// employee_lab_id
		// employee_position_id
		// employee_salary

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// employee_id
		$this->employee_id->ViewValue = $this->employee_id->CurrentValue;
		$this->employee_id->ViewCustomAttributes = "";

		// employee_login
		$this->employee_login->ViewValue = $this->employee_login->CurrentValue;
		$this->employee_login->ViewCustomAttributes = "";

		// employee_level_id
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->employee_level_id->CurrentValue) <> "") {
			$this->employee_level_id->ViewValue = $this->employee_level_id->OptionCaption($this->employee_level_id->CurrentValue);
		} else {
			$this->employee_level_id->ViewValue = NULL;
		}
		} else {
			$this->employee_level_id->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->employee_level_id->ViewCustomAttributes = "";

		// employee_first_name
		$this->employee_first_name->ViewValue = $this->employee_first_name->CurrentValue;
		$this->employee_first_name->ViewCustomAttributes = "";

		// employee_last_name
		$this->employee_last_name->ViewValue = $this->employee_last_name->CurrentValue;
		$this->employee_last_name->ViewCustomAttributes = "";

		// employee_telephone
		$this->employee_telephone->ViewValue = $this->employee_telephone->CurrentValue;
		$this->employee_telephone->ViewCustomAttributes = "";

		// employee_lab_id
		if (strval($this->employee_lab_id->CurrentValue) <> "") {
			$sFilterWrk = "`lab_id`" . ew_SearchString("=", $this->employee_lab_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `lab_id`, `lab_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `labs`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->employee_lab_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->employee_lab_id->ViewValue = $this->employee_lab_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->employee_lab_id->ViewValue = $this->employee_lab_id->CurrentValue;
			}
		} else {
			$this->employee_lab_id->ViewValue = NULL;
		}
		$this->employee_lab_id->ViewCustomAttributes = "";

		// employee_position_id
		if (strval($this->employee_position_id->CurrentValue) <> "") {
			$sFilterWrk = "`position_id`" . ew_SearchString("=", $this->employee_position_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `position_id`, `position_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `positions`";
		$sWhereWrk = "";
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->employee_position_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->employee_position_id->ViewValue = $this->employee_position_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->employee_position_id->ViewValue = $this->employee_position_id->CurrentValue;
			}
		} else {
			$this->employee_position_id->ViewValue = NULL;
		}
		$this->employee_position_id->ViewCustomAttributes = "";

		// employee_salary
		$this->employee_salary->ViewValue = $this->employee_salary->CurrentValue;
		$this->employee_salary->ViewCustomAttributes = "";

			// employee_login
			$this->employee_login->LinkCustomAttributes = "";
			$this->employee_login->HrefValue = "";
			$this->employee_login->TooltipValue = "";

			// employee_level_id
			$this->employee_level_id->LinkCustomAttributes = "";
			$this->employee_level_id->HrefValue = "";
			$this->employee_level_id->TooltipValue = "";

			// employee_first_name
			$this->employee_first_name->LinkCustomAttributes = "";
			$this->employee_first_name->HrefValue = "";
			$this->employee_first_name->TooltipValue = "";

			// employee_last_name
			$this->employee_last_name->LinkCustomAttributes = "";
			$this->employee_last_name->HrefValue = "";
			$this->employee_last_name->TooltipValue = "";

			// employee_telephone
			$this->employee_telephone->LinkCustomAttributes = "";
			$this->employee_telephone->HrefValue = "";
			$this->employee_telephone->TooltipValue = "";

			// employee_lab_id
			$this->employee_lab_id->LinkCustomAttributes = "";
			$this->employee_lab_id->HrefValue = "";
			$this->employee_lab_id->TooltipValue = "";

			// employee_position_id
			$this->employee_position_id->LinkCustomAttributes = "";
			$this->employee_position_id->HrefValue = "";
			$this->employee_position_id->TooltipValue = "";

			// employee_salary
			$this->employee_salary->LinkCustomAttributes = "";
			$this->employee_salary->HrefValue = "";
			$this->employee_salary->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();
		if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteBegin")); // Batch delete begin

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['employee_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
			if ($DeleteRows) {
				foreach ($rsold as $row)
					$this->WriteAuditTrailOnDelete($row);
			}
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteSuccess")); // Batch delete success
		} else {
			$conn->RollbackTrans(); // Rollback changes
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteRollback")); // Batch delete rollback
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->employee_id->CurrentValue);
		return TRUE;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("employeeslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'employees';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnDelete) return;
		$table = 'employees';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['employee_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$curUser = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$oldvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				if ($fldname == 'employee_password')
					$oldvalue = $Language->Phrase("PasswordMask");
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($employees_delete)) $employees_delete = new cemployees_delete();

// Page init
$employees_delete->Page_Init();

// Page main
$employees_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$employees_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = femployeesdelete = new ew_Form("femployeesdelete", "delete");

// Form_CustomValidate event
femployeesdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
femployeesdelete.ValidateRequired = true;
<?php } else { ?>
femployeesdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
femployeesdelete.Lists["x_employee_level_id"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
femployeesdelete.Lists["x_employee_level_id"].Options = <?php echo json_encode($employees->employee_level_id->Options()) ?>;
femployeesdelete.Lists["x_employee_lab_id"] = {"LinkField":"x_lab_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_lab_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
femployeesdelete.Lists["x_employee_position_id"] = {"LinkField":"x_position_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_position_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $employees_delete->ShowPageHeader(); ?>
<?php
$employees_delete->ShowMessage();
?>
<form name="femployeesdelete" id="femployeesdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($employees_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $employees_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="employees">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($employees_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $employees->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($employees->employee_login->Visible) { // employee_login ?>
		<th><span id="elh_employees_employee_login" class="employees_employee_login"><?php echo $employees->employee_login->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_level_id->Visible) { // employee_level_id ?>
		<th><span id="elh_employees_employee_level_id" class="employees_employee_level_id"><?php echo $employees->employee_level_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_first_name->Visible) { // employee_first_name ?>
		<th><span id="elh_employees_employee_first_name" class="employees_employee_first_name"><?php echo $employees->employee_first_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_last_name->Visible) { // employee_last_name ?>
		<th><span id="elh_employees_employee_last_name" class="employees_employee_last_name"><?php echo $employees->employee_last_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_telephone->Visible) { // employee_telephone ?>
		<th><span id="elh_employees_employee_telephone" class="employees_employee_telephone"><?php echo $employees->employee_telephone->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_lab_id->Visible) { // employee_lab_id ?>
		<th><span id="elh_employees_employee_lab_id" class="employees_employee_lab_id"><?php echo $employees->employee_lab_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_position_id->Visible) { // employee_position_id ?>
		<th><span id="elh_employees_employee_position_id" class="employees_employee_position_id"><?php echo $employees->employee_position_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($employees->employee_salary->Visible) { // employee_salary ?>
		<th><span id="elh_employees_employee_salary" class="employees_employee_salary"><?php echo $employees->employee_salary->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$employees_delete->RecCnt = 0;
$i = 0;
while (!$employees_delete->Recordset->EOF) {
	$employees_delete->RecCnt++;
	$employees_delete->RowCnt++;

	// Set row properties
	$employees->ResetAttrs();
	$employees->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$employees_delete->LoadRowValues($employees_delete->Recordset);

	// Render row
	$employees_delete->RenderRow();
?>
	<tr<?php echo $employees->RowAttributes() ?>>
<?php if ($employees->employee_login->Visible) { // employee_login ?>
		<td<?php echo $employees->employee_login->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_login" class="employees_employee_login">
<span<?php echo $employees->employee_login->ViewAttributes() ?>>
<?php echo $employees->employee_login->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_level_id->Visible) { // employee_level_id ?>
		<td<?php echo $employees->employee_level_id->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_level_id" class="employees_employee_level_id">
<span<?php echo $employees->employee_level_id->ViewAttributes() ?>>
<?php echo $employees->employee_level_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_first_name->Visible) { // employee_first_name ?>
		<td<?php echo $employees->employee_first_name->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_first_name" class="employees_employee_first_name">
<span<?php echo $employees->employee_first_name->ViewAttributes() ?>>
<?php echo $employees->employee_first_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_last_name->Visible) { // employee_last_name ?>
		<td<?php echo $employees->employee_last_name->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_last_name" class="employees_employee_last_name">
<span<?php echo $employees->employee_last_name->ViewAttributes() ?>>
<?php echo $employees->employee_last_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_telephone->Visible) { // employee_telephone ?>
		<td<?php echo $employees->employee_telephone->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_telephone" class="employees_employee_telephone">
<span<?php echo $employees->employee_telephone->ViewAttributes() ?>>
<?php echo $employees->employee_telephone->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_lab_id->Visible) { // employee_lab_id ?>
		<td<?php echo $employees->employee_lab_id->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_lab_id" class="employees_employee_lab_id">
<span<?php echo $employees->employee_lab_id->ViewAttributes() ?>>
<?php echo $employees->employee_lab_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_position_id->Visible) { // employee_position_id ?>
		<td<?php echo $employees->employee_position_id->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_position_id" class="employees_employee_position_id">
<span<?php echo $employees->employee_position_id->ViewAttributes() ?>>
<?php echo $employees->employee_position_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($employees->employee_salary->Visible) { // employee_salary ?>
		<td<?php echo $employees->employee_salary->CellAttributes() ?>>
<span id="el<?php echo $employees_delete->RowCnt ?>_employees_employee_salary" class="employees_employee_salary">
<span<?php echo $employees->employee_salary->ViewAttributes() ?>>
<?php echo $employees->employee_salary->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$employees_delete->Recordset->MoveNext();
}
$employees_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $employees_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
femployeesdelete.Init();
</script>
<?php
$employees_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$employees_delete->Page_Terminate();
?>
